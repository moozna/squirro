import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { isAsinNumberValid, isGoodreadsIdValid } from './validate';

describe('validate', () => {
    const mock = new MockAdapter(axios);

    afterEach(() => {
        mock.reset();
    });

    describe('isAsinNumberValid()', () => {
        test('returns the response', async () => {
            const mockId = 'asin';
            mock.onGet(`/api/books/validate/asin/${mockId}`).reply(200, true);

            const response = await isAsinNumberValid(mockId);
            expect(response).toBe(true);
        });
    });

    describe('isGoodreadsIdValid()', () => {
        test('returns the response', async () => {
            const mockId = 20;
            mock.onGet(`/api/books/validate/goodreadsId/${mockId}`).reply(
                200,
                true
            );

            const response = await isGoodreadsIdValid(mockId);
            expect(response).toBe(true);
        });
    });
});
