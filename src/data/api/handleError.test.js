import { handleError, createErrorMessage } from './handleError';

describe('error handling', () => {
    describe('handleError()', () => {
        test('is a function', () => {
            expect(typeof handleError).toBe('function');
        });

        describe('returns the details of the error when received a status code that falls out of the range of 2xx', () => {
            test('Validation error', () => {
                const message = '"seriesTitle" is not allowed at body';
                const title = 'ValidationError';
                const status = 422;
                const statusText = 'Unprocessable Entity';
                const headers = {
                    'content-type': 'application/json; charset=utf-8'
                };
                const mockError = {
                    response: {
                        data: { message, title },
                        status,
                        statusText,
                        headers
                    }
                };
                const expectedError = {
                    message,
                    status,
                    title,
                    headers
                };

                const error = handleError(mockError);

                expect(error).toEqual(expectedError);
            });

            test('Not found error', () => {
                const message = 'Route not found';
                const status = 404;
                const statusText = 'Not found';
                const headers = {
                    'content-type': 'application/json; charset=utf-8'
                };
                const mockError = {
                    response: {
                        data: { message },
                        status,
                        statusText,
                        headers
                    }
                };
                const expectedError = {
                    message,
                    status,
                    title: statusText,
                    headers
                };

                const error = handleError(mockError);

                expect(error).toEqual(expectedError);
            });

            test('Internal Server Error', () => {
                const data = 'Proxy error: Could not proxy request';
                const status = 500;
                const statusText = 'Internal Server Error';
                const headers = {
                    'content-type': 'application/json; charset=utf-8'
                };
                const mockError = {
                    response: {
                        data,
                        status,
                        statusText,
                        headers
                    }
                };
                const expectedError = {
                    message: data,
                    status,
                    title: statusText,
                    headers
                };

                const error = handleError(mockError);

                expect(error).toEqual(expectedError);
            });
        });

        test('returns an instance of XMLHttpRequest when request was made but no response was received', () => {
            const mockError = {
                request: {
                    status: 404,
                    statusText: 'Not Found'
                }
            };

            expect(handleError(mockError)).toEqual(mockError.request);
        });

        test('returns the error message when something triggered an Error during setting up the request', () => {
            const mockError = {
                message: 'Route not found'
            };

            expect(handleError(mockError)).toEqual(mockError);
        });
    });

    describe('createErrorMessage()', () => {
        test('is a function', () => {
            expect(typeof createErrorMessage).toBe('function');
        });

        describe('returns the error', () => {
            test('when error contains message', () => {
                const mockError = {
                    message: 'Route not found'
                };

                expect(createErrorMessage(mockError)).toEqual(mockError);
            });
        });

        describe('returns a default error message', () => {
            const defaultError = { message: 'Something went wrong.' };

            test('when error is missing', () => {
                const mockError = '';

                expect(createErrorMessage(mockError)).toEqual(defaultError);
            });

            test('when error is empty', () => {
                const mockError = {};

                expect(createErrorMessage(mockError)).toEqual(defaultError);
            });
        });
    });
});
