import get from '../../utils/get';

export const handleError = error => {
    if (error.response) {
        const { status, statusText, data, headers } = error.response;
        const title = get(data, 'title') || statusText;
        const message = get(data, 'message') || data;

        return {
            status,
            message,
            ...(title ? { title } : {}),
            ...(headers ? { headers } : {})
        };
    }

    if (error.request) {
        return error.request;
    }

    return { message: error.message };
};

export const createErrorMessage = error => {
    if (error && error.message) {
        return error;
    }
    return { message: 'Something went wrong.' };
};
