import axios from 'axios';
import urlEncodeQueryParams from '../../utils/urlEncodeQueryParams';

export function isAsinNumberValid(id, bookId) {
    const query = urlEncodeQueryParams({ bookId });
    return axios
        .get(`/api/books/validate/asin/${id}${query}`)
        .then(response => response.data);
}

export function isGoodreadsIdValid(id, bookId) {
    const query = urlEncodeQueryParams({ bookId });
    return axios
        .get(`/api/books/validate/goodreadsId/${id}${query}`)
        .then(response => response.data);
}
