import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './routingActions';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    SELECT_BOOK,
    UNSELECT_BOOK,
    SELECT_START_INDEX,
    SELECT_PAGE_NUMBER,
    REQUEST_SELECTED_BOOK,
    RECEIVE_SELECTED_BOOK_SUCCESS,
    REQUEST_SUGGESTION_LISTS,
    RECEIVE_SUGGESTION_LISTS_SUCCESS,
    CREATE_INITIAL_BOOK,
    REQUEST_SHELVES,
    RECEIVE_SHELVES_SUCCESS
} from '../constants';

describe('routing actions', () => {
    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('BOOKS', () => {
            test('requests books', async () => {
                const mockResponse = {
                    books: [
                        { _id: '1', title: 'title' },
                        { _id: '2', title: 'title2' }
                    ],
                    count: 0,
                    hasNext: true
                };
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

                const expectedActions = [
                    { type: REQUEST_BOOKS },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
                ];
                const initialState = {
                    pagination: {
                        currentPageNumber: 1
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToBooks());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates UNSELECT_BOOK when a selection was saved before', async () => {
                const mockResponse = {
                    books: [
                        { _id: '1', title: 'title' },
                        { _id: '2', title: 'title2' }
                    ],
                    count: 0,
                    hasNext: true
                };
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

                const expectedActions = [
                    { type: UNSELECT_BOOK },
                    { type: REQUEST_BOOKS },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
                ];
                const initialState = {
                    selectedBook: {
                        selectedBookId: '2'
                    },
                    pagination: {
                        currentPageNumber: 1
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToBooks());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates SELECT_START_INDEX and SELECT_PAGE_NUMBER when page number is present in the url', async () => {
                const mockPageNumber = '3';
                const mockResponse = {
                    books: [
                        { _id: '1', title: 'title' },
                        { _id: '2', title: 'title2' }
                    ],
                    count: 0,
                    hasNext: true
                };
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

                const expectedActions = [
                    { type: SELECT_START_INDEX, startIndex: 50 },
                    { type: SELECT_PAGE_NUMBER, pageNumber: 3 },
                    { type: REQUEST_BOOKS },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
                ];
                const initialState = {
                    pagination: {
                        currentPageNumber: 1,
                        pageSize: 25
                    },
                    location: {
                        query: {
                            page: mockPageNumber
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToBooks());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('BOOK', () => {
            test('updates selected book when it has different id than the current one', async () => {
                const mockId = '1';
                const mockResponse = {
                    _id: '1',
                    asin: 'asin'
                };
                mock.onGet(`api/books/${mockId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: SELECT_BOOK, id: '1' },
                    { type: REQUEST_SELECTED_BOOK },
                    {
                        type: RECEIVE_SELECTED_BOOK_SUCCESS,
                        response: mockResponse
                    }
                ];
                const initialState = {
                    selectedBook: {
                        selectedBookId: '3',
                        data: {
                            _id: '3',
                            asin: 'asin'
                        },
                        isFetching: false
                    },
                    location: {
                        payload: {
                            id: mockId
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToBookDetails());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('does not update selected book data when it is already present on the state', async () => {
                const mockId = '1';
                const mockResponse = {
                    _id: '1',
                    asin: 'asin'
                };
                mock.onGet(`api/books/${mockId}`).reply(200, mockResponse);

                const expectedActions = [];
                const initialState = {
                    selectedBook: {
                        selectedBookId: '1',
                        data: {
                            _id: '1',
                            asin: 'asin'
                        },
                        isFetching: false
                    },
                    location: {
                        payload: {
                            id: mockId
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToBookDetails());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('EDIT', () => {
            const mockBookResponse = {
                _id: '1',
                asin: 'asin'
            };

            const mockGenresResponse = [
                { name: 'Adventure' },
                { name: 'Horror' },
                { name: 'Action' }
            ];
            const mockTagsResponse = [
                { name: 'ebook' },
                { name: 'read' },
                { name: 'want to read' }
            ];
            const mockAuthorsResponse = [
                { name: 'Cassandra Clare' },
                { name: 'Kristin Cashore' },
                { name: 'Stephen King' }
            ];
            const mockSeriesResponse = [
                { name: 'Above World' },
                { name: 'Graceling Realm' },
                { name: 'Some Quiet Place' }
            ];

            const mockShelvesResponse = [
                { name: 'Best Historical Fiction' },
                { name: 'Best Book Cover Art' },
                { name: 'Best Horror Books' }
            ];

            test('updates suggestion lists and selected book when it has different id than the current one', async () => {
                const mockId = '1';

                mock.onGet(`api/books/${mockId}`).reply(200, mockBookResponse);

                mock.onGet('api/filters/genres').reply(200, mockGenresResponse);
                mock.onGet('api/filters/tags').reply(200, mockTagsResponse);
                mock.onGet('api/filters/authors').reply(
                    200,
                    mockAuthorsResponse
                );
                mock.onGet('api/filters/series').reply(200, mockSeriesResponse);
                mock.onGet('api/filters/shelves').reply(
                    200,
                    mockShelvesResponse
                );

                const expectedActions = [
                    { type: SELECT_BOOK, id: '1' },
                    { type: REQUEST_SELECTED_BOOK },
                    { type: REQUEST_SUGGESTION_LISTS },
                    {
                        type: RECEIVE_SELECTED_BOOK_SUCCESS,
                        response: mockBookResponse
                    },
                    {
                        type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                        response: {
                            genres: mockGenresResponse,
                            tags: mockTagsResponse,
                            authors: mockAuthorsResponse,
                            series: mockSeriesResponse,
                            shelves: mockShelvesResponse
                        }
                    }
                ];
                const initialState = {
                    selectedBook: {
                        selectedBookId: '3',
                        data: {
                            _id: '3',
                            asin: 'asin'
                        },
                        isFetching: false
                    },
                    location: {
                        payload: {
                            id: mockId
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToEditBook());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('updates only suggestion lists when selected book data is already present on the state', async () => {
                const mockId = '1';

                mock.onGet(`api/books/${mockId}`).reply(200, mockBookResponse);

                mock.onGet('api/filters/genres').reply(200, mockGenresResponse);
                mock.onGet('api/filters/tags').reply(200, mockTagsResponse);
                mock.onGet('api/filters/authors').reply(
                    200,
                    mockAuthorsResponse
                );
                mock.onGet('api/filters/series').reply(200, mockSeriesResponse);
                mock.onGet('api/filters/shelves').reply(
                    200,
                    mockShelvesResponse
                );

                const expectedActions = [
                    { type: REQUEST_SUGGESTION_LISTS },
                    {
                        type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                        response: {
                            genres: mockGenresResponse,
                            tags: mockTagsResponse,
                            authors: mockAuthorsResponse,
                            series: mockSeriesResponse,
                            shelves: mockShelvesResponse
                        }
                    }
                ];
                const initialState = {
                    selectedBook: {
                        selectedBookId: '1',
                        data: {
                            _id: '1',
                            asin: 'asin'
                        },
                        isFetching: false
                    },
                    location: {
                        payload: {
                            id: mockId
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToEditBook());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('EDIT_IMPORT', () => {
            const mockGenresResponse = [
                { name: 'Adventure' },
                { name: 'Horror' },
                { name: 'Action' }
            ];
            const mockTagsResponse = [
                { name: 'ebook' },
                { name: 'read' },
                { name: 'want to read' }
            ];
            const mockAuthorsResponse = [
                { name: 'Cassandra Clare' },
                { name: 'Kristin Cashore' },
                { name: 'Stephen King' }
            ];
            const mockSeriesResponse = [
                { name: 'Above World' },
                { name: 'Graceling Realm' },
                { name: 'Some Quiet Place' }
            ];
            const mockShelvesResponse = [
                { name: 'Best Historical Fiction' },
                { name: 'Best Book Cover Art' },
                { name: 'Best Horror Books' }
            ];

            test('creates initial book data and updates suggestion lists', async () => {
                mock.onGet('api/filters/genres').reply(200, mockGenresResponse);
                mock.onGet('api/filters/tags').reply(200, mockTagsResponse);
                mock.onGet('api/filters/authors').reply(
                    200,
                    mockAuthorsResponse
                );
                mock.onGet('api/filters/series').reply(200, mockSeriesResponse);
                mock.onGet('api/filters/shelves').reply(
                    200,
                    mockShelvesResponse
                );

                const sitesData = {
                    goodreads: {
                        goodreadsId: 12,
                        title: 'title',
                        pages: 210
                    },
                    amazon: { asin: 'asin', title: 'title', pages: 200 }
                };

                const expectedActions = [
                    { type: CREATE_INITIAL_BOOK, sitesData },
                    { type: REQUEST_SUGGESTION_LISTS },
                    {
                        type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                        response: {
                            genres: mockGenresResponse,
                            tags: mockTagsResponse,
                            authors: mockAuthorsResponse,
                            series: mockSeriesResponse,
                            shelves: mockShelvesResponse
                        }
                    }
                ];
                const initialState = {
                    importing: {
                        data: sitesData
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToEditImport());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('SHELVES', () => {
            test('requests shelves', async () => {
                const mockResponse = [
                    { _id: 'Best Book Cover Art', count: 45 },
                    { _id: 'Best Books', count: 40 },
                    { _id: 'Best Horror Novels', count: 51 }
                ];

                mock.onGet('api/filters/shelves/stat').reply(200, mockResponse);

                const expectedActions = [
                    { type: REQUEST_SHELVES },
                    { type: RECEIVE_SHELVES_SUCCESS, response: mockResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.navigateToShelves());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('does not request shelves when it is already available on the state', async () => {
                const shelves = [
                    { _id: 'Best Book Cover Art', count: 45 },
                    { _id: 'Best Books', count: 40 },
                    { _id: 'Best Horror Novels', count: 51 }
                ];

                const expectedActions = [];
                const initialState = {
                    shelves: {
                        data: shelves
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.navigateToShelves());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });
    });
});
