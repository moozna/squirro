import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './suggestionsActions';
import {
    REQUEST_SUGGESTION_LISTS,
    RECEIVE_SUGGESTION_LISTS_SUCCESS,
    RECEIVE_SUGGESTION_LISTS_FAILURE
} from '../constants';

describe('suggestions actions', () => {
    describe('action creators', () => {
        test('creates an action to request suggestion lists', () => {
            const expectedAction = {
                type: REQUEST_SUGGESTION_LISTS
            };

            const action = actionTypes.requestSuggestionLists();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful suggestion lists request', () => {
            const response = {
                authors: [],
                genres: [],
                tags: [],
                series: []
            };
            const expectedAction = {
                type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                response
            };

            const action = actionTypes.receiveSuggestionListsSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed suggestion lists request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_SUGGESTION_LISTS_FAILURE,
                error
            };

            const action = actionTypes.receiveSuggestionListsFailure(error);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        test('creates RECEIVE_SUGGESTION_LISTS_SUCCESS when the request was successful', async () => {
            const mockGenresResponse = ['Adventure', 'Horror', 'Action'];
            const mockTagsResponse = ['ebook', 'read', 'want to read'];
            const mockAuthorsResponse = [
                'Cassandra Clare',
                'Kristin Cashore',
                'Stephen King'
            ];
            const mockSeriesResponse = [
                'Above World',
                'Graceling Realm',
                'Some Quiet Place'
            ];
            const mockShelvesResponse = [
                'Best Historical Fiction',
                'Best Book Cover Art',
                'Best Horror Books'
            ];

            mock.onGet('api/filters/genres').reply(200, mockGenresResponse);
            mock.onGet('api/filters/tags').reply(200, mockTagsResponse);
            mock.onGet('api/filters/authors').reply(200, mockAuthorsResponse);
            mock.onGet('api/filters/series').reply(200, mockSeriesResponse);
            mock.onGet('api/filters/shelves').reply(200, mockShelvesResponse);

            const expectedActions = [
                { type: REQUEST_SUGGESTION_LISTS },
                {
                    type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                    response: {
                        genres: mockGenresResponse,
                        tags: mockTagsResponse,
                        authors: mockAuthorsResponse,
                        series: mockSeriesResponse,
                        shelves: mockShelvesResponse
                    }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSuggestionLists());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_SUGGESTION_LISTS_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet('api/filters/genres').reply(500, { message });
            mock.onGet('api/filters/tags').reply(500, { message });
            mock.onGet('api/filters/authors').reply(500, { message });
            mock.onGet('api/filters/series').reply(500, { message });
            mock.onGet('api/filters/shelves').reply(500, { message });

            const expectedActions = [
                { type: REQUEST_SUGGESTION_LISTS },
                {
                    type: RECEIVE_SUGGESTION_LISTS_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSuggestionLists());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
