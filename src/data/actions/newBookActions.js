import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    ADD_NEW_BOOK,
    ADD_NEW_BOOK_SUCCESS,
    ADD_NEW_BOOK_FAILURE
} from '../constants';
import { filterBooksWithAllFilters } from './rangeActions';
import { bookDetailsPage } from './pageActions';

export const addNewBook = () => ({
    type: ADD_NEW_BOOK
});

export const addNewBookSuccess = (id, response) => ({
    type: ADD_NEW_BOOK_SUCCESS,
    id,
    response
});

export const addNewBookFailure = error => ({
    type: ADD_NEW_BOOK_FAILURE,
    error: createErrorMessage(error)
});

export const saveAddedBook = bookData => dispatch => {
    dispatch(addNewBook());

    return axios.post('/api/books', bookData).then(
        response => {
            const bookId = response.data._id;
            dispatch(addNewBookSuccess(bookId, response.data));
            dispatch(filterBooksWithAllFilters());
            return dispatch(bookDetailsPage(bookId));
        },
        error => dispatch(addNewBookFailure(handleError(error)))
    );
};
