import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './booksActions';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INVALIDATE_BOOKS
} from '../constants';

describe('books actions', () => {
    describe('action creators', () => {
        test('creates an action to request books', () => {
            const expectedAction = {
                type: REQUEST_BOOKS
            };

            const action = actionTypes.requestBooks();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful request', () => {
            const response = {
                books: [],
                count: 0,
                hasNext: true
            };
            const expectedAction = {
                type: RECEIVE_BOOKS_SUCCESS,
                response
            };

            const action = actionTypes.receiveBooksSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_BOOKS_FAILURE,
                error
            };

            const action = actionTypes.receiveBooksFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to request more books', () => {
            const expectedAction = {
                type: REQUEST_MORE_BOOKS
            };

            const action = actionTypes.requestMoreBooks();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful more books request', () => {
            const response = {
                books: [],
                count: 0,
                hasNext: true
            };
            const expectedAction = {
                type: RECEIVE_MORE_BOOKS_SUCCESS,
                response
            };

            const action = actionTypes.receiveMoreBooksSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed more books request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_MORE_BOOKS_FAILURE,
                error
            };

            const action = actionTypes.receiveMoreBooksFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to invalidating books', () => {
            const expectedAction = {
                type: INVALIDATE_BOOKS
            };

            const action = actionTypes.invalidateBooks();
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        test('creates RECEIVE_BOOKS_SUCCESS when the request was successful', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchBooks());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_BOOKS_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet(/\/api\/books(\?.+)?/).reply(500, { message });

            const expectedActions = [
                { type: REQUEST_BOOKS },
                {
                    type: RECEIVE_BOOKS_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchBooks());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_MORE_BOOKS_SUCCESS when the request was successful', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_MORE_BOOKS },
                { type: RECEIVE_MORE_BOOKS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchMoreBooks());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_MORE_BOOKS_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet(/\/api\/books(\?.+)?/).reply(500, { message });

            const expectedActions = [
                { type: REQUEST_MORE_BOOKS },
                {
                    type: RECEIVE_MORE_BOOKS_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchMoreBooks());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('requests books when books is empty on the state', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchBooksIfNeeded());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('requests books when books is outdated on the state', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];
            const initialState = {
                books: {
                    data: [{ id: '3', title: 'title3' }],
                    didInvalidate: true
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.fetchBooksIfNeeded());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('does not request books when books is not outdated on the state', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet('/api/books').reply(200, mockResponse);

            const expectedActions = [];
            const initialState = {
                books: {
                    data: [{ id: '3', title: 'title3' }],
                    didInvalidate: false
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.fetchBooksIfNeeded());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('does not request books during fetching', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet('/api/books').reply(200, mockResponse);

            const expectedActions = [];
            const initialState = {
                books: {
                    data: [{ id: '3', title: 'title3' }],
                    isFetching: true,
                    didInvalidate: true
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.fetchBooksIfNeeded());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
