import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './sortingActions';
import {
    CHANGE_SORTFIELD,
    TOGGLE_SORTORDER,
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RESET_PAGINATION
} from '../constants';

describe('sorting actions', () => {
    describe('action creators', () => {
        test('creates an action to change sort field', () => {
            const sortField = 'author';
            const expectedAction = {
                type: CHANGE_SORTFIELD,
                sortField
            };

            const action = actionTypes.changeSortfield(sortField);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to toggle sort order', () => {
            const expectedAction = {
                type: TOGGLE_SORTORDER
            };

            const action = actionTypes.toggleSortOrder();
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        const mockResponse = {
            books: [
                { _id: '1', title: 'title' },
                { _id: '2', title: 'title2' }
            ],
            count: 0,
            hasNext: true
        };

        afterEach(() => {
            mock.reset();
        });

        test('creates REQUEST_BOOKS after CHANGE_SORTFIELD', async () => {
            const sortField = 'author';
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: CHANGE_SORTFIELD, sortField },
                { type: RESET_PAGINATION },
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.updateSortField(sortField));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates REQUEST_BOOKS after TOGGLE_SORTORDER', async () => {
            const sortOrder = -1;
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: TOGGLE_SORTORDER },
                { type: RESET_PAGINATION },
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.updateSortOrder(sortOrder));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
