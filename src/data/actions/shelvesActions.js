import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    REQUEST_SHELVES,
    RECEIVE_SHELVES_SUCCESS,
    RECEIVE_SHELVES_FAILURE,
    SELECT_SHELF,
    UNSELECT_SHELF,
    UPDATE_SHELF,
    UPDATE_SHELF_SUCCESS,
    UPDATE_SHELF_FAILURE,
    DELETE_SHELF,
    DELETE_SHELF_SUCCESS,
    DELETE_SHELF_FAILURE
} from '../constants';
import { filterBooksWithAllFilters } from './rangeActions';
import { booksPage } from './pageActions';

export const requestShelves = () => ({
    type: REQUEST_SHELVES
});

export const receiveShelvesSuccess = response => ({
    type: RECEIVE_SHELVES_SUCCESS,
    response
});

export const receiveShelvesFailure = error => ({
    type: RECEIVE_SHELVES_FAILURE,
    error: createErrorMessage(error)
});

export const selectShelf = shelfName => ({
    type: SELECT_SHELF,
    shelfName
});

export const unSelectShelf = () => ({
    type: UNSELECT_SHELF
});

export const updateShelf = () => ({
    type: UPDATE_SHELF
});

export const updateShelfSuccess = (shelfName, newName) => ({
    type: UPDATE_SHELF_SUCCESS,
    shelfName,
    newName
});

export const updateShelfFailure = error => ({
    type: UPDATE_SHELF_FAILURE,
    error: createErrorMessage(error)
});

export const deleteShelf = () => ({
    type: DELETE_SHELF
});

export const deleteShelfSuccess = shelfName => ({
    type: DELETE_SHELF_SUCCESS,
    shelfName
});

export const deleteShelfFailure = error => ({
    type: DELETE_SHELF_FAILURE,
    error: createErrorMessage(error)
});

export const fetchShelves = () => dispatch => {
    dispatch(requestShelves());

    return axios.get('/api/filters/shelves/stat').then(
        response => dispatch(receiveShelvesSuccess(response.data)),
        error => dispatch(receiveShelvesFailure(handleError(error)))
    );
};

export const selectShelfByName = shelfName => dispatch => {
    dispatch(selectShelf(shelfName));
    return dispatch(filterBooksWithAllFilters());
};

export const removeShelfSelection = () => dispatch => {
    dispatch(unSelectShelf());
    dispatch(filterBooksWithAllFilters());
    return dispatch(booksPage());
};

export const saveEditedShelf = (shelfName, data) => dispatch => {
    dispatch(updateShelf());

    return axios.put(`/api/filters/shelves/${shelfName}`, data).then(
        () => dispatch(updateShelfSuccess(shelfName, data.shelfName)),
        error => dispatch(updateShelfFailure(handleError(error)))
    );
};

export const deleteSelectedShelf = shelfName => dispatch => {
    dispatch(deleteShelf());

    return axios.delete(`/api/filters/shelves/${shelfName}`).then(
        () => dispatch(deleteShelfSuccess(shelfName)),
        error => dispatch(deleteShelfFailure(handleError(error)))
    );
};
