import { redirect } from 'redux-first-router';
import { booksPage } from './pageActions';
import { fetchBooksIfNeeded } from './booksActions';
import { selectPage } from './paginationActions';
import {
    selectBook,
    unSelectBook,
    fetchSelectedBook
} from './selectedBookActions';
import { fetchSuggestionLists } from './suggestionsActions';
import { createInitialBook } from './importActions';
import { fetchShelves } from './shelvesActions';
import { getCurrentPageNumber } from '../reducers/paginationReducer';
import {
    getSelectedBookId,
    getSelectedBook
} from '../reducers/selectedBookReducer';
import { getSitesData } from '../reducers/importReducer';
import { getShelves } from '../reducers/shelvesReducer';
import get from '../../utils/get';

export const navigateToHome = () => dispatch => dispatch(redirect(booksPage()));

export const navigateToBooks = () => (dispatch, getState) => {
    const state = getState();
    const { page = 1 } = get(state, 'location.query', {});
    const selectedBookId = getSelectedBookId(state);
    const currentPageNumber = getCurrentPageNumber(state);
    const pageNumber = Number(page);

    if (selectedBookId) {
        dispatch(unSelectBook());
    }

    if (pageNumber !== currentPageNumber) {
        dispatch(selectPage(pageNumber));
    }
    return dispatch(fetchBooksIfNeeded());
};

const shouldUpdateSelectedBook = (state, id) => id !== getSelectedBookId(state);
const shouldUpdateSelectedBookData = (state, id) =>
    id !== getSelectedBook(state)._id;

export const navigateToBookDetails = () => (dispatch, getState) => {
    const {
        location: {
            payload: { id }
        }
    } = getState();

    if (shouldUpdateSelectedBook(getState(), id)) {
        dispatch(selectBook(id));
        return dispatch(fetchSelectedBook(id));
    }

    return Promise.resolve();
};

export const navigateToEditBook = () => (dispatch, getState) => {
    const {
        location: {
            payload: { id }
        }
    } = getState();

    if (shouldUpdateSelectedBookData(getState(), id)) {
        dispatch(selectBook(id));
        dispatch(fetchSelectedBook(id));
    }

    return dispatch(fetchSuggestionLists());
};

export const navigateToEditImport = () => (dispatch, getState) => {
    const sitesData = getSitesData(getState());

    dispatch(createInitialBook(sitesData));
    return dispatch(fetchSuggestionLists());
};

export const navigateToShelves = () => (dispatch, getState) => {
    const shelves = getShelves(getState());

    if (!shelves.length) {
        return dispatch(fetchShelves());
    }

    return Promise.resolve();
};
