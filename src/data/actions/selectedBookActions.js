import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    SELECT_BOOK,
    UNSELECT_BOOK,
    REQUEST_SELECTED_BOOK,
    RECEIVE_SELECTED_BOOK_SUCCESS,
    RECEIVE_SELECTED_BOOK_FAILURE,
    UPDATE_SELECTED_BOOK,
    UPDATE_SELECTED_BOOK_SUCCESS,
    UPDATE_SELECTED_BOOK_FAILURE,
    DELETE_SELECTED_BOOK,
    DELETE_SELECTED_BOOK_SUCCESS,
    DELETE_SELECTED_BOOK_FAILURE
} from '../constants';
import { updateAllFiltersAndFiltering } from './rangeActions';
import { bookDetailsPage, booksPage } from './pageActions';
import {
    setLimitForRestoreScrolling,
    resetLimit,
    selectStartIndex,
    resetStartIndexForPage
} from './paginationActions';
import {
    getCurrentPageNumber,
    getStartIndex
} from '../reducers/paginationReducer';

export const selectBook = id => ({
    type: SELECT_BOOK,
    id
});

export const unSelectBook = () => ({
    type: UNSELECT_BOOK
});

export const requestSelectedBook = () => ({
    type: REQUEST_SELECTED_BOOK
});

export const receiveSelectedBookSuccess = response => ({
    type: RECEIVE_SELECTED_BOOK_SUCCESS,
    response
});

export const receiveSelectedBookFailure = error => ({
    type: RECEIVE_SELECTED_BOOK_FAILURE,
    error: createErrorMessage(error)
});

export const updateSelectedBook = () => ({
    type: UPDATE_SELECTED_BOOK
});

export const updateSelectedBookSuccess = response => ({
    type: UPDATE_SELECTED_BOOK_SUCCESS,
    response
});

export const updateSelectedBookFailure = error => ({
    type: UPDATE_SELECTED_BOOK_FAILURE,
    error: createErrorMessage(error)
});

export const deleteSelectedBook = () => ({
    type: DELETE_SELECTED_BOOK
});

export const deleteSelectedBookSuccess = () => ({
    type: DELETE_SELECTED_BOOK_SUCCESS
});

export const deleteSelectedBookFailure = error => ({
    type: DELETE_SELECTED_BOOK_FAILURE,
    error: createErrorMessage(error)
});

export const fetchSelectedBook = bookId => dispatch => {
    dispatch(requestSelectedBook());

    return axios.get(`/api/books/${bookId}`).then(
        response => dispatch(receiveSelectedBookSuccess(response.data)),
        error => dispatch(receiveSelectedBookFailure(handleError(error)))
    );
};

const navigateToCurrentBooksPage = () => (dispatch, getState) => {
    const currentPageNumber = getCurrentPageNumber(getState());
    return dispatch(booksPage({ page: currentPageNumber }));
};

export const filterBooksWithAllFiltersAndRestoreScrolling = () => (
    dispatch,
    getState
) => {
    const currentStartIndex = getStartIndex(getState());
    dispatch(setLimitForRestoreScrolling(currentStartIndex));
    dispatch(updateAllFiltersAndFiltering());
    dispatch(resetLimit());
    return dispatch(selectStartIndex(currentStartIndex));
};

export const filterBooksWithAllFiltersAndRestorePage = () => dispatch => {
    dispatch(resetStartIndexForPage());
    return dispatch(updateAllFiltersAndFiltering());
};

export const saveEditedBook = (bookId, bookData) => dispatch => {
    dispatch(updateSelectedBook());

    return axios.put(`/api/books/${bookId}`, bookData).then(
        response => {
            dispatch(updateSelectedBookSuccess(response.data));
            dispatch(filterBooksWithAllFiltersAndRestoreScrolling());
            return dispatch(bookDetailsPage(bookId));
        },
        error => dispatch(updateSelectedBookFailure(handleError(error)))
    );
};

export const deleteBook = bookId => dispatch => {
    dispatch(deleteSelectedBook());

    return axios.delete(`/api/books/${bookId}`).then(
        () => {
            dispatch(deleteSelectedBookSuccess());
            dispatch(filterBooksWithAllFiltersAndRestorePage());
            return dispatch(navigateToCurrentBooksPage());
        },
        error => dispatch(deleteSelectedBookFailure(handleError(error)))
    );
};
