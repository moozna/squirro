import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './paginationActions';
import {
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    INCREASE_START_INDEX,
    SELECT_START_INDEX,
    SELECT_PAGE_NUMBER,
    RESET_PAGINATION,
    SELECT_LIMIT,
    RESET_LIMIT
} from '../constants';

describe('pagination actions', () => {
    describe('action creators', () => {
        test('creates an action to increase start index', () => {
            const expectedAction = {
                type: INCREASE_START_INDEX,
                limit: 25
            };

            const actions = actionTypes.increaseStartIndex(25);
            expect(actions).toEqual(expectedAction);
        });

        test('creates an action to select start index', () => {
            const expectedAction = {
                type: SELECT_START_INDEX,
                startIndex: 120
            };

            const actions = actionTypes.selectStartIndex(120);
            expect(actions).toEqual(expectedAction);
        });

        test('creates an action to select page number', () => {
            const expectedAction = {
                type: SELECT_PAGE_NUMBER,
                pageNumber: 3
            };

            const actions = actionTypes.selectPageNumber(3);
            expect(actions).toEqual(expectedAction);
        });

        test('creates an action to reset pagination', () => {
            const expectedAction = {
                type: RESET_PAGINATION
            };

            const actions = actionTypes.resetPagination();
            expect(actions).toEqual(expectedAction);
        });

        test('creates an action to select limit', () => {
            const expectedAction = {
                type: SELECT_LIMIT,
                limit: 150
            };

            const actions = actionTypes.selectLimit(150);
            expect(actions).toEqual(expectedAction);
        });

        test('creates an action to reset limit', () => {
            const expectedAction = {
                type: RESET_LIMIT
            };

            const actions = actionTypes.resetLimit();
            expect(actions).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        test('creates the correct actions when loadMoreBooks called', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: INCREASE_START_INDEX, limit: 25 },
                { type: REQUEST_MORE_BOOKS },
                { type: RECEIVE_MORE_BOOKS_SUCCESS, response: mockResponse }
            ];
            const initialState = {
                pagination: {
                    limit: 25
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.loadMoreBooks());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates the correct actions when selectPage is called', async () => {
            const expectedActions = [
                { type: SELECT_START_INDEX, startIndex: 100 },
                { type: SELECT_PAGE_NUMBER, pageNumber: 3 }
            ];
            const initialState = {
                pagination: {
                    pageSize: 50
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.selectPage(3));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates the correct actions when loadPage is called', async () => {
            const mockResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockResponse);

            const expectedActions = [
                { type: SELECT_START_INDEX, startIndex: 100 },
                { type: SELECT_PAGE_NUMBER, pageNumber: 3 },
                { type: REQUEST_BOOKS },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockResponse }
            ];
            const initialState = {
                pagination: {
                    pageSize: 50
                }
            };

            const store = mockStore(initialState);
            await store.dispatch(actionTypes.loadPage(3));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
