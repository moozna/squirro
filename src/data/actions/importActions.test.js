import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './importActions';
import {
    IMPORT_BOOK,
    IMPORT_BOOK_SUCCESS,
    IMPORT_BOOK_FAILURE,
    CLEAR_IMPORTED_DATA,
    UPDATE_DUPLICATE_KEYS,
    CREATE_INITIAL_BOOK
} from '../constants';

describe('import actions', () => {
    describe('action creators', () => {
        test('creates an action to importing book', () => {
            const siteName = 'amazon';
            const expectedAction = {
                type: IMPORT_BOOK,
                siteName
            };
            const action = actionTypes.importBook(siteName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful book import', () => {
            const siteName = 'amazon';
            const response = {
                asin: 'asin',
                title: 'title'
            };
            const expectedAction = {
                type: IMPORT_BOOK_SUCCESS,
                response,
                siteName
            };

            const action = actionTypes.importBookSuccess(response, siteName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed book import', () => {
            const siteName = 'amazon';
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: IMPORT_BOOK_FAILURE,
                error,
                siteName
            };

            const action = actionTypes.importBookFailure(error, siteName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to clear imported data', () => {
            const expectedAction = {
                type: CLEAR_IMPORTED_DATA
            };

            const action = actionTypes.clearImportedData();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to update duplicate keys', () => {
            const response = { goodreadsId: 12, title: 'title' };
            const otherData = { asin: 'AB', title: 'title' };
            const expectedAction = {
                type: UPDATE_DUPLICATE_KEYS,
                response,
                otherData
            };

            const action = actionTypes.updateDuplicateKeys(response, otherData);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to create initial book', () => {
            const sitesData = {
                goodreads: { goodreadsId: 12 },
                amazon: { asin: 'AB' }
            };
            const expectedAction = {
                type: CREATE_INITIAL_BOOK,
                sitesData
            };
            const action = actionTypes.createInitialBook(sitesData);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('amazon', () => {
            test('creates IMPORT_BOOK_SUCCESS and UPDATE_DUPLICATE_KEYS when the import was successful', async () => {
                const id = '1';
                const siteName = 'amazon';
                const bookData = {
                    asin: 'asin',
                    title: 'title'
                };
                const goodreadsData = {
                    goodreadsId: 12,
                    title: 'title'
                };
                const mockResponse = bookData;
                mock.onGet(`api/books/import/amazon/${id}`).reply(
                    200,
                    mockResponse
                );

                const expectedActions = [
                    {
                        type: IMPORT_BOOK,
                        siteName
                    },
                    {
                        type: IMPORT_BOOK_SUCCESS,
                        response: mockResponse,
                        siteName
                    },
                    {
                        type: UPDATE_DUPLICATE_KEYS,
                        response: mockResponse,
                        otherData: goodreadsData
                    }
                ];
                const initialState = {
                    importing: {
                        data: {
                            goodreads: goodreadsData
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.importAmazonBook(id));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates IMPORT_BOOK_FAILURE when the import is failed', async () => {
                const id = '1';
                const siteName = 'amazon';
                const message = 'Error message';
                mock.onGet(`api/books/import/amazon/${id}`).reply(500, {
                    message
                });

                const expectedActions = [
                    {
                        type: IMPORT_BOOK,
                        siteName
                    },
                    {
                        type: IMPORT_BOOK_FAILURE,
                        error: { message, status: 500 },
                        siteName
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.importAmazonBook(id));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('goodreads', () => {
            test('creates IMPORT_BOOK_SUCCESS and UPDATE_DUPLICATE_KEYS when the import was successful', async () => {
                const id = '1';
                const siteName = 'goodreads';
                const bookData = {
                    goodreadsId: 12,
                    title: 'title'
                };
                const amazonData = {
                    asin: 'AN',
                    title: 'title'
                };
                const mockResponse = bookData;
                mock.onGet(`api/books/import/goodreads/${id}`).reply(
                    200,
                    mockResponse
                );

                const expectedActions = [
                    {
                        type: IMPORT_BOOK,
                        siteName
                    },
                    {
                        type: IMPORT_BOOK_SUCCESS,
                        response: mockResponse,
                        siteName
                    },
                    {
                        type: UPDATE_DUPLICATE_KEYS,
                        response: mockResponse,
                        otherData: amazonData
                    }
                ];
                const initialState = {
                    importing: {
                        data: {
                            amazon: amazonData
                        }
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.importGoodreadsBook(id));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates IMPORT_BOOK_FAILURE when the import is failed', async () => {
                const id = '1';
                const siteName = 'goodreads';
                const message = 'Error message';
                mock.onGet(`api/books/import/goodreads/${id}`).reply(500, {
                    message
                });

                const expectedActions = [
                    {
                        type: IMPORT_BOOK,
                        siteName
                    },
                    {
                        type: IMPORT_BOOK_FAILURE,
                        error: { message, status: 500 },
                        siteName
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.importGoodreadsBook(id));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });
    });
});
