import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    CHANGE_OWN_RATING_RANGE,
    CHANGE_AMAZON_RATING_RANGE,
    CHANGE_GOODREADS_RATING_RANGE,
    REQUEST_RANGE_FILTERS,
    RECEIVE_RANGE_FILTERS_SUCCESS,
    RECEIVE_RANGE_FILTERS_FAILURE,
    CHANGE_YEAR_RANGE,
    CHANGE_PAGE_RANGE,
    CHANGE_INTEREST_RANGE
} from '../constants';
import { fetchFilters, updateFiltering, filterBooks } from './filtersActions';
import { resetPagination } from './paginationActions';

export const changeOwnRatingRange = ratingValues => ({
    type: CHANGE_OWN_RATING_RANGE,
    ratingValues
});

export const changeAmazonRatingRange = ratingValues => ({
    type: CHANGE_AMAZON_RATING_RANGE,
    ratingValues
});

export const changeGoodreadsRatingRange = ratingValues => ({
    type: CHANGE_GOODREADS_RATING_RANGE,
    ratingValues
});

export const requestRangeFilters = () => ({
    type: REQUEST_RANGE_FILTERS
});

export const receiveRangeFiltersSuccess = response => ({
    type: RECEIVE_RANGE_FILTERS_SUCCESS,
    response
});

export const receiveRangeFiltersFailure = error => ({
    type: RECEIVE_RANGE_FILTERS_FAILURE,
    error: createErrorMessage(error)
});

export const changeYearRange = values => ({
    type: CHANGE_YEAR_RANGE,
    values
});

export const changePageRange = values => ({
    type: CHANGE_PAGE_RANGE,
    values
});

export const changeInterestRange = values => ({
    type: CHANGE_INTEREST_RANGE,
    values
});

export const fetchRangeFilters = () => dispatch => {
    dispatch(requestRangeFilters());

    return axios.get('/api/filters/ranges').then(
        response => dispatch(receiveRangeFiltersSuccess(response.data)),
        error => dispatch(receiveRangeFiltersFailure(handleError(error)))
    );
};

export const fetchAllFilters = () => dispatch => {
    dispatch(fetchFilters());
    return dispatch(fetchRangeFilters());
};

export const updateAllFiltersAndFiltering = () => dispatch => {
    dispatch(fetchRangeFilters());
    return dispatch(updateFiltering());
};

export const filterBooksWithAllFilters = () => dispatch => {
    dispatch(resetPagination());
    return dispatch(updateAllFiltersAndFiltering());
};

export const updateOwnRatingRange = ratingValues => dispatch => {
    dispatch(changeOwnRatingRange(ratingValues));
    return dispatch(filterBooks());
};

export const updateAmazonRatingRange = ratingValues => dispatch => {
    dispatch(changeAmazonRatingRange(ratingValues));
    return dispatch(filterBooks());
};

export const updateGoodreadsRatingRange = ratingValues => dispatch => {
    dispatch(changeGoodreadsRatingRange(ratingValues));
    return dispatch(filterBooks());
};

export const updateYearRange = values => dispatch => {
    dispatch(changeYearRange(values));
    return dispatch(filterBooks());
};

export const updatePageRange = values => dispatch => {
    dispatch(changePageRange(values));
    return dispatch(filterBooks());
};

export const updateInterestRange = values => dispatch => {
    dispatch(changeInterestRange(values));
    return dispatch(filterBooks());
};
