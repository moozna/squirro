import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    UPLOAD_IMAGE,
    UPLOAD_IMAGE_SUCCESS,
    UPLOAD_IMAGE_FAILURE,
    DELETE_IMAGE,
    DELETE_IMAGE_SUCCESS,
    DELETE_IMAGE_FAILURE
} from '../constants';

export const uploadImage = () => ({
    type: UPLOAD_IMAGE
});

export const uploadImageSuccess = response => ({
    type: UPLOAD_IMAGE_SUCCESS,
    images: response
});

export const uploadImageFailure = error => ({
    type: UPLOAD_IMAGE_FAILURE,
    error: createErrorMessage(error)
});

export const deleteImage = () => ({
    type: DELETE_IMAGE
});

export const deleteImageSuccess = () => ({
    type: DELETE_IMAGE_SUCCESS
});

export const deleteImageFailure = error => ({
    type: DELETE_IMAGE_FAILURE,
    error: createErrorMessage(error)
});

export function createCoverFormData(asin, files) {
    const formData = new FormData();
    files.forEach(file => formData.append('images', file, file.name));
    formData.append('asin', asin);
    return formData;
}

export const uploadCoverImage = (asin, files) => dispatch => {
    dispatch(uploadImage());

    return axios
        .post('/api/images/covers', createCoverFormData(asin, files))
        .then(
            response => dispatch(uploadImageSuccess(response.data)),
            error => dispatch(uploadImageFailure(handleError(error)))
        );
};

export const deleteCoverImage = urls => dispatch => {
    dispatch(deleteImage());

    return axios.post('/api/images/covers/deletes', { files: urls }).then(
        () => dispatch(deleteImageSuccess()),
        error => dispatch(deleteImageFailure(handleError(error)))
    );
};
