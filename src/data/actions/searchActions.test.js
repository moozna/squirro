import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './searchActions';
import {
    CHANGE_SEARCH_VALUE,
    CLEAR_SEARCH_VALUE,
    REQUEST_SUGGESTIONS,
    RECEIVE_SUGGESTIONS_SUCCESS,
    RECEIVE_SUGGESTIONS_FAILURE,
    CLEAR_SUGGESTIONS,
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    BOOKS,
    BOOK,
    RESET_PAGINATION
} from '../constants';

describe('search actions', () => {
    describe('action creators', () => {
        test('creates an action to change the search value', () => {
            const value = 'king';
            const expectedAction = {
                type: CHANGE_SEARCH_VALUE,
                value
            };

            const action = actionTypes.changeSearchValue(value);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to clear the search value', () => {
            const expectedAction = {
                type: CLEAR_SEARCH_VALUE
            };

            const action = actionTypes.clearSearchValue();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to request suggestions', () => {
            const expectedAction = {
                type: REQUEST_SUGGESTIONS
            };

            const action = actionTypes.requestSuggestions();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful suggestions request', () => {
            const response = {
                authors: [{ name: 'Owen King' }, { name: 'Stephen King' }],
                seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
                titles: [
                    { name: 'The Mirror King' },
                    { name: 'The King of Attolia' }
                ]
            };
            const expectedAction = {
                type: RECEIVE_SUGGESTIONS_SUCCESS,
                response
            };

            const action = actionTypes.receiveSuggestionsSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed suggestions request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_SUGGESTIONS_FAILURE,
                error
            };

            const action = actionTypes.receiveSuggestionsFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to clear suggestions', () => {
            const expectedAction = {
                type: CLEAR_SUGGESTIONS
            };

            const action = actionTypes.clearSuggestions();
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        test('creates RECEIVE_SUGGESTIONS_SUCCESS when the request was successful', async () => {
            const mockResponse = {
                authors: [{ name: 'Owen King' }, { name: 'Stephen King' }],
                seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
                titles: [
                    { name: 'The Mirror King' },
                    { name: 'The King of Attolia' }
                ]
            };
            mock.onGet(/\/api\/books\/suggestions(\?.+)?/).reply(
                200,
                mockResponse
            );

            const expectedActions = [
                { type: REQUEST_SUGGESTIONS },
                { type: RECEIVE_SUGGESTIONS_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSuggestions());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_SUGGESTIONS_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet(/\/api\/books\/suggestions(\?.+)?/).reply(500, {
                message
            });

            const expectedActions = [
                { type: REQUEST_SUGGESTIONS },
                {
                    type: RECEIVE_SUGGESTIONS_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSuggestions());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('filters books after CHANGE_SEARCH_VALUE', async () => {
            const mockFilterResponse = {
                genres: [{ _id: 'Action', count: 1 }],
                tags: [{ _id: 'free book', count: 193 }]
            };
            mock.onGet(/\/api\/filters(\?.+)?/).reply(200, mockFilterResponse);

            const value = 'king';
            const mockBooksResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

            const expectedActions = [
                { type: CHANGE_SEARCH_VALUE, value },
                { type: RESET_PAGINATION },
                { type: REQUEST_FILTERS },
                { type: REQUEST_BOOKS },
                { type: RECEIVE_FILTERS_SUCCESS, response: mockFilterResponse },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
            ];

            const store = mockStore({ page: 'Books' });
            await store.dispatch(actionTypes.searchBooks(value));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('navigates to bookDetails when there is only one search result', async () => {
            const mockBooks = [{ _id: '1', title: 'title' }];
            const expectedActions = [{ type: BOOK, payload: { id: '1' } }];

            const store = mockStore({ books: { data: mockBooks } });
            await store.dispatch(actionTypes.navigateToSearchResult());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('navigates to books when it is not the current page', async () => {
            const mockBooks = [
                { _id: '1', title: 'title' },
                { _id: '2', title: 'title2' }
            ];
            const expectedActions = [{ type: BOOKS, query: {} }];

            const store = mockStore({ books: { data: mockBooks } });
            await store.dispatch(actionTypes.navigateToSearchResult());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('filters books after CLEAR_SEARCH_VALUE', async () => {
            const mockFilterResponse = {
                genres: [{ _id: 'Action', count: 1 }],
                tags: [{ _id: 'free book', count: 193 }]
            };
            mock.onGet(/\/api\/filters(\?.+)?/).reply(200, mockFilterResponse);

            const mockBooksResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };
            mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

            const expectedActions = [
                { type: CLEAR_SEARCH_VALUE },
                { type: RESET_PAGINATION },
                { type: BOOKS, query: {} },
                { type: REQUEST_FILTERS },
                { type: REQUEST_BOOKS },
                { type: RECEIVE_FILTERS_SUCCESS, response: mockFilterResponse },
                { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.clearSearch());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
