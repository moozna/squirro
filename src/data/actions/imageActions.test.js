import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './imageActions';
import {
    UPLOAD_IMAGE,
    UPLOAD_IMAGE_SUCCESS,
    UPLOAD_IMAGE_FAILURE,
    DELETE_IMAGE,
    DELETE_IMAGE_SUCCESS,
    DELETE_IMAGE_FAILURE
} from '../constants';

class FormDataMock {
    values = {};

    append = (prop, value) => {
        this.values[prop] = value;
    };
}
global.FormData = FormDataMock;

describe('image actions', () => {
    describe('action creators', () => {
        test('creates an action to upload an image', () => {
            const expectedAction = {
                type: UPLOAD_IMAGE
            };

            const action = actionTypes.uploadImage();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful image upload', () => {
            const response = [
                {
                    url: '/images/covers/B000FBFMFW.jpg--960c.jpg',
                    thumbnail: '/images/smallcovers/B000FBFMFW.jpg--960c.jpg'
                }
            ];

            const expectedAction = {
                type: UPLOAD_IMAGE_SUCCESS,
                images: response
            };

            const action = actionTypes.uploadImageSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed image upload', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: UPLOAD_IMAGE_FAILURE,
                error
            };

            const action = actionTypes.uploadImageFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to delete an image', () => {
            const expectedAction = {
                type: DELETE_IMAGE
            };

            const action = actionTypes.deleteImage();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful image delete', () => {
            const expectedAction = {
                type: DELETE_IMAGE_SUCCESS
            };

            const action = actionTypes.deleteImageSuccess();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed image delete', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: DELETE_IMAGE_FAILURE,
                error
            };

            const action = actionTypes.deleteImageFailure(error);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('uploading cover image', () => {
            const mockAsin = 'B000FBFMFW';
            const mockFiles = [{ url: 'url1.jpg' }];
            const mockResponse = [
                {
                    url: '/images/covers/B000FBFMFW.jpg--960c.jpg',
                    thumbnail: '/images/smallcovers/B000FBFMFW.jpg--960c.jpg'
                }
            ];

            test('creates UPLOAD_IMAGE_SUCCESS when the upload was successful', async () => {
                mock.onPost('api/images/covers').reply(200, mockResponse);

                const expectedActions = [
                    { type: UPLOAD_IMAGE },
                    {
                        type: UPLOAD_IMAGE_SUCCESS,
                        images: mockResponse
                    }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.uploadCoverImage(mockAsin, mockFiles)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates UPLOAD_IMAGE_FAILURE when the upload failed', async () => {
                const message = 'Error message';
                mock.onPost('api/images/covers').reply(500, { message });

                const expectedActions = [
                    { type: UPLOAD_IMAGE },
                    {
                        type: UPLOAD_IMAGE_FAILURE,
                        error: { message, status: 500 }
                    }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.uploadCoverImage(mockAsin, mockFiles)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('deleting cover image', () => {
            const mockFiles = [{ url: 'url1.jpg' }];

            test('creates DELETE_IMAGE_SUCCESS when the upload was successful', async () => {
                mock.onPost('api/images/covers/deletes').reply(200);

                const expectedActions = [
                    { type: DELETE_IMAGE },
                    { type: DELETE_IMAGE_SUCCESS }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.deleteCoverImage(mockFiles));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates DELETE_IMAGE_FAILURE when the delete failed', async () => {
                const message = 'Error message';
                mock.onPost('api/images/covers/deletes').reply(500, {
                    message
                });

                const expectedActions = [
                    { type: DELETE_IMAGE },
                    {
                        type: DELETE_IMAGE_FAILURE,
                        error: { message, status: 500 }
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.deleteCoverImage(mockFiles));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });
    });
});
