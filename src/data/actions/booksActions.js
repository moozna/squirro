import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INVALIDATE_BOOKS
} from '../constants';
import {
    getBooks,
    getFetching,
    getDidInvalidate
} from '../reducers/booksReducer';
import { getStartIndex, getPageLimit } from '../reducers/paginationReducer';
import {
    getActiveGenres,
    getActiveTags,
    getExcludedTags,
    getReadStatus
} from '../reducers/filtersReducer';
import {
    getOwnRating,
    getAmazonRating,
    getGoodreadsRating,
    getYear,
    getPages,
    getInterestRange
} from '../reducers/rangeReducer';
import { getSelectedShelf } from '../reducers/shelvesReducer';
import { getSearchFieldValue } from '../reducers/searchReducer';
import { getSortField, getSortOrder } from '../reducers/sortingReducer';
import urlEncodeQueryParams from '../../utils/urlEncodeQueryParams';

export const requestBooks = () => ({
    type: REQUEST_BOOKS
});

export const receiveBooksSuccess = response => ({
    type: RECEIVE_BOOKS_SUCCESS,
    response
});

export const receiveBooksFailure = error => ({
    type: RECEIVE_BOOKS_FAILURE,
    error: createErrorMessage(error)
});

export const requestMoreBooks = () => ({
    type: REQUEST_MORE_BOOKS
});

export const receiveMoreBooksSuccess = response => ({
    type: RECEIVE_MORE_BOOKS_SUCCESS,
    response
});

export const receiveMoreBooksFailure = error => ({
    type: RECEIVE_MORE_BOOKS_FAILURE,
    error: createErrorMessage(error)
});

export const invalidateBooks = () => ({
    type: INVALIDATE_BOOKS
});

export const getCurrentFiltering = state => ({
    search: getSearchFieldValue(state),
    genres: getActiveGenres(state),
    tags: getActiveTags(state),
    excludedTags: getExcludedTags(state),
    rating: getOwnRating(state),
    amazonRating: getAmazonRating(state),
    goodreadsRating: getGoodreadsRating(state),
    year: getYear(state),
    pages: getPages(state),
    readStatus: getReadStatus(state),
    shelf: getSelectedShelf(state),
    interestRange: getInterestRange(state)
});

const getCurrentQuery = state => ({
    ...getCurrentFiltering(state),
    startIndex: getStartIndex(state),
    limit: getPageLimit(state),
    sort: getSortField(state),
    order: getSortOrder(state)
});

const requestBooksWithQuery = getState => {
    const queryObj = getCurrentQuery(getState());
    const query = urlEncodeQueryParams(queryObj);

    return axios.get(`/api/books${query}`);
};

export const fetchBooks = () => (dispatch, getState) => {
    dispatch(requestBooks());

    return requestBooksWithQuery(getState).then(
        response => dispatch(receiveBooksSuccess(response.data)),
        error => dispatch(receiveBooksFailure(handleError(error)))
    );
};

export const fetchMoreBooks = () => (dispatch, getState) => {
    dispatch(requestMoreBooks());

    return requestBooksWithQuery(getState).then(
        response => dispatch(receiveMoreBooksSuccess(response.data)),
        error => dispatch(receiveMoreBooksFailure(handleError(error)))
    );
};

const shouldFetchBooks = state => {
    if (getFetching(state)) {
        return false;
    }
    if (!getBooks(state).length) {
        return true;
    }
    return getDidInvalidate(state);
};

export const fetchBooksIfNeeded = () => (dispatch, getState) => {
    if (shouldFetchBooks(getState())) {
        return dispatch(fetchBooks());
    }

    return Promise.resolve(false);
};
