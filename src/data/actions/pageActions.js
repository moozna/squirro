import {
    BOOKS,
    BOOK,
    EDIT,
    ADD,
    IMPORT,
    EDIT_IMPORT,
    SHELVES
} from '../constants';

export const booksPage = (query = {}) => ({
    type: BOOKS,
    query
});

export const bookDetailsPage = id => ({
    type: BOOK,
    payload: { id }
});

export const editBookPage = id => ({
    type: EDIT,
    payload: { id }
});

export const addBookPage = () => ({
    type: ADD
});

export const importBookPage = () => ({
    type: IMPORT
});

export const editImportPage = () => ({
    type: EDIT_IMPORT
});

export const shelvesPage = () => ({
    type: SHELVES
});
