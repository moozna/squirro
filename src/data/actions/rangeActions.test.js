import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './rangeActions';
import {
    CHANGE_OWN_RATING_RANGE,
    CHANGE_AMAZON_RATING_RANGE,
    CHANGE_GOODREADS_RATING_RANGE,
    REQUEST_RANGE_FILTERS,
    RECEIVE_RANGE_FILTERS_SUCCESS,
    RECEIVE_RANGE_FILTERS_FAILURE,
    CHANGE_YEAR_RANGE,
    CHANGE_PAGE_RANGE,
    CHANGE_INTEREST_RANGE,
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    RESET_PAGINATION,
    BOOKS
} from '../constants';

describe('range actions', () => {
    describe('action creators', () => {
        test('creates an action to change own rating range', () => {
            const ratingValues = [2, 5];
            const expectedAction = {
                type: CHANGE_OWN_RATING_RANGE,
                ratingValues
            };

            const action = actionTypes.changeOwnRatingRange(ratingValues);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change Amazon rating range', () => {
            const ratingValues = [2, 5];
            const expectedAction = {
                type: CHANGE_AMAZON_RATING_RANGE,
                ratingValues
            };

            const action = actionTypes.changeAmazonRatingRange(ratingValues);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change Goodreads rating range', () => {
            const ratingValues = [2, 5];
            const expectedAction = {
                type: CHANGE_GOODREADS_RATING_RANGE,
                ratingValues
            };

            const action = actionTypes.changeGoodreadsRatingRange(ratingValues);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to request range filters', () => {
            const expectedAction = {
                type: REQUEST_RANGE_FILTERS
            };

            const action = actionTypes.requestRangeFilters();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful range filters request', () => {
            const response = {
                year: { min: 1980, max: 2017 },
                pages: { min: 50, max: 1671 }
            };
            const expectedAction = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response
            };

            const action = actionTypes.receiveRangeFiltersSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed range filters request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_RANGE_FILTERS_FAILURE,
                error
            };

            const action = actionTypes.receiveRangeFiltersFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change year range', () => {
            const rangeValues = [1955, 2006];
            const expectedAction = {
                type: CHANGE_YEAR_RANGE,
                values: rangeValues
            };

            const action = actionTypes.changeYearRange(rangeValues);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change page range', () => {
            const rangeValues = [77, 450];
            const expectedAction = {
                type: CHANGE_PAGE_RANGE,
                values: rangeValues
            };

            const action = actionTypes.changePageRange(rangeValues);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change interest range', () => {
            const rangeValues = [2, 4];
            const expectedAction = {
                type: CHANGE_INTEREST_RANGE,
                values: rangeValues
            };

            const action = actionTypes.changeInterestRange(rangeValues);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('range filters', () => {
            test('creates RECEIVE_RANGE_FILTERS_SUCCESS when the request was successful', async () => {
                const mockResponse = {
                    year: { min: 1980, max: 2017 },
                    pages: { min: 50, max: 1671 }
                };

                mock.onGet('api/filters/ranges').reply(200, mockResponse);

                const expectedActions = [
                    { type: REQUEST_RANGE_FILTERS },
                    {
                        type: RECEIVE_RANGE_FILTERS_SUCCESS,
                        response: mockResponse
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.fetchRangeFilters());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates RECEIVE_RANGE_FILTERS_FAILURE when the request failed', async () => {
                const message = 'Error message';
                mock.onGet('api/filters/ranges').reply(500, { message });

                const expectedActions = [
                    { type: REQUEST_RANGE_FILTERS },
                    {
                        type: RECEIVE_RANGE_FILTERS_FAILURE,
                        error: { message, status: 500 }
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.fetchRangeFilters());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('filtering', () => {
            const mockFilterResponse = {
                genres: [{ _id: 'Action', count: 1 }],
                tags: [{ _id: 'free book', count: 193 }]
            };

            const mockBooksResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };

            test('filters books after CHANGE_OWN_RATING_RANGE', async () => {
                const ratingValues = [1, 3];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_OWN_RATING_RANGE, ratingValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.updateOwnRatingRange(ratingValues)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_AMAZON_RATING_RANGE', async () => {
                const ratingValues = [1, 3];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_AMAZON_RATING_RANGE, ratingValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.updateAmazonRatingRange(ratingValues)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_GOODREADS_RATING_RANGE', async () => {
                const ratingValues = [1, 3];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_GOODREADS_RATING_RANGE, ratingValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.updateGoodreadsRatingRange(ratingValues)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_YEAR_RANGE', async () => {
                const rangeValues = [1920, 2006];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_YEAR_RANGE, values: rangeValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.updateYearRange(rangeValues));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_PAGE_RANGE', async () => {
                const rangeValues = [77, 455];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_PAGE_RANGE, values: rangeValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.updatePageRange(rangeValues));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_INTEREST_RANGE', async () => {
                const rangeValues = [2, 4];
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_INTEREST_RANGE, values: rangeValues },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.updateInterestRange(rangeValues)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });
    });
});
