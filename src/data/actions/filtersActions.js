import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    RECEIVE_FILTERS_FAILURE,
    TOGGLE_GENRE,
    TOGGLE_TAG,
    EXCLUDE_TAG,
    CHANGE_READ_STATUS
} from '../constants';
import { getCurrentFiltering, fetchBooks } from './booksActions';
import { resetPagination } from './paginationActions';
import { booksPage } from './pageActions';
import urlEncodeQueryParams from '../../utils/urlEncodeQueryParams';

export const requestFilters = () => ({
    type: REQUEST_FILTERS
});

export const receiveFiltersSuccess = response => ({
    type: RECEIVE_FILTERS_SUCCESS,
    response
});

export const receiveFiltersFailure = error => ({
    type: RECEIVE_FILTERS_FAILURE,
    error: createErrorMessage(error)
});

export const toggleGenre = filterName => ({
    type: TOGGLE_GENRE,
    filterName
});

export const toggleTag = filterName => ({
    type: TOGGLE_TAG,
    filterName
});

export const excludeTag = filterName => ({
    type: EXCLUDE_TAG,
    filterName
});

export const changeReadStatus = filterValue => ({
    type: CHANGE_READ_STATUS,
    filterValue
});

export const fetchFilters = () => (dispatch, getState) => {
    dispatch(requestFilters());

    const queryObj = getCurrentFiltering(getState());
    const query = urlEncodeQueryParams(queryObj);

    return axios.get(`/api/filters${query}`).then(
        response => dispatch(receiveFiltersSuccess(response.data)),
        error => dispatch(receiveFiltersFailure(handleError(error)))
    );
};

export const updateFiltering = () => dispatch => {
    dispatch(fetchFilters());
    return dispatch(fetchBooks());
};

export const filterBooks = () => dispatch => {
    dispatch(resetPagination());
    dispatch(booksPage());
    return dispatch(updateFiltering());
};

export const updateGenres = filterName => dispatch => {
    dispatch(toggleGenre(filterName));
    return dispatch(filterBooks());
};

export const updateTags = filterName => dispatch => {
    dispatch(toggleTag(filterName));
    return dispatch(filterBooks());
};

export const updateReadStatus = value => dispatch => {
    dispatch(changeReadStatus(value));
    return dispatch(filterBooks());
};

export const updateExcludedTags = filterName => dispatch => {
    dispatch(excludeTag(filterName));
    return dispatch(filterBooks());
};
