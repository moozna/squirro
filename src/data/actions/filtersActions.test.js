import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './filtersActions';
import {
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    RECEIVE_FILTERS_FAILURE,
    TOGGLE_GENRE,
    TOGGLE_TAG,
    EXCLUDE_TAG,
    CHANGE_READ_STATUS,
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RESET_PAGINATION,
    BOOKS
} from '../constants';

describe('filters actions', () => {
    describe('action creators', () => {
        test('creates an action to request filters', () => {
            const expectedAction = {
                type: REQUEST_FILTERS
            };

            const action = actionTypes.requestFilters();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful filters request', () => {
            const response = {
                genres: [],
                tags: []
            };
            const expectedAction = {
                type: RECEIVE_FILTERS_SUCCESS,
                response
            };

            const action = actionTypes.receiveFiltersSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed filters request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_FILTERS_FAILURE,
                error
            };

            const action = actionTypes.receiveFiltersFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to toggle genre', () => {
            const filterName = 'Adventure';
            const expectedAction = {
                type: TOGGLE_GENRE,
                filterName
            };

            const action = actionTypes.toggleGenre(filterName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to toggle tag', () => {
            const filterName = 'ebook';
            const expectedAction = {
                type: TOGGLE_TAG,
                filterName
            };

            const action = actionTypes.toggleTag(filterName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to exclude tag', () => {
            const filterName = 'free book';
            const expectedAction = {
                type: EXCLUDE_TAG,
                filterName
            };

            const action = actionTypes.excludeTag(filterName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to change read status', () => {
            const filterValue = 'read';
            const expectedAction = {
                type: CHANGE_READ_STATUS,
                filterValue
            };

            const action = actionTypes.changeReadStatus(filterValue);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('filters', () => {
            test('creates RECEIVE_FILTERS_SUCCESS when the request was successful', async () => {
                const mockResponse = {
                    genres: [{ _id: 'Action', count: 1 }],
                    tags: [{ _id: 'free book', count: 193 }]
                };

                mock.onGet(/\/api\/filters(\?.+)?/).reply(200, mockResponse);

                const expectedActions = [
                    { type: REQUEST_FILTERS },
                    { type: RECEIVE_FILTERS_SUCCESS, response: mockResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.fetchFilters());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('creates RECEIVE_FILTERS_FAILURE when the request failed', async () => {
                const message = 'Error message';
                mock.onGet(/\/api\/filters(\?.+)?/).reply(500, { message });

                const expectedActions = [
                    { type: REQUEST_FILTERS },
                    {
                        type: RECEIVE_FILTERS_FAILURE,
                        error: { message, status: 500 }
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.fetchFilters());
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('filtering', () => {
            const mockFilterResponse = {
                genres: [{ _id: 'Action', count: 1 }],
                tags: [{ _id: 'free book', count: 193 }]
            };

            const mockBooksResponse = {
                books: [
                    { _id: '1', title: 'title' },
                    { _id: '2', title: 'title2' }
                ],
                count: 0,
                hasNext: true
            };

            test('filters books after TOGGLE_GENRE', async () => {
                const filterName = 'Action';
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: TOGGLE_GENRE, filterName },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.updateGenres(filterName));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after TOGGLE_TAG', async () => {
                const filterName = 'read';
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: TOGGLE_TAG, filterName },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.updateTags(filterName));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after EXCLUDE_TAG', async () => {
                const filterName = 'read';
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: EXCLUDE_TAG, filterName },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.updateExcludedTags(filterName)
                );
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });

            test('filters books after CHANGE_READ_STATUS', async () => {
                const filterValue = 'read';
                mock.onGet(/\/api\/filters(\?.+)?/).reply(
                    200,
                    mockFilterResponse
                );
                mock.onGet(/\/api\/books(\?.+)?/).reply(200, mockBooksResponse);

                const expectedActions = [
                    { type: CHANGE_READ_STATUS, filterValue },
                    { type: RESET_PAGINATION },
                    { type: BOOKS, query: {} },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    {
                        type: RECEIVE_FILTERS_SUCCESS,
                        response: mockFilterResponse
                    },
                    { type: RECEIVE_BOOKS_SUCCESS, response: mockBooksResponse }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.updateReadStatus(filterValue));
                const actions = store.getActions();

                expect(actions).toEqual(expectedActions);
            });
        });
    });
});
