import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './shelvesActions';
import {
    REQUEST_SHELVES,
    RECEIVE_SHELVES_SUCCESS,
    RECEIVE_SHELVES_FAILURE,
    SELECT_SHELF,
    UNSELECT_SHELF,
    UPDATE_SHELF,
    UPDATE_SHELF_SUCCESS,
    UPDATE_SHELF_FAILURE,
    DELETE_SHELF,
    DELETE_SHELF_SUCCESS,
    DELETE_SHELF_FAILURE,
    REQUEST_RANGE_FILTERS,
    REQUEST_FILTERS,
    REQUEST_BOOKS,
    RESET_PAGINATION,
    BOOKS
} from '../constants';

describe('shelves actions', () => {
    describe('action creators', () => {
        test('creates an action to request shelves', () => {
            const expectedAction = {
                type: REQUEST_SHELVES
            };

            const action = actionTypes.requestShelves();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful shelves request', () => {
            const response = [];
            const expectedAction = {
                type: RECEIVE_SHELVES_SUCCESS,
                response
            };

            const action = actionTypes.receiveShelvesSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed shelves request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_SHELVES_FAILURE,
                error
            };

            const action = actionTypes.receiveShelvesFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to select shelf', () => {
            const mockName = 'Best Horror Novels';
            const expectedAction = {
                type: SELECT_SHELF,
                shelfName: mockName
            };

            const action = actionTypes.selectShelf(mockName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to unselect shelf', () => {
            const expectedAction = {
                type: UNSELECT_SHELF
            };

            const action = actionTypes.unSelectShelf();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to update shelf', () => {
            const expectedAction = {
                type: UPDATE_SHELF
            };

            const action = actionTypes.updateShelf();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful update', () => {
            const shelfName = 'Best Books';
            const newName = 'Best Horror Books';
            const expectedAction = {
                type: UPDATE_SHELF_SUCCESS,
                shelfName,
                newName
            };

            const action = actionTypes.updateShelfSuccess(shelfName, newName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed update', () => {
            const error = { message: 'error' };
            const expectedAction = {
                type: UPDATE_SHELF_FAILURE,
                error
            };

            const action = actionTypes.updateShelfFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to delete shelf', () => {
            const expectedAction = {
                type: DELETE_SHELF
            };

            const action = actionTypes.deleteShelf();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful delete', () => {
            const shelfName = 'Best Books';
            const expectedAction = {
                type: DELETE_SHELF_SUCCESS,
                shelfName
            };

            const action = actionTypes.deleteShelfSuccess(shelfName);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed delete', () => {
            const error = { message: 'error' };
            const expectedAction = {
                type: DELETE_SHELF_FAILURE,
                error
            };

            const action = actionTypes.deleteShelfFailure(error);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);
        const shelfName = 'Best Books';

        afterEach(() => {
            mock.reset();
        });

        test('creates RECEIVE_SHELVES_SUCCESS when the request was successful', async () => {
            const mockResponse = [
                { _id: 'Best Book Cover Art', count: 45 },
                { _id: 'Best Books', count: 40 },
                { _id: 'Best Horror Novels', count: 51 }
            ];

            mock.onGet('api/filters/shelves/stat').reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_SHELVES },
                { type: RECEIVE_SHELVES_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchShelves());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_SHELVES_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet('api/filters/shelves/stat').reply(500, { message });

            const expectedActions = [
                { type: REQUEST_SHELVES },
                {
                    type: RECEIVE_SHELVES_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchShelves());
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        describe('selectShelfByName', () => {
            test('filters books after SELECT_SHELF', async () => {
                const expectedActions = [
                    { type: SELECT_SHELF, shelfName },
                    { type: RESET_PAGINATION },
                    { type: REQUEST_RANGE_FILTERS },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.selectShelfByName(shelfName));
                const actions = store.getActions().slice(0, 5);

                expect(actions).toEqual(expectedActions);
            });
        });

        describe('removeShelfSelection', () => {
            test('filters books after UNSELECT_SHELF', async () => {
                const expectedActions = [
                    { type: UNSELECT_SHELF },
                    { type: RESET_PAGINATION },
                    { type: REQUEST_RANGE_FILTERS },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS },
                    { type: BOOKS, query: {} }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.removeShelfSelection());
                const actions = store.getActions().slice(0, 6);

                expect(actions).toEqual(expectedActions);
            });
        });

        test('creates UPDATE_SHELF_SUCCESS when the update was successful', async () => {
            const shelfData = { shelfName: 'Best Horror Books' };
            mock.onPut(`api/filters/shelves/${shelfName}`).reply(200);

            const expectedActions = [
                { type: UPDATE_SHELF },
                {
                    type: UPDATE_SHELF_SUCCESS,
                    shelfName,
                    newName: shelfData.shelfName
                }
            ];

            const store = mockStore();
            await store.dispatch(
                actionTypes.saveEditedShelf(shelfName, shelfData)
            );
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates UPDATE_SHELF_FAILURE when the update failed', async () => {
            const shelfData = { shelfName: 'Best Horror Books' };
            const message = 'Error message';
            mock.onPut(`api/filters/shelves/${shelfName}`).reply(500, {
                message
            });

            const expectedActions = [
                { type: UPDATE_SHELF },
                { type: UPDATE_SHELF_FAILURE, error: { message, status: 500 } }
            ];

            const store = mockStore();
            await store.dispatch(
                actionTypes.saveEditedShelf(shelfName, shelfData)
            );
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates DELETE_SHELF_SUCCESS when the delete was successful', async () => {
            mock.onDelete(`api/filters/shelves/${shelfName}`).reply(200);

            const expectedActions = [
                { type: DELETE_SHELF },
                { type: DELETE_SHELF_SUCCESS, shelfName }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.deleteSelectedShelf(shelfName));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates DELETE_SHELF_FAILURE when the delete failed', async () => {
            const message = 'Error message';
            mock.onDelete(`api/filters/shelves/${shelfName}`).reply(500, {
                message
            });

            const expectedActions = [
                { type: DELETE_SHELF },
                { type: DELETE_SHELF_FAILURE, error: { message, status: 500 } }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.deleteSelectedShelf(shelfName));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
