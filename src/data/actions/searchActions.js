import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    CHANGE_SEARCH_VALUE,
    CLEAR_SEARCH_VALUE,
    REQUEST_SUGGESTIONS,
    RECEIVE_SUGGESTIONS_SUCCESS,
    RECEIVE_SUGGESTIONS_FAILURE,
    CLEAR_SUGGESTIONS
} from '../constants';
import { getCurrentFiltering } from './booksActions';
import urlEncodeQueryParams from '../../utils/urlEncodeQueryParams';
import { booksPage, bookDetailsPage } from './pageActions';
import { filterBooks, updateFiltering } from './filtersActions';
import { resetPagination } from './paginationActions';
import { getBooks } from '../reducers/booksReducer';
import { getCurrentPage } from '../reducers/pageReducer';

export const changeSearchValue = value => ({
    type: CHANGE_SEARCH_VALUE,
    value
});

export const clearSearchValue = () => ({
    type: CLEAR_SEARCH_VALUE
});

export const requestSuggestions = () => ({
    type: REQUEST_SUGGESTIONS
});

export const receiveSuggestionsSuccess = response => ({
    type: RECEIVE_SUGGESTIONS_SUCCESS,
    response
});

export const receiveSuggestionsFailure = error => ({
    type: RECEIVE_SUGGESTIONS_FAILURE,
    error: createErrorMessage(error)
});

export const clearSuggestions = () => ({
    type: CLEAR_SUGGESTIONS
});

export const fetchSuggestions = () => (dispatch, getState) => {
    dispatch(requestSuggestions());

    const queryObj = getCurrentFiltering(getState());
    const query = urlEncodeQueryParams(queryObj);

    return axios.get(`/api/books/suggestions${query}`).then(
        response => dispatch(receiveSuggestionsSuccess(response.data)),
        error => dispatch(receiveSuggestionsFailure(handleError(error)))
    );
};

const shouldNavigateToSingleBook = books => books.length === 1;
const shouldNavigateToBooks = state => getCurrentPage(state) !== 'Books';

export const navigateToSearchResult = () => (dispatch, getState) => {
    const state = getState();
    const books = getBooks(state);

    if (shouldNavigateToSingleBook(books)) {
        return dispatch(bookDetailsPage(books[0]._id));
    }

    if (shouldNavigateToBooks(state)) {
        return dispatch(booksPage());
    }

    return Promise.resolve();
};

export const searchBooks = value => dispatch => {
    dispatch(changeSearchValue(value));
    dispatch(resetPagination());
    return dispatch(updateFiltering()).then(() =>
        dispatch(navigateToSearchResult())
    );
};

export const clearSearch = () => dispatch => {
    dispatch(clearSearchValue());
    return dispatch(filterBooks());
};
