import { CLEAR_ERROR_MESSAGE } from '../constants';

export const clearErrorMessage = id => ({
    type: CLEAR_ERROR_MESSAGE,
    id
});
