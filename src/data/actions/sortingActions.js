import { CHANGE_SORTFIELD, TOGGLE_SORTORDER } from '../constants';
import { fetchBooks } from './booksActions';
import { resetPagination } from './paginationActions';

export const changeSortfield = sortField => ({
    type: CHANGE_SORTFIELD,
    sortField
});

export const toggleSortOrder = () => ({
    type: TOGGLE_SORTORDER
});

export const updateSortField = sortField => dispatch => {
    dispatch(changeSortfield(sortField));
    dispatch(resetPagination());
    return dispatch(fetchBooks());
};

export const updateSortOrder = () => dispatch => {
    dispatch(toggleSortOrder());
    dispatch(resetPagination());
    return dispatch(fetchBooks());
};
