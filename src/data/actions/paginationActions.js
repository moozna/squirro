import {
    INCREASE_START_INDEX,
    SELECT_START_INDEX,
    SELECT_PAGE_NUMBER,
    RESET_PAGINATION,
    SELECT_LIMIT,
    RESET_LIMIT
} from '../constants';
import {
    getPageLimit,
    getPageSize,
    getCurrentPageNumber
} from '../reducers/paginationReducer';
import { fetchMoreBooks, fetchBooks } from './booksActions';

export const increaseStartIndex = limit => ({
    type: INCREASE_START_INDEX,
    limit
});

export const selectStartIndex = startIndex => ({
    type: SELECT_START_INDEX,
    startIndex
});

export const selectPageNumber = pageNumber => ({
    type: SELECT_PAGE_NUMBER,
    pageNumber
});

export const resetPagination = () => ({
    type: RESET_PAGINATION
});

export const selectLimit = limit => ({
    type: SELECT_LIMIT,
    limit
});

export const resetLimit = () => ({
    type: RESET_LIMIT
});

const calculateStartIndexForPage = (state, pageNumber) => {
    const pageSize = getPageSize(state);
    return (pageNumber - 1) * pageSize;
};

export const loadMoreBooks = () => (dispatch, getState) => {
    const limit = getPageLimit(getState());
    dispatch(increaseStartIndex(limit));
    return dispatch(fetchMoreBooks());
};

export const selectPage = pageNumber => (dispatch, getState) => {
    const newStartIndex = calculateStartIndexForPage(getState(), pageNumber);

    dispatch(selectStartIndex(newStartIndex));
    return dispatch(selectPageNumber(pageNumber));
};

export const loadPage = pageNumber => dispatch => {
    dispatch(selectPage(pageNumber));
    return dispatch(fetchBooks());
};

export const setLimitForRestoreScrolling = currentStartIndex => (
    dispatch,
    getState
) => {
    const state = getState();
    const limit = getPageLimit(state);
    const currentPageNumber = getCurrentPageNumber(state);
    const newStartIndex = calculateStartIndexForPage(state, currentPageNumber);
    const newLimit = currentStartIndex - newStartIndex + limit;

    dispatch(selectStartIndex(newStartIndex));
    return dispatch(selectLimit(newLimit));
};

export const resetStartIndexForPage = () => (dispatch, getState) => {
    const state = getState();
    const currentPageNumber = getCurrentPageNumber(state);

    const newStartIndex = calculateStartIndexForPage(state, currentPageNumber);
    return dispatch(selectStartIndex(newStartIndex));
};
