import * as actionTypes from './pageActions';
import {
    BOOKS,
    BOOK,
    EDIT,
    ADD,
    IMPORT,
    EDIT_IMPORT,
    SHELVES
} from '../constants';

describe('page actions', () => {
    describe('action creators', () => {
        test('creates an action for books page', () => {
            const mockQuery = { page: 3 };
            const expectedAction = {
                type: BOOKS,
                query: mockQuery
            };

            const action = actionTypes.booksPage(mockQuery);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for book details page', () => {
            const expectedAction = {
                type: BOOK,
                payload: {
                    id: '1'
                }
            };

            const action = actionTypes.bookDetailsPage('1');
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for edit book page', () => {
            const expectedAction = {
                type: EDIT,
                payload: {
                    id: '1'
                }
            };

            const action = actionTypes.editBookPage('1');
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for add book page', () => {
            const expectedAction = {
                type: ADD
            };

            const action = actionTypes.addBookPage();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for import book page', () => {
            const expectedAction = {
                type: IMPORT
            };

            const action = actionTypes.importBookPage();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for edit imported book page', () => {
            const expectedAction = {
                type: EDIT_IMPORT
            };

            const action = actionTypes.editImportPage();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action for shelves page', () => {
            const expectedAction = {
                type: SHELVES
            };

            const action = actionTypes.shelvesPage();
            expect(action).toEqual(expectedAction);
        });
    });
});
