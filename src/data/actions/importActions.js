import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    IMPORT_BOOK,
    IMPORT_BOOK_SUCCESS,
    IMPORT_BOOK_FAILURE,
    CLEAR_IMPORTED_DATA,
    UPDATE_DUPLICATE_KEYS,
    CREATE_INITIAL_BOOK
} from '../constants';
import {
    getImportedAmazonData,
    getImportedGoodreadsData
} from '../reducers/importReducer';

export const importBook = siteName => ({
    type: IMPORT_BOOK,
    siteName
});

export const importBookSuccess = (response, siteName) => ({
    type: IMPORT_BOOK_SUCCESS,
    response,
    siteName
});

export const importBookFailure = (error, siteName) => ({
    type: IMPORT_BOOK_FAILURE,
    error: createErrorMessage(error),
    siteName
});

export const clearImportedData = () => ({
    type: CLEAR_IMPORTED_DATA
});

export const updateDuplicateKeys = (response, otherData) => ({
    type: UPDATE_DUPLICATE_KEYS,
    response,
    otherData
});

export const createInitialBook = sitesData => ({
    type: CREATE_INITIAL_BOOK,
    sitesData
});

export const importAmazonBook = id => (dispatch, getState) => {
    const siteName = 'amazon';
    dispatch(importBook(siteName));

    return axios.get(`/api/books/import/amazon/${id}`).then(
        response => {
            const otherData = getImportedGoodreadsData(getState());
            dispatch(importBookSuccess(response.data, siteName));
            return dispatch(updateDuplicateKeys(response.data, otherData));
        },
        error => dispatch(importBookFailure(handleError(error), siteName))
    );
};

export const importGoodreadsBook = id => (dispatch, getState) => {
    const siteName = 'goodreads';
    dispatch(importBook(siteName));

    return axios.get(`/api/books/import/goodreads/${id}`).then(
        response => {
            const otherData = getImportedAmazonData(getState());
            dispatch(importBookSuccess(response.data, siteName));
            return dispatch(updateDuplicateKeys(response.data, otherData));
        },
        error => dispatch(importBookFailure(handleError(error), siteName))
    );
};
