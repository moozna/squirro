import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './selectedBookActions';
import {
    SELECT_BOOK,
    UNSELECT_BOOK,
    REQUEST_SELECTED_BOOK,
    RECEIVE_SELECTED_BOOK_SUCCESS,
    RECEIVE_SELECTED_BOOK_FAILURE,
    UPDATE_SELECTED_BOOK,
    UPDATE_SELECTED_BOOK_SUCCESS,
    UPDATE_SELECTED_BOOK_FAILURE,
    DELETE_SELECTED_BOOK,
    DELETE_SELECTED_BOOK_SUCCESS,
    DELETE_SELECTED_BOOK_FAILURE,
    REQUEST_FILTERS,
    REQUEST_RANGE_FILTERS,
    REQUEST_BOOKS,
    BOOK,
    BOOKS,
    SELECT_START_INDEX,
    SELECT_LIMIT,
    RESET_LIMIT
} from '../constants';

describe('selected book actions', () => {
    describe('action creators', () => {
        test('creates an action to select book', () => {
            const mockId = '2';
            const expectedAction = {
                type: SELECT_BOOK,
                id: mockId
            };

            const action = actionTypes.selectBook(mockId);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to unselect book', () => {
            const expectedAction = {
                type: UNSELECT_BOOK
            };

            const action = actionTypes.unSelectBook();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to request selected book', () => {
            const expectedAction = {
                type: REQUEST_SELECTED_BOOK
            };

            const action = actionTypes.requestSelectedBook();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful request', () => {
            const response = {
                _id: '1',
                asin: 'asin'
            };
            const expectedAction = {
                type: RECEIVE_SELECTED_BOOK_SUCCESS,
                response
            };

            const action = actionTypes.receiveSelectedBookSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed request', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: RECEIVE_SELECTED_BOOK_FAILURE,
                error
            };

            const action = actionTypes.receiveSelectedBookFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to update selected book', () => {
            const expectedAction = {
                type: UPDATE_SELECTED_BOOK
            };

            const action = actionTypes.updateSelectedBook();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful update', () => {
            const response = {
                _id: '1',
                asin: 'asin'
            };
            const expectedAction = {
                type: UPDATE_SELECTED_BOOK_SUCCESS,
                response
            };

            const action = actionTypes.updateSelectedBookSuccess(response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed update', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: UPDATE_SELECTED_BOOK_FAILURE,
                error
            };

            const action = actionTypes.updateSelectedBookFailure(error);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to delete selected book', () => {
            const expectedAction = {
                type: DELETE_SELECTED_BOOK
            };

            const action = actionTypes.deleteSelectedBook();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful delete', () => {
            const expectedAction = {
                type: DELETE_SELECTED_BOOK_SUCCESS
            };

            const action = actionTypes.deleteSelectedBookSuccess();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed delete', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: DELETE_SELECTED_BOOK_FAILURE,
                error
            };

            const action = actionTypes.deleteSelectedBookFailure(error);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);
        const bookId = '1';

        afterEach(() => {
            mock.reset();
        });

        test('creates RECEIVE_SELECTED_BOOK_SUCCESS when the request was successful', async () => {
            const mockResponse = {
                _id: '1',
                asin: 'asin'
            };
            mock.onGet(`api/books/${bookId}`).reply(200, mockResponse);

            const expectedActions = [
                { type: REQUEST_SELECTED_BOOK },
                { type: RECEIVE_SELECTED_BOOK_SUCCESS, response: mockResponse }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSelectedBook(bookId));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        test('creates RECEIVE_SELECTED_BOOK_FAILURE when the request failed', async () => {
            const message = 'Error message';
            mock.onGet(`api/books/${bookId}`).reply(500, { message });

            const expectedActions = [
                { type: REQUEST_SELECTED_BOOK },
                {
                    type: RECEIVE_SELECTED_BOOK_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.fetchSelectedBook(bookId));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        describe('when the update was successful', () => {
            const bookData = {
                _id: '1',
                asin: 'asin'
            };
            const mockResponse = bookData;

            test('creates UPDATE_SELECTED_BOOK_SUCCESS', async () => {
                mock.onPut(`api/books/${bookId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: UPDATE_SELECTED_BOOK },
                    {
                        type: UPDATE_SELECTED_BOOK_SUCCESS,
                        response: mockResponse
                    }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.saveEditedBook(bookId, bookData)
                );
                const actions = store.getActions().slice(0, 2);

                expect(actions).toEqual(expectedActions);
            });

            test('sets start index and limit to restore downloaded books before requesting filters and books', async () => {
                mock.onPut(`api/books/${bookId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: SELECT_START_INDEX, startIndex: 75 },
                    { type: SELECT_LIMIT, limit: 50 }
                ];
                const initialState = {
                    pagination: {
                        startIndex: 100,
                        currentPageNumber: 2,
                        limit: 25,
                        pageSize: 75
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(
                    actionTypes.saveEditedBook(bookId, bookData)
                );
                const actions = store.getActions().slice(2, 4);

                expect(actions).toEqual(expectedActions);
            });

            test('creates REQUEST_RANGE_FILTERS, REQUEST_FILTERS and REQUEST_BOOKS', async () => {
                mock.onPut(`api/books/${bookId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: REQUEST_RANGE_FILTERS },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.saveEditedBook(bookId, bookData)
                );
                const actions = store.getActions().slice(4, 7);

                expect(actions).toEqual(expectedActions);
            });

            test('resets start index and limit after requesting filters and books', async () => {
                mock.onPut(`api/books/${bookId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: RESET_LIMIT },
                    { type: SELECT_START_INDEX, startIndex: 100 }
                ];
                const initialState = {
                    pagination: {
                        startIndex: 100,
                        currentPageNumber: 2,
                        limit: 25,
                        pageSize: 75
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(
                    actionTypes.saveEditedBook(bookId, bookData)
                );
                const actions = store.getActions().slice(7, 9);

                expect(actions).toEqual(expectedActions);
            });

            test('navigates to BOOK', async () => {
                mock.onPut(`api/books/${bookId}`).reply(200, mockResponse);

                const expectedActions = [
                    { type: BOOK, payload: { id: bookId } }
                ];

                const store = mockStore();
                await store.dispatch(
                    actionTypes.saveEditedBook(bookId, bookData)
                );
                const actions = store.getActions().slice(9);

                expect(actions).toEqual(expectedActions);
            });
        });

        test('creates UPDATE_SELECTED_BOOK_FAILURE when the update failed', async () => {
            const bookData = {
                _id: '1',
                asin: 'asin'
            };
            const message = 'Error message';
            mock.onPut(`api/books/${bookId}`).reply(500, { message });

            const expectedActions = [
                { type: UPDATE_SELECTED_BOOK },
                {
                    type: UPDATE_SELECTED_BOOK_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.saveEditedBook(bookId, bookData));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });

        describe('when the delete was successful', () => {
            test('creates DELETE_SELECTED_BOOK_SUCCESS', async () => {
                mock.onDelete(`api/books/${bookId}`).reply(200);

                const expectedActions = [
                    { type: DELETE_SELECTED_BOOK },
                    { type: DELETE_SELECTED_BOOK_SUCCESS }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.deleteBook(bookId));
                const actions = store.getActions().slice(0, 2);

                expect(actions).toEqual(expectedActions);
            });

            test('resets start index to the start of the page before requesting filters and books', async () => {
                mock.onDelete(`api/books/${bookId}`).reply(200);

                const expectedActions = [
                    { type: SELECT_START_INDEX, startIndex: 75 }
                ];
                const initialState = {
                    pagination: {
                        startIndex: 100,
                        currentPageNumber: 2,
                        limit: 25,
                        pageSize: 75
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.deleteBook(bookId));
                const actions = store.getActions().slice(2, 3);

                expect(actions).toEqual(expectedActions);
            });

            test('creates REQUEST_RANGE_FILTERS, REQUEST_FILTERS and REQUEST_BOOKS', async () => {
                mock.onDelete(`api/books/${bookId}`).reply(200);

                const expectedActions = [
                    { type: REQUEST_RANGE_FILTERS },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.deleteBook(bookId));
                const actions = store.getActions().slice(3, 6);

                expect(actions).toEqual(expectedActions);
            });

            test('navigates to BOOKS', async () => {
                mock.onDelete(`api/books/${bookId}`).reply(200);

                const expectedActions = [{ type: BOOKS, query: { page: 3 } }];
                const initialState = {
                    pagination: {
                        currentPageNumber: 3
                    }
                };

                const store = mockStore(initialState);
                await store.dispatch(actionTypes.deleteBook(bookId));
                const actions = store.getActions().slice(6);

                expect(actions).toEqual(expectedActions);
            });
        });

        test('creates DELETE_SELECTED_BOOK_FAILURE when the delete failed', async () => {
            const message = 'Error message';
            mock.onDelete(`api/books/${bookId}`).reply(500, { message });

            const expectedActions = [
                { type: DELETE_SELECTED_BOOK },
                {
                    type: DELETE_SELECTED_BOOK_FAILURE,
                    error: { message, status: 500 }
                }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.deleteBook(bookId));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
