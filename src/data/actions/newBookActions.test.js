import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actionTypes from './newBookActions';
import {
    ADD_NEW_BOOK,
    ADD_NEW_BOOK_SUCCESS,
    ADD_NEW_BOOK_FAILURE,
    REQUEST_FILTERS,
    REQUEST_RANGE_FILTERS,
    REQUEST_BOOKS,
    BOOK,
    RESET_PAGINATION
} from '../constants';

describe('new book actions', () => {
    describe('action creators', () => {
        test('creates an action to add new book', () => {
            const expectedAction = {
                type: ADD_NEW_BOOK
            };

            const action = actionTypes.addNewBook();
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to successful addition of a new book', () => {
            const bookId = '1';
            const response = {
                _id: bookId,
                asin: 'asin'
            };
            const expectedAction = {
                type: ADD_NEW_BOOK_SUCCESS,
                id: bookId,
                response
            };

            const action = actionTypes.addNewBookSuccess(bookId, response);
            expect(action).toEqual(expectedAction);
        });

        test('creates an action to failed addition of a new book', () => {
            const error = {
                message: 'error'
            };
            const expectedAction = {
                type: ADD_NEW_BOOK_FAILURE,
                error
            };

            const action = actionTypes.addNewBookFailure(error);
            expect(action).toEqual(expectedAction);
        });
    });

    describe('async actions', () => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const mock = new MockAdapter(axios);

        afterEach(() => {
            mock.reset();
        });

        describe('when the addition was successful', () => {
            const bookId = '1';
            const bookData = {
                _id: bookId,
                asin: 'asin'
            };
            const mockResponse = bookData;

            test('creates ADD_NEW_BOOK_SUCCESS', async () => {
                mock.onPost('api/books').reply(200, mockResponse);

                const expectedActions = [
                    { type: ADD_NEW_BOOK },
                    {
                        type: ADD_NEW_BOOK_SUCCESS,
                        id: bookId,
                        response: mockResponse
                    }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.saveAddedBook(bookData));
                const actions = store.getActions().slice(0, 2);

                expect(actions).toEqual(expectedActions);
            });

            test('creates REQUEST_RANGE_FILTERS, REQUEST_FILTERS and REQUEST_BOOKS', async () => {
                mock.onPost('api/books').reply(200, mockResponse);

                const expectedActions = [
                    { type: RESET_PAGINATION },
                    { type: REQUEST_RANGE_FILTERS },
                    { type: REQUEST_FILTERS },
                    { type: REQUEST_BOOKS }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.saveAddedBook(bookData));
                const actions = store.getActions().slice(2, 6);

                expect(actions).toEqual(expectedActions);
            });

            test('navigates to BOOK', async () => {
                mock.onPost('api/books').reply(200, mockResponse);

                const expectedActions = [
                    { type: BOOK, payload: { id: bookId } }
                ];

                const store = mockStore();
                await store.dispatch(actionTypes.saveAddedBook(bookData));
                const actions = store.getActions().slice(6);

                expect(actions).toEqual(expectedActions);
            });
        });

        test('creates ADD_NEW_BOOK_FAILURE when the addition is failed', async () => {
            const bookData = {
                _id: '1',
                asin: 'asin'
            };
            const message = 'Error message';
            mock.onPost('api/books').reply(500, { message });

            const expectedActions = [
                { type: ADD_NEW_BOOK },
                { type: ADD_NEW_BOOK_FAILURE, error: { message, status: 500 } }
            ];

            const store = mockStore();
            await store.dispatch(actionTypes.saveAddedBook(bookData));
            const actions = store.getActions();

            expect(actions).toEqual(expectedActions);
        });
    });
});
