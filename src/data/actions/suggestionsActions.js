import axios from 'axios';
import { createErrorMessage, handleError } from '../api/handleError';
import {
    REQUEST_SUGGESTION_LISTS,
    RECEIVE_SUGGESTION_LISTS_SUCCESS,
    RECEIVE_SUGGESTION_LISTS_FAILURE
} from '../constants';

export const requestSuggestionLists = () => ({
    type: REQUEST_SUGGESTION_LISTS
});

export const receiveSuggestionListsSuccess = response => ({
    type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
    response
});

export const receiveSuggestionListsFailure = error => ({
    type: RECEIVE_SUGGESTION_LISTS_FAILURE,
    error: createErrorMessage(error)
});

const getGenres = () => axios.get('/api/filters/genres');
const getTags = () => axios.get('/api/filters/tags');
const getAuthors = () => axios.get('/api/filters/authors');
const getSeries = () => axios.get('/api/filters/series');
const getShelves = () => axios.get('/api/filters/shelves');

export const fetchSuggestionLists = () => dispatch => {
    dispatch(requestSuggestionLists());

    return axios
        .all([getGenres(), getTags(), getAuthors(), getSeries(), getShelves()])
        .then(
            response => {
                const [
                    { data: genres },
                    { data: tags },
                    { data: authors },
                    { data: series },
                    { data: shelves }
                ] = response;
                return dispatch(
                    receiveSuggestionListsSuccess({
                        genres,
                        tags,
                        authors,
                        series,
                        shelves
                    })
                );
            },
            error => dispatch(receiveSuggestionListsFailure(handleError(error)))
        );
};
