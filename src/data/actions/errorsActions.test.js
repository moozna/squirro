import * as actionTypes from './errorsActions';
import { CLEAR_ERROR_MESSAGE } from '../constants';

describe('errors actions', () => {
    describe('action creators', () => {
        test('creates an action for clearing error message', () => {
            const mockId = 'IMPORT_BOOK_FAILURE';
            const expectedAction = {
                type: CLEAR_ERROR_MESSAGE,
                id: mockId
            };

            const action = actionTypes.clearErrorMessage(mockId);
            expect(action).toEqual(expectedAction);
        });
    });
});
