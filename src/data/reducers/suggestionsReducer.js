import { combineReducers } from 'redux';
import {
    REQUEST_SUGGESTION_LISTS,
    RECEIVE_SUGGESTION_LISTS_SUCCESS,
    RECEIVE_SUGGESTION_LISTS_FAILURE
} from '../constants';
import get from '../../utils/get';

const suggestionLists = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_SUGGESTION_LISTS:
        case RECEIVE_SUGGESTION_LISTS_FAILURE:
            return {};
        case RECEIVE_SUGGESTION_LISTS_SUCCESS:
            return action.response;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_SUGGESTION_LISTS:
            return true;
        case RECEIVE_SUGGESTION_LISTS_SUCCESS:
        case RECEIVE_SUGGESTION_LISTS_FAILURE:
            return false;
        default:
            return state;
    }
};

const suggestions = combineReducers({
    suggestionLists,
    isFetching
});

export default suggestions;

export const getSuggestions = state =>
    get(state, 'suggestions.suggestionLists', {});
