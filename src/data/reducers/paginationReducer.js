import { combineReducers } from 'redux';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INCREASE_START_INDEX,
    SELECT_START_INDEX,
    SELECT_PAGE_NUMBER,
    RESET_PAGINATION,
    SELECT_LIMIT,
    RESET_LIMIT
} from '../constants';
import get from '../../utils/get';

const startIndex = (state = 0, action) => {
    switch (action.type) {
        case INCREASE_START_INDEX:
            return state + action.limit;
        case SELECT_START_INDEX:
            return action.startIndex;
        case RESET_PAGINATION:
            return 0;
        default:
            return state;
    }
};

const currentPageNumber = (state = 1, action) => {
    switch (action.type) {
        case SELECT_PAGE_NUMBER:
            return action.pageNumber;
        case RESET_PAGINATION:
            return 1;
        default:
            return state;
    }
};

const limit = (state = 50, action) => {
    switch (action.type) {
        case SELECT_LIMIT:
            return action.limit;
        case RESET_LIMIT:
            return 50;
        default:
            return state;
    }
};

const pageSize = (state = 150, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

const hasMore = (state = false, action) => {
    switch (action.type) {
        case REQUEST_BOOKS:
        case REQUEST_MORE_BOOKS:
        case RECEIVE_BOOKS_FAILURE:
        case RECEIVE_MORE_BOOKS_FAILURE:
            return false;
        case RECEIVE_BOOKS_SUCCESS:
        case RECEIVE_MORE_BOOKS_SUCCESS:
            return action.response.hasNext;
        default:
            return state;
    }
};

const pagination = combineReducers({
    startIndex,
    currentPageNumber,
    limit,
    pageSize,
    hasMore
});

export default pagination;

export const getStartIndex = state => get(state, 'pagination.startIndex');
export const getCurrentPageNumber = state =>
    get(state, 'pagination.currentPageNumber');
export const getPageLimit = state => get(state, 'pagination.limit', 0);
export const getPageSize = state => get(state, 'pagination.pageSize', 0);
export const getHasMore = state => get(state, 'pagination.hasMore', false);

export const getShouldStartNextPage = state => {
    const startIndex = getStartIndex(state);
    const limit = getPageLimit(state);
    const currentPageNumber = getCurrentPageNumber(state);
    const pageSize = getPageSize(state);

    const nextStartIndex = startIndex + limit;
    const maxStartIndexForPage = currentPageNumber * pageSize;
    return nextStartIndex >= maxStartIndexForPage;
};

export const getHasMoreForPage = state =>
    getHasMore(state) && !getShouldStartNextPage(state);
