import reducer, * as selectors from './suggestionsReducer';
import {
    REQUEST_SUGGESTION_LISTS,
    RECEIVE_SUGGESTION_LISTS_SUCCESS,
    RECEIVE_SUGGESTION_LISTS_FAILURE
} from '../constants';

const initialState = {
    suggestionLists: {},
    isFetching: false
};

const testNormalizedLists = {
    genres: [{ name: 'Action' }, { name: 'Fantasy' }],
    tags: [{ name: 'ebook' }, { name: 'wishlist' }],
    authors: [{ name: 'Cassandra Clare' }, { name: 'Kristin Cashore' }],
    series: [{ name: 'Graceling Realm' }, { name: 'Some Quiet Place' }]
};

describe('suggestions reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('REQUEST_SUGGESTION_LISTS', () => {
        test('starts requesting suggestion lists', () => {
            const currentState = {};

            const action = {
                type: REQUEST_SUGGESTION_LISTS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SUGGESTION_LISTS_SUCCESS', () => {
        const mockResponse = {
            authors: [{ name: 'Cassandra Clare' }, { name: 'Kristin Cashore' }],
            genres: [{ name: 'Action' }, { name: 'Fantasy' }],
            tags: [{ name: 'ebook' }, { name: 'wishlist' }],
            series: [{ name: 'Graceling Realm' }, { name: 'Some Quiet Place' }]
        };

        test('stops requesting suggestion lists on success', () => {
            const currentState = {};

            const action = {
                type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                suggestionLists: testNormalizedLists,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new suggestion lists on success', () => {
            const currentState = {
                suggestionLists: {},
                isFetching: false
            };

            const action = {
                type: RECEIVE_SUGGESTION_LISTS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                suggestionLists: testNormalizedLists,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SUGGESTION_LISTS_FAILURE', () => {
        test('stops requesting suggestion lists on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_SUGGESTION_LISTS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears suggestion lists on failure', () => {
            const currentState = {
                suggestionLists: testNormalizedLists,
                isFetching: true
            };

            const action = {
                type: RECEIVE_SUGGESTION_LISTS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the suggestions from state', () => {
            const currentState = {
                suggestions: {
                    suggestionLists: testNormalizedLists
                }
            };

            const expected = {
                genres: [{ name: 'Action' }, { name: 'Fantasy' }],
                tags: [{ name: 'ebook' }, { name: 'wishlist' }],
                authors: [
                    { name: 'Cassandra Clare' },
                    { name: 'Kristin Cashore' }
                ],
                series: [
                    { name: 'Graceling Realm' },
                    { name: 'Some Quiet Place' }
                ]
            };

            expect(selectors.getSuggestions(currentState)).toEqual(expected);
        });
    });
});
