import reducer, * as selectors from './filtersReducer';
import {
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    RECEIVE_FILTERS_FAILURE,
    TOGGLE_GENRE,
    TOGGLE_TAG,
    EXCLUDE_TAG,
    CHANGE_READ_STATUS
} from '../constants';

const initialState = {
    genres: {},
    tags: {},
    readStatus: '',
    isFetching: false
};

const testNormalizedFilters = {
    genres: {
        Action: {
            name: 'Action',
            checked: false,
            excluded: false
        },
        Fantasy: {
            name: 'Fantasy',
            checked: false,
            excluded: false
        }
    },
    tags: {
        ebook: {
            name: 'ebook',
            checked: false,
            excluded: false
        },
        wishlist: {
            name: 'wishlist',
            checked: false,
            excluded: false
        }
    }
};

describe('filters reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('REQUEST_FILTERS', () => {
        test('starts requesting filters', () => {
            const currentState = {};

            const action = {
                type: REQUEST_FILTERS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_FILTERS_SUCCESS', () => {
        const mockResponse = {
            genres: [
                { _id: 'Action', count: 1 },
                { _id: 'Fantasy', count: 30 }
            ],
            tags: [{ _id: 'ebook', count: 193 }, { _id: 'wishlist', count: 54 }]
        };

        test('stops requesting filters on success', () => {
            const currentState = {};

            const action = {
                type: RECEIVE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                ...testNormalizedFilters,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new filters on success', () => {
            const currentState = {
                genres: {},
                tags: {},
                isFetching: false
            };

            const action = {
                type: RECEIVE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                ...testNormalizedFilters,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('previously selected state is preserved on filters', () => {
            const currentFilters = {
                genres: {
                    Action: {
                        name: 'Action',
                        checked: true,
                        excluded: false
                    },
                    Fantasy: {
                        name: 'Fantasy',
                        checked: false,
                        excluded: true
                    }
                },
                tags: {
                    ebook: {
                        name: 'ebook',
                        checked: false,
                        excluded: false
                    },
                    wishlist: {
                        name: 'wishlist',
                        checked: true,
                        excluded: true
                    }
                }
            };

            const currentState = {
                ...currentFilters
            };

            const action = {
                type: RECEIVE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                ...currentFilters
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('excluded tags are preserved', () => {
            const currentFilters = {
                genres: {
                    Action: {
                        name: 'Action',
                        checked: true,
                        excluded: false
                    },
                    Fantasy: {
                        name: 'Fantasy',
                        checked: false,
                        excluded: true
                    }
                },
                tags: {
                    ebook: {
                        name: 'ebook',
                        checked: false,
                        excluded: true
                    },
                    wishlist: {
                        name: 'wishlist',
                        checked: true,
                        excluded: false
                    }
                }
            };

            const currentState = {
                ...currentFilters
            };

            const action = {
                type: RECEIVE_FILTERS_SUCCESS,
                response: { genres: [], tags: [] }
            };

            const expectedState = {
                ...initialState,
                genres: {},
                tags: {
                    ebook: {
                        name: 'ebook',
                        checked: false,
                        excluded: true
                    }
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_FILTERS_FAILURE', () => {
        test('stops requesting filters on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_FILTERS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears filters on failure', () => {
            const currentState = {
                ...testNormalizedFilters,
                isFetching: true
            };

            const action = {
                type: RECEIVE_FILTERS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('TOGGLE_GENRE', () => {
        test('toggle checked property on the given genre', () => {
            const currentState = {
                ...testNormalizedFilters
            };

            const action = {
                type: TOGGLE_GENRE,
                filterName: 'Action'
            };

            const expectedState = {
                ...initialState,
                ...testNormalizedFilters,
                genres: {
                    ...testNormalizedFilters.genres,
                    Action: {
                        name: 'Action',
                        checked: true,
                        excluded: false
                    }
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('TOGGLE_TAG', () => {
        test('toggle checked property on the given tag', () => {
            const currentState = {
                ...testNormalizedFilters
            };

            const action = {
                type: TOGGLE_TAG,
                filterName: 'wishlist'
            };

            const expectedState = {
                ...initialState,
                ...testNormalizedFilters,
                tags: {
                    ...testNormalizedFilters.tags,
                    wishlist: {
                        name: 'wishlist',
                        checked: true,
                        excluded: false
                    }
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('EXCLUDE_TAG', () => {
        test('toggle excluded property on the given tag', () => {
            const currentState = {
                ...testNormalizedFilters
            };

            const action = {
                type: EXCLUDE_TAG,
                filterName: 'wishlist'
            };

            const expectedState = {
                ...initialState,
                ...testNormalizedFilters,
                tags: {
                    ...testNormalizedFilters.tags,
                    wishlist: {
                        name: 'wishlist',
                        checked: false,
                        excluded: true
                    }
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CHANGE_READ_STATUS', () => {
        test('sets the given value as readStatus', () => {
            const filterValue = 'read';
            const currentState = {};

            const action = {
                type: CHANGE_READ_STATUS,
                filterValue
            };

            const expectedState = {
                ...initialState,
                readStatus: filterValue
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        const testGenres = {
            Action: {
                name: 'Action',
                checked: false,
                excluded: true
            },
            Fantasy: {
                name: 'Fantasy',
                checked: true,
                excluded: false
            },
            Thriller: {
                name: 'Thriller',
                checked: true,
                excluded: false
            }
        };

        const testTags = {
            ebook: {
                name: 'ebook',
                checked: true,
                excluded: false
            },
            wishlist: {
                name: 'wishlist',
                checked: false,
                excluded: true
            },
            read: {
                name: 'read',
                checked: true,
                excluded: false
            }
        };

        describe('getFilterNames()', () => {
            test('returns the name of all filters', () => {
                const testData = [
                    { name: 'Action', checked: false, excluded: true },
                    { name: 'Fantasy', checked: true, excluded: false },
                    { name: 'Thriller', checked: true, excluded: true }
                ];

                const expected = ['Action', 'Fantasy', 'Thriller'];

                expect(selectors.getFilterNames(testData)).toEqual(expected);
            });
        });

        describe('getAllFilterOptions()', () => {
            test('returns all options from the selected filter', () => {
                const currentState = {
                    filters: {
                        genres: testGenres
                    }
                };

                const expected = [
                    { name: 'Action', checked: false, excluded: true },
                    { name: 'Fantasy', checked: true, excluded: false },
                    { name: 'Thriller', checked: true, excluded: false }
                ];

                expect(
                    selectors.getAllFilterOptions(currentState, 'genres')
                ).toEqual(expected);
            });

            test('returns an empty array when the specified filter is missing', () => {
                const currentState = {
                    filters: {}
                };

                const expected = [];

                expect(
                    selectors.getAllFilterOptions(currentState, 'some filter')
                ).toEqual(expected);
            });
        });

        describe('getActiveFilters()', () => {
            test('returns the name property of all checked but not excluded filters', () => {
                const testData = [
                    { name: 'Action', checked: false, excluded: true },
                    { name: 'Crime', checked: true, excluded: true },
                    { name: 'Fantasy', checked: true, excluded: false },
                    { name: 'Thriller', checked: true, excluded: false }
                ];

                const expected = ['Fantasy', 'Thriller'];

                expect(selectors.getActiveFilters(testData)).toEqual(expected);
            });

            test('returns an empty array when all filters are unchecked', () => {
                const testData = [
                    { name: 'Action', checked: false, excluded: true },
                    { name: 'Fantasy', checked: false, excluded: false },
                    { name: 'Thriller', checked: false, excluded: true }
                ];

                const expected = [];

                expect(selectors.getActiveFilters(testData)).toEqual(expected);
            });

            test('returns an empty array when filterData is empty', () => {
                const testData = [];
                const expected = [];

                expect(selectors.getActiveFilters(testData)).toEqual(expected);
            });
        });

        describe('getExcludedFilters()', () => {
            test('returns the name property of all excluded filter', () => {
                const testData = [
                    { name: 'Action', checked: false, excluded: true },
                    { name: 'Fantasy', checked: true, excluded: false },
                    { name: 'Thriller', checked: true, excluded: true }
                ];

                const expected = ['Action', 'Thriller'];

                expect(selectors.getExcludedFilters(testData)).toEqual(
                    expected
                );
            });

            test('returns an empty array when none of the filters is excluded', () => {
                const testData = [
                    { name: 'Action', checked: false, excluded: false },
                    { name: 'Fantasy', checked: false, excluded: false },
                    { name: 'Thriller', checked: false, excluded: false }
                ];

                const expected = [];

                expect(selectors.getExcludedFilters(testData)).toEqual(
                    expected
                );
            });

            test('returns an empty array when filterData is empty', () => {
                const testData = [];
                const expected = [];

                expect(selectors.getExcludedFilters(testData)).toEqual(
                    expected
                );
            });
        });

        test('selects the genres from state', () => {
            const currentState = {
                filters: {
                    genres: testGenres
                }
            };

            const expected = [
                { name: 'Action', checked: false, excluded: true },
                { name: 'Fantasy', checked: true, excluded: false },
                { name: 'Thriller', checked: true, excluded: false }
            ];

            expect(selectors.getGenres(currentState)).toEqual(expected);
        });

        test('selects the tags from state', () => {
            const currentState = {
                filters: {
                    tags: testTags
                }
            };

            const expected = [
                { name: 'ebook', checked: true, excluded: false },
                { name: 'wishlist', checked: false, excluded: true },
                { name: 'read', checked: true, excluded: false }
            ];

            expect(selectors.getTags(currentState)).toEqual(expected);
        });

        test('selects the active genres from state', () => {
            const currentState = {
                filters: {
                    genres: testGenres
                }
            };

            const expected = ['Fantasy', 'Thriller'];

            expect(selectors.getActiveGenres(currentState)).toEqual(expected);
        });

        test('selects the active tags from state', () => {
            const currentState = {
                filters: {
                    tags: testTags
                }
            };

            const expected = ['ebook', 'read'];

            expect(selectors.getActiveTags(currentState)).toEqual(expected);
        });

        test('selects the read status from state', () => {
            const readStatus = 'read';
            const currentState = {
                filters: {
                    readStatus
                }
            };

            expect(selectors.getReadStatus(currentState)).toBe(readStatus);
        });
    });
});
