import { CLEAR_ERROR_MESSAGE } from '../constants';

const errors = (state = [], action) => {
    const { type, id, error } = action;

    if (type === CLEAR_ERROR_MESSAGE) {
        return state.filter(item => item.id !== id);
    }

    if (error) {
        return [
            ...state,
            {
                id: type,
                message: error.message,
                title: error.title
            }
        ];
    }

    return state;
};

export default errors;

export const getErrors = state => state.errors;
