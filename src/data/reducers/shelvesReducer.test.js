import reducer, * as selectors from './shelvesReducer';
import {
    REQUEST_SHELVES,
    RECEIVE_SHELVES_SUCCESS,
    RECEIVE_SHELVES_FAILURE,
    SELECT_SHELF,
    UNSELECT_SHELF,
    UPDATE_SHELF_SUCCESS,
    DELETE_SHELF_SUCCESS
} from '../constants';

const initialState = {
    data: [],
    selectedShelf: null,
    isFetching: false
};

describe('shelves reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('REQUEST_SHELVES', () => {
        test('starts requesting shelves', () => {
            const currentState = {};

            const action = {
                type: REQUEST_SHELVES
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SHELVES_SUCCESS', () => {
        test('stops requesting shelves on success', () => {
            const currentState = {};
            const mockResponse = [
                { _id: 'Best Book Cover Art', count: 45 },
                { _id: 'Best Books', count: 40 },
                { _id: 'Best Horror Novels', count: 51 }
            ];

            const action = {
                type: RECEIVE_SHELVES_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                data: mockResponse,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new shelves on success', () => {
            const currentState = {
                data: [
                    { _id: 'Best Fantasy', count: 11 },
                    { _id: 'Best Thrillers', count: 88 }
                ],
                isFetching: false
            };

            const mockResponse = [
                { _id: 'Best Book Cover Art', count: 45 },
                { _id: 'Best Books', count: 40 },
                { _id: 'Best Horror Novels', count: 51 }
            ];

            const action = {
                type: RECEIVE_SHELVES_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                data: mockResponse,
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SHELVES_FAILURE', () => {
        test('stops requesting shelves on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_SHELVES_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears shelves on failure', () => {
            const currentState = {
                data: [
                    { _id: 'Best Fantasy', count: 11 },
                    { _id: 'Best Thrillers', count: 88 }
                ],
                isFetching: true
            };

            const action = {
                type: RECEIVE_SHELVES_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('SELECT_SHELF', () => {
        test('sets the given name as selected shelf', () => {
            const currentState = {
                selectedShelf: 'Best Fantasy'
            };

            const action = {
                type: SELECT_SHELF,
                shelfName: 'Best Book Cover Art'
            };

            const expectedState = {
                ...initialState,
                selectedShelf: 'Best Book Cover Art'
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UNSELECT_SHELF', () => {
        test('resets the selected shelfname to null', () => {
            const currentState = {
                selectedShelf: 'Best Fantasy'
            };

            const action = {
                type: UNSELECT_SHELF
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UPDATE_SHELF_SUCCESS', () => {
        test('returns updated shelves on success', () => {
            const currentState = {
                data: [
                    { _id: 'Best Fantasy', count: 11 },
                    { _id: 'Best Thrillers', count: 88 }
                ]
            };

            const action = {
                type: UPDATE_SHELF_SUCCESS,
                shelfName: 'Best Fantasy',
                newName: 'Best Fantasy Books'
            };

            const expectedState = {
                ...initialState,
                data: [
                    { _id: 'Best Fantasy Books', count: 11 },
                    { _id: 'Best Thrillers', count: 88 }
                ]
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('updates the selected shelfname when it is the updated book', () => {
            const currentState = {
                selectedShelf: 'Best Fantasy'
            };

            const action = {
                type: UPDATE_SHELF_SUCCESS,
                shelfName: 'Best Fantasy',
                newName: 'Best Fantasy Books'
            };

            const expectedState = {
                ...initialState,
                selectedShelf: 'Best Fantasy Books'
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('DELETE_SHELF_SUCCESS', () => {
        test('deletes the specified shelf', () => {
            const currentState = {
                data: [
                    { _id: 'Best Fantasy', count: 11 },
                    { _id: 'Best Thrillers', count: 88 }
                ]
            };

            const action = {
                type: DELETE_SHELF_SUCCESS,
                shelfName: 'Best Fantasy'
            };

            const expectedState = {
                ...initialState,
                data: [{ _id: 'Best Thrillers', count: 88 }]
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('resets the selected shelfname to null when it is the deleted book', () => {
            const currentState = {
                selectedShelf: 'Best Fantasy'
            };

            const action = {
                type: DELETE_SHELF_SUCCESS,
                shelfName: 'Best Fantasy'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the shelves data from state', () => {
            const shelvesData = [
                { _id: 'Best Book Cover Art', count: 45 },
                { _id: 'Best Books', count: 40 },
                { _id: 'Best Horror Novels', count: 51 }
            ];
            const currentState = {
                shelves: {
                    data: shelvesData
                }
            };

            expect(selectors.getShelves(currentState)).toEqual(shelvesData);
        });

        test('selects the selected shelfname from state', () => {
            const shelfName = 'Best Horror Novels';
            const currentState = {
                shelves: {
                    selectedShelf: shelfName
                }
            };

            expect(selectors.getSelectedShelf(currentState)).toBe(shelfName);
        });
    });
});
