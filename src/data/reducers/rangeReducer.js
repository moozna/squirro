import { combineReducers } from 'redux';
import {
    CHANGE_OWN_RATING_RANGE,
    CHANGE_AMAZON_RATING_RANGE,
    CHANGE_GOODREADS_RATING_RANGE,
    REQUEST_RANGE_FILTERS,
    RECEIVE_RANGE_FILTERS_SUCCESS,
    RECEIVE_RANGE_FILTERS_FAILURE,
    CHANGE_YEAR_RANGE,
    CHANGE_PAGE_RANGE,
    CHANGE_INTEREST_RANGE
} from '../constants';
import get from '../../utils/get';

const ensureValuesInRange = (values = [], newValues) => {
    const { min, max } = newValues;
    const newSelectedValues = values.map((value, index) => {
        if (index === 0) {
            return Math.max(value, min);
        }
        return Math.min(value, max);
    });
    const maximumRange = [min, max];
    const isNotMaximumRange = newSelectedValues.some(
        (value, index) => value !== maximumRange[index]
    );

    return isNotMaximumRange ? newSelectedValues : [];
};

const ownRating = (state = [], action) => {
    switch (action.type) {
        case CHANGE_OWN_RATING_RANGE:
            return action.ratingValues;
        default:
            return state;
    }
};

const amazonRating = (state = [], action) => {
    switch (action.type) {
        case CHANGE_AMAZON_RATING_RANGE:
            return action.ratingValues;
        default:
            return state;
    }
};

const goodreadsRating = (state = [], action) => {
    switch (action.type) {
        case CHANGE_GOODREADS_RATING_RANGE:
            return action.ratingValues;
        default:
            return state;
    }
};

const year = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_RANGE_FILTERS_FAILURE:
            return {};
        case RECEIVE_RANGE_FILTERS_SUCCESS:
            return {
                ...action.response.year,
                values: ensureValuesInRange(state.values, action.response.year)
            };
        case CHANGE_YEAR_RANGE:
            return {
                ...state,
                values: action.values
            };
        default:
            return state;
    }
};

const pages = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_RANGE_FILTERS_FAILURE:
            return {};
        case RECEIVE_RANGE_FILTERS_SUCCESS:
            return {
                ...action.response.pages,
                values: ensureValuesInRange(state.values, action.response.pages)
            };
        case CHANGE_PAGE_RANGE:
            return {
                ...state,
                values: action.values
            };
        default:
            return state;
    }
};

const interestRange = (state = [], action) => {
    switch (action.type) {
        case CHANGE_INTEREST_RANGE:
            return action.values;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_RANGE_FILTERS:
            return true;
        case RECEIVE_RANGE_FILTERS_SUCCESS:
        case RECEIVE_RANGE_FILTERS_FAILURE:
            return false;
        default:
            return state;
    }
};

const ranges = combineReducers({
    ownRating,
    amazonRating,
    goodreadsRating,
    year,
    pages,
    interestRange,
    isFetching
});

export default ranges;

export const getOwnRating = state => get(state, 'ranges.ownRating');
export const getAmazonRating = state => get(state, 'ranges.amazonRating');
export const getGoodreadsRating = state => get(state, 'ranges.goodreadsRating');
export const getYearRanges = state => get(state, 'ranges.year');
export const getYear = state => get(state, 'ranges.year.values');
export const getPageRanges = state => get(state, 'ranges.pages');
export const getPages = state => get(state, 'ranges.pages.values');
export const getInterestRange = state => get(state, 'ranges.interestRange');
