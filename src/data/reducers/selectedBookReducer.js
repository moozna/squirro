import { combineReducers } from 'redux';
import {
    SELECT_BOOK,
    UNSELECT_BOOK,
    REQUEST_SELECTED_BOOK,
    RECEIVE_SELECTED_BOOK_SUCCESS,
    RECEIVE_SELECTED_BOOK_FAILURE,
    UPDATE_SELECTED_BOOK,
    UPDATE_SELECTED_BOOK_SUCCESS,
    UPDATE_SELECTED_BOOK_FAILURE,
    ADD_NEW_BOOK,
    ADD_NEW_BOOK_SUCCESS,
    ADD_NEW_BOOK_FAILURE,
    DELETE_SELECTED_BOOK,
    DELETE_SELECTED_BOOK_SUCCESS,
    DELETE_SELECTED_BOOK_FAILURE
} from '../constants';
import get from '../../utils/get';

const selectedBookId = (state = null, action) => {
    switch (action.type) {
        case SELECT_BOOK:
        case ADD_NEW_BOOK_SUCCESS:
            return action.id;
        case UNSELECT_BOOK:
        case RECEIVE_SELECTED_BOOK_FAILURE:
        case ADD_NEW_BOOK:
        case DELETE_SELECTED_BOOK_SUCCESS:
            return null;
        default:
            return state;
    }
};

const data = (state = {}, action) => {
    switch (action.type) {
        case UNSELECT_BOOK:
        case RECEIVE_SELECTED_BOOK_FAILURE:
        case ADD_NEW_BOOK:
        case DELETE_SELECTED_BOOK_SUCCESS:
            return {};
        case RECEIVE_SELECTED_BOOK_SUCCESS:
        case UPDATE_SELECTED_BOOK_SUCCESS:
        case ADD_NEW_BOOK_SUCCESS:
            return action.response;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_SELECTED_BOOK:
        case UPDATE_SELECTED_BOOK:
        case ADD_NEW_BOOK:
        case DELETE_SELECTED_BOOK:
            return true;
        case RECEIVE_SELECTED_BOOK_SUCCESS:
        case RECEIVE_SELECTED_BOOK_FAILURE:
        case UPDATE_SELECTED_BOOK_SUCCESS:
        case UPDATE_SELECTED_BOOK_FAILURE:
        case ADD_NEW_BOOK_SUCCESS:
        case ADD_NEW_BOOK_FAILURE:
        case DELETE_SELECTED_BOOK_SUCCESS:
        case DELETE_SELECTED_BOOK_FAILURE:
            return false;
        default:
            return state;
    }
};

const selectedBook = combineReducers({
    selectedBookId,
    data,
    isFetching
});

export default selectedBook;

export const getSelectedBook = state => state.selectedBook.data;
export const getSelectedBookId = state =>
    get(state, 'selectedBook.selectedBookId');
