import reducer, * as selectors from './booksReducer';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INVALIDATE_BOOKS
} from '../constants';

const initialState = {
    data: [],
    count: 0,
    isFetching: false,
    didInvalidate: true
};

describe('books reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('REQUEST_BOOKS', () => {
        test('starts requesting books', () => {
            const currentState = {};

            const action = {
                type: REQUEST_BOOKS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears books', () => {
            const currentState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: true,
                didInvalidate: true
            };

            const action = {
                type: REQUEST_BOOKS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_BOOKS_SUCCESS', () => {
        test('stops requesting books on success', () => {
            const currentState = {};
            const mockResponse = {
                books: [{ id: '1', title: 'title' }],
                count: 10,
                hasNext: true
            };

            const action = {
                type: RECEIVE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: false,
                didInvalidate: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new books on success', () => {
            const currentState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: false,
                didInvalidate: true
            };

            const mockResponse = {
                books: [{ id: '2', title: 'title2' }],
                count: 20,
                hasNext: true
            };

            const action = {
                type: RECEIVE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                data: [{ id: '2', title: 'title2' }],
                count: 20,
                isFetching: false,
                didInvalidate: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_BOOKS_FAILURE', () => {
        test('stops requesting books on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_BOOKS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears books on failure', () => {
            const currentState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: true,
                didInvalidate: true
            };

            const action = {
                type: RECEIVE_BOOKS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('REQUEST_MORE_BOOKS', () => {
        test('starts requesting more books', () => {
            const currentState = {};

            const action = {
                type: REQUEST_MORE_BOOKS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_MORE_BOOKS_SUCCESS', () => {
        test('stops requesting more books on success', () => {
            const currentState = {};
            const mockResponse = {
                books: [{ id: '1', title: 'title' }],
                count: 10,
                hasNext: true
            };

            const action = {
                type: RECEIVE_MORE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: false,
                didInvalidate: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('adds newly received books on success', () => {
            const currentState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: false,
                didInvalidate: true
            };

            const mockResponse = {
                books: [{ id: '2', title: 'title2' }],
                count: 20,
                hasNext: true
            };

            const action = {
                type: RECEIVE_MORE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                data: [
                    { id: '1', title: 'title' },
                    { id: '2', title: 'title2' }
                ],
                count: 20,
                isFetching: false,
                didInvalidate: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_MORE_BOOKS_FAILURE', () => {
        test('stops requesting more books on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_MORE_BOOKS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('INVALIDATE_BOOKS', () => {
        test('sets didInvalidate to true', () => {
            const currentState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                didInvalidate: false
            };

            const action = {
                type: INVALIDATE_BOOKS
            };

            const expectedState = {
                data: [{ id: '1', title: 'title' }],
                count: 10,
                isFetching: false,
                didInvalidate: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects books data from state', () => {
            const booksData = [
                {
                    id: '1',
                    title: 'title'
                }
            ];
            const currentState = {
                books: {
                    data: booksData
                }
            };

            expect(selectors.getBooks(currentState)).toEqual(booksData);
        });

        test('selects count from state', () => {
            const count = 404;
            const currentState = {
                books: {
                    count
                }
            };

            expect(selectors.getBooksCount(currentState)).toBe(count);
        });

        test('selects isFetching from state', () => {
            const isFetching = true;
            const currentState = {
                books: {
                    isFetching
                }
            };

            expect(selectors.getFetching(currentState)).toBe(isFetching);
        });

        test('selects didInvalidate from state', () => {
            const didInvalidate = true;
            const currentState = {
                books: {
                    didInvalidate
                }
            };

            expect(selectors.getDidInvalidate(currentState)).toBe(
                didInvalidate
            );
        });
    });
});
