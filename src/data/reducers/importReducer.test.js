import reducer, * as selectors from './importReducer';
import {
    IMPORT_BOOK,
    IMPORT_BOOK_SUCCESS,
    IMPORT_BOOK_FAILURE,
    CLEAR_IMPORTED_DATA,
    UPDATE_DUPLICATE_KEYS,
    CREATE_INITIAL_BOOK,
    ADD_NEW_BOOK_SUCCESS
} from '../constants';

const initialState = {
    data: {},
    duplicates: {},
    initialBook: {},
    isFetching: {}
};

describe('import reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('IMPORT_BOOK', () => {
        test('starts importing book', () => {
            const currentState = {
                isFetching: { amazon: false, goodreads: true }
            };

            const action = {
                type: IMPORT_BOOK,
                siteName: 'amazon'
            };

            const expectedState = {
                ...initialState,
                isFetching: { amazon: true, goodreads: true }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('IMPORT_BOOK_SUCCESS', () => {
        test('stops importing on success', () => {
            const currentState = {
                isFetching: { amazon: true, goodreads: true }
            };
            const mockResponse = {
                asin: 'asin',
                title: 'title'
            };

            const action = {
                type: IMPORT_BOOK_SUCCESS,
                response: mockResponse,
                siteName: 'amazon'
            };

            const expectedState = {
                ...initialState,
                data: { amazon: mockResponse },
                isFetching: { amazon: false, goodreads: true }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new book data on success', () => {
            const currentState = {
                data: {
                    amazon: { asin: 'asin', title: 'title' }
                },
                isFetching: { amazon: true }
            };

            const mockResponse = {
                asin: 'asin2',
                title: 'title2'
            };

            const action = {
                type: IMPORT_BOOK_SUCCESS,
                response: mockResponse,
                siteName: 'amazon'
            };

            const expectedState = {
                ...initialState,
                data: { amazon: mockResponse },
                isFetching: { amazon: false }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('IMPORT_BOOK_FAILURE', () => {
        test('stops importing book on failure', () => {
            const currentState = {
                isFetching: { amazon: true }
            };
            const action = {
                type: IMPORT_BOOK_FAILURE,
                message: 'error',
                siteName: 'amazon'
            };

            const expectedState = {
                ...initialState,
                data: {
                    amazon: {}
                },
                isFetching: { amazon: false }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears book data of the site on failure', () => {
            const currentState = {
                data: {
                    amazon: { asin: 'asin', title: 'title' },
                    goodreads: { goodreadsId: 12, title: 'title' }
                },
                isFetching: { amazon: true }
            };

            const action = {
                type: IMPORT_BOOK_FAILURE,
                message: 'error',
                siteName: 'amazon'
            };

            const expectedState = {
                ...initialState,
                data: {
                    amazon: {},
                    goodreads: { goodreadsId: 12, title: 'title' }
                },
                isFetching: { amazon: false }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CLEAR_IMPORTED_DATA', () => {
        test('clears all imported data', () => {
            const currentState = {
                data: {
                    amazon: { asin: 'asin', title: 'title' },
                    goodreads: {
                        goodreadsId: 12,
                        title: 'title'
                    }
                },
                duplicates: {
                    title: true
                },
                initialBook: {
                    asin: 'asin',
                    goodreadsId: 12,
                    title: 'title'
                }
            };

            const action = {
                type: CLEAR_IMPORTED_DATA
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UPDATE_DUPLICATE_KEYS', () => {
        test('updates the list of duplicate keys', () => {
            const amazonData = { asin: 'asin', title: 'title' };
            const goodreadsData = {
                goodreadsId: 12,
                title: 'title'
            };
            const currentState = {};

            const action = {
                type: UPDATE_DUPLICATE_KEYS,
                response: amazonData,
                otherData: goodreadsData
            };

            const expectedState = {
                ...initialState,
                duplicates: { title: true }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns empty duplicate keys when only one data is available', () => {
            const amazonData = { asin: 'asin', title: 'title' };
            const currentState = {};

            const action = {
                type: UPDATE_DUPLICATE_KEYS,
                response: amazonData,
                otherData: {}
            };

            const expectedState = {
                ...initialState,
                duplicates: {}
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CREATE_INITIAL_BOOK', () => {
        test('creates initial book data', () => {
            const sitesData = {
                goodreads: {
                    goodreadsId: 12,
                    title: 'title',
                    pages: 210
                },
                amazon: { asin: 'asin', title: 'title', pages: 200 }
            };
            const currentState = {};

            const action = {
                type: CREATE_INITIAL_BOOK,
                sitesData
            };

            const expectedState = {
                ...initialState,
                initialBook: {
                    asin: 'asin',
                    goodreadsId: 12,
                    title: 'title',
                    pages: 200
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('adds only properties with value to the initial book data', () => {
            const sitesData = {
                goodreads: {
                    goodreadsId: 12,
                    title: 'title',
                    pages: 210
                },
                amazon: { asin: 'asin', title: 'title', pages: null }
            };
            const currentState = {};

            const action = {
                type: CREATE_INITIAL_BOOK,
                sitesData
            };

            const expectedState = {
                ...initialState,
                initialBook: {
                    asin: 'asin',
                    goodreadsId: 12,
                    title: 'title',
                    pages: 210
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('normalizes description html', () => {
            const sitesData = {
                goodreads: {
                    description:
                        'Example description<br><br>First paragraph<br><br>Another paragraph'
                },
                amazon: {}
            };
            const currentState = {};

            const action = {
                type: CREATE_INITIAL_BOOK,
                sitesData
            };

            const expectedState = {
                ...initialState,
                initialBook: {
                    description:
                        '<p>Example description</p>\n<p>First paragraph</p>\n<p>Another paragraph</p>'
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('ADD_NEW_BOOK_SUCCESS', () => {
        test('clears all imported data', () => {
            const currentState = {
                data: {
                    amazon: { asin: 'asin', title: 'title' },
                    goodreads: {
                        goodreadsId: 12,
                        title: 'title'
                    }
                },
                duplicates: {
                    title: true
                },
                initialBook: {
                    asin: 'asin',
                    goodreadsId: 12,
                    title: 'title'
                }
            };

            const action = {
                type: ADD_NEW_BOOK_SUCCESS
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        describe('getImportedAmazonData()', () => {
            test('selects amazon data from state', () => {
                const bookData = { asin: 'asin', title: 'title', pages: 200 };
                const currentState = {
                    importing: {
                        data: {
                            amazon: bookData
                        }
                    }
                };

                expect(selectors.getImportedAmazonData(currentState)).toEqual(
                    bookData
                );
            });
        });

        describe('getImportedGoodreadsData()', () => {
            test('selects goodreads data from state', () => {
                const bookData = {
                    goodreadsId: 12,
                    title: 'title',
                    pages: 210
                };
                const currentState = {
                    importing: {
                        data: {
                            goodreads: bookData
                        }
                    }
                };

                expect(
                    selectors.getImportedGoodreadsData(currentState)
                ).toEqual(bookData);
            });
        });

        describe('getSitesData()', () => {
            test('selects all book data from state', () => {
                const bookData = {
                    amazon: { asin: 'asin', title: 'title', pages: 200 },
                    goodreads: {
                        goodreadsId: 12,
                        title: 'title',
                        pages: 210
                    }
                };
                const currentState = {
                    importing: {
                        data: bookData
                    }
                };

                expect(selectors.getSitesData(currentState)).toEqual(bookData);
            });
        });

        describe('getDuplicateData()', () => {
            test('selects duplicate data from state', () => {
                const duplicates = {
                    title: true,
                    description: true
                };
                const currentState = {
                    importing: {
                        duplicates
                    }
                };

                expect(selectors.getDuplicateData(currentState)).toEqual(
                    duplicates
                );
            });
        });

        describe('getInitialBook()', () => {
            test('selects initial book data from state', () => {
                const bookData = {
                    asin: 'asin',
                    goodreadsId: 12,
                    title: 'title',
                    pages: 200
                };
                const currentState = {
                    importing: {
                        initialBook: bookData
                    }
                };

                expect(selectors.getInitialBook(currentState)).toEqual(
                    bookData
                );
            });
        });

        describe('getIsFetching()', () => {
            test('selects isFetching data from state', () => {
                const isFetching = { amazon: true, goodreads: false };
                const currentState = {
                    importing: {
                        isFetching
                    }
                };

                expect(selectors.getIsFetching(currentState)).toEqual(
                    isFetching
                );
            });
        });
    });
});
