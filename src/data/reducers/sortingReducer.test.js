import reducer, * as selectors from './sortingReducer';
import { CHANGE_SORTFIELD, TOGGLE_SORTORDER } from '../constants';

const initialState = {
    sortField: 'createdDate',
    sortOrder: -1
};

describe('sorting reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('CHANGE_SORTFIELD', () => {
        test('sets the given value as sortField', () => {
            const sortField = 'author';
            const currentState = {};

            const action = {
                type: CHANGE_SORTFIELD,
                sortField
            };

            const expectedState = {
                ...initialState,
                sortField
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('TOGGLE_SORTORDER', () => {
        test('toggles sort order', () => {
            const currentState = {
                sortOrder: 1
            };

            const action = {
                type: TOGGLE_SORTORDER
            };

            const expectedState = {
                sortField: 'createdDate',
                sortOrder: -1
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the selected sort field from state', () => {
            const sortField = 'author';
            const currentState = {
                sorting: {
                    sortField
                }
            };

            expect(selectors.getSortField(currentState)).toEqual(sortField);
        });

        test('selects the selected sort order from state', () => {
            const sortOrder = -1;
            const currentState = {
                sorting: {
                    sortOrder
                }
            };

            expect(selectors.getSortOrder(currentState)).toBe(sortOrder);
        });
    });
});
