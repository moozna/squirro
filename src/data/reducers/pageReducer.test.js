import { NOT_FOUND } from 'redux-first-router';
import reducer, * as selectors from './pageReducer';
import {
    BOOKS,
    BOOK,
    EDIT,
    ADD,
    IMPORT,
    EDIT_IMPORT,
    SHELVES
} from '../constants';

describe('page reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual('Books');
    });

    describe('BOOKS', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: BOOKS
            };
            const expectedState = 'Books';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('BOOK', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: BOOK
            };
            const expectedState = 'BookDetails';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('EDIT', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: EDIT
            };
            const expectedState = 'EditBook';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('ADD', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: ADD
            };
            const expectedState = 'AddBook';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('IMPORT', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: IMPORT
            };
            const expectedState = 'ImportBook';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('EDIT_IMPORT', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: EDIT_IMPORT
            };
            const expectedState = 'EditImport';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('SHELVES', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: SHELVES
            };
            const expectedState = 'BookShelves';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('NOT_FOUND', () => {
        test('sets the current page based on the action type', () => {
            const currentState = {};
            const action = {
                type: NOT_FOUND
            };
            const expectedState = 'NotFound';

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the current page from state', () => {
            const currentPage = 'BookDetails';
            const currentState = {
                page: currentPage
            };

            expect(selectors.getCurrentPage(currentState)).toBe(currentPage);
        });

        test('selects the previous book id from state', () => {
            const bookId = '1';
            const currentState = {
                location: { prev: { payload: { id: bookId } } }
            };

            expect(selectors.getPreviouslySelectedBookId(currentState)).toBe(
                bookId
            );
        });
    });
});
