import reducer, * as selectors from './paginationReducer';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INCREASE_START_INDEX,
    SELECT_START_INDEX,
    SELECT_PAGE_NUMBER,
    RESET_PAGINATION,
    SELECT_LIMIT,
    RESET_LIMIT
} from '../constants';

const initialState = {
    startIndex: 0,
    currentPageNumber: 1,
    limit: 50,
    pageSize: 150,
    hasMore: false
};

describe('pagination reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('REQUEST_BOOKS', () => {
        test('sets hasMore to false', () => {
            const currentState = {
                hasMore: true
            };

            const action = {
                type: REQUEST_BOOKS
            };

            const expectedState = {
                ...initialState,
                hasMore: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_BOOKS_SUCCESS', () => {
        test('sets hasMore based on the response value', () => {
            const currentState = {};
            const mockResponse = {
                books: [],
                count: 10,
                hasNext: true
            };

            const action = {
                type: RECEIVE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                hasMore: mockResponse.hasNext
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_BOOKS_FAILURE', () => {
        test('sets hasMore to false', () => {
            const currentState = {
                hasMore: true
            };

            const action = {
                type: RECEIVE_BOOKS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState,
                hasMore: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('REQUEST_MORE_BOOKS', () => {
        test('sets hasMore to false', () => {
            const currentState = {
                hasMore: true
            };

            const action = {
                type: REQUEST_MORE_BOOKS
            };

            const expectedState = {
                ...initialState,
                hasMore: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_MORE_BOOKS_SUCCESS', () => {
        test('sets hasMore based on the response value', () => {
            const currentState = {};
            const mockResponse = {
                books: [],
                count: 10,
                hasNext: true
            };

            const action = {
                type: RECEIVE_MORE_BOOKS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                hasMore: mockResponse.hasNext
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_MORE_BOOKS_FAILURE', () => {
        test('sets hasMore to false', () => {
            const currentState = {
                hasMore: true
            };

            const action = {
                type: RECEIVE_MORE_BOOKS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState,
                hasMore: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('INCREASE_START_INDEX', () => {
        test('increases startIndex with current limit', () => {
            const currentState = {
                startIndex: 50,
                limit: 25
            };

            const action = {
                type: INCREASE_START_INDEX,
                limit: 25
            };

            const expectedState = {
                ...initialState,
                limit: 25,
                startIndex: 75
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('SELECT_START_INDEX', () => {
        test('sets startIndex to the specified value', () => {
            const currentState = {
                startIndex: 50
            };

            const action = {
                type: SELECT_START_INDEX,
                startIndex: 120
            };

            const expectedState = {
                ...initialState,
                startIndex: 120
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('SELECT_PAGE_NUMBER', () => {
        test('sets currentPageNumber to the specified value', () => {
            const currentState = {
                currentPageNumber: 2
            };

            const action = {
                type: SELECT_PAGE_NUMBER,
                pageNumber: 4
            };

            const expectedState = {
                ...initialState,
                currentPageNumber: 4
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RESET_PAGINATION', () => {
        test('resets startIndex to default value', () => {
            const currentState = {
                startIndex: 120
            };

            const action = {
                type: RESET_PAGINATION
            };

            const expectedState = {
                ...initialState,
                startIndex: 0
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('resets currentPageNumber to default value', () => {
            const currentState = {
                currentPageNumber: 4
            };

            const action = {
                type: RESET_PAGINATION
            };

            const expectedState = {
                ...initialState,
                currentPageNumber: 1
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('SELECT_LIMIT', () => {
        test('sets limit to the specified value', () => {
            const currentState = {
                limit: 50
            };

            const action = {
                type: SELECT_LIMIT,
                limit: 150
            };

            const expectedState = {
                ...initialState,
                limit: 150
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RESET_LIMIT', () => {
        test('resets limit to default value', () => {
            const currentState = {
                limit: 120
            };

            const action = {
                type: RESET_LIMIT
            };

            const expectedState = {
                ...initialState,
                limit: 50
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects startIndex from state', () => {
            const startIndex = 50;
            const currentState = {
                pagination: {
                    startIndex
                }
            };

            expect(selectors.getStartIndex(currentState)).toBe(startIndex);
        });

        test('selects currentPageNumber from state', () => {
            const currentPageNumber = 5;
            const currentState = {
                pagination: {
                    currentPageNumber
                }
            };

            expect(selectors.getCurrentPageNumber(currentState)).toBe(
                currentPageNumber
            );
        });

        test('selects limit from state', () => {
            const limit = 20;
            const currentState = {
                pagination: {
                    limit
                }
            };

            expect(selectors.getPageLimit(currentState)).toBe(limit);
        });

        test('selects pageSize from state', () => {
            const pageSize = 50;
            const currentState = {
                pagination: {
                    pageSize
                }
            };

            expect(selectors.getPageSize(currentState)).toBe(pageSize);
        });

        test('selects hasMore from state', () => {
            const hasMore = true;
            const currentState = {
                pagination: {
                    hasMore
                }
            };

            expect(selectors.getHasMore(currentState)).toBe(hasMore);
        });

        test('returns false when max startIndex for that page is not reached', () => {
            const currentState = {
                pagination: {
                    startIndex: 50,
                    currentPageNumber: 1,
                    limit: 25,
                    pageSize: 100
                }
            };

            expect(selectors.getShouldStartNextPage(currentState)).toBe(false);
        });

        test('returns true when max startIndex for that page is reached', () => {
            const currentState = {
                pagination: {
                    startIndex: 75,
                    currentPageNumber: 1,
                    limit: 25,
                    pageSize: 100
                }
            };

            expect(selectors.getShouldStartNextPage(currentState)).toBe(true);
        });

        test('returns false when hasMore is false on the state', () => {
            const currentState = {
                pagination: {
                    hasMore: false
                }
            };

            expect(selectors.getHasMoreForPage(currentState)).toBe(false);
        });

        test('returns true when hasMore is true and max startIndex for that page is not reached', () => {
            const currentState = {
                pagination: {
                    hasMore: true,
                    startIndex: 50,
                    currentPageNumber: 1,
                    limit: 25,
                    pageSize: 100
                }
            };

            expect(selectors.getHasMoreForPage(currentState)).toBe(true);
        });

        test('returns false when hasMore is true but max startIndex for that page is reached', () => {
            const currentState = {
                pagination: {
                    hasMore: true,
                    startIndex: 75,
                    currentPageNumber: 1,
                    limit: 25,
                    pageSize: 100
                }
            };

            expect(selectors.getHasMoreForPage(currentState)).toBe(false);
        });
    });
});
