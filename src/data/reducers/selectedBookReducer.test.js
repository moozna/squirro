import reducer, * as selectors from './selectedBookReducer';
import {
    SELECT_BOOK,
    UNSELECT_BOOK,
    REQUEST_SELECTED_BOOK,
    RECEIVE_SELECTED_BOOK_SUCCESS,
    RECEIVE_SELECTED_BOOK_FAILURE,
    UPDATE_SELECTED_BOOK,
    UPDATE_SELECTED_BOOK_SUCCESS,
    UPDATE_SELECTED_BOOK_FAILURE,
    ADD_NEW_BOOK,
    ADD_NEW_BOOK_SUCCESS,
    ADD_NEW_BOOK_FAILURE,
    DELETE_SELECTED_BOOK,
    DELETE_SELECTED_BOOK_SUCCESS,
    DELETE_SELECTED_BOOK_FAILURE
} from '../constants';

const initialState = {
    selectedBookId: null,
    data: {},
    isFetching: false
};

describe('selected book reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('SELECT_BOOK', () => {
        test('sets the given id as selected book id', () => {
            const currentState = {
                selectedBookId: '1'
            };

            const action = {
                type: SELECT_BOOK,
                id: '3'
            };

            const expectedState = {
                ...initialState,
                selectedBookId: '3'
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UNSELECT_BOOK', () => {
        test('resets the selected book id to null', () => {
            const currentState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' }
            };

            const action = {
                type: UNSELECT_BOOK
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('REQUEST_SELECTED_BOOK', () => {
        test('starts requesting selected book', () => {
            const currentState = {};

            const action = {
                type: REQUEST_SELECTED_BOOK
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SELECTED_BOOK_SUCCESS', () => {
        test('stops requesting selected book on success', () => {
            const currentState = {};
            const mockResponse = {
                _id: '1',
                asin: 'asin'
            };

            const action = {
                type: RECEIVE_SELECTED_BOOK_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: null,
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new book on success', () => {
            const currentState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            const mockResponse = {
                _id: '2',
                asin: 'asin'
            };

            const action = {
                type: RECEIVE_SELECTED_BOOK_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: '1',
                data: { _id: '2', asin: 'asin' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SELECTED_BOOK_FAILURE', () => {
        test('stops requesting selected book on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears book on failure', () => {
            const currentState = {
                data: { _id: '1', asin: 'asin' },
                isFetching: true
            };

            const action = {
                type: RECEIVE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UPDATE_SELECTED_BOOK', () => {
        test('starts updating selected book', () => {
            const currentState = {};

            const action = {
                type: UPDATE_SELECTED_BOOK
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UPDATE_SELECTED_BOOK_SUCCESS', () => {
        test('stops updating selected book on success', () => {
            const currentState = {
                selectedBookId: '1'
            };
            const mockResponse = {
                _id: '1',
                asin: 'asin'
            };

            const action = {
                type: UPDATE_SELECTED_BOOK_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns updated book on success', () => {
            const currentState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            const mockResponse = {
                _id: '1',
                asin: 'asin2'
            };

            const action = {
                type: UPDATE_SELECTED_BOOK_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin2' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('UPDATE_SELECTED_BOOK_FAILURE', () => {
        test('stops updating selected book on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: UPDATE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('does not changes book on failure', () => {
            const currentState = {
                data: { _id: '1', asin: 'asin' },
                isFetching: true
            };

            const action = {
                type: UPDATE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState,
                data: { _id: '1', asin: 'asin' }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('ADD_NEW_BOOK', () => {
        test('starts adding new book', () => {
            const currentState = {
                selectedBookId: '1'
            };

            const action = {
                type: ADD_NEW_BOOK
            };

            const expectedState = {
                ...initialState,
                selectedBookId: null,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('ADD_NEW_BOOK_SUCCESS', () => {
        test('stops adding new book on success', () => {
            const currentState = {};
            const mockResponse = {
                _id: '1',
                asin: 'asin'
            };

            const action = {
                type: ADD_NEW_BOOK_SUCCESS,
                id: mockResponse._id,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns added book on success', () => {
            const currentState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' },
                isFetching: false
            };

            const mockResponse = {
                _id: '2',
                asin: 'asin'
            };

            const action = {
                type: ADD_NEW_BOOK_SUCCESS,
                id: mockResponse._id,
                response: mockResponse
            };

            const expectedState = {
                selectedBookId: '2',
                data: { _id: '2', asin: 'asin' },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('ADD_NEW_BOOK_FAILURE', () => {
        test('stops adding new book on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: ADD_NEW_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('does not changes book on failure', () => {
            const currentState = {
                selectedBookId: null,
                data: {},
                isFetching: true
            };

            const action = {
                type: ADD_NEW_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('DELETE_SELECTED_BOOK', () => {
        test('starts updating selected book', () => {
            const currentState = {};

            const action = {
                type: DELETE_SELECTED_BOOK
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('DELETE_SELECTED_BOOK_SUCCESS', () => {
        test('stops deleting selected book and removes selected book data on success', () => {
            const currentState = {
                selectedBookId: '1',
                data: { _id: '1', asin: 'asin' }
            };

            const action = {
                type: DELETE_SELECTED_BOOK_SUCCESS
            };

            const expectedState = {
                selectedBookId: null,
                data: {},
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('DELETE_SELECTED_BOOK_FAILURE', () => {
        test('stops deleting selected book on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: DELETE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('does not changes book on failure', () => {
            const currentState = {
                data: { _id: '1', asin: 'asin' },
                isFetching: true
            };

            const action = {
                type: DELETE_SELECTED_BOOK_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState,
                data: { _id: '1', asin: 'asin' }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the selected book data from state', () => {
            const bookData = {
                title: 'title',
                author: 'author'
            };
            const currentState = {
                selectedBook: {
                    data: bookData
                }
            };

            expect(selectors.getSelectedBook(currentState)).toEqual(bookData);
        });

        test('selects the selectedBookId from state', () => {
            const bookId = '1';
            const currentState = {
                selectedBook: {
                    selectedBookId: bookId
                }
            };

            expect(selectors.getSelectedBookId(currentState)).toBe(bookId);
        });
    });
});
