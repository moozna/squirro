import { combineReducers } from 'redux';
import {
    REQUEST_SHELVES,
    RECEIVE_SHELVES_SUCCESS,
    RECEIVE_SHELVES_FAILURE,
    SELECT_SHELF,
    UNSELECT_SHELF,
    UPDATE_SHELF_SUCCESS,
    DELETE_SHELF_SUCCESS
} from '../constants';
import get from '../../utils/get';

const data = (state = [], action) => {
    switch (action.type) {
        case REQUEST_SHELVES:
        case RECEIVE_SHELVES_FAILURE:
            return [];
        case RECEIVE_SHELVES_SUCCESS:
            return action.response;
        case UPDATE_SHELF_SUCCESS:
            return state.map(item => {
                if (item._id !== action.shelfName) {
                    return item;
                }
                return {
                    ...item,
                    _id: action.newName
                };
            });
        case DELETE_SHELF_SUCCESS:
            return state.filter(item => item._id !== action.shelfName);
        default:
            return state;
    }
};

const selectedShelf = (state = null, action) => {
    switch (action.type) {
        case SELECT_SHELF:
            return action.shelfName;
        case UNSELECT_SHELF:
            return null;
        case UPDATE_SHELF_SUCCESS:
            return state === action.shelfName ? action.newName : state;
        case DELETE_SHELF_SUCCESS:
            return state === action.shelfName ? null : state;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_SHELVES:
            return true;
        case RECEIVE_SHELVES_SUCCESS:
        case RECEIVE_SHELVES_FAILURE:
            return false;
        default:
            return state;
    }
};

const shelves = combineReducers({
    data,
    selectedShelf,
    isFetching
});

export default shelves;

export const getShelves = state => get(state, 'shelves.data', []);
export const getSelectedShelf = state =>
    get(state, 'shelves.selectedShelf', '');
