import { combineReducers } from 'redux';
import {
    REQUEST_BOOKS,
    RECEIVE_BOOKS_SUCCESS,
    RECEIVE_BOOKS_FAILURE,
    REQUEST_MORE_BOOKS,
    RECEIVE_MORE_BOOKS_SUCCESS,
    RECEIVE_MORE_BOOKS_FAILURE,
    INVALIDATE_BOOKS
} from '../constants';
import get from '../../utils/get';

const data = (state = [], action) => {
    switch (action.type) {
        case REQUEST_BOOKS:
        case RECEIVE_BOOKS_FAILURE:
            return [];
        case RECEIVE_BOOKS_SUCCESS:
            return action.response.books;
        case RECEIVE_MORE_BOOKS_SUCCESS:
            return [...state, ...action.response.books];
        default:
            return state;
    }
};

const count = (state = 0, action) => {
    switch (action.type) {
        case REQUEST_BOOKS:
        case RECEIVE_BOOKS_FAILURE:
            return 0;
        case RECEIVE_BOOKS_SUCCESS:
        case RECEIVE_MORE_BOOKS_SUCCESS:
            return action.response.count;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_BOOKS:
        case REQUEST_MORE_BOOKS:
            return true;
        case RECEIVE_BOOKS_SUCCESS:
        case RECEIVE_BOOKS_FAILURE:
        case RECEIVE_MORE_BOOKS_SUCCESS:
        case RECEIVE_MORE_BOOKS_FAILURE:
            return false;
        default:
            return state;
    }
};

const didInvalidate = (state = true, action) => {
    switch (action.type) {
        case INVALIDATE_BOOKS:
            return true;
        case RECEIVE_BOOKS_SUCCESS:
        case RECEIVE_MORE_BOOKS_SUCCESS:
            return false;
        default:
            return state;
    }
};

const books = combineReducers({
    data,
    count,
    isFetching,
    didInvalidate
});

export default books;

export const getBooks = state => get(state, 'books.data', []);
export const getBooksCount = state => get(state, 'books.count', 0);
export const getFetching = state => get(state, 'books.isFetching');
export const getDidInvalidate = state => state.books.didInvalidate;
