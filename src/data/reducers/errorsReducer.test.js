import reducer, * as selectors from './errorsReducer';
import { IMPORT_BOOK_FAILURE, CLEAR_ERROR_MESSAGE } from '../constants';

describe('errors reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual([]);
    });

    test('adds error field from any action', () => {
        const currentState = [
            {
                id: 'RECEIVE_BOOKS_FAILURE',
                message: 'Books error message',
                title: 'Error'
            }
        ];

        const action = {
            type: IMPORT_BOOK_FAILURE,
            error: {
                message: 'Import error message',
                title: 'Error'
            }
        };

        const expectedState = [
            {
                id: 'RECEIVE_BOOKS_FAILURE',
                message: 'Books error message',
                title: 'Error'
            },
            {
                id: 'IMPORT_BOOK_FAILURE',
                message: 'Import error message',
                title: 'Error'
            }
        ];

        expect(reducer(currentState, action)).toEqual(expectedState);
    });

    describe('CLEAR_ERROR_MESSAGE', () => {
        test('clears the error with the specified id', () => {
            const currentState = [
                {
                    id: 'RECEIVE_BOOKS_FAILURE',
                    error: 'Books error message',
                    title: 'Error'
                },
                {
                    id: 'IMPORT_BOOK_FAILURE',
                    error: 'Import error message',
                    title: 'Error'
                },
                {
                    id: 'RECEIVE_FILTERS_FAILURE',
                    error: 'Filters error message',
                    title: 'Error'
                }
            ];

            const action = {
                type: CLEAR_ERROR_MESSAGE,
                id: 'IMPORT_BOOK_FAILURE'
            };

            const expectedState = [
                {
                    id: 'RECEIVE_BOOKS_FAILURE',
                    error: 'Books error message',
                    title: 'Error'
                },
                {
                    id: 'RECEIVE_FILTERS_FAILURE',
                    error: 'Filters error message',
                    title: 'Error'
                }
            ];

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the errors from state', () => {
            const errors = [
                {
                    id: 'RECEIVE_BOOKS_FAILURE',
                    message: 'Books error message',
                    title: 'Error'
                },
                {
                    id: 'IMPORT_BOOK_FAILURE',
                    message: 'Import error message',
                    title: 'Error'
                },
                {
                    id: 'RECEIVE_FILTERS_FAILURE',
                    message: 'Filters error message',
                    title: 'Error'
                }
            ];

            const currentState = { errors };

            expect(selectors.getErrors(currentState)).toEqual(errors);
        });
    });
});
