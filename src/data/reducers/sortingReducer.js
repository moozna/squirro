import { combineReducers } from 'redux';
import { CHANGE_SORTFIELD, TOGGLE_SORTORDER } from '../constants';
import get from '../../utils/get';

const sortField = (state = 'createdDate', action) => {
    switch (action.type) {
        case CHANGE_SORTFIELD:
            return action.sortField;
        default:
            return state;
    }
};

const sortOrder = (state = -1, action) => {
    switch (action.type) {
        case TOGGLE_SORTORDER:
            return state * -1;
        default:
            return state;
    }
};

const sorting = combineReducers({
    sortField,
    sortOrder
});

export default sorting;

export const getSortField = state => get(state, 'sorting.sortField');
export const getSortOrder = state => get(state, 'sorting.sortOrder');
