import reducer, * as selectors from './searchReducer';
import {
    CHANGE_SEARCH_VALUE,
    CLEAR_SEARCH_VALUE,
    REQUEST_SUGGESTIONS,
    RECEIVE_SUGGESTIONS_SUCCESS,
    RECEIVE_SUGGESTIONS_FAILURE,
    CLEAR_SUGGESTIONS
} from '../constants';

const initialState = {
    searchFieldValue: '',
    suggestions: {},
    isFetching: false
};

describe('search reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('CHANGE_SEARCH_VALUE', () => {
        test('sets the given value as searchFieldValue', () => {
            const value = 'king';
            const currentState = {};

            const action = {
                type: CHANGE_SEARCH_VALUE,
                value
            };

            const expectedState = {
                ...initialState,
                searchFieldValue: value
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CLEAR_SEARCH_VALUE', () => {
        test('resets searchFieldValue and suggestions to default value', () => {
            const currentState = {
                searchFieldValue: 'king',
                suggestions: {
                    authors: [{ name: 'Owen King' }, { name: 'Stephen King' }],
                    seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
                    titles: [
                        { name: 'The Mirror King' },
                        { name: 'The King of Attolia' }
                    ]
                }
            };

            const action = { type: CLEAR_SEARCH_VALUE };

            expect(reducer(currentState, action)).toEqual(initialState);
        });
    });

    describe('REQUEST_SUGGESTIONS', () => {
        test('starts requesting suggestions', () => {
            const currentState = {};

            const action = {
                type: REQUEST_SUGGESTIONS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SUGGESTIONS_SUCCESS', () => {
        const mockResponse = {
            authors: [{ name: 'Owen King' }, { name: 'Stephen King' }],
            seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
            titles: [
                { name: 'The Mirror King' },
                { name: 'The King of Attolia' }
            ]
        };

        test('stops requesting suggestions on success', () => {
            const currentState = {};

            const action = {
                type: RECEIVE_SUGGESTIONS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                suggestions: mockResponse
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new suggestions on success', () => {
            const mockResponse = {
                authors: [{ name: 'Owen King' }, { name: 'Stephen King' }],
                seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
                titles: [
                    { name: 'The Mirror King' },
                    { name: 'The King of Attolia' }
                ]
            };

            const currentState = {
                searchFieldValue: 'king',
                suggestions: {
                    titles: [
                        { name: 'A Storm of Swords' },
                        { name: 'Glass Sword' }
                    ]
                },
                isFetching: false
            };

            const action = {
                type: RECEIVE_SUGGESTIONS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                searchFieldValue: 'king',
                suggestions: mockResponse
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_SUGGESTIONS_FAILURE', () => {
        test('stops requesting suggestions on failure', () => {
            const currentState = {
                isFetching: true
            };
            const action = {
                type: RECEIVE_SUGGESTIONS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('clears suggestion on failure', () => {
            const currentState = {
                searchFieldValue: 'king',
                suggestions: {
                    authors: [],
                    seriesTitles: [],
                    titles: [
                        { name: 'A Storm of Swords' },
                        { name: 'Glass Sword' }
                    ]
                },
                isFetching: true
            };

            const action = {
                type: RECEIVE_SUGGESTIONS_FAILURE,
                message: 'error'
            };

            const expectedState = {
                ...initialState,
                searchFieldValue: 'king'
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CLEAR_SUGGESTIONS', () => {
        test('resets suggestions to default value', () => {
            const currentState = {
                searchFieldValue: 'king',
                suggestions: {
                    authors: [],
                    seriesTitles: [],
                    titles: [
                        { name: 'A Storm of Swords' },
                        { name: 'Glass Sword' }
                    ]
                }
            };

            const action = {
                type: CLEAR_SUGGESTIONS
            };

            const expectedState = {
                ...initialState,
                searchFieldValue: 'king'
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects searchFieldValue from state', () => {
            const value = 'king';
            const currentState = {
                search: {
                    searchFieldValue: value
                }
            };

            expect(selectors.getSearchFieldValue(currentState)).toBe(value);
        });

        test('selects suggestions from state', () => {
            const currentState = {
                search: {
                    suggestions: {
                        authors: [
                            { name: 'Owen King' },
                            { name: 'Stephen King' }
                        ],
                        seriesTitles: [{ name: 'The Kingkiller Chronicle' }],
                        titles: [
                            { name: 'The Mirror King' },
                            { name: 'The King of Attolia' }
                        ]
                    }
                }
            };

            const expectedSuggestions = [
                { name: 'Owen King' },
                { name: 'Stephen King' },
                { name: 'The Kingkiller Chronicle' },
                { name: 'The Mirror King' },
                { name: 'The King of Attolia' }
            ];

            expect(selectors.getSearchSuggestions(currentState)).toEqual(
                expectedSuggestions
            );
        });
    });
});
