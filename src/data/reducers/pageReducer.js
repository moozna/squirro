import { NOT_FOUND } from 'redux-first-router';
import get from '../../utils/get';

export const routeComponents = {
    BOOKS: 'Books',
    BOOK: 'BookDetails',
    EDIT: 'EditBook',
    ADD: 'AddBook',
    IMPORT: 'ImportBook',
    EDIT_IMPORT: 'EditImport',
    SHELVES: 'BookShelves',
    [NOT_FOUND]: 'NotFound'
};

const page = (state = 'Books', action) => routeComponents[action.type] || state;

export default page;

export const getCurrentPage = state => state.page;
export const getPreviouslySelectedBookId = state =>
    get(state, 'location.prev.payload.id', '');
