import { combineReducers } from 'redux';
import {
    REQUEST_FILTERS,
    RECEIVE_FILTERS_SUCCESS,
    RECEIVE_FILTERS_FAILURE,
    TOGGLE_GENRE,
    TOGGLE_TAG,
    EXCLUDE_TAG,
    CHANGE_READ_STATUS
} from '../constants';
import get from '../../utils/get';
import compareAlphabetically from '../../utils/compareAlphabetically';

const setStatusOrDefault = (state, filterName, prop) => {
    const hasValue = state[filterName];
    return { [prop]: hasValue ? state[filterName][prop] : false };
};

export const addExcludedFilters = (state, newFilters) => {
    const excludedFilters = Object.values(state)
        .filter(value => value.excluded)
        .map(obj => ({ _id: obj.name, count: 0 }));

    if (excludedFilters.length) {
        return newFilters
            .concat(excludedFilters)
            .sort(compareAlphabetically('_id'));
    }

    return newFilters;
};

const normalizeFilters = (state, filters) =>
    filters.reduce((acc, item) => {
        const filterName = item._id;
        acc[filterName] = {
            name: filterName,
            ...setStatusOrDefault(state, filterName, 'checked'),
            ...setStatusOrDefault(state, filterName, 'excluded')
        };
        return acc;
    }, {});

const toggleFilter = (state, filterName, propName) => ({
    ...state,
    [filterName]: {
        ...state[filterName],
        [propName]: !state[filterName][propName]
    }
});

const genres = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_FILTERS_FAILURE:
            return {};
        case RECEIVE_FILTERS_SUCCESS:
            return normalizeFilters(state, action.response.genres);
        case TOGGLE_GENRE:
            return toggleFilter(state, action.filterName, 'checked');
        default:
            return state;
    }
};

const tags = (state = {}, action) => {
    switch (action.type) {
        case RECEIVE_FILTERS_FAILURE:
            return {};
        case RECEIVE_FILTERS_SUCCESS:
            return normalizeFilters(
                state,
                addExcludedFilters(state, action.response.tags)
            );
        case TOGGLE_TAG:
            return toggleFilter(state, action.filterName, 'checked');
        case EXCLUDE_TAG:
            return toggleFilter(state, action.filterName, 'excluded');
        default:
            return state;
    }
};

const readStatus = (state = '', action) => {
    switch (action.type) {
        case CHANGE_READ_STATUS:
            return action.filterValue;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_FILTERS:
            return true;
        case RECEIVE_FILTERS_SUCCESS:
        case RECEIVE_FILTERS_FAILURE:
            return false;
        default:
            return state;
    }
};

const filters = combineReducers({
    genres,
    tags,
    readStatus,
    isFetching
});

export default filters;

export const getFilterNames = filters => filters.map(obj => obj.name);

export const getAllFilterOptions = (state, filterName) =>
    Object.values(get(state, `filters.${filterName}`, {}));

export const getActiveFilters = filterData => {
    const activeFilters = filterData.filter(
        value => value.checked && !value.excluded
    );
    return getFilterNames(activeFilters);
};

export const getExcludedFilters = filterData => {
    const excludedFilters = filterData.filter(value => value.excluded);
    return getFilterNames(excludedFilters);
};

export const getGenres = state => getAllFilterOptions(state, 'genres');
export const getTags = state => getAllFilterOptions(state, 'tags');

export const getActiveGenres = state => getActiveFilters(getGenres(state));
export const getActiveTags = state => getActiveFilters(getTags(state));
export const getExcludedTags = state => getExcludedFilters(getTags(state));

export const getReadStatus = state => get(state, 'filters.readStatus');
