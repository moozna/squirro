import { combineReducers } from 'redux';
import {
    CHANGE_SEARCH_VALUE,
    CLEAR_SEARCH_VALUE,
    REQUEST_SUGGESTIONS,
    RECEIVE_SUGGESTIONS_SUCCESS,
    RECEIVE_SUGGESTIONS_FAILURE,
    CLEAR_SUGGESTIONS
} from '../constants';
import get from '../../utils/get';

const searchFieldValue = (state = '', action) => {
    switch (action.type) {
        case CHANGE_SEARCH_VALUE:
            return action.value;
        case CLEAR_SEARCH_VALUE:
            return '';
        default:
            return state;
    }
};

const suggestions = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_SUGGESTIONS:
        case RECEIVE_SUGGESTIONS_FAILURE:
        case CLEAR_SEARCH_VALUE:
        case CLEAR_SUGGESTIONS:
            return {};
        case RECEIVE_SUGGESTIONS_SUCCESS:
            return action.response;
        default:
            return state;
    }
};

const isFetching = (state = false, action) => {
    switch (action.type) {
        case REQUEST_SUGGESTIONS:
            return true;
        case RECEIVE_SUGGESTIONS_SUCCESS:
        case RECEIVE_SUGGESTIONS_FAILURE:
            return false;
        default:
            return state;
    }
};

const search = combineReducers({
    searchFieldValue,
    suggestions,
    isFetching
});

export default search;

export const getSearchFieldValue = state =>
    get(state, 'search.searchFieldValue');
export const getSearchSuggestions = state =>
    [].concat(...Object.values(get(state, 'search.suggestions')));
