import reducer, * as selectors from './rangeReducer';
import {
    CHANGE_OWN_RATING_RANGE,
    CHANGE_AMAZON_RATING_RANGE,
    CHANGE_GOODREADS_RATING_RANGE,
    REQUEST_RANGE_FILTERS,
    RECEIVE_RANGE_FILTERS_SUCCESS,
    RECEIVE_RANGE_FILTERS_FAILURE,
    CHANGE_YEAR_RANGE,
    CHANGE_PAGE_RANGE,
    CHANGE_INTEREST_RANGE
} from '../constants';

const initialState = {
    ownRating: [],
    amazonRating: [],
    goodreadsRating: [],
    year: {},
    pages: {},
    interestRange: [],
    isFetching: false
};

describe('range reducer', () => {
    test('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    describe('CHANGE_OWN_RATING_RANGE', () => {
        test('sets the given values as ownRating', () => {
            const ratingValues = [1, 3];
            const currentState = {};

            const action = {
                type: CHANGE_OWN_RATING_RANGE,
                ratingValues
            };

            const expectedState = {
                ...initialState,
                ownRating: ratingValues
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CHANGE_AMAZON_RATING_RANGE', () => {
        test('sets the given values as amazonRating', () => {
            const ratingValues = [1, 3];
            const currentState = {};

            const action = {
                type: CHANGE_AMAZON_RATING_RANGE,
                ratingValues
            };

            const expectedState = {
                ...initialState,
                amazonRating: ratingValues
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CHANGE_GOODREADS_RATING_RANGE', () => {
        test('sets the given values as goodreadsRating', () => {
            const ratingValues = [1, 3];
            const currentState = {};

            const action = {
                type: CHANGE_GOODREADS_RATING_RANGE,
                ratingValues
            };

            const expectedState = {
                ...initialState,
                goodreadsRating: ratingValues
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('REQUEST_RANGE_FILTERS', () => {
        test('starts requesting range filters', () => {
            const currentState = {};

            const action = {
                type: REQUEST_RANGE_FILTERS
            };

            const expectedState = {
                ...initialState,
                isFetching: true
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('RECEIVE_RANGE_FILTERS_SUCCESS', () => {
        const testYearRange = { min: 1810, max: 2055 };
        const testPageRange = { min: 12, max: 2000 };
        const mockResponse = {
            year: { min: 1980, max: 2017 },
            pages: { min: 50, max: 1300 }
        };

        test('stops requesting range filters on success', () => {
            const currentState = {
                isFetching: true
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: []
                },
                pages: {
                    ...mockResponse.pages,
                    values: []
                },
                isFetching: false
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('returns new range filters on success', () => {
            const currentState = {
                year: {
                    ...testYearRange,
                    values: []
                },
                pages: {
                    ...testPageRange,
                    values: []
                }
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: []
                },
                pages: {
                    ...mockResponse.pages,
                    values: []
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('previously selected range remains when it is in range', () => {
            const currentState = {
                year: {
                    ...testYearRange,
                    values: [1994, 2000]
                },
                pages: {
                    ...testPageRange,
                    values: [120, 450]
                }
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: [1994, 2000]
                },
                pages: {
                    ...mockResponse.pages,
                    values: [120, 450]
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('selected min value changes to mimimum, when it is not in range', () => {
            const currentState = {
                year: {
                    ...testYearRange,
                    values: [1940, 2000]
                },
                pages: {
                    ...testPageRange,
                    values: [40, 450]
                }
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: [1980, 2000]
                },
                pages: {
                    ...mockResponse.pages,
                    values: [50, 450]
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('selected max value changes to maximum, when it is not in range', () => {
            const currentState = {
                year: {
                    ...testYearRange,
                    values: [1994, 2020]
                },
                pages: {
                    ...testPageRange,
                    values: [120, 1455]
                }
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: [1994, 2017]
                },
                pages: {
                    ...mockResponse.pages,
                    values: [120, 1300]
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        test('removes selected values, when both are out of range', () => {
            const currentState = {
                year: {
                    ...testYearRange,
                    values: [1940, 2020]
                },
                pages: {
                    ...testPageRange,
                    values: [40, 1455]
                }
            };

            const action = {
                type: RECEIVE_RANGE_FILTERS_SUCCESS,
                response: mockResponse
            };

            const expectedState = {
                ...initialState,
                year: {
                    ...mockResponse.year,
                    values: []
                },
                pages: {
                    ...mockResponse.pages,
                    values: []
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });

        describe('RECEIVE_RANGE_FILTERS_FAILURE', () => {
            test('stops requesting filters on failure', () => {
                const currentState = {
                    isFetching: true
                };
                const action = {
                    type: RECEIVE_RANGE_FILTERS_FAILURE,
                    message: 'error'
                };

                const expectedState = {
                    ...initialState
                };

                expect(reducer(currentState, action)).toEqual(expectedState);
            });

            test('clears range filters on failure', () => {
                const currentState = {
                    year: {
                        ...testYearRange,
                        values: [1940, 2020]
                    },
                    pages: {
                        ...testPageRange,
                        values: [40, 1455]
                    }
                };

                const action = {
                    type: RECEIVE_RANGE_FILTERS_FAILURE,
                    message: 'error'
                };

                const expectedState = {
                    ...initialState
                };

                expect(reducer(currentState, action)).toEqual(expectedState);
            });
        });
    });

    describe('CHANGE_YEAR_RANGE', () => {
        test('sets the given values as year range', () => {
            const rangeValues = [1920, 2006];
            const currentState = {};

            const action = {
                type: CHANGE_YEAR_RANGE,
                values: rangeValues
            };

            const expectedState = {
                ...initialState,
                year: {
                    values: rangeValues
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CHANGE_PAGE_RANGE', () => {
        test('sets the given values as page range', () => {
            const rangeValues = [77, 450];
            const currentState = {};

            const action = {
                type: CHANGE_PAGE_RANGE,
                values: rangeValues
            };

            const expectedState = {
                ...initialState,
                pages: {
                    values: rangeValues
                }
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('CHANGE_INTEREST_RANGE', () => {
        test('sets the given values as interest range', () => {
            const rangeValues = [2, 3];
            const currentState = {};

            const action = {
                type: CHANGE_INTEREST_RANGE,
                values: rangeValues
            };

            const expectedState = {
                ...initialState,
                interestRange: rangeValues
            };

            expect(reducer(currentState, action)).toEqual(expectedState);
        });
    });

    describe('selectors', () => {
        test('selects the selected range of own rating from state', () => {
            const ownRating = [2, 4];
            const currentState = {
                ranges: {
                    ownRating
                }
            };

            expect(selectors.getOwnRating(currentState)).toEqual(ownRating);
        });

        test('selects the selected range of amazon rating from state', () => {
            const amazonRating = [1, 3];
            const currentState = {
                ranges: {
                    amazonRating
                }
            };

            expect(selectors.getAmazonRating(currentState)).toEqual(
                amazonRating
            );
        });

        test('selects the selected range of goodreads rating from state', () => {
            const goodreadsRating = [3, 5];
            const currentState = {
                ranges: {
                    goodreadsRating
                }
            };

            expect(selectors.getGoodreadsRating(currentState)).toEqual(
                goodreadsRating
            );
        });

        test('selects the year range data from state', () => {
            const year = {
                min: 1982,
                max: 2017,
                values: [1999, 2006]
            };
            const currentState = {
                ranges: {
                    year
                }
            };

            expect(selectors.getYearRanges(currentState)).toEqual(year);
        });

        test('selects the selected year values from state', () => {
            const year = {
                min: 1982,
                max: 2017,
                values: [1999, 2006]
            };
            const currentState = {
                ranges: {
                    year
                }
            };

            expect(selectors.getYear(currentState)).toEqual([1999, 2006]);
        });

        test('selects the page range data from state', () => {
            const pages = {
                min: 55,
                max: 1500,
                values: [77, 450]
            };
            const currentState = {
                ranges: {
                    pages
                }
            };

            expect(selectors.getPageRanges(currentState)).toEqual(pages);
        });

        test('selects the selected page values from state', () => {
            const pages = {
                min: 55,
                max: 1500,
                values: [77, 450]
            };
            const currentState = {
                ranges: {
                    pages
                }
            };

            expect(selectors.getPages(currentState)).toEqual([77, 450]);
        });

        test('selects the selected interest range values from state', () => {
            const interestRange = [2, 3];
            const currentState = {
                ranges: {
                    interestRange
                }
            };

            expect(selectors.getInterestRange(currentState)).toEqual([2, 3]);
        });
    });
});
