import { combineReducers } from 'redux';
import {
    IMPORT_BOOK,
    IMPORT_BOOK_SUCCESS,
    IMPORT_BOOK_FAILURE,
    CLEAR_IMPORTED_DATA,
    UPDATE_DUPLICATE_KEYS,
    CREATE_INITIAL_BOOK,
    ADD_NEW_BOOK_SUCCESS
} from '../constants';
import get from '../../utils/get';
import isEqual from '../../utils/isEqual';
import isEmptyObject from '../../utils/isEmptyObject';
import normalizeHtmlText from '../../utils/normalizeHtmlText';

const INITIAL_PROPS = {
    title: 'goodreads',
    seriesTitle: 'goodreads',
    seriesIndex: 'goodreads',
    author: 'goodreads',
    description: 'goodreads',
    genres: 'goodreads',
    publicationYear: 'goodreads',
    pages: 'amazon'
};

const getDuplicateKeys = (data, otherData) => {
    const result = {};

    if (!isEmptyObject(data) && !isEmptyObject(otherData)) {
        Object.entries(data).forEach(([key, value]) => {
            if (isEqual(value, otherData[key])) {
                result[key] = true;
            }
        });
    }

    return result;
};

const getInitialValues = sitesData => {
    const result = {};

    Object.entries(sitesData).forEach(([siteName, siteData]) => {
        Object.entries(siteData).forEach(([prop, value]) => {
            if (value) {
                if (!result[prop]) {
                    result[prop] = value;
                } else if (INITIAL_PROPS[prop] === siteName) {
                    result[prop] = value;
                }
            }
        });
    });

    if (result.description) {
        result.description = normalizeHtmlText(result.description);
    }

    return result;
};

const data = (state = {}, action) => {
    switch (action.type) {
        case CLEAR_IMPORTED_DATA:
        case ADD_NEW_BOOK_SUCCESS:
            return {};
        case IMPORT_BOOK_FAILURE:
            return {
                ...state,
                [action.siteName]: {}
            };
        case IMPORT_BOOK_SUCCESS:
            return {
                ...state,
                [action.siteName]: action.response
            };
        default:
            return state;
    }
};

const duplicates = (state = {}, action) => {
    switch (action.type) {
        case IMPORT_BOOK:
        case CLEAR_IMPORTED_DATA:
        case ADD_NEW_BOOK_SUCCESS:
            return {};
        case UPDATE_DUPLICATE_KEYS:
            return getDuplicateKeys(action.response, action.otherData);
        default:
            return state;
    }
};

const initialBook = (state = {}, action) => {
    switch (action.type) {
        case IMPORT_BOOK:
        case CLEAR_IMPORTED_DATA:
        case ADD_NEW_BOOK_SUCCESS:
            return {};
        case CREATE_INITIAL_BOOK:
            return getInitialValues(action.sitesData);
        default:
            return state;
    }
};

const isFetching = (state = {}, action) => {
    switch (action.type) {
        case IMPORT_BOOK:
            return {
                ...state,
                [action.siteName]: true
            };
        case IMPORT_BOOK_SUCCESS:
        case IMPORT_BOOK_FAILURE:
            return {
                ...state,
                [action.siteName]: false
            };
        default:
            return state;
    }
};

const importing = combineReducers({
    data,
    duplicates,
    initialBook,
    isFetching
});

export default importing;

export const getImportedAmazonData = state =>
    get(state, 'importing.data.amazon', {});
export const getImportedGoodreadsData = state =>
    get(state, 'importing.data.goodreads', {});
export const getSitesData = state => get(state, 'importing.data', {});
export const getDuplicateData = state => get(state, 'importing.duplicates', {});
export const getInitialBook = state => get(state, 'importing.initialBook', {});
export const getIsFetching = state => get(state, 'importing.isFetching', {});
