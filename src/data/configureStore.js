import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { logger } from 'redux-logger';
import thunk from 'redux-thunk';
import {
    middleware as routesMiddleware,
    enhancer as routesEnhancer,
    reducer as routesReducer
} from '../app/routes';
import books from './reducers/booksReducer';
import selectedBook from './reducers/selectedBookReducer';
import pagination from './reducers/paginationReducer';
import filters from './reducers/filtersReducer';
import ranges from './reducers/rangeReducer';
import suggestions from './reducers/suggestionsReducer';
import shelves from './reducers/shelvesReducer';
import search from './reducers/searchReducer';
import sorting from './reducers/sortingReducer';
import importing from './reducers/importReducer';
import page from './reducers/pageReducer';
import errors from './reducers/errorsReducer';

const rootReducer = combineReducers({
    books,
    selectedBook,
    pagination,
    filters,
    ranges,
    suggestions,
    shelves,
    search,
    sorting,
    importing,
    page,
    errors,
    location: routesReducer
});

const configureStore = () => {
    const middlewares = [thunk, routesMiddleware];
    if (process.env.NODE_ENV === 'development') {
        middlewares.push(logger);
    }

    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        rootReducer,
        composeEnhancers(routesEnhancer, applyMiddleware(...middlewares))
    );
};

export default configureStore;
