import { string, number, bool, shape, arrayOf, oneOfType } from 'prop-types';

export const ratingType = shape({ rating: number, numberOfRatings: number });
export const coverType = shape({ url: string, thumbnail: string });
export const coversType = arrayOf(coverType);

export const bookType = shape({
    _id: string,
    asin: string,
    goodreadsId: number,
    title: string,
    author: arrayOf(string),
    seriesTitle: string,
    seriesIndex: string,
    rating: number,
    interestLevel: number,
    amazonRating: ratingType,
    goodreadsRating: ratingType,
    genres: arrayOf(string),
    tags: arrayOf(string),
    publicationYear: number,
    pages: number,
    numberOfTimesRead: number,
    description: string,
    covers: coversType
});

const numberFieldType = oneOfType([string, number]);

export const valuesType = shape({
    _id: string,
    asin: string,
    goodreadsId: numberFieldType,
    title: string,
    author: arrayOf(string),
    seriesTitle: string,
    seriesIndex: string,
    rating: numberFieldType,
    interestLevel: numberFieldType,
    amazonRating: shape({
        rating: numberFieldType,
        numberOfRatings: numberFieldType
    }),
    goodreadsRating: shape({
        rating: numberFieldType,
        numberOfRatings: numberFieldType
    }),
    genres: arrayOf(string),
    tags: arrayOf(string),
    publicationYear: numberFieldType,
    pages: numberFieldType,
    numberOfTimesRead: numberFieldType,
    description: string,
    covers: coversType
});

export const touchedType = shape({
    asin: bool,
    goodreadsId: bool,
    title: bool,
    author: bool,
    seriesTitle: bool,
    seriesIndex: bool,
    amazonRating: shape({
        rating: bool,
        numberOfRatings: bool
    }),
    goodreadsRating: shape({
        rating: bool,
        numberOfRatings: bool
    }),
    genres: bool,
    tags: bool,
    publicationYear: bool,
    pages: bool,
    numberOfTimesRead: bool,
    description: bool
});

export const errorsType = shape({
    asin: string,
    goodreadsId: string,
    title: string,
    author: string,
    seriesTitle: string,
    seriesIndex: string,
    amazonRating: shape({
        rating: string,
        numberOfRatings: string
    }),
    goodreadsRating: shape({
        rating: string,
        numberOfRatings: string
    }),
    genres: string,
    tags: string,
    publicationYear: string,
    pages: string,
    numberOfTimesRead: string,
    description: string
});

export const suggestionsType = arrayOf(shape({ name: string }));

export const suggestionGroupType = shape({
    genres: suggestionsType,
    tags: suggestionsType,
    authors: suggestionsType,
    series: suggestionsType
});

export const amazonDataType = shape({
    asin: string,
    title: string,
    author: arrayOf(string),
    seriesTitle: string,
    seriesIndex: string,
    rating: ratingType,
    genres: arrayOf(string),
    publicationYear: number,
    pages: number,
    description: string
});

export const goodreadsDataType = shape({
    goodreadsId: oneOfType([string, number]),
    title: string,
    author: arrayOf(string),
    seriesTitle: string,
    seriesIndex: string,
    rating: ratingType,
    genres: arrayOf(string),
    publicationYear: number,
    pages: number,
    description: string
});

export const initialBookType = shape({
    goodreadsId: oneOfType([string, number]),
    asin: string,
    title: string,
    author: arrayOf(string),
    seriesTitle: string,
    seriesIndex: string,
    goodreadsRating: ratingType,
    amazonRating: ratingType,
    genres: arrayOf(string),
    publicationYear: number,
    pages: number,
    description: string
});
