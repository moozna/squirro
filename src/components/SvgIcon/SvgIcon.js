import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './SvgIcon.css';

const SvgIcon = ({ children, className, iconSize }) => {
    const classes = classnames(styles.svgIcon, className);

    return (
        <svg
            className={classes}
            viewBox="0 0 24 24"
            width={`${iconSize}px`}
            height={`${iconSize}px`}
        >
            {children}
        </svg>
    );
};

SvgIcon.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    iconSize: PropTypes.number
};

SvgIcon.defaultProps = {
    children: null,
    className: '',
    iconSize: 24
};

export default SvgIcon;
