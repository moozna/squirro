import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SvgIcon from './SvgIcon';

describe('<SvgIcon />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<SvgIcon />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<SvgIcon>{testChildren}</SvgIcon>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<SvgIcon className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    test('renders an svg with the specified size', () => {
        const iconSize = 16;
        const wrapper = shallow(<SvgIcon iconSize={iconSize} />);
        const expected = `${iconSize}px`;

        expect(wrapper.props().width).toBe(expected);
        expect(wrapper.props().height).toBe(expected);
    });
});
