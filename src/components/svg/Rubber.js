import React from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';

const Rubber = () => (
    <SvgIcon>
        <path d="M20.57 8.023l-3.424-3.425a1.451 1.451 0 0 0-2.017 0l-7.142 7.14 5.442 5.442 7.14-7.141A1.45 1.45 0 0 0 21 9.019a1.384 1.384 0 0 0-.43-.997z" />
        <path d="M16.285 19.131h-4.76l1.428-1.451-5.442-5.465-2.13 2.131a1.474 1.474 0 0 0 0 2.04L8.1 19.13H3.34a.34.34 0 0 0 0 .68h12.946a.34.34 0 0 0 0-.68z" />
    </SvgIcon>
);

export default Rubber;
