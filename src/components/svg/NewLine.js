import React from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';

const NewLine = () => (
    <SvgIcon>
        <path d="M19.27 5.4h-5.95V6.7h5.95c.36 0 .66.3.66.67v5.28c0 .37-.3.66-.66.66h-14L8.3 10.3l-.93-.93-4.62 4.61 4.63 4.63.93-.94-3.03-3.03h13.99c1.1 0 1.98-.89 1.98-1.98V7.38c0-1.1-.89-1.99-1.98-1.99z" />
    </SvgIcon>
);

export default NewLine;
