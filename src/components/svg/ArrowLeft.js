import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '../SvgIcon/SvgIcon';

const ArrowLeft = ({ className, iconSize }) => (
    <SvgIcon className={className} iconSize={iconSize}>
        <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z" />
    </SvgIcon>
);

ArrowLeft.propTypes = {
    className: PropTypes.string,
    iconSize: PropTypes.number
};

ArrowLeft.defaultProps = {
    className: '',
    iconSize: 24
};

export default ArrowLeft;
