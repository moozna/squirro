import React from 'react';
import PropTypes from 'prop-types';

const Squirrel = ({ imageClass }) => (
    <svg className={imageClass} viewBox="0 0 332 110">
        <path d="M302.9 4.8c-13.5.5-15.5 6.5-28.5 0-3 6-3.1 20.3.5 33-38.5-27.5-97.6-34.7-129.5-37-98-7-159.3 31.9-142.7 76.3 26.9 31.7 50.4 4.4 90.2-26.8 50.5-39.8 105.5-33 105.5-33-23 6.8-60 36.5-72 54 22-10.5 53.5-36 100.5-27s64.5 60 66.5 67.5c2.1-6-10-60.5 36-72.5 1.5-19-13-35-26.5-34.5zm10.2 23.5c-3.2.9-5.3-3-5.3-3 3.3 1 6-.5 7.8-2.3 0 .1.4 4.5-2.5 5.3z" />
    </svg>
);

Squirrel.propTypes = {
    imageClass: PropTypes.string
};

Squirrel.defaultProps = {
    imageClass: ''
};

export default Squirrel;
