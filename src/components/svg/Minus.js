import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '../SvgIcon/SvgIcon';

const Minus = ({ className, iconSize }) => (
    <SvgIcon className={className} iconSize={iconSize}>
        <path d="M19 13H5v-2h14v2z" />
    </SvgIcon>
);

Minus.propTypes = {
    className: PropTypes.string,
    iconSize: PropTypes.number
};

Minus.defaultProps = {
    className: '',
    iconSize: 24
};

export default Minus;
