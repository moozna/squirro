import React from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';

const User = () => (
    <SvgIcon>
        <path d="M17.4 17.9c0-3-2.4-5.5-5.5-5.5s-5.5 2.4-5.5 5.5c0 0 1.5 1.4 5.5 1.4s5.5-1.4 5.5-1.4z" />
        <circle cx="11.9" cy="8.3000002" r="3" />
    </SvgIcon>
);

export default User;
