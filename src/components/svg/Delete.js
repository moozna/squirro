import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '../SvgIcon/SvgIcon';

const Delete = ({ className, iconSize }) => (
    <SvgIcon className={className} iconSize={iconSize}>
        <path d="M7.92 6.5L12 10.59l4.07-4.08 1.4 1.41L13.42 12l4.09 4.08-1.42 1.41L12 13.4 7.92 17.5 6.5 16.08 10.59 12 6.5 7.92z" />
    </SvgIcon>
);

Delete.propTypes = {
    className: PropTypes.string,
    iconSize: PropTypes.number
};

Delete.defaultProps = {
    className: '',
    iconSize: 24
};

export default Delete;
