import React from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';

const ArrowDown = () => (
    <SvgIcon>
        <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z" />
    </SvgIcon>
);

export default ArrowDown;
