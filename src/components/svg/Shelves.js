import React from 'react';
import SvgIcon from '../SvgIcon/SvgIcon';

const Shelves = () => (
    <SvgIcon>
        <path d="M3 18.02v2.1h18v-2.1zM5.14 7.47v9.46h2.64V7.47zM8.84 3.87v13.06h2.63V3.87zM12.53 5.48v11.45h2.63V5.48zM16.22 7.52v9.4h2.64v-9.4z" />
    </SvgIcon>
);

export default Shelves;
