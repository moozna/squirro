import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '../SvgIcon/SvgIcon';

const ArrowRight = ({ className, iconSize }) => (
    <SvgIcon className={className} iconSize={iconSize}>
        <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
    </SvgIcon>
);

ArrowRight.propTypes = {
    className: PropTypes.string,
    iconSize: PropTypes.number
};

ArrowRight.defaultProps = {
    className: '',
    iconSize: 24
};

export default ArrowRight;
