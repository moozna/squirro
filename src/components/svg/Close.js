import React from 'react';
import PropTypes from 'prop-types';
import SvgIcon from '../SvgIcon/SvgIcon';

const Close = ({ iconSize }) => (
    <SvgIcon iconSize={iconSize}>
        <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
    </SvgIcon>
);

Close.propTypes = {
    iconSize: PropTypes.number
};

Close.defaultProps = {
    iconSize: 24
};

export default Close;
