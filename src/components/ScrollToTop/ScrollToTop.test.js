import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ScrollToTop from './ScrollToTop';

describe('<ScrollToTop />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ScrollToTop />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('showScrollToTop', () => {
        test('renders a button when showScrollToTop is true', () => {
            const wrapper = shallow(<ScrollToTop />);

            wrapper.setState({ showScrollToTop: true });
            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('sets showScrollToTop to true when scroll position is larger than showAtPosition', () => {
            const wrapper = mount(<ScrollToTop showAtPosition={150} />);

            wrapper.instance().scrollComponent = {
                parentNode: { scrollTop: 1500 }
            };

            wrapper.instance().scrollListener();
            expect(wrapper.state('showScrollToTop')).toBe(true);
        });

        test('sets showScrollToTop to false when scroll position is smaller than showAtPosition', () => {
            const wrapper = mount(<ScrollToTop showAtPosition={150} />);

            wrapper.setState({ showScrollToTop: true });
            wrapper.instance().scrollComponent = {
                parentNode: {
                    scrollTop: 50
                }
            };

            wrapper.instance().scrollListener();
            expect(wrapper.state('showScrollToTop')).toBe(false);
        });
    });

    test('sets scrollToTop value after clicking on the button', () => {
        const mockSetScrollTop = jest.spyOn(
            ScrollToTop.prototype,
            'setScrollTop'
        );
        let time = 12000;
        jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb => {
            time += 1500;
            cb(time);
        });
        const wrapper = shallow(<ScrollToTop />);

        wrapper.setState({ showScrollToTop: true });
        wrapper.instance().scrollComponent = {
            parentNode: { scrollTop: 300 }
        };
        wrapper.find('Button').simulate('click');

        expect(mockSetScrollTop.mock.calls.length).toBe(2);
        expect(mockSetScrollTop.mock.calls[0][0]).toBe(300);
        expect(mockSetScrollTop.mock.calls[1][0]).toBe(-150);
        mockSetScrollTop.mockRestore();
    });

    test('has a ref on the parent node', () => {
        const wrapper = mount(<ScrollToTop />);

        expect(wrapper.instance().scrollComponent).toBeDefined();
    });

    describe('event listeners on parent node', () => {
        test('attaches event listeners after mount', () => {
            const mockAttachScrollListener = jest.spyOn(
                ScrollToTop.prototype,
                'attachScrollListener'
            );

            mount(<ScrollToTop />);

            expect(mockAttachScrollListener.mock.calls.length).toBe(1);
            mockAttachScrollListener.mockRestore();
        });

        test('detaches event listeners when the component is unmounted', () => {
            const mockDetachScrollListener = jest.spyOn(
                ScrollToTop.prototype,
                'detachScrollListener'
            );
            const wrapper = mount(<ScrollToTop />);

            wrapper.unmount();

            expect(mockDetachScrollListener.mock.calls.length).toBe(1);
            mockDetachScrollListener.mockRestore();
        });
    });

    describe('does not throw', () => {
        test('when parentNode is missing during scrollUp', () => {
            const mockSetScrollTop = jest.spyOn(
                ScrollToTop.prototype,
                'setScrollTop'
            );
            jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb =>
                cb(1000)
            );
            const wrapper = shallow(<ScrollToTop />);

            wrapper.setState({ showScrollToTop: true });
            wrapper.find('Button').simulate('click');

            expect(mockSetScrollTop.mock.calls.length).toBe(1);
            expect(mockSetScrollTop.mock.calls[0][0]).toBe(0);

            mockSetScrollTop.mockRestore();
        });

        test('when parentNode is missing during unmount', () => {
            const mockDetachScrollListener = jest.spyOn(
                ScrollToTop.prototype,
                'detachScrollListener'
            );
            const wrapper = shallow(<ScrollToTop />);

            wrapper.unmount();

            expect(mockDetachScrollListener.mock.calls.length).toBe(1);
            mockDetachScrollListener.mockRestore();
        });
    });
});
