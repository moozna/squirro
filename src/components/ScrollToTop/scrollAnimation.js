function easeInOutCubic(currentTime, startValue, finalValue, duration) {
    const delta = finalValue - startValue;
    const halfDuration = duration / 2;
    const halfDelta = delta / 2;
    let time = currentTime / halfDuration;

    if (time < 1) {
        return halfDelta * time * time * time + startValue;
    }

    time -= 2;
    return halfDelta * (time * time * time + 2) + startValue;
}

export default easeInOutCubic;
