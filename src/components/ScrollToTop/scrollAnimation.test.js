import scrollAnimation from './scrollAnimation';

describe('scrollAnimation()', () => {
    test('returns zero', () => {
        expect(scrollAnimation(1000, 300, 0, 1000)).toBe(0);
    });

    test('returns positive value', () => {
        expect(scrollAnimation(500, 300, 0, 1000)).toBe(150);
    });

    test('returns negative value', () => {
        expect(scrollAnimation(5000, 300, 0, 1000)).toBe(-76800);
    });
});
