import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import styles from './ScrollToTop.css';
import Button from '../Button/Button';
import ArrowUp from '../svg/ArrowUp';
import scrollAnimation from './scrollAnimation';
import get from '../../utils/get';

class ScrollToTop extends Component {
    state = {
        showScrollToTop: false
    };

    componentDidMount() {
        this.attachScrollListener();
    }

    componentWillUnmount() {
        this.detachScrollListener();
    }

    getScrollElement() {
        return get(this.scrollComponent, 'parentNode', null);
    }

    getScrollTop() {
        const scrollElement = this.getScrollElement();
        return get(scrollElement, 'scrollTop', 0);
    }

    setScrollTop(value) {
        const scrollElement = this.getScrollElement();
        if (scrollElement) {
            scrollElement.scrollTop = value;
        }
    }

    scrollListener = () => {
        const { showAtPosition } = this.props;
        const scrollPosition = this.getScrollTop();
        const showScrollToTop = scrollPosition > showAtPosition;

        this.setState({ showScrollToTop });
    };

    scrollUp = () => {
        this.initScrollTop = this.getScrollTop();
        this.startTime = null;
        window.requestAnimationFrame(this.srollStep);
    };

    srollStep = timestamp => {
        const { stopPosition, animationDuration } = this.props;

        if (!this.startTime) {
            this.startTime = timestamp;
        }

        const delta = timestamp - this.startTime;
        const newPosition = scrollAnimation(
            delta,
            this.initScrollTop,
            stopPosition,
            animationDuration
        );
        this.setScrollTop(newPosition);

        if (newPosition > stopPosition) {
            window.requestAnimationFrame(this.srollStep);
        }
    };

    saveRef = node => {
        this.scrollComponent = node;
    };

    attachScrollListener() {
        const scrollElement = this.getScrollElement();
        if (scrollElement) {
            scrollElement.addEventListener('scroll', this.scrollListener);
        }
    }

    detachScrollListener() {
        const scrollElement = this.getScrollElement();
        if (scrollElement) {
            scrollElement.removeEventListener('scroll', this.scrollListener);
        }
    }

    render() {
        const { showScrollToTop } = this.state;
        const { children } = this.props;

        return (
            <Fragment>
                {children}
                <div className={styles.scrollToTop} ref={this.saveRef}>
                    {showScrollToTop && (
                        <Button
                            color="magenta"
                            shadow="outer"
                            onClick={this.scrollUp}
                        >
                            <ArrowUp />
                        </Button>
                    )}
                </div>
            </Fragment>
        );
    }
}

ScrollToTop.propTypes = {
    children: PropTypes.node,
    showAtPosition: PropTypes.number,
    stopPosition: PropTypes.number,
    animationDuration: PropTypes.number
};

ScrollToTop.defaultProps = {
    children: null,
    showAtPosition: window.innerHeight,
    stopPosition: 0,
    animationDuration: 1000
};

export default ScrollToTop;
