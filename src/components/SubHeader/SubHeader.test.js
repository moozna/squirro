import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SubHeader from './SubHeader';

describe('<SubHeader />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<SubHeader />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided caption', () => {
        const wrapper = shallow(<SubHeader caption="title" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a centered header', () => {
        const wrapper = shallow(<SubHeader caption="title" />);

        expect(wrapper.hasClass('center')).toBe(true);
    });

    test('renders a left aligned header', () => {
        const wrapper = shallow(<SubHeader caption="title" align="left" />);

        expect(wrapper.hasClass('left')).toBe(true);
    });
});
