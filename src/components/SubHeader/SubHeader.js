import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './SubHeader.css';

const SubHeader = ({ caption, align }) => {
    const classes = classnames(styles.subHeader, { [styles[align]]: align });
    return <h5 className={classes}>{caption}</h5>;
};

SubHeader.propTypes = {
    caption: PropTypes.string,
    align: PropTypes.oneOf(['center', 'left'])
};

SubHeader.defaultProps = {
    caption: '',
    align: 'center'
};

export default SubHeader;
