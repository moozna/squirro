import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './ButtonGroup.css';
import noop from '../../utils/noop';
import ArrowDown from '../svg/ArrowDown';
import ArrowUp from '../svg/ArrowUp';

class RadioButton extends Component {
    handleClick = () => {
        const { value, disabled, onChange } = this.props;

        if (!disabled) {
            onChange(value);
        }
    };

    renderArrow() {
        const { checked, sortOrder } = this.props;

        if (!checked || !sortOrder) {
            return null;
        }

        return sortOrder === 1 ? <ArrowUp /> : <ArrowDown />;
    }

    render() {
        const {
            children,
            name,
            label,
            checked,
            disabled,
            withDropdown
        } = this.props;

        const classes = classnames(styles.field, {
            [styles.selected]: checked,
            [styles.disabled]: disabled,
            [styles.withDropdown]: withDropdown
        });

        return (
            <label className={classes}>
                <input
                    className={styles.input}
                    type="radio"
                    name={name}
                    checked={checked}
                    disabled={disabled}
                    onChange={noop}
                    onClick={this.handleClick}
                />
                {children}
                {label ? <span className={styles.text}>{label}</span> : null}
                {this.renderArrow()}
            </label>
        );
    }
}

RadioButton.propTypes = {
    children: PropTypes.node,
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    sortOrder: PropTypes.oneOf([0, 1, -1]),
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    withDropdown: PropTypes.bool,
    onChange: PropTypes.func
};

RadioButton.defaultProps = {
    children: null,
    value: '',
    name: '',
    label: '',
    sortOrder: 0,
    checked: false,
    disabled: false,
    withDropdown: false,
    onChange: noop
};

export default RadioButton;
