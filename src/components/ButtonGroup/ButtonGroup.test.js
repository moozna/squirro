import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ButtonGroup from './ButtonGroup';

describe('<ButtonGroup />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ButtonGroup />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<ButtonGroup>{testChildren}</ButtonGroup>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
