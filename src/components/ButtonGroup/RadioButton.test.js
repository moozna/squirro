import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RadioButton from './RadioButton';

describe('<RadioButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<RadioButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<RadioButton>{testChildren}</RadioButton>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label', () => {
        const wrapper = shallow(<RadioButton label="myLabel" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a selected button', () => {
        const wrapper = shallow(<RadioButton checked />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with ascending sort arrow', () => {
        const wrapper = shallow(<RadioButton checked sortOrder={1} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with descending sort arrow', () => {
        const wrapper = shallow(<RadioButton checked sortOrder={-1} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled button', () => {
        const wrapper = shallow(<RadioButton disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button that contains dropdown', () => {
        const wrapper = shallow(<RadioButton withDropdown />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onChange()', () => {
        test('fires the onChange callback', () => {
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <RadioButton value="author" onChange={handleOnChange} />
            );

            wrapper.find('.input').simulate('click');
            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('author');
        });

        test('does not fire the onChange callback when the button is disabled', () => {
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <RadioButton onChange={handleOnChange} disabled />
            );

            wrapper.find('.input').simulate('click');
            expect(handleOnChange.mock.calls.length).toBe(0);
        });
    });
});
