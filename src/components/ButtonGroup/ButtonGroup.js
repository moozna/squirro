import React from 'react';
import PropTypes from 'prop-types';
import styles from './ButtonGroup.css';
import noop from '../../utils/noop';

const ButtonGroup = ({
    children,
    name,
    value,
    sortOrder,
    disabled,
    onSelectionChange
}) => (
    <div className={styles.buttonGroup}>
        {React.Children.map(children, child =>
            React.cloneElement(child, {
                checked: value === child.props.value,
                name,
                sortOrder,
                disabled: disabled || child.props.disabled,
                onChange: onSelectionChange
            })
        )}
    </div>
);

ButtonGroup.propTypes = {
    children: PropTypes.node,
    name: PropTypes.string,
    value: PropTypes.string,
    sortOrder: PropTypes.oneOf([0, 1, -1]),
    disabled: PropTypes.bool,
    onSelectionChange: PropTypes.func
};

ButtonGroup.defaultProps = {
    children: null,
    name: '',
    value: '',
    sortOrder: 0,
    disabled: false,
    onSelectionChange: noop
};

export default ButtonGroup;
