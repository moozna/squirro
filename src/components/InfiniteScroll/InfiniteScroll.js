import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';

class InfiniteScroll extends Component {
    componentDidMount() {
        this.attachScrollListenersIfNeeded();
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.hasMore && this.props.hasMore) {
            this.attachScrollListener();
        }

        if (prevProps.hasMore && !this.props.hasMore) {
            this.detachScrollListener();
        }
    }

    componentWillUnmount() {
        this.detachScrollListener();
    }

    scrollListener = () => {
        const { threshold, loadMore } = this.props;

        const element = this.scrollComponent;

        const offset =
            element.scrollHeight -
            element.parentNode.scrollTop -
            element.parentNode.clientHeight;

        if (offset < Number(threshold)) {
            loadMore();
        }
    };

    saveRef = node => {
        this.scrollComponent = node;
    };

    attachScrollListenersIfNeeded() {
        const { hasMore } = this.props;

        if (!hasMore) {
            return;
        }

        this.attachScrollListener();
    }

    attachScrollListener() {
        const scrollElement = this.scrollComponent.parentNode;
        scrollElement.addEventListener('scroll', this.scrollListener);
        scrollElement.addEventListener('resize', this.scrollListener);
    }

    detachScrollListener() {
        const scrollElement = this.scrollComponent.parentNode;
        scrollElement.removeEventListener('scroll', this.scrollListener);
        scrollElement.removeEventListener('resize', this.scrollListener);
    }

    render() {
        const {
            children,
            element,
            hasMore,
            threshold,
            loadMore,
            ...props
        } = this.props;

        const ElementType = element;
        return (
            <ElementType ref={this.saveRef} {...props}>
                {children}
            </ElementType>
        );
    }
}

InfiniteScroll.propTypes = {
    children: PropTypes.node,
    element: PropTypes.string,
    hasMore: PropTypes.bool,
    threshold: PropTypes.number,
    loadMore: PropTypes.func
};

InfiniteScroll.defaultProps = {
    children: null,
    element: 'div',
    hasMore: false,
    threshold: 250,
    loadMore: noop
};

export default InfiniteScroll;
