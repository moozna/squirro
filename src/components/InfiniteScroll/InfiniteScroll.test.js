import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import InfiniteScroll from './InfiniteScroll';

describe('<InfiniteScroll />', () => {
    const testChildren = (
        <div className="parent-class">
            <div>1</div>
            <div>2</div>
            <div>3</div>
        </div>
    );

    test('has the expected html structure', () => {
        const wrapper = shallow(<InfiniteScroll />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const wrapper = shallow(
            <InfiniteScroll>{testChildren}</InfiniteScroll>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('event listeners on parent node', () => {
        test('attaches event listeners when more elements are available', () => {
            const mockAttachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'attachScrollListener'
            );

            mount(<InfiniteScroll hasMore>{testChildren}</InfiniteScroll>);

            expect(mockAttachScrollListener.mock.calls.length).toBe(1);
            mockAttachScrollListener.mockRestore();
        });

        test('does not attach event listeners when more elements are not available', () => {
            const mockAttachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'attachScrollListener'
            );

            mount(<InfiniteScroll>{testChildren}</InfiniteScroll>);

            expect(mockAttachScrollListener.mock.calls.length).toBe(0);
            mockAttachScrollListener.mockRestore();
        });

        test('detaches event listeners when the component is unmounted', () => {
            const mockDetachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'detachScrollListener'
            );
            const wrapper = mount(
                <InfiniteScroll>{testChildren}</InfiniteScroll>
            );

            wrapper.unmount();

            expect(mockDetachScrollListener.mock.calls.length).toBe(1);
            mockDetachScrollListener.mockRestore();
        });

        test('attaches event listeners when the component is updated', () => {
            const mockAttachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'attachScrollListener'
            );
            const wrapper = mount(
                <InfiniteScroll>{testChildren}</InfiniteScroll>
            );

            expect(mockAttachScrollListener.mock.calls.length).toBe(0);
            wrapper.setProps({ hasMore: true });

            expect(mockAttachScrollListener.mock.calls.length).toBe(1);
            mockAttachScrollListener.mockRestore();
        });

        test('detaches event listeners when more elements are not available', () => {
            const mockAttachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'attachScrollListener'
            );
            const mockDetachScrollListener = jest.spyOn(
                InfiniteScroll.prototype,
                'detachScrollListener'
            );
            const wrapper = mount(
                <InfiniteScroll>{testChildren}</InfiniteScroll>
            );

            wrapper.setProps({ hasMore: true });
            expect(mockAttachScrollListener.mock.calls.length).toBe(1);
            wrapper.setProps({ hasMore: false });

            expect(mockDetachScrollListener.mock.calls.length).toBe(1);
            mockAttachScrollListener.mockRestore();
            mockDetachScrollListener.mockRestore();
        });
    });

    describe('loadMore()', () => {
        test('calls loadMore when scrolling is in the right position', () => {
            const mockLoadMore = jest.fn();
            const wrapper = mount(
                <InfiniteScroll loadMore={mockLoadMore}>
                    {testChildren}
                </InfiniteScroll>
            );

            wrapper.instance().scrollComponent = {
                scrollHeight: 1800,
                parentNode: {
                    scrollTop: 1500,
                    clientHeight: 280,
                    removeEventListener: () => {}
                }
            };

            wrapper.instance().scrollListener();
            expect(mockLoadMore.mock.calls.length).toBe(1);
        });

        test('does not call loadMore when scrolling is not in the right position', () => {
            const mockLoadMore = jest.fn();
            const wrapper = mount(
                <InfiniteScroll loadMore={mockLoadMore}>
                    {testChildren}
                </InfiniteScroll>
            );

            wrapper.instance().scrollComponent = {
                scrollHeight: 1800,
                parentNode: {
                    scrollTop: 1200,
                    clientHeight: 280,
                    removeEventListener: () => {}
                }
            };

            wrapper.instance().scrollListener();
            expect(mockLoadMore.mock.calls.length).toBe(0);
        });
    });
});
