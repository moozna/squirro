import React, { Component } from 'react';
import PropTypes from 'prop-types';

function withScrollIntoView(WrappedComponent, config = {}) {
    return class ScrollIntoView extends Component {
        static propTypes = {
            isSelected: PropTypes.bool
        };

        static defaultProps = {
            isSelected: false
        };

        componentDidMount() {
            this.scrollIntoView();
        }

        componentDidUpdate(prevProps) {
            if (!prevProps.isSelected) {
                this.scrollIntoView();
            }
        }

        getScrollParent(node) {
            if (node == null) {
                return null;
            }

            if (node.scrollHeight > node.clientHeight) {
                return node;
            }
            return this.getScrollParent(node.parentNode);
        }

        saveRef = node => {
            this.node = node;
        };

        isElementOutOfView() {
            const container = this.getScrollParent(this.node);

            if (container) {
                const containerTop = container.scrollTop;
                const containerBottom = containerTop + container.clientHeight;

                const elementTop = this.node.offsetTop;
                const elementBottom = elementTop + this.node.clientHeight;

                return (
                    elementTop < containerTop || elementBottom > containerBottom
                );
            }

            return false;
        }

        scrollIntoView() {
            const { isSelected } = this.props;
            const { behavior = 'auto' } = config;

            if (isSelected && this.isElementOutOfView()) {
                this.node.scrollIntoView({
                    block: 'center',
                    behavior
                });
            }
        }

        render() {
            return <WrappedComponent {...this.props} saveRef={this.saveRef} />;
        }
    };
}

export default withScrollIntoView;
