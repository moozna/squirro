import React, { Component } from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ScrollIntoView from './ScrollIntoView';

describe('<ScrollIntoView />', () => {
    class MockComponent extends Component {
        render() {
            return (
                <div className="testDiv" ref={this.props.saveRef}>
                    Test div
                </div>
            );
        }
    }
    const ScrollIntoViewComponent = ScrollIntoView(MockComponent);

    test('has the expected html structure', () => {
        const wrapper = shallow(<ScrollIntoViewComponent />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('calls scrollIntoView when item becomes active and it is scrolled out of view', () => {
        const mockScrollIntoViewFn = jest.fn();
        const wrapper = mount(<ScrollIntoViewComponent />);

        wrapper.instance().node = {
            offsetTop: 728,
            clientHeight: 48,
            parentNode: {
                scrollTop: 0,
                clientHeight: 558,
                scrollHeight: 2416
            },
            scrollIntoView: mockScrollIntoViewFn
        };

        wrapper.setProps({ isSelected: true });

        expect(mockScrollIntoViewFn.mock.calls.length).toBe(1);
    });

    test('does not call scrollIntoView when item becomes active but it is not scrolled out of view', () => {
        const mockScrollIntoViewFn = jest.fn();
        const wrapper = mount(<ScrollIntoViewComponent />);

        wrapper.instance().node = {
            offsetTop: 776,
            clientHeight: 48,
            parentNode: {
                scrollTop: 473,
                clientHeight: 558,
                scrollHeight: 2416
            },
            scrollIntoView: mockScrollIntoViewFn
        };

        wrapper.setProps({ isSelected: true });

        expect(mockScrollIntoViewFn.mock.calls.length).toBe(0);
    });

    test('does not call scrollIntoView when item is not active', () => {
        const mockScrollIntoViewFn = jest.fn();
        const wrapper = mount(<ScrollIntoViewComponent isSelected />);

        wrapper.instance().node = {
            offsetTop: 728,
            clientHeight: 48,
            parentNode: {
                scrollTop: 0,
                clientHeight: 558,
                scrollHeight: 2416
            },
            scrollIntoView: mockScrollIntoViewFn
        };

        wrapper.setProps({ isSelected: false });

        expect(mockScrollIntoViewFn.mock.calls.length).toBe(0);
    });
});
