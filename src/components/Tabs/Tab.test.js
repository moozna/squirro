import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Tab from './Tab';

describe('<Tab />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Tab />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label', () => {
        const wrapper = shallow(<Tab label="Tab1" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided className for the label', () => {
        const wrapper = shallow(<Tab className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    test('renders an active tab', () => {
        const wrapper = shallow(<Tab active />);
        const button = wrapper.find('Button');

        expect(wrapper.hasClass('active')).toBe(true);
        expect(button.prop('active')).toBe(true);
    });

    test('renders a disabled tab', () => {
        const wrapper = shallow(<Tab disabled />);
        const button = wrapper.find('Button');

        expect(wrapper.hasClass('disabled')).toBe(true);
        expect(button.prop('disabled')).toBe(true);
    });

    describe('onClick()', () => {
        test('fires the onTabClick callback', () => {
            const handleOnTabClick = jest.fn();
            const wrapper = shallow(<Tab onTabClick={handleOnTabClick} />);

            wrapper.find('Button').simulate('click');
            expect(handleOnTabClick.mock.calls.length).toBe(1);
        });

        test('does not fire the onTabClick callback when the tab is disabled', () => {
            const handleOnTabClick = jest.fn();
            const wrapper = shallow(
                <Tab disabled onTabClick={handleOnTabClick} />
            );

            wrapper.find('Button').simulate('click');
            expect(handleOnTabClick.mock.calls.length).toBe(0);
        });
    });
});
