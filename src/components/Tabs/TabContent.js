import React from 'react';
import PropTypes from 'prop-types';
import styles from './Tabs.css';

const TabContent = ({ children, tabIndex }) => (
    <section className={styles.tabContent} tabIndex={tabIndex}>
        {children}
    </section>
);

TabContent.propTypes = {
    children: PropTypes.node,
    tabIndex: PropTypes.number
};

TabContent.defaultProps = {
    children: null,
    tabIndex: null
};

export default TabContent;
