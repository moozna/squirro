import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Tabs.css';
import noop from '../../utils/noop';
import Button from '../Button/Button';

class Tab extends Component {
    handleClick = event => {
        const { index, disabled, onTabClick } = this.props;

        if (!disabled) {
            onTabClick(event, index);
        }
    };

    render() {
        const { label, className, buttonStyle, active, disabled } = this.props;

        const classes = classnames(
            styles[buttonStyle],
            {
                [styles.white]: buttonStyle === 'halo',
                [styles.active]: active,
                [styles.disabled]: disabled
            },
            className
        );

        return (
            <Button
                className={classes}
                role="tab"
                color="none"
                active={active}
                disabled={disabled}
                onClick={this.handleClick}
            >
                {label}
            </Button>
        );
    }
}

Tab.propTypes = {
    label: PropTypes.node,
    className: PropTypes.string,
    buttonStyle: PropTypes.oneOf(['halo', 'tab']),
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    index: PropTypes.number,
    onTabClick: PropTypes.func
};

Tab.defaultProps = {
    label: '',
    className: '',
    buttonStyle: 'tab',
    active: false,
    disabled: false,
    index: null,
    onTabClick: noop
};

export default Tab;
