import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Tabs from './Tabs';
import Tab from './Tab';

describe('<Tabs />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Tabs />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = [
            <Tab key="1" label="tab1">
                tab1
            </Tab>,
            <Tab key="2" label="tab2">
                tab2
            </Tab>,
            <Tab key="3" label="tab3">
                tab3
            </Tab>
        ];
        const wrapper = shallow(<Tabs>{testChildren}</Tabs>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render children that are not tabs', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Tabs>{testChildren}</Tabs>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders when Tab has no content', () => {
        const testChildren = [
            <Tab key="1" label="tab1" />,
            <Tab key="2" label="tab2" />,
            <Tab key="3" label="tab3" />
        ];
        const wrapper = shallow(<Tabs>{testChildren}</Tabs>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided className for the tab header', () => {
        const wrapper = shallow(<Tabs headerClass="headerClass" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onTabChange', () => {
        const testChildren = [
            <Tab key="1" label="tab1">
                tab1
            </Tab>,
            <Tab key="2" label="tab2">
                tab2
            </Tab>,
            <Tab key="3" label="tab3">
                tab3
            </Tab>
        ];

        test('triggers onTabChange function after switching tabs', () => {
            const handleTabChange = jest.fn();
            const wrapper = shallow(
                <Tabs onTabChange={handleTabChange}>{testChildren}</Tabs>
            );

            wrapper.instance().handleHeaderClick(null, 2);
            expect(handleTabChange.mock.calls.length).toBe(1);
        });

        test('triggers onTabChange function with the correct index switching tabs', () => {
            const handleTabChange = jest.fn();
            const wrapper = shallow(
                <Tabs onTabChange={handleTabChange}>{testChildren}</Tabs>
            );

            wrapper.instance().handleHeaderClick(null, 2);
            expect(handleTabChange.mock.calls[0][0]).toBe(2);
        });
    });
});
