import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TabContent from './TabContent';

describe('<TabContent />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<TabContent />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<TabContent>{testChildren}</TabContent>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets TabIndex when it is provided', () => {
        const wrapper = shallow(<TabContent tabIndex={1} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
