import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Tabs.css';
import noop from '../../utils/noop';
import TabContent from './TabContent';
import Tab from './Tab';

const isComponentOfType = (classType, reactElement) =>
    reactElement && reactElement.type === classType;

const isTab = child => isComponentOfType(Tab, child);

class Tabs extends Component {
    handleHeaderClick = (event, index) => {
        const { onTabChange } = this.props;
        onTabChange(index);
    };

    parseChildren() {
        const { children } = this.props;
        const headers = [];
        const contents = [];

        React.Children.forEach(children, child => {
            if (isTab(child)) {
                headers.push(child);
                if (child.props.children) {
                    contents.push(
                        <TabContent>{child.props.children}</TabContent>
                    );
                }
            }
        });

        return { headers, contents };
    }

    renderHeaders(headers) {
        const { activeTab } = this.props;

        return headers.map((item, index) =>
            React.cloneElement(item, {
                index,
                children: null,
                key: index,
                active: activeTab === index,
                onTabClick: this.handleHeaderClick
            })
        );
    }

    renderContents(contents) {
        const { activeTab: activeTabIndex } = this.props;
        const activeContent = contents[activeTabIndex] || [];

        return React.cloneElement(activeContent, {
            tabIndex: activeTabIndex
        });
    }

    render() {
        const { headerClass } = this.props;
        const { headers, contents } = this.parseChildren();
        const classes = classnames(styles.tabHeader, headerClass);

        return (
            <div className={styles.tabs}>
                <div className={classes}>{this.renderHeaders(headers)}</div>
                {this.renderContents(contents)}
            </div>
        );
    }
}

Tabs.propTypes = {
    children: PropTypes.node,
    headerClass: PropTypes.string,
    activeTab: PropTypes.number,
    onTabChange: PropTypes.func
};

Tabs.defaultProps = {
    children: null,
    headerClass: '',
    activeTab: 0,
    onTabChange: noop
};

export default Tabs;
