import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DialogCard from './DialogCard';

describe('<DialogCard />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DialogCard />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<DialogCard>{testChildren}</DialogCard>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockActions = [
            { label: 'first', className: 'firstClass', onClick: jest.fn() },
            { label: 'second', className: 'secondClass', onClick: jest.fn() }
        ];
        const wrapper = shallow(
            <DialogCard title="myTitle" actions={mockActions} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
