import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Dialog from './Dialog';

describe('<Dialog />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Dialog />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Dialog>{testChildren}</Dialog>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the dialog', () => {
        const wrapper = mount(<Dialog />);

        expect(wrapper.instance().dialogNode).toBeDefined();
    });

    describe('scrollLock', () => {
        test('locks scrolling on the body after mount', () => {
            shallow(<Dialog />);

            expect(document.body.style.overflow).toBe('hidden');
        });

        test('allows scrolling on the body after unmount', () => {
            const wrapper = shallow(<Dialog />);
            wrapper.unmount();

            expect(document.body.style.overflow).toBe('');
        });
    });

    describe('onKeyDown()', () => {
        test('pressing escape fires onClose callback with the saved original value', () => {
            const mockEvent = {
                key: 'Escape'
            };
            const handleOnClose = jest.fn();
            const wrapper = shallow(<Dialog onClose={handleOnClose} />);

            wrapper.find('.overlay').simulate('keydown', mockEvent);

            expect(handleOnClose.mock.calls.length).toBe(1);
        });

        test('does not fire onClose callback when not Escape was pressed', () => {
            const mockEvent = {
                key: 'Enter'
            };
            const handleOnClose = jest.fn();
            const wrapper = shallow(<Dialog onClose={handleOnClose} />);

            wrapper.find('.overlay').simulate('keydown', mockEvent);

            expect(handleOnClose.mock.calls.length).toBe(0);
        });
    });

    describe('onClick()', () => {
        test('clicking outside the dialog fires the onClose callback', () => {
            const handleOnClose = jest.fn();
            const wrapper = shallow(<Dialog onClose={handleOnClose} />);

            wrapper.find('.overlay').simulate('click');

            expect(handleOnClose.mock.calls.length).toBe(1);
        });

        test('clicking inside the dialog does not fire the onClose callback', () => {
            const handleOnClose = jest.fn();
            const wrapper = mount(<Dialog onClose={handleOnClose} />);

            wrapper.find('.dialog').simulate('click');

            expect(handleOnClose.mock.calls.length).toBe(0);
        });
    });
});
