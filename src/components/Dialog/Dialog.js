import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styles from './Dialog.css';
import noop from '../../utils/noop';

const setScrollLock = value => {
    document.body.style.overflow = value;
};

class Dialog extends Component {
    componentDidMount() {
        setScrollLock('hidden');
    }

    componentWillUnmount() {
        setScrollLock('');
    }

    handleOverlayClick = event => {
        const { onClose } = this.props;

        if (this.dialogNode && this.dialogNode.contains(event.target)) {
            return;
        }

        onClose();
    };

    handleKeyDown = event => {
        const { onClose } = this.props;

        if (event.key === 'Escape') {
            onClose();
        }
    };

    saveRef = node => {
        this.dialogNode = node;
    };

    render() {
        const { children, role, ariaLabel } = this.props;

        return ReactDOM.createPortal(
            <aside
                className={styles.overlay}
                role={role}
                tabIndex="-1"
                aria-modal="true"
                aria-label={ariaLabel}
                onClick={this.handleOverlayClick}
                onKeyDown={this.handleKeyDown}
            >
                <div className={styles.dialog} ref={this.saveRef}>
                    {children}
                </div>
            </aside>,
            document.body
        );
    }
}

Dialog.propTypes = {
    children: PropTypes.node,
    role: PropTypes.string,
    ariaLabel: PropTypes.string,
    onClose: PropTypes.func
};

Dialog.defaultProps = {
    children: null,
    role: 'dialog',
    ariaLabel: '',
    onClose: noop
};

export default Dialog;
