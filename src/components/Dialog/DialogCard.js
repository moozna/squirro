import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import styles from './Dialog.css';
import Button from '../Button/Button';

class DialogCard extends Component {
    renderActions() {
        const { actions } = this.props;

        if (!actions.length) {
            return null;
        }

        return actions.map((action, index) => (
            <Button
                key={index}
                className={action.className}
                shape="stadium"
                color={index === 0 ? 'magenta' : 'orange'}
                onClick={action.onClick}
            >
                {action.label}
            </Button>
        ));
    }

    render() {
        const { title, children } = this.props;

        return (
            <Fragment>
                <section>
                    {title ? <h6 className={styles.title}>{title}</h6> : null}
                    {children ? (
                        <div className={styles.textContent}>{children}</div>
                    ) : null}
                </section>
                <nav className={styles.dialogActions}>
                    {this.renderActions()}
                </nav>
            </Fragment>
        );
    }
}

DialogCard.propTypes = {
    children: PropTypes.node,
    title: PropTypes.string,
    actions: PropTypes.arrayOf(
        PropTypes.shape({
            className: PropTypes.string,
            label: PropTypes.string,
            onClick: PropTypes.func
        })
    )
};

DialogCard.defaultProps = {
    children: null,
    title: '',
    actions: []
};

export default DialogCard;
