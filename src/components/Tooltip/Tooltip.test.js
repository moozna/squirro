import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Tooltip from './Tooltip';

describe('<Tooltip />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Tooltip />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when label is specified', () => {
        const wrapper = shallow(<Tooltip label="text" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Tooltip>{testChildren}</Tooltip>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<Tooltip className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    test('renders a tooltip with the specified position', () => {
        const position = 'bottom';
        const wrapper = shallow(<Tooltip position={position} />);

        expect(wrapper.hasClass(position)).toBe(true);
    });
});
