import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Tooltip.css';

const Tooltip = ({ children, className, label, position, ...props }) => {
    const classes = classnames(styles.tooltip, styles[position], className);

    return (
        <span className={classes} data-tooltip={label} {...props}>
            {children}
        </span>
    );
};

Tooltip.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    label: PropTypes.string,
    position: PropTypes.string
};

Tooltip.defaultProps = {
    children: null,
    className: '',
    label: '',
    position: 'top'
};

export default Tooltip;
