import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './CircularProgressBar.css';

const MIN_PERCENTAGE = 0;
const MAX_PERCENTAGE = 100;

class CircularProgressbar extends Component {
    state = {
        percentage: this.props.initialAnimation ? 0 : this.props.percentage
    };

    componentDidMount() {
        const { initialAnimation, percentage } = this.props;

        if (initialAnimation) {
            this.initialTimeout = setTimeout(() => {
                this.requestAnimationFrame = window.requestAnimationFrame(() =>
                    this.updatePercentage(percentage)
                );
            }, 0);
        }
    }

    componentWillUnmount() {
        clearTimeout(this.initialTimeout);
        window.cancelAnimationFrame(this.requestAnimationFrame);
    }

    getRadius() {
        const { strokeWidth } = this.props;
        return 50 - strokeWidth / 2;
    }

    getPathDescription() {
        const radius = this.getRadius();
        return `M 50,50 m 0,-${radius}
                a ${radius},${radius} 0 1 1 0,${2 * radius}
                a ${radius},${radius} 0 1 1 0,-${2 * radius}`;
    }

    getProgressStyle() {
        const { percentage } = this.state;

        const circumference = Math.PI * 2 * this.getRadius();
        const truncatedPercentage = Math.min(
            Math.max(percentage, MIN_PERCENTAGE),
            MAX_PERCENTAGE
        );
        const dashoffset = ((100 - truncatedPercentage) / 100) * circumference;

        return {
            strokeDasharray: `${circumference}px ${circumference}px`,
            strokeDashoffset: `${dashoffset}px`
        };
    }

    updatePercentage(newPercentage) {
        this.setState({
            percentage: newPercentage
        });
    }

    render() {
        const { label, strokeWidth } = this.props;
        const pathDescription = this.getPathDescription();
        const text = label || label === 0 ? label : '';

        return (
            <svg viewBox="0 0 100 100" className={styles.circularProgressbar}>
                <path
                    className={styles.trail}
                    d={pathDescription}
                    strokeWidth={strokeWidth}
                    fillOpacity={0}
                />
                <path
                    className={styles.path}
                    d={pathDescription}
                    strokeWidth={strokeWidth + 1}
                    fillOpacity={0}
                    style={this.getProgressStyle()}
                />
                <text className={styles.text} x={50} y={52}>
                    {text}
                </text>
            </svg>
        );
    }
}

CircularProgressbar.propTypes = {
    percentage: PropTypes.number,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    strokeWidth: PropTypes.number,
    initialAnimation: PropTypes.bool
};

CircularProgressbar.defaultProps = {
    percentage: 0,
    label: null,
    strokeWidth: 7,
    initialAnimation: true
};

export default CircularProgressbar;
