import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CircularProgressBar from './CircularProgressBar';

describe('<CircularProgressBar />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<CircularProgressBar />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when percentage is specified', () => {
        const wrapper = shallow(
            <CircularProgressBar percentage={80} label={4} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not display label when it is not specified', () => {
        const wrapper = shallow(
            <CircularProgressBar percentage={80} label={null} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('displays label when it is 0', () => {
        const wrapper = shallow(
            <CircularProgressBar percentage={80} label={0} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets percentage to 0 when initialAnimation is true', () => {
        const wrapper = shallow(
            <CircularProgressBar percentage={80} initialAnimation />
        );

        expect(wrapper.state('percentage')).toBe(0);
    });

    test('sets percentage to the given value after animation when initialAnimation is true', () => {
        const percentage = 80;
        jest.useFakeTimers();
        jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb =>
            cb()
        );
        const wrapper = shallow(
            <CircularProgressBar percentage={percentage} initialAnimation />
        );

        jest.runAllTimers();
        expect(wrapper.state('percentage')).toBe(percentage);
    });

    test('sets percentage to the given value when initialAnimation is false', () => {
        const percentage = 80;
        const wrapper = shallow(
            <CircularProgressBar
                percentage={percentage}
                initialAnimation={false}
            />
        );

        expect(wrapper.state('percentage')).toBe(percentage);
    });

    test('cancels timeouts on unmount', () => {
        const mockRequestAnimationFrame = jest.spyOn(
            window,
            'requestAnimationFrame'
        );
        const mockCancelAnimationFrame = jest.spyOn(
            window,
            'cancelAnimationFrame'
        );
        jest.useFakeTimers();
        const wrapper = shallow(
            <CircularProgressBar percentage={80} label={4} />
        );

        jest.runAllTimers();
        wrapper.unmount();
        expect(clearTimeout).toHaveBeenCalledTimes(1);
        expect(window.cancelAnimationFrame).toHaveBeenCalledTimes(1);

        mockRequestAnimationFrame.mockRestore();
        mockCancelAnimationFrame.mockRestore();
    });
});
