import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import styles from './Grid.css';
import Rating from '../Rating/Rating';

const ANIMATION_CLASSES = {
    enter: styles.ratingEnter,
    enterActive: styles.ratingEnterActive,
    exit: styles.ratingLeave,
    exitActive: styles.ratingLeaveActive
};

const RatingBar = ({ isVisible, rating }) => (
    <CSSTransition
        in={isVisible}
        classNames={ANIMATION_CLASSES}
        timeout={350}
        unmountOnExit
    >
        <div className={styles.ratingBar}>
            <Rating rating={rating} numberOfStars={5} iconSize={20} readOnly />
        </div>
    </CSSTransition>
);

RatingBar.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    rating: PropTypes.number
};

RatingBar.defaultProps = {
    rating: 0
};

export default RatingBar;
