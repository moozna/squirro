import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Grid.css';
import noop from '../../utils/noop';
import Cover from '../Cover/Cover';
import RatingBar from './RatingBar';
import Tooltip from '../Tooltip/Tooltip';
import withScrollIntoView from '../ScrollIntoView/ScrollIntoView';

export class GridElement extends Component {
    state = {
        isHovered: false
    };

    handleMouseEnter = () => {
        this.setState({ isHovered: true });
    };

    handleMouseLeave = () => {
        this.setState({ isHovered: false });
    };

    render() {
        const { saveRef, imagePath, title, subtitle, rating } = this.props;
        const { isHovered } = this.state;

        return (
            <figure
                ref={saveRef}
                className={styles.gridElement}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
            >
                <div className={styles.coverContainer}>
                    <Cover imagePath={imagePath} altText={title} />
                    <RatingBar isVisible={isHovered} rating={rating} />
                </div>
                <figcaption className={styles.caption}>
                    <Tooltip className={styles.titleTooltip} label={title}>
                        <div className={styles.title}>{title}</div>
                    </Tooltip>
                    <div className={styles.subtitle}>{subtitle}</div>
                </figcaption>
            </figure>
        );
    }
}

GridElement.propTypes = {
    saveRef: PropTypes.func,
    imagePath: PropTypes.string,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    rating: PropTypes.number
};

GridElement.defaultProps = {
    saveRef: noop,
    imagePath: '',
    title: '',
    subtitle: '',
    rating: 0
};

export default withScrollIntoView(GridElement);
