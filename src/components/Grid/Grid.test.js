import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Grid from './Grid';

describe('<Grid />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Grid />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Grid>{testChildren}</Grid>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
