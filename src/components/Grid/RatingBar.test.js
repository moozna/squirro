import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RatingBar from './RatingBar';

describe('<RatingBar />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<RatingBar isVisible />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
