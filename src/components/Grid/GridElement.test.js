import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import GridElementWithScrollIntoView, { GridElement } from './GridElement';

describe('<GridElement />', () => {
    describe('<GridElementWithScrollIntoView />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<GridElementWithScrollIntoView />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<GridElement />', () => {
        test('has the expected html structure by default', () => {
            const wrapper = shallow(<GridElement />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('has the expected html structure when props are specified', () => {
            const wrapper = shallow(
                <GridElement
                    imagePath="imagePath"
                    title="title"
                    subtitle="authorName"
                    rating={2.5}
                />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('sets isHovered to true on mouseEnter', () => {
            const wrapper = shallow(<GridElement />);

            wrapper.find('.gridElement').simulate('mouseenter');
            expect(wrapper.state('isHovered')).toBe(true);
        });

        test('sets isHovered to false on mouseLeave', () => {
            const wrapper = shallow(<GridElement />);
            wrapper.setState({ isHovered: true });

            wrapper.find('.gridElement').simulate('mouseleave');
            expect(wrapper.state('isHovered')).toBe(false);
        });

        test('calls saveRef on the root node', () => {
            const mockSaveRef = jest.fn();
            mount(<GridElement saveRef={mockSaveRef} />);

            expect(mockSaveRef).toHaveBeenCalledTimes(1);
        });
    });
});
