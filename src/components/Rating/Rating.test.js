import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Rating from './Rating';

describe('<Rating />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Rating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when half rating is specified', () => {
        const wrapper = shallow(<Rating rating={2.5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when full rating is specified', () => {
        const wrapper = shallow(<Rating rating={3} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the specified number of icons', () => {
        const numberOfIcons = 8;
        const wrapper = shallow(<Rating numberOfStars={numberOfIcons} />);

        expect(wrapper.children().length).toBe(numberOfIcons);
    });

    test('renders the icons with the specified size', () => {
        const sizeOfIcons = 20;
        const wrapper = shallow(<Rating iconSize={sizeOfIcons} />);

        expect(
            wrapper
                .children()
                .first()
                .props().iconSize
        ).toBe(sizeOfIcons);
    });

    describe('state changes', () => {
        test('sets new hovered value on the state', () => {
            const wrapper = shallow(<Rating />);
            const newRating = 3;

            wrapper.instance().handleIconHover(null, newRating);

            expect(wrapper.state('hoveredValue')).toBe(newRating);
        });

        test('resets hoveredValue to default when hovering ends', () => {
            const wrapper = shallow(<Rating />);
            const hoveredValue = 3;
            wrapper.setState({ hoveredValue });

            wrapper.find('div').simulate('mouseleave');

            expect(wrapper.state('hoveredValue')).toBe(-1);
        });
    });

    describe('handleKeyDown()', () => {
        describe('ArrowRight', () => {
            test('stops keydown event', () => {
                const mockEvent = {
                    key: 'ArrowRight',
                    preventDefault: jest.fn()
                };
                const wrapper = shallow(<Rating />);

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
            });

            test('increases the rating by 0.5', () => {
                const mockEvent = {
                    key: 'ArrowRight',
                    preventDefault: jest.fn()
                };
                const handleRate = jest.fn();
                const wrapper = shallow(
                    <Rating rating={2} onRate={handleRate} />
                );

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(handleRate.mock.calls.length).toBe(1);
                expect(handleRate.mock.calls[0][0]).toBe(2.5);
            });

            test('does not increase the rating to greater than the maximum value', () => {
                const mockEvent = {
                    key: 'ArrowRight',
                    preventDefault: jest.fn()
                };
                const handleRate = jest.fn();
                const wrapper = shallow(
                    <Rating rating={5} onRate={handleRate} />
                );

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(handleRate.mock.calls.length).toBe(0);
            });
        });

        describe('ArrowLeft', () => {
            test('stops keydown event', () => {
                const mockEvent = {
                    key: 'ArrowLeft',
                    preventDefault: jest.fn()
                };
                const wrapper = shallow(<Rating />);

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
            });

            test('decreases the rating by 0.5', () => {
                const mockEvent = {
                    key: 'ArrowLeft',
                    preventDefault: jest.fn()
                };
                const handleRate = jest.fn();
                const wrapper = shallow(
                    <Rating rating={2} onRate={handleRate} />
                );

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(handleRate.mock.calls.length).toBe(1);
                expect(handleRate.mock.calls[0][0]).toBe(1.5);
            });

            test('does not decrease the value to less than zero', () => {
                const mockEvent = {
                    key: 'ArrowLeft',
                    preventDefault: jest.fn()
                };
                const handleRate = jest.fn();
                const wrapper = shallow(
                    <Rating rating={0} onRate={handleRate} />
                );

                wrapper.find('div').simulate('keydown', mockEvent);

                expect(handleRate.mock.calls.length).toBe(0);
            });
        });
    });

    describe('onRate()', () => {
        const previousRating = 2;

        test('triggers onRate function after clicking on a new value', () => {
            const handleRate = jest.fn();
            const wrapper = shallow(
                <Rating rating={previousRating} onRate={handleRate} />
            );

            wrapper.instance().handleIconClick(null, 3);
            expect(handleRate.mock.calls.length).toBe(1);
        });

        test('does not trigger onRate function after clicking on the same value', () => {
            const handleRate = jest.fn();
            const wrapper = shallow(
                <Rating rating={previousRating} onRate={handleRate} />
            );

            wrapper.instance().handleIconClick(null, previousRating);
            expect(handleRate.mock.calls.length).toBe(0);
        });

        test('triggers onRate function with the correct value after clicking on a new value', () => {
            const handleRate = jest.fn();
            const wrapper = shallow(
                <Rating rating={previousRating} onRate={handleRate} />
            );
            const newRating = 3;

            wrapper.instance().handleIconClick(null, newRating);
            expect(handleRate.mock.calls[0][0]).toBe(newRating);
        });
    });

    describe('read only mode', () => {
        test('has the expected html structure', () => {
            const wrapper = shallow(<Rating readOnly />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('does not change hovered value on the state', () => {
            const wrapper = shallow(<Rating readOnly />);

            wrapper.instance().handleIconHover(null, 3);
            expect(wrapper.state('hoveredValue')).toBe(-1);
        });

        test('does not trigger onRate function', () => {
            const handleRate = jest.fn();
            const wrapper = shallow(
                <Rating rating={2} onRate={handleRate} readOnly />
            );

            wrapper.instance().handleIconClick(null, 3);
            expect(handleRate.mock.calls.length).toBe(0);
        });

        test('does not reset hovered value on the state', () => {
            const wrapper = shallow(<Rating readOnly />);
            const hoveredValue = 3;
            wrapper.setState({ hoveredValue });

            wrapper.find('div').simulate('mouseleave');
            expect(wrapper.state('hoveredValue')).toBe(hoveredValue);
        });
    });
});
