import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Star from './Star';

describe('<Star />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Star />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an empty star', () => {
        const wrapper = shallow(<Star iconState="empty" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a half star', () => {
        const wrapper = shallow(<Star iconState="half" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a full star', () => {
        const wrapper = shallow(<Star iconState="full" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
