import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import Star from './Star';

const moreThanHalf = event => {
    const { left, width } = event.target.getBoundingClientRect();
    const mouseAt = event.clientX - left;
    return mouseAt > width / 2;
};

class RatingIcon extends Component {
    getNewRatingValue(event) {
        const { index } = this.props;
        const isMoreThanHalf = moreThanHalf(event);
        return isMoreThanHalf ? index + 1 : index + 0.5;
    }

    handleClick = event => {
        const { onIconClick } = this.props;
        const newValue = this.getNewRatingValue(event);
        onIconClick(event, newValue);
    };

    handleMouseMove = event => {
        const { onIconHover } = this.props;
        const newValue = this.getNewRatingValue(event);
        onIconHover(event, newValue);
    };

    render() {
        const { iconState, iconSize } = this.props;

        return (
            <span onClick={this.handleClick} onMouseMove={this.handleMouseMove}>
                <Star iconState={iconState} iconSize={iconSize} />
            </span>
        );
    }
}

RatingIcon.propTypes = {
    index: PropTypes.number.isRequired,
    iconState: PropTypes.string,
    iconSize: PropTypes.number,
    onIconClick: PropTypes.func,
    onIconHover: PropTypes.func
};

RatingIcon.defaultProps = {
    iconState: 'empty',
    iconSize: 30,
    onIconClick: noop(),
    onIconHover: noop()
};

export default RatingIcon;
