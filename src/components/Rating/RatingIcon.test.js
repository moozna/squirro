import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RatingIcon from './RatingIcon';

describe('<RatingIcon />', () => {
    const index = 3;

    test('has the expected html structure by default', () => {
        const wrapper = shallow(<RatingIcon index={index} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when iconState is specified', () => {
        const wrapper = shallow(<RatingIcon index={index} iconState="half" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('events', () => {
        const mockEvent = {
            target: {
                getBoundingClientRect: () => ({
                    left: 0,
                    width: 10
                })
            }
        };

        test('triggers onClick event', () => {
            const handleIconClick = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconClick={handleIconClick} index={index} />
            );

            wrapper.find('span').simulate('click', mockEvent);
            expect(handleIconClick.mock.calls.length).toBe(1);
        });

        test('sends the expected value when the first half of the component is clicked', () => {
            const handleIconClick = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconClick={handleIconClick} index={index} />
            );
            const firstHalfMockEvent = { ...mockEvent, clientX: 3 };
            const expectedValue = index + 0.5;

            wrapper.find('span').simulate('click', firstHalfMockEvent);
            expect(handleIconClick.mock.calls[0][1]).toBe(expectedValue);
        });

        test('sends the expected value when the second half of the component is clicked', () => {
            const handleIconClick = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconClick={handleIconClick} index={index} />
            );
            const secondHalfMockEvent = { ...mockEvent, clientX: 7 };
            const expectedValue = index + 1;

            wrapper.find('span').simulate('click', secondHalfMockEvent);
            expect(handleIconClick.mock.calls[0][1]).toBe(expectedValue);
        });

        test('triggers onMouseMove event', () => {
            const handleIconHover = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconHover={handleIconHover} index={index} />
            );

            wrapper.find('span').simulate('mousemove', mockEvent);
            expect(handleIconHover.mock.calls.length).toBe(1);
        });

        test('sends the expected value when the first half of the component is hovered', () => {
            const handleIconHover = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconHover={handleIconHover} index={index} />
            );
            const firstHalfMockEvent = { ...mockEvent, clientX: 3 };
            const expectedValue = index + 0.5;

            wrapper.find('span').simulate('mousemove', firstHalfMockEvent);
            expect(handleIconHover.mock.calls[0][1]).toBe(expectedValue);
        });

        test('sends the expected value when the second half of the component is hovered', () => {
            const handleIconHover = jest.fn();
            const wrapper = shallow(
                <RatingIcon onIconHover={handleIconHover} index={index} />
            );
            const secondHalfMockEvent = { ...mockEvent, clientX: 7 };
            const expectedValue = index + 1;

            wrapper.find('span').simulate('mousemove', secondHalfMockEvent);
            expect(handleIconHover.mock.calls[0][1]).toBe(expectedValue);
        });
    });
});
