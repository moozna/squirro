import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import RatingIcon from './RatingIcon';

const getIndexes = n => Array.from({ length: n }, (value, key) => key);
const roundToNearestHalf = number => Math.round(number * 2) / 2;

class Rating extends Component {
    state = {
        hoveredValue: -1
    };

    get rating() {
        const { rating } = this.props;
        return roundToNearestHalf(rating);
    }

    getCurrentValue() {
        return this.isHoverHappening() ? this.state.hoveredValue : this.rating;
    }

    handleMouseLeave = () => {
        const { readOnly } = this.props;
        if (readOnly) {
            return;
        }

        this.setState({ hoveredValue: -1 });
    };

    handleKeyDown = event => {
        const { numberOfStars } = this.props;

        if (event.key === 'ArrowRight') {
            event.preventDefault();

            if (this.rating < numberOfStars) {
                const newRating = this.rating + 0.5;
                this.updateRating(newRating);
            }
        }

        if (event.key === 'ArrowLeft') {
            event.preventDefault();

            if (this.rating > 0) {
                const newRating = this.rating - 0.5;
                this.updateRating(newRating);
            }
        }
    };

    handleIconClick = (event, newRating) => {
        const { readOnly } = this.props;
        if (readOnly) {
            return;
        }

        if (newRating !== this.rating) {
            this.updateRating(newRating);
        }
    };

    handleIconHover = (event, newRating) => {
        const { readOnly } = this.props;
        if (readOnly) {
            return;
        }

        this.setState({ hoveredValue: newRating });
    };

    updateRating(newRating) {
        const { onRate } = this.props;
        onRate(newRating);
    }

    selectIconState(index) {
        const half = this.isHalf(index);
        if (half) {
            return 'half';
        }
        return this.isFull(index) ? 'full' : 'empty';
    }

    isHalf(index) {
        const value = index + 0.5;
        const currentValue = this.getCurrentValue();
        return value === currentValue;
    }

    isFull(index) {
        const value = index + 1;
        const currentValue = this.getCurrentValue();
        return value <= currentValue;
    }

    isHoverHappening() {
        return this.state.hoveredValue !== -1;
    }

    renderRatingIcons() {
        const { numberOfStars, iconSize } = this.props;

        const indexes = getIndexes(numberOfStars);
        return indexes.map(i => (
            <RatingIcon
                key={i}
                index={i}
                iconState={this.selectIconState(i)}
                iconSize={iconSize}
                onIconClick={this.handleIconClick}
                onIconHover={this.handleIconHover}
            />
        ));
    }

    render() {
        const { readOnly } = this.props;

        return (
            <div
                tabIndex={readOnly ? null : 0}
                onMouseLeave={this.handleMouseLeave}
                onKeyDown={this.handleKeyDown}
            >
                {this.renderRatingIcons()}
            </div>
        );
    }
}

Rating.propTypes = {
    rating: PropTypes.number,
    numberOfStars: PropTypes.number,
    iconSize: PropTypes.number,
    readOnly: PropTypes.bool,
    onRate: PropTypes.func
};

Rating.defaultProps = {
    rating: 0,
    numberOfStars: 5,
    iconSize: 30,
    readOnly: false,
    onRate: noop
};

export default Rating;
