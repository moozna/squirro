import React from 'react';
import PropTypes from 'prop-types';
import StarFull from '../svg/StarFull';
import StarEmpty from '../svg/StarEmpty';
import StarHalf from '../svg/StarHalf';

const STAR_STATES = {
    empty: StarEmpty,
    half: StarHalf,
    full: StarFull
};

const Star = ({ iconState, iconSize }) => {
    const StarIcon = STAR_STATES[iconState];
    return <StarIcon iconSize={iconSize} />;
};

Star.propTypes = {
    iconState: PropTypes.string,
    iconSize: PropTypes.number
};

Star.defaultProps = {
    iconState: 'empty',
    iconSize: 30
};

export default Star;
