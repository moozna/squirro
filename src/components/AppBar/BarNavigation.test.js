import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import BarNavigation from './BarNavigation';

describe('<BarNavigation />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<BarNavigation />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
