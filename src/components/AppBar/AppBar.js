import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './AppBar.css';
import BooksCount from '../../containers/BooksCount/BooksCount';
import Sorting from '../../containers/Sorting/Sorting';
import Search from '../../containers/Search/Search';
import BarNavigation from './BarNavigation';

const AppBar = ({ children, flat }) => {
    const classes = classnames(styles.appBar, {
        [styles.flat]: flat
    });

    return (
        <header className={classes}>
            {children}
            <div className={styles.counter}>
                <BooksCount />
            </div>
            <div className={styles.rightAligned}>
                <Sorting />
            </div>
            <div className={styles.search}>
                <Search />
            </div>
            <div className={styles.rightAligned}>
                <BarNavigation />
            </div>
        </header>
    );
};

AppBar.propTypes = {
    children: PropTypes.node,
    flat: PropTypes.bool
};

AppBar.defaultProps = {
    children: null,
    flat: false
};

export default AppBar;
