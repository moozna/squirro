import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AppBar from './AppBar';

describe('<AppBar />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<AppBar />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<AppBar>{testChildren}</AppBar>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the component with flat style', () => {
        const wrapper = shallow(<AppBar flat />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
