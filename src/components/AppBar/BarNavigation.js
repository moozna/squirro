import React from 'react';
import styles from './AppBar.css';
import {
    addBookPage,
    importBookPage,
    shelvesPage
} from '../../data/actions/pageActions';
import NavButtons from '../NavButtons/NavButtons';
import NavButton from '../NavButtons/NavButton';
import Plus from '../svg/Plus';
import Import from '../svg/Import';
import Shelves from '../svg/Shelves';

const BarNavigation = () => (
    <NavButtons buttonsClass={styles.barNavigation}>
        <NavButton to={addBookPage()} color="orange">
            <Plus />
        </NavButton>
        <NavButton to={importBookPage()} color="orange">
            <Import />
        </NavButton>
        <NavButton to={shelvesPage()} color="orange">
            <Shelves />
        </NavButton>
    </NavButtons>
);

export default BarNavigation;
