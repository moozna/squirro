import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import MetaDataInput from './MetaDataInput';

describe('<MetaDataInput />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<MetaDataInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };
        const mockErrors = {
            publicationYear: 'Year error message',
            pages: 'Pages error message',
            numberOfTimesRead: 'Number of times read error message'
        };
        const mockTouched = {
            publicationYear: false,
            pages: true,
            numberOfTimesRead: false
        };

        const wrapper = shallow(
            <MetaDataInput
                name="publicationYear"
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when name is publicationYear', () => {
        const mockValues = {
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };

        const wrapper = shallow(
            <MetaDataInput
                name="publicationYear"
                values={mockValues}
                label="year"
                withIcon
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when name is pages', () => {
        const mockValues = {
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };

        const wrapper = shallow(
            <MetaDataInput
                name="pages"
                values={mockValues}
                label="pages"
                withIcon
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when name is numberOfTimesRead', () => {
        const mockValues = {
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };

        const wrapper = shallow(
            <MetaDataInput
                name="numberOfTimesRead"
                values={mockValues}
                label="read"
                withIcon
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does mot render icon when name is null', () => {
        const mockValues = {
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };

        const wrapper = shallow(
            <MetaDataInput
                name={null}
                values={mockValues}
                label={null}
                withIcon
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
