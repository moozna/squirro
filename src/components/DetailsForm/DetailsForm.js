import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import {
    valuesType,
    touchedType,
    errorsType,
    suggestionGroupType
} from '../../types/types';
import noop from '../../utils/noop';
import withForm from '../Form/Form';
import ImageUpload from '../ImageUpload/ImageUpload';
import StarRatingInput from './StarRatingInput';
import InterestLevelInput from './InterestLevelInput';
import BooksitesRatingFields from './BooksitesRatingFields';
import InfoGroupFields from './InfoGroupFields';
import CardHeaderFields from './CardHeaderFields';
import TagSectionFields from './TagSectionFields';
import ShelfTagFields from './ShelfTagFields';
import HtmlInput from '../HtmlInput/HtmlInput';
import FormNavigation from './FormNavigation';

export const DetailsForm = ({
    values,
    touched,
    errors,
    suggestions,
    isFormValid,
    isFormDirty,
    onChange,
    onSubmit,
    onCancel,
    onBlur,
    uploadCoverImage,
    saveUploadedFiles,
    saveDeletedFiles
}) => {
    const inputProps = {
        values,
        touched,
        errors,
        onChange,
        onBlur
    };

    const isSubmittable = isFormDirty && isFormValid;

    return (
        <div className={styles.detailsCard}>
            <div className={styles.imageColumn}>
                <ImageUpload
                    name="covers"
                    value={values.covers}
                    asin={values.asin}
                    onChange={onChange}
                    uploadCoverImage={uploadCoverImage}
                    saveUploadedFiles={saveUploadedFiles}
                    saveDeletedFiles={saveDeletedFiles}
                />
                <div className={styles.ratings}>
                    <StarRatingInput
                        name="rating"
                        value={values.rating}
                        onChange={onChange}
                    />
                    <InterestLevelInput
                        name="interestLevel"
                        value={values.interestLevel}
                        onChange={onChange}
                    />
                    <BooksitesRatingFields {...inputProps} />
                </div>
                <InfoGroupFields {...inputProps} />
            </div>
            <div className={styles.detailsColumn}>
                <CardHeaderFields suggestions={suggestions} {...inputProps} />
                <TagSectionFields suggestions={suggestions} {...inputProps} />
                <div className={styles.description}>
                    <HtmlInput
                        name="description"
                        label="Description"
                        value={values.description}
                        onChange={onChange}
                    />
                </div>
                <ShelfTagFields suggestions={suggestions} {...inputProps} />
                <div className={styles.review}>
                    <hr />
                    <HtmlInput
                        name="review"
                        label="Review"
                        value={values.review}
                        onChange={onChange}
                    />
                </div>
            </div>
            <FormNavigation
                onSubmit={onSubmit}
                onCancel={onCancel}
                isSubmittable={isSubmittable}
                isDirty={isFormDirty}
            />
        </div>
    );
};

DetailsForm.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    suggestions: suggestionGroupType,
    isFormValid: PropTypes.bool,
    isFormDirty: PropTypes.bool,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    onBlur: PropTypes.func,
    uploadCoverImage: PropTypes.func,
    saveUploadedFiles: PropTypes.func,
    saveDeletedFiles: PropTypes.func
};

DetailsForm.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    suggestions: {},
    isFormValid: null,
    isFormDirty: false,
    onChange: noop,
    onSubmit: noop,
    onCancel: noop,
    onBlur: noop,
    uploadCoverImage: noop,
    saveUploadedFiles: noop,
    saveDeletedFiles: noop
};

export default withForm(DetailsForm);
