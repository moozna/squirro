import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import IdentifierFields from './IdentifierFields';

describe('<IdentifierFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<IdentifierFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            asin: 'asin',
            goodreadsId: 12
        };
        const mockErrors = {
            asin: 'ASIN error message',
            goodreadsId: 'goodreadsId error message'
        };
        const mockTouched = {
            asin: false,
            goodreadsId: true
        };

        const wrapper = shallow(
            <IdentifierFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
