import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DetailsFormWithForm, { DetailsForm } from './DetailsForm';

describe('<DetailsForm />', () => {
    describe('<DetailsFormWithForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<DetailsFormWithForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('has the expected html structure when initial values are specified', () => {
            const mockValues = {
                title: 'title',
                author: ['author1', 'author2'],
                seriesTitle: 'seriesTitle',
                seriesIndex: '2',
                asin: 'asin',
                goodreadsId: 12,
                publicationYear: 2011,
                pages: 123,
                numberOfTimesRead: 2,
                amazonRating: {
                    rating: 2.4,
                    numberOfRatings: 517889
                },
                goodreadsRating: {
                    rating: 4.1,
                    numberOfRatings: 17300
                },
                rating: 4.4,
                interestLevel: 3,
                genres: ['Action', 'Horror', 'Science Fiction'],
                tags: ['read', 'want to read', 'wishlist'],
                shelves: ['Best Historical Fiction', 'Best Book Cover Art'],
                review: 'review',
                cover: [
                    { url: '/covers/image1.jpg' },
                    { url: '/covers/image2.jpg' }
                ]
            };
            const wrapper = shallow(
                <DetailsFormWithForm initialValues={mockValues} />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<DetailsForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<DetailsForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('has the expected html structure when values are specified', () => {
            const mockValues = {
                title: 'title',
                author: ['author1', 'author2'],
                seriesTitle: 'seriesTitle',
                seriesIndex: '2',
                asin: 'asin',
                goodreadsId: 12,
                publicationYear: 2011,
                pages: 123,
                numberOfTimesRead: 2,
                amazonRating: {
                    rating: 2.4,
                    numberOfRatings: 517889
                },
                goodreadsRating: {
                    rating: 4.1,
                    numberOfRatings: 17300
                },
                rating: 4.4,
                interestLevel: 3,
                genres: ['Action', 'Horror', 'Science Fiction'],
                tags: ['read', 'want to read', 'wishlist'],
                review: 'review',
                covers: [
                    { url: '/images/covers/image1.jpg' },
                    { url: '/images/covers/image2.jpg' }
                ]
            };
            const wrapper = shallow(<DetailsForm values={mockValues} />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a disabled submit button when the form is not dirty', () => {
            const wrapper = shallow(
                <DetailsForm isFormDirty={false} isFormValid />
            );
            const formNavigation = wrapper.find('FormNavigation');

            expect(formNavigation.prop('isSubmittable')).toBe(false);
        });

        test('renders a disabled submit button when the form is invalid', () => {
            const wrapper = shallow(
                <DetailsForm isFormDirty isFormValid={false} />
            );
            const formNavigation = wrapper.find('FormNavigation');

            expect(formNavigation.prop('isSubmittable')).toBe(false);
        });
    });
});
