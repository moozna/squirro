import bookValidation from './bookValidation';
import {
    asinNumberValidation,
    goodreadsIdValidation
} from '../ImportForm/importValidation';

jest.mock('../ImportForm/importValidation');

describe('bookValidation()', () => {
    test('is a function', () => {
        expect(typeof bookValidation).toBe('function');
    });

    test('returns an empty object when every field is valid', async () => {
        const testObj = {
            title: 'title',
            author: 'author',
            asin: 'asin',
            goodreadsId: 12
        };
        asinNumberValidation.mockReturnValue({});
        goodreadsIdValidation.mockReturnValue({});
        const errors = await bookValidation(testObj);

        expect(errors).toEqual({});
    });

    describe('returns errors for the required fields', () => {
        const defaultErrors = {
            title: 'Title is required',
            author: 'Author is required'
        };

        test('when object is undefined', async () => {
            const testObj = undefined;

            expect(await bookValidation(testObj)).toEqual(defaultErrors);
        });

        test('when object is an empty object', async () => {
            const testObj = {};

            expect(await bookValidation(testObj)).toEqual(defaultErrors);
        });
    });

    describe('checks title', () => {
        const titleError = 'Title is required';

        test('returns error for title, when title is missing', async () => {
            const testObj = {};
            const errors = await bookValidation(testObj);

            expect(errors.title).toBe(titleError);
        });

        test('returns error for title, when title is empty string', async () => {
            const testObj = { title: '' };
            const errors = await bookValidation(testObj);

            expect(errors.title).toBe(titleError);
        });

        test('does not return error for title, when title is provided', async () => {
            const testObj = { title: 'title' };
            const errors = await bookValidation(testObj);

            expect(errors.title).toBeUndefined();
        });
    });

    describe('checks author', () => {
        const authorError = 'Author is required';

        test('returns error for author, when author is missing', async () => {
            const testObj = {};
            const errors = await bookValidation(testObj);

            expect(errors.author).toBe(authorError);
        });

        test('returns error for author, when author is empty array', async () => {
            const testObj = { author: [] };
            const errors = await bookValidation(testObj);

            expect(errors.author).toBe(authorError);
        });

        test('does not return error for author, when author is provided', async () => {
            const testObj = { author: 'author' };
            const errors = await bookValidation(testObj);

            expect(errors.author).toBeUndefined();
        });
    });

    describe('checks ids', () => {
        test('returns error for asin number, when it has validation error', async () => {
            const testObj = { asin: 'asin' };
            const validationError = 'Validation error';
            asinNumberValidation.mockReturnValueOnce({ asin: validationError });
            const errors = await bookValidation(testObj);

            expect(errors.asin).toEqual(validationError);
        });

        test('returns error for goodreadsId, when it has validation error', async () => {
            const testObj = { goodreadsId: 20 };
            const validationError = 'Validation error';
            goodreadsIdValidation.mockReturnValueOnce({
                goodreadsId: validationError
            });
            const errors = await bookValidation(testObj);

            expect(errors.goodreadsId).toEqual(validationError);
        });
    });
});
