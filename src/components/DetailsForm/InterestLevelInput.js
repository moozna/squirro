import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import noop from '../../utils/noop';
import Slider from '../Slider/Slider';
import { INTEREST_LEVELS } from '../DetailsCard/InterestLevel';

const InterestLevelInput = ({ name, value, onChange }) => (
    <div className={styles.interestLevel}>
        <Slider
            name={name}
            value={value}
            label="Interest"
            outputLabels={INTEREST_LEVELS}
            onChange={onChange}
            marks={[0, 1, 2, 3, 4, 5]}
            withTooltip
        />
    </div>
);

InterestLevelInput.propTypes = {
    name: PropTypes.string,
    value: PropTypes.number,
    onChange: PropTypes.func
};

InterestLevelInput.defaultProps = {
    name: '',
    value: 0,
    onChange: noop
};

export default InterestLevelInput;
