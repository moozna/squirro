import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import { valuesType, touchedType, errorsType } from '../../types/types';
import noop from '../../utils/noop';
import CircleRatingFields from './CircleRatingFields';

const BooksitesRatingFields = ({ ...inputProps }) => (
    <div className={styles.booksitesRating}>
        <CircleRatingFields
            title="Amazon"
            ratingName="amazonRating"
            {...inputProps}
        />
        <CircleRatingFields
            title="Goodreads"
            ratingName="goodreadsRating"
            {...inputProps}
        />
    </div>
);

BooksitesRatingFields.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

BooksitesRatingFields.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    onChange: noop,
    onBlur: noop
};

export default BooksitesRatingFields;
