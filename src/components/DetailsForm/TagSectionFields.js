import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import {
    valuesType,
    touchedType,
    errorsType,
    suggestionGroupType
} from '../../types/types';
import noop from '../../utils/noop';
import TagInput from '../Tags/TagInput';
import Tags from '../Tags/Tags';

const TagSectionFields = ({
    values,
    touched,
    errors,
    onChange,
    onBlur,
    suggestions
}) => (
    <div className={styles.tagSection}>
        <div className={styles.genres}>
            <TagInput
                data={values.genres}
                name="genres"
                label="Genres"
                suggestions={suggestions.genres}
                touched={touched.genres}
                error={errors.genres}
                onChange={onChange}
                onBlur={onBlur}
            >
                <Tags color="orange" inline />
            </TagInput>
        </div>
        <div>
            <TagInput
                data={values.tags}
                name="tags"
                label="Tags"
                suggestions={suggestions.tags}
                touched={touched.tags}
                error={errors.tags}
                onChange={onChange}
                onBlur={onBlur}
            >
                <Tags inline />
            </TagInput>
            <hr />
        </div>
    </div>
);

TagSectionFields.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    suggestions: suggestionGroupType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

TagSectionFields.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    suggestions: {},
    onChange: noop,
    onBlur: noop
};

export default TagSectionFields;
