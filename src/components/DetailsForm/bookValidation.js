import {
    asinNumberValidation,
    goodreadsIdValidation
} from '../ImportForm/importValidation';

const bookValidation = async (values = {}) => {
    const { title, author = [], asin, goodreadsId } = values;
    const errors = {};

    if (!title) {
        errors.title = 'Title is required';
    }

    if (!author.length) {
        errors.author = 'Author is required';
    }

    if (asin) {
        const asinError = await asinNumberValidation(values);
        Object.assign(errors, asinError);
    }

    if (goodreadsId) {
        const goodreadsIdError = await goodreadsIdValidation(values);
        Object.assign(errors, goodreadsIdError);
    }

    return errors;
};

export default bookValidation;
