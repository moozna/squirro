import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InterestLevelInput from './InterestLevelInput';

describe('<InterestLevelInput />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<InterestLevelInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when interest level is specified', () => {
        const wrapper = shallow(<InterestLevelInput value={3} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
