import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import BooksitesRatingFields from './BooksitesRatingFields';

describe('<BooksitesRatingFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<BooksitesRatingFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            'amazonRating.rating': 2.4,
            'amazonRating.numberOfRatings': 517889,
            'goodreadsRating.rating': 4.1,
            'goodreadsRating.numberOfRatings': 17300
        };
        const mockErrors = {
            'amazonRating.rating': 'Amazon rating error',
            'amazonRating.numberOfRatings': 'Amazon number of ratings error',
            'goodreadsRating.rating': 'Goodreads rating error',
            'goodreadsRating.numberOfRatings':
                'Goodreads number of ratings error'
        };
        const mockTouched = {
            'amazonRating.rating': true,
            'amazonRating.numberOfRatings': false,
            'goodreadsRating.rating': false,
            'goodreadsRating.numberOfRatings': true
        };

        const wrapper = shallow(
            <BooksitesRatingFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
