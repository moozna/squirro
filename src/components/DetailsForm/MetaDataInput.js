import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import { valuesType, touchedType, errorsType } from '../../types/types';
import noop from '../../utils/noop';
import Calendar from '../svg/Calendar';
import OpenBook from '../svg/OpenBook';
import ReadingMan from '../svg/Reading';
import NumberInput from '../Form/NumberInput';

class MetaDataInput extends Component {
    renderIcon() {
        const { name } = this.props;

        switch (name) {
            case 'publicationYear':
                return <Calendar />;
            case 'pages':
                return <OpenBook />;
            case 'numberOfTimesRead':
                return <ReadingMan />;
            default:
                return null;
        }
    }

    render() {
        const {
            name,
            label,
            values,
            errors,
            touched,
            onChange,
            onBlur,
            withIcon,
            min,
            precision
        } = this.props;

        return (
            <div className={styles.metaData}>
                <div className={styles.metaDataLabel}>
                    {withIcon ? this.renderIcon() : null}
                </div>
                <NumberInput
                    value={values[name]}
                    name={name}
                    label={label}
                    error={errors[name]}
                    touched={touched[name]}
                    onChange={onChange}
                    onBlur={onBlur}
                    align="center"
                    min={min}
                    precision={precision}
                />
            </div>
        );
    }
}

MetaDataInput.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    withIcon: PropTypes.bool,
    min: PropTypes.number,
    precision: PropTypes.number
};

MetaDataInput.defaultProps = {
    name: '',
    label: '',
    values: {},
    touched: {},
    errors: {},
    onChange: noop,
    onBlur: noop,
    withIcon: false,
    min: null,
    precision: null
};

export default MetaDataInput;
