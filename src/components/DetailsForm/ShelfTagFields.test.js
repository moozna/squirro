import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ShelfTagFields from './ShelfTagFields';

describe('<ShelfTagFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ShelfTagFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            shelves: ['Best Historical Fiction', 'Best Book Cover Art']
        };
        const mockErrors = {
            shelves: 'shelves error message'
        };
        const mockTouched = {
            shelves: true
        };

        const wrapper = shallow(
            <ShelfTagFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
