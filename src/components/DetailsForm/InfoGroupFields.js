import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import { valuesType, touchedType, errorsType } from '../../types/types';
import noop from '../../utils/noop';
import MetaDataInput from './MetaDataInput';
import IdentifierFields from './IdentifierFields';

const InfoGroupFields = ({ ...inputProps }) => (
    <div className={styles.infoGroup}>
        <IdentifierFields {...inputProps} />
        <MetaDataInput
            name="publicationYear"
            label="year"
            precision={0}
            withIcon
            {...inputProps}
        />
        <MetaDataInput
            name="pages"
            label="pages"
            min={0}
            precision={0}
            withIcon
            {...inputProps}
        />
        <MetaDataInput
            name="numberOfTimesRead"
            label="read"
            min={0}
            precision={0}
            withIcon
            {...inputProps}
        />
    </div>
);

InfoGroupFields.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

InfoGroupFields.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    onChange: noop,
    onBlur: noop
};

export default InfoGroupFields;
