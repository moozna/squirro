import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './DetailsForm.css';
import {
    valuesType,
    touchedType,
    errorsType,
    suggestionGroupType
} from '../../types/types';
import noop from '../../utils/noop';
import InputField from '../Form/InputField';
import AutoCompleteField from '../AutoComplete/AutoCompleteField';
import TagInput from '../Tags/TagInput';
import Tags from '../Tags/Tags';

const CardHeaderFields = ({
    className,
    values,
    touched,
    errors,
    suggestions,
    onChange,
    onBlur
}) => {
    const classes = classnames(styles.cardHeader, className);

    return (
        <div className={classes}>
            <div className={styles.title}>
                <InputField
                    value={values.title}
                    name="title"
                    label="Title"
                    touched={touched.title}
                    error={errors.title}
                    onChange={onChange}
                    onBlur={onBlur}
                    flexibleWidth
                />
            </div>
            <div className={styles.subtitle}>
                <span className={styles.seriesTitle}>
                    <AutoCompleteField
                        value={values.seriesTitle}
                        name="seriesTitle"
                        label="Series Title"
                        suggestions={suggestions.series}
                        touched={touched.seriesTitle}
                        error={errors.seriesTitle}
                        onChange={onChange}
                        onBlur={onBlur}
                    />
                </span>
                <span className={styles.seriesIndex}>
                    <InputField
                        value={values.seriesIndex}
                        name="seriesIndex"
                        label="Number"
                        touched={touched.seriesIndex}
                        error={errors.seriesIndex}
                        onChange={onChange}
                        onBlur={onBlur}
                        align="center"
                    />
                </span>
            </div>
            <div className={styles.authors}>
                <TagInput
                    data={values.author}
                    name="author"
                    label="Author"
                    suggestions={suggestions.authors}
                    touched={touched.author}
                    error={errors.author}
                    onChange={onChange}
                    onBlur={onBlur}
                >
                    <Tags inputStyle inline />
                </TagInput>
            </div>
        </div>
    );
};

CardHeaderFields.propTypes = {
    className: PropTypes.string,
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    suggestions: suggestionGroupType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

CardHeaderFields.defaultProps = {
    className: '',
    values: {},
    touched: {},
    errors: {},
    suggestions: {},
    onChange: noop,
    onBlur: noop
};

export default CardHeaderFields;
