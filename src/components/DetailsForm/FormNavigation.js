import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import noop from '../../utils/noop';
import NavButtons from '../NavButtons/NavButtons';
import NavButton from '../NavButtons/NavButton';
import Check from '../svg/Check';
import CloseButton from '../Button/CloseButton';

const FormNavigation = ({ isSubmittable, isDirty, onSubmit, onCancel }) => (
    <NavButtons buttonsClass={styles.navigation}>
        <NavButton disabled={!isSubmittable} onClick={onSubmit}>
            <Check />
        </NavButton>
        <CloseButton autoFocus hasUnsavedChanges={isDirty} onClose={onCancel} />
    </NavButtons>
);

FormNavigation.propTypes = {
    isSubmittable: PropTypes.bool,
    isDirty: PropTypes.bool,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func
};

FormNavigation.defaultProps = {
    isSubmittable: null,
    isDirty: false,
    onSubmit: noop,
    onCancel: noop
};

export default FormNavigation;
