import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import noop from '../../utils/noop';
import Rating from '../Rating/Rating';

class StarRatingInput extends Component {
    handleOnRate = newValue => {
        const { name, onChange } = this.props;
        onChange(name, newValue);
    };

    render() {
        const { value } = this.props;

        return (
            <div className={styles.starRating}>
                <Rating
                    rating={value}
                    numberOfStars={5}
                    onRate={this.handleOnRate}
                />
            </div>
        );
    }
}

StarRatingInput.propTypes = {
    name: PropTypes.string,
    value: PropTypes.number,
    onChange: PropTypes.func
};

StarRatingInput.defaultProps = {
    name: '',
    value: 0,
    onChange: noop
};

export default StarRatingInput;
