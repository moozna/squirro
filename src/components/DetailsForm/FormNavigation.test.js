import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FormNavigation from './FormNavigation';

describe('<FormNavigation />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<FormNavigation />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
