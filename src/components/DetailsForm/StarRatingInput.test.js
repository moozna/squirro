import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import StarRatingInput from './StarRatingInput';

describe('<StarRatingInput />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<StarRatingInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when rating is specified', () => {
        const wrapper = shallow(<StarRatingInput value={3.5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onChange', () => {
        test('fires the onChange callback', () => {
            const newValue = 3.4;
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <StarRatingInput name="rating" onChange={handleOnChange} />
            );

            wrapper.instance().handleOnRate(newValue);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('rating');
            expect(handleOnChange.mock.calls[0][1]).toBe(newValue);
        });
    });
});
