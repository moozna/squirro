import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TagSectionFields from './TagSectionFields';

describe('<TagSectionFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<TagSectionFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            genres: ['Action', 'Horror', 'Science Fiction'],
            tags: ['read', 'want to read', 'wishlist']
        };
        const mockErrors = {
            genres: 'Genres error message',
            tags: 'Tags error message'
        };
        const mockTouched = {
            genres: true,
            tags: false
        };

        const mockSuggestions = {
            genres: [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ],
            tags: [
                { name: 'on Kindle' },
                { name: 'want to read' },
                { name: 'favourite author' }
            ]
        };

        const wrapper = shallow(
            <TagSectionFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
                suggestions={mockSuggestions}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
