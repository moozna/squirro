import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import { valuesType, touchedType, errorsType } from '../../types/types';
import noop from '../../utils/noop';
import NumberInput from '../Form/NumberInput';
import { formatNumber, parseNumber } from '../Form/NumberUtils';

const CircleRatingFields = ({
    title,
    ratingName,
    values,
    errors,
    touched,
    onChange,
    onBlur
}) => {
    const ratingValueName = ratingName ? `${ratingName}.rating` : '';
    const numberofRatingsName = ratingName
        ? `${ratingName}.numberOfRatings`
        : '';

    return (
        <div className={styles.circleRating}>
            <div className={styles.title}>{title}</div>
            <NumberInput
                value={values[ratingValueName]}
                name={ratingValueName}
                label="Rating"
                min={0}
                max={5}
                error={errors[ratingValueName]}
                touched={touched[ratingValueName]}
                onChange={onChange}
                onBlur={onBlur}
            />
            <NumberInput
                value={values[numberofRatingsName]}
                name={numberofRatingsName}
                label="votes"
                min={0}
                format={formatNumber}
                parse={parseNumber}
                precision={0}
                error={errors[numberofRatingsName]}
                touched={touched[numberofRatingsName]}
                onChange={onChange}
                onBlur={onBlur}
            />
        </div>
    );
};

CircleRatingFields.propTypes = {
    title: PropTypes.string,
    ratingName: PropTypes.string,
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

CircleRatingFields.defaultProps = {
    title: '',
    ratingName: '',
    values: {},
    touched: {},
    errors: {},
    onChange: noop,
    onBlur: noop
};

export default CircleRatingFields;
