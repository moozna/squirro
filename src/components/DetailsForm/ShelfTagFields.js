import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import {
    valuesType,
    touchedType,
    errorsType,
    suggestionGroupType
} from '../../types/types';
import noop from '../../utils/noop';
import TagInput from '../Tags/TagInput';
import Tags from '../Tags/Tags';

const ShelfTagFields = ({
    values,
    touched,
    errors,
    onChange,
    onBlur,
    suggestions
}) => (
    <div className={styles.shelfTagFields}>
        <div className={styles.shelves}>
            <TagInput
                data={values.shelves}
                name="shelves"
                label="Shelves"
                suggestions={suggestions.shelves}
                touched={touched.shelves}
                error={errors.shelves}
                onChange={onChange}
                onBlur={onBlur}
            >
                <Tags inputStyle inline />
            </TagInput>
        </div>
    </div>
);

ShelfTagFields.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    suggestions: suggestionGroupType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

ShelfTagFields.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    suggestions: {},
    onChange: noop,
    onBlur: noop
};

export default ShelfTagFields;
