import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsForm.css';
import { valuesType, touchedType, errorsType } from '../../types/types';
import noop from '../../utils/noop';
import AmazonLogo from '../svg/AmazonLogo';
import GoodreadsLogo from '../svg/GoodreadsLogo';
import InputField from '../Form/InputField';
import NumberInput from '../Form/NumberInput';

const IdentifierFields = ({ values, errors, touched, onChange, onBlur }) => (
    <div className={styles.identifiers}>
        <div className={styles.identifier}>
            <span className={styles.identifierIcon}>
                <AmazonLogo />
            </span>
            <InputField
                value={values.asin}
                name="asin"
                label="ASIN"
                error={errors.asin}
                touched={touched.asin}
                onChange={onChange}
                onBlur={onBlur}
            />
        </div>
        <div className={styles.identifier}>
            <span className={styles.identifierIcon}>
                <GoodreadsLogo />
            </span>
            <NumberInput
                value={values.goodreadsId}
                name="goodreadsId"
                label="Goodreads ID"
                error={errors.goodreadsId}
                touched={touched.goodreadsId}
                onChange={onChange}
                onBlur={onBlur}
                align="left"
            />
        </div>
    </div>
);

IdentifierFields.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

IdentifierFields.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    onChange: noop,
    onBlur: noop
};

export default IdentifierFields;
