import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CardHeaderFields from './CardHeaderFields';

describe('<CardHeaderFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<CardHeaderFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            title: 'Title',
            author: ['author1', 'author2'],
            seriesTitle: 'seriesTitle',
            seriesIndex: '2'
        };
        const mockErrors = {
            title: 'Title error message',
            author: 'Author error message',
            seriesTitle: 'Series title error message',
            seriesIndex: 'Series index error message'
        };
        const mockTouched = {
            title: true,
            author: false,
            seriesTitle: false,
            seriesIndex: true
        };

        const wrapper = shallow(
            <CardHeaderFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<CardHeaderFields className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });
});
