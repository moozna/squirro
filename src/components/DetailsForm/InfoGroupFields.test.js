import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InfoGroupFields from './InfoGroupFields';

describe('<InfoGroupFields />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<InfoGroupFields />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockValues = {
            asin: 'asin',
            goodreadsId: 12,
            publicationYear: 2011,
            pages: 123,
            numberOfTimesRead: 2
        };
        const mockErrors = {
            asin: 'ASIN error message',
            goodreadsId: 'GoodreadsId error message',
            publicationYear: 'Year error message',
            pages: 'Pages error message',
            numberOfTimesRead: 'Number of times read error message'
        };
        const mockTouched = {
            asin: false,
            goodreadsId: true,
            publicationYear: false,
            pages: true,
            numberOfTimesRead: false
        };

        const wrapper = shallow(
            <InfoGroupFields
                values={mockValues}
                errors={mockErrors}
                touched={mockTouched}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
