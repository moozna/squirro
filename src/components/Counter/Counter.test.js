import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Counter from './Counter';

describe('<Counter />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Counter />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <Counter startIndex={50} pageLimit={25} count={500} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('displays count when the last page has fewer items than pageLimit', () => {
        const wrapper = shallow(
            <Counter startIndex={500} pageLimit={25} count={502} />
        );
        const numberOfBooks = wrapper.find('.numberOfBooks').text();

        expect(numberOfBooks).toBe('502');
    });
});
