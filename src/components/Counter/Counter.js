import React from 'react';
import PropTypes from 'prop-types';
import styles from './Counter.css';

const Counter = ({ startIndex, pageLimit, count }) => {
    const totalBooksOnAllPage = startIndex + pageLimit;
    const numberOfBooks = Math.min(totalBooksOnAllPage, count);
    return (
        <div className={styles.counter}>
            <div className={styles.numberOfBooks}>{numberOfBooks}</div>
            <div className={styles.count}>{count}</div>
        </div>
    );
};

Counter.propTypes = {
    startIndex: PropTypes.number,
    pageLimit: PropTypes.number,
    count: PropTypes.number
};

Counter.defaultProps = {
    startIndex: 0,
    pageLimit: 50,
    count: 0
};

export default Counter;
