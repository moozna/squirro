import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ViewToggleButton from './ViewToggleButton';

describe('<ViewToggleButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ViewToggleButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an active button', () => {
        const wrapper = shallow(<ViewToggleButton active />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an inactive button', () => {
        const wrapper = shallow(<ViewToggleButton active={false} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
