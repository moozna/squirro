import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './HtmlInput.css';
import noop from '../../utils/noop';
import Tooltip from '../Tooltip/Tooltip';

const BUTTON_COLORS = {
    orange: styles.orange,
    magenta: styles.magenta
};

class ToggleButton extends Component {
    handleToggle = () => {
        const { checked, disabled, onToggle } = this.props;
        if (!checked && !disabled) {
            onToggle();
        }
    };

    render() {
        const { icon, label, checked, disabled, color } = this.props;
        const classes = classnames(styles.toggleButton, BUTTON_COLORS[color]);

        return (
            <Tooltip label={label}>
                <label className={classes}>
                    <input
                        type="radio"
                        onChange={this.handleToggle}
                        checked={checked}
                        disabled={disabled}
                    />
                    {icon || label}
                </label>
            </Tooltip>
        );
    }
}

ToggleButton.propTypes = {
    icon: PropTypes.node,
    label: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    color: PropTypes.string,
    onToggle: PropTypes.func
};

ToggleButton.defaultProps = {
    icon: null,
    label: '',
    checked: false,
    disabled: false,
    color: '',
    onToggle: noop
};

export default ToggleButton;
