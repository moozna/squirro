import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import HtmlInput from './HtmlInput';

describe('<HtmlInput />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<HtmlInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when showHtml is true', () => {
        const wrapper = shallow(<HtmlInput />);
        wrapper.setState({ showHtml: true });

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleFormatToggle()', () => {
        test('sets showHtml to true when it was false', () => {
            const wrapper = shallow(<HtmlInput />);

            expect(wrapper.state('showHtml')).toBe(false);
            wrapper.instance().handleFormatToggle();

            expect(wrapper.state('showHtml')).toBe(true);
        });

        test('sets showHtml to false when it was true', () => {
            const wrapper = shallow(<HtmlInput />);

            wrapper.setState({ showHtml: true });
            expect(wrapper.state('showHtml')).toBe(true);
            wrapper.instance().handleFormatToggle();

            expect(wrapper.state('showHtml')).toBe(false);
        });
    });
});
