import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './HtmlInput.css';
import noop from '../../utils/noop';
import ToggleButton from './ToggleButton';
import RenderedView from '../svg/RenderedView';
import CodeView from '../svg/CodeView';

const VIEW_TOGGLE_BUTTONS = {
    inactive: {
        label: 'Text',
        icon: <RenderedView />,
        color: 'magenta'
    },
    active: {
        label: 'Html',
        icon: <CodeView />,
        color: 'orange'
    }
};

const ViewToggleButton = ({ active, disabled, onToggle }) => {
    const classes = classnames(styles.viewToggleButton, {
        [styles.active]: active
    });

    return (
        <div className={classes}>
            <ToggleButton
                {...VIEW_TOGGLE_BUTTONS.inactive}
                checked={!active}
                disabled={disabled}
                onToggle={onToggle}
            />
            <ToggleButton
                {...VIEW_TOGGLE_BUTTONS.active}
                checked={active}
                disabled={disabled}
                onToggle={onToggle}
            />
        </div>
    );
};

ViewToggleButton.propTypes = {
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    onToggle: PropTypes.func
};

ViewToggleButton.defaultProps = {
    active: false,
    disabled: false,
    onToggle: noop
};

export default ViewToggleButton;
