import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ToggleButton from './ToggleButton';

describe('<ToggleButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ToggleButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an icon', () => {
        const wrapper = shallow(<ToggleButton icon={<div />} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a label, when icon is not specified', () => {
        const wrapper = shallow(<ToggleButton label="label" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled button', () => {
        const wrapper = shallow(<ToggleButton disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a magenta button', () => {
        const wrapper = shallow(<ToggleButton color="magenta" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an orange button', () => {
        const wrapper = shallow(<ToggleButton color="orange" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onToggle()', () => {
        test('fires the onToggle callback', () => {
            const handleOnToggle = jest.fn();
            const wrapper = shallow(<ToggleButton onToggle={handleOnToggle} />);

            wrapper.instance().handleToggle();
            expect(handleOnToggle.mock.calls.length).toBe(1);
        });

        test('does not fire the onToggle callback when it is checked', () => {
            const handleOnToggle = jest.fn();
            const wrapper = shallow(
                <ToggleButton onToggle={handleOnToggle} checked />
            );

            wrapper.instance().handleToggle();
            expect(handleOnToggle.mock.calls.length).toBe(0);
        });

        test('does not fire the onToggle callback when it is disabled', () => {
            const handleOnToggle = jest.fn();
            const wrapper = shallow(
                <ToggleButton onToggle={handleOnToggle} disabled />
            );

            wrapper.instance().handleToggle();
            expect(handleOnToggle.mock.calls.length).toBe(0);
        });
    });
});
