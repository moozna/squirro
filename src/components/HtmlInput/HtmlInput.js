import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import ViewToggleButton from './ViewToggleButton';
import InputField from '../Form/InputField';
import RichTextEditor from '../RichTextEditor/RichTextEditor';

class HtmlInput extends Component {
    state = {
        showHtml: false
    };

    handleFormatToggle = () => {
        this.setState(prevState => ({
            showHtml: !prevState.showHtml
        }));
    };

    renderHtml() {
        const { value, name, label, onChange } = this.props;

        return (
            <InputField
                multiline
                name={name}
                label={label}
                value={value}
                onChange={onChange}
                onBlur={this.handleInputBlur}
            />
        );
    }

    renderEditor() {
        const { value, name, label, onChange } = this.props;

        return (
            <RichTextEditor
                value={value}
                name={name}
                label={label}
                onChange={onChange}
            />
        );
    }

    render() {
        const { showHtml } = this.state;

        return (
            <div>
                <ViewToggleButton
                    active={showHtml}
                    onToggle={this.handleFormatToggle}
                />
                <div>{showHtml ? this.renderHtml() : this.renderEditor()}</div>
            </div>
        );
    }
}

HtmlInput.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

HtmlInput.defaultProps = {
    value: '',
    name: '',
    label: '',
    onChange: noop
};

export default HtmlInput;
