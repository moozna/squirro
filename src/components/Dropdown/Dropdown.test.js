import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Dropdown from './Dropdown';
import DropdownItem from './DropdownItem';
import noop from '../../utils/noop';

describe('<Dropdown />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Dropdown />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Dropdown>{testChildren}</Dropdown>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the selected item', () => {
        const wrapper = shallow(
            <Dropdown selected="item2">
                <DropdownItem value="item1" />
                <DropdownItem value="item2" />
                <DropdownItem value="item3" />
            </Dropdown>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an open dropdown', () => {
        const wrapper = shallow(
            <Dropdown selected="item2">
                <DropdownItem value="item1" />
                <DropdownItem value="item2" />
                <DropdownItem value="item3" />
            </Dropdown>
        );

        wrapper.setState({ open: true });
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled dropdown', () => {
        const wrapper = shallow(<Dropdown disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the dropdown', () => {
        const wrapper = mount(<Dropdown />);

        expect(wrapper.instance().dropdownNode).toBeDefined();
    });

    describe('onSelectItem()', () => {
        test('fires the onSelectItem callback', () => {
            const handleOnSelectItem = jest.fn();
            const newSelection = 'amazonRating';

            const wrapper = shallow(
                <Dropdown name="rating" onSelectItem={handleOnSelectItem} />
            );

            wrapper.instance().handleSelect(newSelection);
            expect(handleOnSelectItem.mock.calls.length).toBe(1);
            expect(handleOnSelectItem.mock.calls[0][0]).toBe('rating');
            expect(handleOnSelectItem.mock.calls[0][1]).toBe(newSelection);
        });
    });

    describe('open', () => {
        test('opens by clicking on the dropdown', () => {
            const mockEvent = {
                preventDefault: noop
            };
            const wrapper = shallow(<Dropdown />);

            wrapper.find('.selectedItem').simulate('click', mockEvent);
            expect(wrapper.state('open')).toBe(true);
        });

        test('stops click event', () => {
            const mockEvent = {
                preventDefault: jest.fn()
            };
            const wrapper = shallow(<Dropdown />);

            wrapper.find('.selectedItem').simulate('click', mockEvent);
            expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('does not open when dropdown is disabled', () => {
            const mockEvent = {
                preventDefault: noop
            };
            const wrapper = shallow(<Dropdown disabled />);

            wrapper.find('.selectedItem').simulate('click', mockEvent);
            expect(wrapper.state('open')).toBe(false);
        });

        test('closes by selecting an item', () => {
            const wrapper = shallow(<Dropdown />);
            wrapper.setState({ open: true });

            wrapper.instance().handleSelect('amazonRating');
            expect(wrapper.state('open')).toBe(false);
        });

        test('closes by clicking outside of the component', () => {
            const listeners = {};
            const mockAddEventListener = jest
                .spyOn(document, 'addEventListener')
                .mockImplementation((event, cb) => {
                    listeners[event] = cb;
                });

            const wrapper = mount(<Dropdown />);
            wrapper.setState({ open: true });
            expect(wrapper.state('open')).toBe(true);

            listeners.click({
                target: document.createElement('div')
            });

            expect(wrapper.state('open')).toBe(false);
            mockAddEventListener.mockRestore();
        });

        test('does not closes by clicking inside of the component', () => {
            const listeners = {};
            const mockAddEventListener = jest
                .spyOn(document, 'addEventListener')
                .mockImplementation((event, cb) => {
                    listeners[event] = cb;
                });

            const wrapper = mount(<Dropdown />);
            wrapper.setState({ open: true });

            listeners.click({
                target: wrapper.getDOMNode()
            });

            expect(wrapper.state('open')).toBe(true);
            mockAddEventListener.mockRestore();
        });
    });

    describe('event listeners', () => {
        let mockAddEventListener;
        let mockRemoveEventListener;

        beforeEach(() => {
            mockAddEventListener = jest.spyOn(document, 'addEventListener');
            mockRemoveEventListener = jest.spyOn(
                document,
                'removeEventListener'
            );
        });

        afterEach(() => {
            mockAddEventListener.mockRestore();
            mockRemoveEventListener.mockRestore();
        });

        test('attaches event listeners after the dropdown is opened', () => {
            const wrapper = shallow(<Dropdown />);
            wrapper.setState({ open: true });

            expect(mockAddEventListener.mock.calls.length).toBe(1);
            expect(mockAddEventListener.mock.calls[0][0]).toBe('click');
        });

        test('detaches event listeners after the dropdown is closed', () => {
            const wrapper = shallow(<Dropdown />);
            wrapper.setState({ open: true });
            wrapper.setState({ open: false });

            expect(mockRemoveEventListener.mock.calls.length).toBe(1);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('click');
        });

        test('detaches event listeners after unmount', () => {
            const wrapper = shallow(<Dropdown />);
            wrapper.setState({ open: true });
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(1);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('click');
        });

        test('does not try to detach event listeners when dropdown is not open', () => {
            const wrapper = shallow(<Dropdown />);
            wrapper.setState({ open: false });
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(0);
        });
    });
});
