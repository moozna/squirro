import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Dropdown.css';
import noop from '../../utils/noop';

class DropdownItem extends Component {
    handleSelect = event => {
        const { value, disabled, onItemClick } = this.props;

        event.preventDefault();

        if (!disabled) {
            onItemClick(value);
        }
    };

    render() {
        const { children, selected, disabled } = this.props;

        const className = classnames({
            [styles.selected]: selected,
            [styles.disabled]: disabled
        });

        return (
            <li onClick={this.handleSelect} className={className}>
                {children}
            </li>
        );
    }
}

DropdownItem.propTypes = {
    children: PropTypes.node,
    value: PropTypes.string,
    selected: PropTypes.bool,
    disabled: PropTypes.bool,
    onItemClick: PropTypes.func
};

DropdownItem.defaultProps = {
    children: null,
    value: '',
    selected: false,
    disabled: false,
    onItemClick: noop
};

export default DropdownItem;
