import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Dropdown.css';
import noop from '../../utils/noop';

class Dropdown extends Component {
    state = {
        open: false
    };

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.open && this.state.open) {
            document.addEventListener('click', this.handleDocumentClick);
        }

        if (prevState.open && !this.state.open) {
            document.removeEventListener('click', this.handleDocumentClick);
        }
    }

    componentWillUnmount() {
        if (this.state.open) {
            document.removeEventListener('click', this.handleDocumentClick);
        }
    }

    handleSelect = value => {
        const { name, onSelectItem } = this.props;
        onSelectItem(name, value);
        this.close();
    };

    handleOpen = event => {
        const { disabled } = this.props;
        event.preventDefault();

        if (!disabled) {
            this.setState(prevState => ({
                open: !prevState.open
            }));
        }
    };

    handleDocumentClick = event => {
        const { open } = this.state;
        if (open && !this.dropdownNode.contains(event.target)) {
            this.close();
        }
    };

    saveRef = node => {
        this.dropdownNode = node;
    };

    close() {
        this.setState({ open: false });
    }

    renderSelected() {
        const { children, selected } = this.props;
        return React.Children.map(children, child => {
            if (child.props.value === selected) {
                return child.props.children;
            }
            return null;
        });
    }

    renderItems() {
        const { children, selected, disabled } = this.props;
        return React.Children.map(children, child =>
            React.cloneElement(child, {
                selected: selected === child.props.value,
                disabled: disabled || child.props.disabled,
                onItemClick: this.handleSelect
            })
        );
    }

    render() {
        const { disabled } = this.props;
        const { open } = this.state;

        const className = classnames(styles.dropdown, {
            [styles.disabled]: disabled
        });

        const listClasses = classnames(styles.list, {
            [styles.hidden]: !open
        });

        return (
            <div className={className} tabIndex="-1" ref={this.saveRef}>
                <div onClick={this.handleOpen} className={styles.selectedItem}>
                    {this.renderSelected()}
                </div>
                <ul className={listClasses}>{this.renderItems()}</ul>
            </div>
        );
    }
}

Dropdown.propTypes = {
    children: PropTypes.node,
    name: PropTypes.string,
    selected: PropTypes.string,
    disabled: PropTypes.bool,
    onSelectItem: PropTypes.func
};

Dropdown.defaultProps = {
    children: null,
    name: '',
    selected: '',
    disabled: false,
    onSelectItem: noop
};

export default Dropdown;
