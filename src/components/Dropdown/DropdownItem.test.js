import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DropdownItem from './DropdownItem';
import noop from '../../utils/noop';

describe('<DropdownItem />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DropdownItem />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<DropdownItem>{testChildren}</DropdownItem>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a selected item', () => {
        const wrapper = shallow(<DropdownItem selected />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled item', () => {
        const wrapper = shallow(<DropdownItem disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onItemClick()', () => {
        test('fires the onItemClick callback', () => {
            const mockEvent = {
                preventDefault: noop
            };

            const handleItemChange = jest.fn();
            const wrapper = shallow(
                <DropdownItem
                    value="amazonRating"
                    onItemClick={handleItemChange}
                />
            );

            wrapper.find('li').simulate('click', mockEvent);
            expect(handleItemChange.mock.calls.length).toBe(1);
            expect(handleItemChange.mock.calls[0][0]).toBe('amazonRating');
        });

        test('stops click event', () => {
            const mockEvent = {
                preventDefault: jest.fn()
            };

            const handleItemChange = jest.fn();
            const wrapper = shallow(
                <DropdownItem onItemClick={handleItemChange} />
            );

            wrapper.find('li').simulate('click', mockEvent);
            expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('does not fire the onItemClick callback when the item is disabled', () => {
            const mockEvent = {
                preventDefault: noop
            };
            const handleItemChange = jest.fn();
            const wrapper = shallow(
                <DropdownItem onItemClick={handleItemChange} disabled />
            );

            wrapper.find('li').simulate('click', mockEvent);
            expect(handleItemChange.mock.calls.length).toBe(0);
        });
    });
});
