import React from 'react';
import PropTypes from 'prop-types';
import styles from './Filters.css';
import noop from '../../utils/noop';
import RangeSlider from '../RangeSlider/RangeSlider';

const getMarks = (n, min, step) =>
    Array.from({ length: n }, (value, key) => min + key * step);

const FilterSlider = ({ data, onValueChange }) => {
    const { min, max, values } = data;

    if (!min || !max) {
        return null;
    }

    const difference = max - min;
    const step = Math.max(1, Math.ceil(difference / 5));
    const markNumber = Math.min(Math.floor(difference) + 1, 6);
    const marks = getMarks(markNumber, min, step);

    return (
        <div className={styles.ratingRow}>
            <RangeSlider
                key={`${min}-${max}`}
                defaultValues={[min, max]}
                values={values}
                min={marks[0]}
                max={marks[marks.length - 1]}
                step={1}
                marks={marks}
                withTooltip
                onAfterChange={onValueChange}
            />
        </div>
    );
};

FilterSlider.propTypes = {
    data: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number,
        values: PropTypes.arrayOf(PropTypes.number)
    }),
    onValueChange: PropTypes.func
};

FilterSlider.defaultProps = {
    data: {},
    onValueChange: noop
};

export default FilterSlider;
