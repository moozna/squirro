import React from 'react';
import PropTypes from 'prop-types';
import styles from './Filters.css';
import noop from '../../utils/noop';
import RangeSlider from '../RangeSlider/RangeSlider';

const RatingSlider = ({ values, onValueChange }) => (
    <div className={styles.ratingRow}>
        <RangeSlider
            defaultValues={[0, 5]}
            values={values}
            min={0}
            max={5}
            step={0.5}
            marks={[0, 1, 2, 3, 4, 5]}
            onAfterChange={onValueChange}
        />
    </div>
);

RatingSlider.propTypes = {
    values: PropTypes.arrayOf(PropTypes.number),
    onValueChange: PropTypes.func
};

RatingSlider.defaultProps = {
    values: [],
    onValueChange: noop
};

export default RatingSlider;
