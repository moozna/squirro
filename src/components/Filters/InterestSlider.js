import React from 'react';
import PropTypes from 'prop-types';
import styles from './Filters.css';
import noop from '../../utils/noop';
import RangeSlider from '../RangeSlider/RangeSlider';

const INTEREST_RANGE_LEVELS = {
    0: 'Not set',
    1: 'Not at all',
    2: 'Maybe',
    3: 'Moderately',
    4: 'Interesting',
    5: 'Definitely'
};

const InterestSlider = ({ values, onValueChange }) => (
    <div className={styles.ratingRow}>
        <RangeSlider
            defaultValues={[0, 5]}
            values={values}
            min={0}
            max={5}
            step={1}
            marks={[0, 1, 2, 3, 4, 5]}
            withTooltip
            tooltipLabels={INTEREST_RANGE_LEVELS}
            onAfterChange={onValueChange}
        />
    </div>
);

InterestSlider.propTypes = {
    values: PropTypes.arrayOf(PropTypes.number),
    onValueChange: PropTypes.func
};

InterestSlider.defaultProps = {
    values: [],
    onValueChange: noop
};

export default InterestSlider;
