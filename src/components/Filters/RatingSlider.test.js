import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RatingSlider from './RatingSlider';

describe('<RatingSlider />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<RatingSlider />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are provided', () => {
        const testValues = [3, 4];
        const wrapper = shallow(<RatingSlider values={testValues} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
