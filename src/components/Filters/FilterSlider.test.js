import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FilterSlider from './FilterSlider';

describe('<FilterSlider />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<FilterSlider />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are provided', () => {
        const testData = {
            min: 1982,
            max: 2017,
            values: [1996, 2000]
        };
        const wrapper = shallow(<FilterSlider data={testData} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
