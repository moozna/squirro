import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InterestSlider from './InterestSlider';

describe('<InterestSlider />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<InterestSlider />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are provided', () => {
        const testValues = [3, 4];
        const wrapper = shallow(<InterestSlider values={testValues} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
