import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImportNavigation from './ImportNavigation';

describe('<ImportNavigation />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ImportNavigation />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
