import {
    asinNumberValidation,
    goodreadsIdValidation,
    validateId
} from './importValidation';
import { isAsinNumberValid, isGoodreadsIdValid } from '../../data/api/validate';

jest.mock('../../data/api/validate');

describe('importValidation', () => {
    const alreadyExistsError = 'This book already exists';
    const failedError = 'This ID is not valid';

    describe('asinNumberValidation()', () => {
        test('is a function', () => {
            expect(typeof asinNumberValidation).toBe('function');
        });

        test('returns an empty object when asin number is missing', () => {
            const testObj = {};
            const errors = asinNumberValidation(testObj);

            expect(errors).toEqual({});
        });

        test('returns an empty object when asin number is empty string', () => {
            const testObj = { asin: '' };
            const errors = asinNumberValidation(testObj);

            expect(errors).toEqual({});
        });

        test('returns error for asin number, when it does not reach the required length', () => {
            const testObj = { asin: 'B004XI' };
            const errors = asinNumberValidation(testObj);

            expect(errors.asin).toBe('It should be minimum 10 characters');
        });

        test('returns error for asin number, when it has validation error', async () => {
            const testObj = { asin: 'B004XISI4A' };
            isAsinNumberValid.mockImplementation(() => Promise.resolve(false));
            const errors = await asinNumberValidation(testObj);

            expect(errors.asin).toBe(alreadyExistsError);
        });

        test('returns an empty object when asin number is valid', async () => {
            const testObj = { asin: 'B004XISI4A' };
            isAsinNumberValid.mockImplementation(() => Promise.resolve(true));
            const errors = await asinNumberValidation(testObj);

            expect(errors).toEqual({});
        });
    });

    describe('goodreadsIdValidation()', () => {
        test('is a function', () => {
            expect(typeof goodreadsIdValidation).toBe('function');
        });

        test('returns an empty object when goodreadsId is missing', () => {
            const testObj = {};
            const errors = goodreadsIdValidation(testObj);

            expect(errors).toEqual({});
        });

        test('returns an empty object when goodreadsId is null', () => {
            const testObj = { goodreadsId: null };
            const errors = goodreadsIdValidation(testObj);

            expect(errors).toEqual({});
        });

        test('returns error for goodreadsId, when it has validation error', async () => {
            const testObj = { goodreadsId: 20 };
            isGoodreadsIdValid.mockImplementation(() => Promise.resolve(false));
            const errors = await goodreadsIdValidation(testObj);

            expect(errors.goodreadsId).toBe(alreadyExistsError);
        });

        test('returns an empty object when goodreadsId is valid', async () => {
            const testObj = { goodreadsId: 12 };
            isGoodreadsIdValid.mockImplementation(() => Promise.resolve(true));
            const errors = await goodreadsIdValidation(testObj);

            expect(errors).toEqual({});
        });
    });

    describe('validateId()', () => {
        const mockValues = { prop: 'prop' };
        const mockName = 'prop';

        test('returns error for the given prop, when it is already used', async () => {
            const mockValidator = jest
                .fn()
                .mockImplementation(() => Promise.resolve(false));
            const expected = { [mockName]: alreadyExistsError };
            const errors = await validateId(
                mockValues,
                mockName,
                mockValidator
            );

            expect(errors).toEqual(expected);
        });

        test('does not return error for the given prop, when it does not exist in the database', async () => {
            const mockValidator = jest
                .fn()
                .mockImplementation(() => Promise.resolve(true));
            const expected = {};
            const errors = await validateId(
                mockValues,
                mockName,
                mockValidator
            );

            expect(errors).toEqual(expected);
        });

        test('returns error for the given prop, when failure happened during its validation', async () => {
            const mockValidator = jest.fn().mockImplementation(() => {
                throw new Error();
            });
            const expected = { [mockName]: failedError };
            const errors = await validateId(
                mockValues,
                mockName,
                mockValidator
            );

            expect(errors).toEqual(expected);
        });
    });
});
