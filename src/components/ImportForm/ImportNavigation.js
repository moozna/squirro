import React from 'react';
import PropTypes from 'prop-types';
import styles from './ImportForm.css';
import noop from '../../utils/noop';
import NavButtons from '../NavButtons/NavButtons';
import NavButton from '../NavButtons/NavButton';
import ArrowRight from '../svg/ArrowRight';
import Close from '../svg/Close';

const ImportNavigation = ({ goToNext, onCancel }) => (
    <NavButtons buttonsClass={styles.navigation}>
        <NavButton onClick={goToNext}>
            <ArrowRight />
        </NavButton>
        <NavButton autoFocus onClick={onCancel}>
            <Close />
        </NavButton>
    </NavButtons>
);

ImportNavigation.propTypes = {
    goToNext: PropTypes.func,
    onCancel: PropTypes.func
};

ImportNavigation.defaultProps = {
    goToNext: noop,
    onCancel: noop
};

export default ImportNavigation;
