import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ImportForm.css';
import noop from '../../utils/noop';
import withForm from '../Form/Form';
import NumberInput from '../Form/NumberInput';
import InputField from '../Form/InputField';
import Button from '../Button/Button';

export class ImportForm extends Component {
    handleKeyDown = event => {
        if (event.key === 'Enter') {
            this.handleSubmit();
        }
    };

    handleSubmit = () => {
        const { isFormValid, onSubmit } = this.props;
        if (isFormValid) {
            onSubmit();
        }
    };

    renderIcon() {
        const { url, icon } = this.props;

        if (!url) {
            return icon;
        }

        return (
            <a
                className={styles.link}
                target="_blank"
                rel="noopener noreferrer"
                href={url}
            >
                {icon}
            </a>
        );
    }

    render() {
        const {
            type,
            name,
            label,
            values,
            touched,
            errors,
            isFormValid,
            onChange,
            onBlur
        } = this.props;

        const inputProps = {
            value: values[name],
            name,
            label,
            touched: touched[name],
            error: errors[name],
            onChange,
            onBlur
        };

        const isSubmittable = inputProps.value && isFormValid;

        return (
            <div className={styles.importForm} onKeyDown={this.handleKeyDown}>
                {this.renderIcon()}
                {type === 'number' ? (
                    <NumberInput
                        {...inputProps}
                        min={0}
                        precision={0}
                        align="left"
                    />
                ) : (
                    <InputField {...inputProps} />
                )}
                <Button
                    shape="stadium"
                    disabledColor="light"
                    disabled={!isSubmittable}
                    onClick={this.handleSubmit}
                >
                    Import
                </Button>
            </div>
        );
    }
}

ImportForm.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    values: PropTypes.shape({
        [PropTypes.string]: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ])
    }),
    touched: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    }),
    errors: PropTypes.shape({
        [PropTypes.string]: PropTypes.string
    }),
    icon: PropTypes.node,
    url: PropTypes.string,
    isFormValid: PropTypes.bool,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onBlur: PropTypes.func
};

ImportForm.defaultProps = {
    type: 'text',
    name: '',
    label: '',
    values: {},
    touched: {},
    errors: {},
    icon: null,
    url: '',
    isFormValid: null,
    onChange: noop,
    onSubmit: noop,
    onBlur: noop
};

export default withForm(ImportForm);
