import { isAsinNumberValid, isGoodreadsIdValid } from '../../data/api/validate';

export const asinNumberValidation = values => {
    const { asin } = values;

    if (!asin) {
        return {};
    }

    if (asin.length < 10) {
        return { asin: 'It should be minimum 10 characters' };
    }

    return validateId(values, 'asin', isAsinNumberValid);
};

export const goodreadsIdValidation = values => {
    const { goodreadsId } = values;

    if (!goodreadsId) {
        return {};
    }

    return validateId(values, 'goodreadsId', isGoodreadsIdValid);
};

export async function validateId(values, name, validator) {
    const { _id: bookId } = values;
    const value = values[name];

    try {
        const isValidId = await validator(value, bookId);
        if (!isValidId) {
            return { [name]: 'This book already exists' };
        }
    } catch (error) {
        return { [name]: 'This ID is not valid' };
    }

    return {};
}
