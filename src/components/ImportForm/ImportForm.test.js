import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImportFormWithForm, { ImportForm } from './ImportForm';

describe('<ImportForm />', () => {
    describe('<ImportFormWithForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ImportFormWithForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<ImportForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ImportForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders an icon', () => {
            const icon = <div>icon</div>;
            const wrapper = shallow(<ImportForm icon={icon} />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a link', () => {
            const icon = <div>icon</div>;
            const wrapper = shallow(<ImportForm icon={icon} url="amazonUrl" />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a number input', () => {
            const wrapper = shallow(<ImportForm type="number" />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a disabled button when the form does not have a value', () => {
            const mockValues = { asin: '' };
            const wrapper = shallow(
                <ImportForm
                    name="asin"
                    values={mockValues}
                    isFormValid={true}
                />
            );
            const button = wrapper.find('Button');

            expect(button.prop('disabled')).toBe(true);
        });

        test('renders a disabled button when the form is invalid', () => {
            const mockValues = { asin: 'asin' };
            const wrapper = shallow(
                <ImportForm
                    name="asin"
                    values={mockValues}
                    isFormValid={false}
                />
            );
            const button = wrapper.find('Button');

            expect(button.prop('disabled')).toBe(true);
        });

        describe('handleKeyDown()', () => {
            test('fires the onSubmit callback when Enter is pressed', () => {
                const mockKeydownEvent = {
                    key: 'Enter'
                };
                const handleOnSubmit = jest.fn();
                const wrapper = shallow(
                    <ImportForm onSubmit={handleOnSubmit} isFormValid />
                );

                wrapper
                    .find('.importForm')
                    .simulate('keydown', mockKeydownEvent);

                expect(handleOnSubmit.mock.calls.length).toBe(1);
            });

            test('does not fire the onSubmit callback when not Enter is pressed', () => {
                const mockKeydownEvent = {
                    key: 'Escape'
                };
                const handleOnSubmit = jest.fn();
                const wrapper = shallow(
                    <ImportForm onSubmit={handleOnSubmit} isFormValid />
                );

                wrapper
                    .find('.importForm')
                    .simulate('keydown', mockKeydownEvent);

                expect(handleOnSubmit.mock.calls.length).toBe(0);
            });

            test('does not fire the onSubmit callback when the form is invalid', () => {
                const mockKeydownEvent = {
                    key: 'Enter'
                };
                const handleOnSubmit = jest.fn();
                const wrapper = shallow(
                    <ImportForm onSubmit={handleOnSubmit} isFormValid={false} />
                );

                wrapper
                    .find('.importForm')
                    .simulate('keydown', mockKeydownEvent);

                expect(handleOnSubmit.mock.calls.length).toBe(0);
            });
        });
    });
});
