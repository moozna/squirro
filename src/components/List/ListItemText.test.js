import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ListItemText from './ListItemText';

describe('<ListItemText />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ListItemText />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<ListItemText>{testChildren}</ListItemText>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<ListItemText className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    test('renders the component with primary text', () => {
        const wrapper = shallow(<ListItemText primary />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the component with subContent', () => {
        const wrapper = shallow(<ListItemText subContent />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
