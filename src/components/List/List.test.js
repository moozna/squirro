import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import List from './List';

describe('<List />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<List />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<List>{testChildren}</List>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
