import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './List.css';

const ListItemText = ({ children, className, primary, subContent }) => {
    const classes = classnames(
        styles.itemText,
        {
            [styles.primary]: primary,
            [styles.subContent]: subContent
        },
        className
    );

    return <div className={classes}>{children}</div>;
};

ListItemText.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    primary: PropTypes.bool,
    subContent: PropTypes.bool
};

ListItemText.defaultProps = {
    children: null,
    className: '',
    primary: false,
    subContent: false
};

export default ListItemText;
