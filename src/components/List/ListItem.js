import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './List.css';
import noop from '../../utils/noop';
import ListItemText from './ListItemText';
import withScrollIntoView from '../ScrollIntoView/ScrollIntoView';

export class ListItem extends Component {
    handleItemClick = () => {
        const { itemId, disabled, onItemClick } = this.props;

        if (!disabled) {
            onItemClick(itemId);
        }
    };

    renderContent() {
        const {
            title,
            subtitle,
            disabled,
            linkComponent,
            href,
            to
        } = this.props;

        if (!title && !subtitle) {
            return null;
        }

        const isLink = !disabled && (href || to);
        const ElementType = isLink ? linkComponent : 'div';
        const elementProps = {
            onClick: this.handleItemClick,
            ...(isLink ? { href, to } : {})
        };

        return (
            <ElementType className={styles.content} {...elementProps}>
                {title && <ListItemText primary>{title}</ListItemText>}
                {subtitle && <ListItemText subContent>{subtitle}</ListItemText>}
            </ElementType>
        );
    }

    render() {
        const {
            children,
            saveRef,
            sideContent,
            isSelected,
            disabled,
            onMouseEnter,
            onMouseLeave
        } = this.props;

        const classes = classnames(styles.listItem, {
            [styles.selected]: isSelected,
            [styles.disabled]: disabled
        });

        return (
            <li
                ref={saveRef}
                className={classes}
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
            >
                {this.renderContent()}
                {sideContent && (
                    <ListItemText className={styles.sideContent}>
                        {sideContent}
                    </ListItemText>
                )}
                {children}
            </li>
        );
    }
}

ListItem.propTypes = {
    children: PropTypes.node,
    saveRef: PropTypes.func,
    itemId: PropTypes.string,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    sideContent: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    linkComponent: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
        PropTypes.object
    ]),
    href: PropTypes.string,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    isSelected: PropTypes.bool,
    disabled: PropTypes.bool,
    onItemClick: PropTypes.func,
    onMouseEnter: PropTypes.func,
    onMouseLeave: PropTypes.func
};

ListItem.defaultProps = {
    children: null,
    saveRef: noop,
    itemId: '',
    title: '',
    subtitle: '',
    sideContent: '',
    linkComponent: 'a',
    href: '',
    to: '',
    isSelected: false,
    disabled: false,
    onItemClick: noop,
    onMouseEnter: noop,
    onMouseLeave: noop
};

export default withScrollIntoView(ListItem, { behavior: 'smooth' });
