import React from 'react';
import PropTypes from 'prop-types';
import styles from './List.css';

const List = ({ children }) => <ul className={styles.list}>{children}</ul>;

List.propTypes = {
    children: PropTypes.node
};

List.defaultProps = {
    children: null
};

export default List;
