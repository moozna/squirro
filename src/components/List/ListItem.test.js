import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ListItemWithScrollIntoView, { ListItem } from './ListItem';

describe('<ListItem />', () => {
    describe('<ListItemWithScrollIntoView />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ListItemWithScrollIntoView />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<ListItem />', () => {
        test('has the expected html structure by default', () => {
            const wrapper = shallow(<ListItem />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('has the expected html structure when props are specified', () => {
            const wrapper = shallow(
                <ListItem title="title" subtitle="authorName" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders children by default', () => {
            const testChildren = <div />;
            const wrapper = shallow(<ListItem>{testChildren}</ListItem>);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('calls saveRef on the root node', () => {
            const mockSaveRef = jest.fn();
            mount(<ListItem saveRef={mockSaveRef} />);

            expect(mockSaveRef).toHaveBeenCalledTimes(1);
        });

        test('renders a selected list item', () => {
            const wrapper = shallow(
                <ListItem title="title" subtitle="authorName" isSelected />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders sideContent when it is provided', () => {
            const wrapper = shallow(<ListItem sideContent="sideContent" />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a Link tag when "href" is provided', () => {
            const wrapper = shallow(<ListItem title="title" href="/books" />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a Link tag with custom link component provided', () => {
            const wrapper = shallow(
                <ListItem title="title" linkComponent="Link" to="/books" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('fires the onItemClick callback', () => {
            const handleOnClick = jest.fn();
            const wrapper = shallow(
                <ListItem
                    itemId="3"
                    title="title"
                    onItemClick={handleOnClick}
                />
            );

            wrapper.find('.content').simulate('click');
            expect(handleOnClick.mock.calls.length).toBe(1);
            expect(handleOnClick.mock.calls[0][0]).toBe('3');
        });

        test('does not fire the onItemClick callback when it is disabled', () => {
            const handleOnClick = jest.fn();
            const wrapper = shallow(
                <ListItem title="title" onItemClick={handleOnClick} disabled />
            );

            wrapper.find('.content').simulate('click');
            expect(handleOnClick.mock.calls.length).toBe(0);
        });
    });
});
