import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Votes from './Votes';

describe('<Votes />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Votes />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<Votes value={1300} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
