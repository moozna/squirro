import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import MetaData from './MetaData';
import Identifier from './Identifier';

const InfoGroup = ({ year, pages, read, asin, goodreadsId }) => (
    <div className={styles.infoGroup}>
        <div className={styles.identifiers}>
            <Identifier value={asin} siteName="amazon" withIcon />
            <Identifier value={goodreadsId} siteName="goodreads" withIcon />
        </div>
        <MetaData value={year} withIcon label="year" />
        <MetaData value={pages} withIcon label="pages" />
        <MetaData value={read} withIcon label="read" />
    </div>
);

InfoGroup.propTypes = {
    year: PropTypes.number,
    pages: PropTypes.number,
    read: PropTypes.number,
    asin: PropTypes.string,
    goodreadsId: PropTypes.number
};

InfoGroup.defaultProps = {
    year: null,
    pages: null,
    read: null,
    asin: '',
    goodreadsId: null
};

export default InfoGroup;
