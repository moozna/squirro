import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import { booksPage } from '../../data/actions/pageActions';
import styles from './DetailsCard.css';
import noop from '../../utils/noop';

class FilterLink extends Component {
    handleClick = () => {
        const { onFiltering, value } = this.props;
        onFiltering(value);
    };

    render() {
        const { children, filterable } = this.props;

        if (!filterable) {
            return children;
        }

        return (
            <Link
                className={styles.filterLink}
                to={booksPage()}
                onClick={this.handleClick}
            >
                {children}
            </Link>
        );
    }
}

FilterLink.propTypes = {
    children: PropTypes.node,
    value: PropTypes.string,
    filterable: PropTypes.bool,
    onFiltering: PropTypes.func
};

FilterLink.defaultProps = {
    children: null,
    value: '',
    filterable: false,
    onFiltering: noop
};

export default FilterLink;
