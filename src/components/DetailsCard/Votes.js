import React from 'react';
import PropTypes from 'prop-types';
import DisplayNumber from '../DisplayNumber/DisplayNumber';

const Votes = ({ value }) => <DisplayNumber value={value} />;

Votes.propTypes = {
    value: PropTypes.number
};

Votes.defaultProps = {
    value: null
};

export default Votes;
