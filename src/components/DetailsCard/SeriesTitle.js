import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FilterLink from './FilterLink';
import WithSeparator from '../WithSeparator/WithSeparator';
import noop from '../../utils/noop';

class SeriesTitle extends Component {
    renderTitleWithLink(title) {
        const { filterable, onFiltering } = this.props;
        return (
            <FilterLink
                value={title}
                filterable={filterable}
                onFiltering={onFiltering}
            >
                {title}
            </FilterLink>
        );
    }

    render() {
        const { value, className } = this.props;
        const { seriesTitle, seriesIndex } = value;

        if (!seriesTitle && !seriesIndex) {
            return null;
        }

        const seriesTitleLink = seriesTitle
            ? this.renderTitleWithLink(seriesTitle)
            : null;
        const formattedIndex = seriesIndex ? `#${seriesIndex}` : '';

        return (
            <div className={className}>
                <WithSeparator separator=" ">
                    {seriesTitleLink}
                    {formattedIndex}
                </WithSeparator>
            </div>
        );
    }
}

SeriesTitle.propTypes = {
    value: PropTypes.shape({
        seriesTitle: PropTypes.string,
        seriesIndex: PropTypes.string
    }),
    className: PropTypes.string,
    filterable: PropTypes.bool,
    onFiltering: PropTypes.func
};

SeriesTitle.defaultProps = {
    value: {},
    className: '',
    filterable: false,
    onFiltering: noop
};

export default SeriesTitle;
