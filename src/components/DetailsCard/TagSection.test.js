import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import TagSection from './TagSection';

describe('<TagSection />', () => {
    const genres = ['Action', 'Horror', 'Science Fiction'];
    const tags = ['read', 'want to read', 'wishlist'];

    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<TagSection />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<TagSection genres={genres} tags={tags} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a separator when either data has items', () => {
        const wrapper = shallow(<TagSection tags={tags} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
