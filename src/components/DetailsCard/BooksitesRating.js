import React from 'react';
import styles from './DetailsCard.css';
import { ratingType } from '../../types/types';
import CircleRating from './CircleRating';

const BooksitesRating = ({ amazon, goodreads }) => (
    <div className={styles.booksitesRating}>
        {amazon ? (
            <CircleRating
                title="amazon"
                value={amazon.rating}
                numberOfRatings={amazon.numberOfRatings}
                maxRating={5}
            />
        ) : null}
        {goodreads ? (
            <CircleRating
                title="goodreads"
                value={goodreads.rating}
                numberOfRatings={goodreads.numberOfRatings}
                maxRating={5}
            />
        ) : null}
    </div>
);

BooksitesRating.propTypes = {
    amazon: ratingType,
    goodreads: ratingType
};

BooksitesRating.defaultProps = {
    amazon: null,
    goodreads: null
};

export default BooksitesRating;
