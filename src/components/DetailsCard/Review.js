import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import SubHeader from '../SubHeader/SubHeader';

const Review = ({ value }) => {
    if (!value) {
        return null;
    }

    return (
        <div className={styles.review}>
            <hr />
            <SubHeader caption="Review" align="left" />
            <div dangerouslySetInnerHTML={{ __html: value }} />
        </div>
    );
};

Review.propTypes = {
    value: PropTypes.string
};

Review.defaultProps = {
    value: ''
};

export default Review;
