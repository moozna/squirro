import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import AmazonLogo from '../svg/AmazonLogo';
import GoodreadsLogo from '../svg/GoodreadsLogo';

const SITE_DATA = {
    amazon: {
        Logo: AmazonLogo,
        baseUrl: 'https://www.amazon.com/dp'
    },
    goodreads: {
        Logo: GoodreadsLogo,
        baseUrl: 'https://www.goodreads.com/book/show'
    }
};

const Identifier = ({ value, siteName, withIcon }) => {
    if (!value || !siteName) {
        return null;
    }

    const { Logo, baseUrl } = SITE_DATA[siteName];

    return (
        <a
            className={styles.identifier}
            target="_blank"
            rel="noopener noreferrer"
            href={`${baseUrl}/${value}`}
        >
            {withIcon ? (
                <span className={styles.identifierIcon}>
                    <Logo />
                </span>
            ) : null}
            {value}
        </a>
    );
};

Identifier.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    siteName: PropTypes.oneOf(['', 'amazon', 'goodreads']),
    withIcon: PropTypes.bool
};

Identifier.defaultProps = {
    value: '',
    siteName: '',
    withIcon: false
};

export default Identifier;
