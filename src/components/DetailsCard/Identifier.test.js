import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Identifier from './Identifier';

describe('<Identifier />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Identifier />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <Identifier value="asin" siteName="amazon" withIcon />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an identifier without icon', () => {
        const wrapper = shallow(
            <Identifier value="asin" siteName="amazon" withIcon={false} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
