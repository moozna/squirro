import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import noop from '../../utils/noop';
import NavButtons from '../NavButtons/NavButtons';
import NavButton from '../NavButtons/NavButton';
import DeleteButton from '../Button/DeleteButton';
import Edit from '../svg/Edit';
import Close from '../svg/Close';
import { editBookPage } from '../../data/actions/pageActions';

const Navigation = ({ bookId, createClosePath, onDelete }) => (
    <NavButtons buttonsClass={styles.navigation}>
        {bookId && (
            <NavButton to={editBookPage(bookId)}>
                <Edit />
            </NavButton>
        )}
        <DeleteButton name="book" shadow="inner" onDelete={onDelete} />
        <NavButton to={createClosePath()}>
            <Close />
        </NavButton>
    </NavButtons>
);

Navigation.propTypes = {
    bookId: PropTypes.string,
    createClosePath: PropTypes.func,
    onDelete: PropTypes.func
};

Navigation.defaultProps = {
    bookId: '',
    createClosePath: noop,
    onDelete: noop
};

export default Navigation;
