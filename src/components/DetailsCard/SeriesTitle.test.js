import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SeriesTitle from './SeriesTitle';

describe('<SeriesTitle />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<SeriesTitle />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <SeriesTitle
                value={{
                    seriesTitle: 'seriesTitle',
                    seriesIndex: '2'
                }}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders series title when series index is missing', () => {
        const wrapper = shallow(
            <SeriesTitle
                value={{
                    seriesTitle: 'seriesTitle'
                }}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders formatted series index when series title is missing', () => {
        const wrapper = shallow(
            <SeriesTitle
                value={{
                    seriesIndex: '2'
                }}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(
            <SeriesTitle
                value={{
                    seriesTitle: 'seriesTitle',
                    seriesIndex: '2'
                }}
                className="myClass"
            />
        );

        expect(wrapper.hasClass('myClass')).toBe(true);
    });
});
