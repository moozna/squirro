import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import { bookType } from '../../types/types';
import CoverViewer from './CoverViewer';
import SeriesTitle from './SeriesTitle';
import Authors from './Authors';
import Description from './Description';
import TagSection from './TagSection';
import StarRating from './StarRating';
import InterestLevel from './InterestLevel';
import BooksitesRating from './BooksitesRating';
import InfoGroup from './InfoGroup';
import ShelfTags from './ShelfTags';
import Review from './Review';
import Navigation from './Navigation';
import noop from '../../utils/noop';

const DetailsCard = ({
    book,
    searchBooks,
    updateGenres,
    updateTags,
    updateSelectedShelf,
    createClosePath,
    deleteBook
}) => (
    <div className={styles.detailsCard}>
        <div className={styles.imageColumn}>
            <CoverViewer covers={book.covers} asin={book.asin} />
            <div className={styles.ratings}>
                <StarRating value={book.rating} />
                <InterestLevel value={book.interestLevel} />
                <BooksitesRating
                    amazon={book.amazonRating}
                    goodreads={book.goodreadsRating}
                />
            </div>
            <InfoGroup
                year={book.publicationYear}
                pages={book.pages}
                read={book.numberOfTimesRead}
                asin={book.asin}
                goodreadsId={book.goodreadsId}
            />
        </div>
        <div className={styles.detailsColumn}>
            <div className={styles.cardHeader}>
                <div className={styles.title}>{book.title}</div>
                <SeriesTitle
                    value={{
                        seriesTitle: book.seriesTitle,
                        seriesIndex: book.seriesIndex
                    }}
                    className={styles.subtitle}
                    filterable
                    onFiltering={searchBooks}
                />
                <Authors
                    value={book.author}
                    className={styles.subtitle}
                    filterable
                    onFiltering={searchBooks}
                />
            </div>
            <TagSection
                genres={book.genres}
                tags={book.tags}
                updateGenres={updateGenres}
                updateTags={updateTags}
            />
            <Description
                className={styles.description}
                value={book.description}
            />
            <ShelfTags
                shelves={book.shelves}
                updateSelectedShelf={updateSelectedShelf}
            />
            <Review value={book.review} />
        </div>
        <Navigation
            bookId={book._id}
            createClosePath={createClosePath}
            onDelete={deleteBook}
        />
    </div>
);

DetailsCard.propTypes = {
    book: bookType,
    searchBooks: PropTypes.func,
    updateGenres: PropTypes.func,
    updateTags: PropTypes.func,
    updateSelectedShelf: PropTypes.func,
    createClosePath: PropTypes.func,
    deleteBook: PropTypes.func
};

DetailsCard.defaultProps = {
    book: {},
    searchBooks: noop,
    updateGenres: noop,
    updateTags: noop,
    updateSelectedShelf: noop,
    createClosePath: noop,
    deleteBook: noop
};

export default DetailsCard;
