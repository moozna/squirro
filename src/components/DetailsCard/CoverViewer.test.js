import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CoverViewer from './CoverViewer';

describe('<CoverViewer />', () => {
    const image1 = { url: 'image1' };
    const image2 = { url: 'image2' };
    const image3 = { url: 'image3' };

    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<CoverViewer />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when covers are specified', () => {
        const mockCovers = [image1, image2, image3];
        const wrapper = shallow(
            <CoverViewer covers={mockCovers} asin="asin" />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
