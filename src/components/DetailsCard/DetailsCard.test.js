import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DetailsCard from './DetailsCard';

describe('<DetailsCard />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<DetailsCard />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when book is specified', () => {
        const mockBook = {
            _id: '1',
            asin: 'B003BKZW4U',
            goodreadsId: 20,
            title: 'title',
            author: ['Stephen King'],
            seriesTitle: 'seriesTitle',
            seriesIndex: '2',
            rating: 3.5,
            interestLevel: 3,
            amazonRating: {
                rating: 2,
                numberOfRatings: 100
            },
            goodreadsRating: {
                rating: 4,
                numberOfRatings: 500
            },
            genres: ['action', 'adventure'],
            tags: ['read', 'wait for beautify'],
            shelves: ['Best Historical Fiction', 'Best Book Cover Art'],
            publicationYear: 2001,
            pages: 400,
            numberOfTimesRead: 3,
            description: 'description',
            review: 'review',
            covers: [
                {
                    url: '/images/covers/image1.jpg',
                    thumbnail: '/images/smallcovers/image1.jpg'
                }
            ]
        };
        const wrapper = shallow(<DetailsCard book={mockBook} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
