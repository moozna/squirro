import React from 'react';
import PropTypes from 'prop-types';
import FilterLink from './FilterLink';
import WithSeparator from '../WithSeparator/WithSeparator';
import noop from '../../utils/noop';

const Authors = ({ value, className, filterable, onFiltering }) => {
    if (!value.length) {
        return null;
    }

    return (
        <div className={className}>
            <WithSeparator separator=", ">
                {value.map(author => (
                    <FilterLink
                        key={author}
                        value={author}
                        filterable={filterable}
                        onFiltering={onFiltering}
                    >
                        {author}
                    </FilterLink>
                ))}
            </WithSeparator>
        </div>
    );
};

Authors.propTypes = {
    value: PropTypes.arrayOf(PropTypes.string),
    className: PropTypes.string,
    filterable: PropTypes.bool,
    onFiltering: PropTypes.func
};

Authors.defaultProps = {
    value: [],
    className: '',
    filterable: false,
    onFiltering: noop
};

export default Authors;
