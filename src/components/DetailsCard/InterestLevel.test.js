import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InterestLevel from './InterestLevel';

describe('<InterestLevel />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<InterestLevel />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when interest level is specified', () => {
        const wrapper = shallow(<InterestLevel value={3} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render slider when interest level is not specified', () => {
        const wrapper = shallow(<InterestLevel value={0} />);

        expect(wrapper.isEmptyRender()).toBe(true);
    });
});
