import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import noop from '../../utils/noop';
import Shelves from '../svg/Shelves';
import Tags from '../Tags/Tags';

const ShelfTags = ({ shelves, updateSelectedShelf }) => {
    if (!shelves.length) {
        return null;
    }

    return (
        <div className={styles.shelfTags}>
            <div className={styles.shelfIcon}>
                <Shelves />
            </div>
            <Tags
                value={shelves}
                filterable
                onFiltering={updateSelectedShelf}
            />
        </div>
    );
};

ShelfTags.propTypes = {
    shelves: PropTypes.arrayOf(PropTypes.string),
    updateSelectedShelf: PropTypes.func
};

ShelfTags.defaultProps = {
    shelves: [],
    updateSelectedShelf: noop
};

export default ShelfTags;
