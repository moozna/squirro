import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Navigation from './Navigation';

describe('<Navigation />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Navigation />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when bookId is specified', () => {
        const wrapper = shallow(<Navigation bookId="1" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
