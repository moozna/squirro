import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import CircularProgressBar from '../CircularProgressBar/CircularProgressBar';
import Votes from './Votes';

class CircleRating extends Component {
    calculatePercentage() {
        const { value, maxRating } = this.props;
        return (value * 100) / maxRating;
    }

    render() {
        const { title, value, numberOfRatings } = this.props;
        const percentageValue = this.calculatePercentage();

        return (
            <div className={styles.circleRating}>
                <div className={styles.title}>{title}</div>
                <CircularProgressBar
                    percentage={percentageValue}
                    label={value}
                />
                <div className={styles.numberOfRatings}>
                    <Votes value={numberOfRatings} />
                </div>
            </div>
        );
    }
}

CircleRating.propTypes = {
    value: PropTypes.number,
    title: PropTypes.string,
    numberOfRatings: PropTypes.number,
    maxRating: PropTypes.number
};

CircleRating.defaultProps = {
    value: null,
    title: '',
    numberOfRatings: null,
    maxRating: 5
};

export default CircleRating;
