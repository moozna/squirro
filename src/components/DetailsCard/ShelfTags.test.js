import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ShelfTags from './ShelfTags';

describe('<ShelfTags />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ShelfTags />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when shelves are specified', () => {
        const shelves = ['Best Historical Fiction', 'Best Book Cover Art'];
        const wrapper = shallow(<ShelfTags shelves={shelves} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
