import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import Slider from '../Slider/Slider';

export const INTEREST_LEVELS = {
    1: 'Not at all',
    2: 'Maybe',
    3: 'Moderately',
    4: 'Interesting',
    5: 'Definitely'
};

const InterestLevel = ({ value }) => {
    if (!value) {
        return null;
    }

    return (
        <div className={styles.interestLevel}>
            <Slider
                label="Interest"
                value={value}
                outputLabels={INTEREST_LEVELS}
                readOnly
            />
        </div>
    );
};

InterestLevel.propTypes = {
    value: PropTypes.number
};

InterestLevel.defaultProps = {
    value: 0
};

export default InterestLevel;
