import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InfoGroup from './InfoGroup';

describe('<InfoGroup />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<InfoGroup />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <InfoGroup
                year={2010}
                pages={100}
                read={2}
                asin="asin"
                goodreadsId={2}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
