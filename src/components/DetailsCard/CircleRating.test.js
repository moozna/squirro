import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CircleRating from './CircleRating';

describe('<CircleRating />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<CircleRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <CircleRating
                title="title"
                value={4.3}
                numberOfRatings={1300}
                maxRating={5}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
