import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Description from './Description';

describe('<Description />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Description />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<Description value="description" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(
            <Description value="description" className="myClass" />
        );

        expect(wrapper.hasClass('myClass')).toBe(true);
    });
});
