import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Review from './Review';

describe('<Review />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Review />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<Review value="Review" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
