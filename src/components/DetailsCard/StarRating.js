import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import Rating from '../Rating/Rating';

const StarRating = ({ value }) => (
    <div className={styles.starRating}>
        <Rating rating={value} numberOfStars={5} readOnly />
    </div>
);

StarRating.propTypes = {
    value: PropTypes.number
};

StarRating.defaultProps = {
    value: 0
};

export default StarRating;
