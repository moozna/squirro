import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import BooksitesRating from './BooksitesRating';

describe('<BooksitesRating />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<BooksitesRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const mockAmazonData = {
            rating: 2.4,
            numberOfRatings: 42000
        };
        const mockGoodreadsData = {
            rating: 4.09,
            numberOfRatings: 6200
        };

        const wrapper = shallow(
            <BooksitesRating
                amazon={mockAmazonData}
                goodreads={mockGoodreadsData}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders Amazon rating when only that data is available', () => {
        const mockAmazonData = {
            rating: 2.4,
            numberOfRatings: 42000
        };

        const wrapper = shallow(<BooksitesRating amazon={mockAmazonData} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders Goodreads rating when only that data is available', () => {
        const mockGoodreadsData = {
            rating: 4.09,
            numberOfRatings: 6200
        };

        const wrapper = shallow(
            <BooksitesRating goodreads={mockGoodreadsData} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
