import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import Calendar from '../svg/Calendar';
import OpenBook from '../svg/OpenBook';
import ReadingMan from '../svg/Reading';

class MetaData extends Component {
    renderIcon() {
        const { label } = this.props;

        switch (label) {
            case 'year':
                return <Calendar />;
            case 'pages':
                return <OpenBook />;
            case 'read':
                return <ReadingMan />;
            default:
                return null;
        }
    }

    render() {
        const { value, label, withIcon } = this.props;

        return (
            <div className={styles.metaData}>
                <div className={styles.metaDataLabel}>
                    {withIcon ? this.renderIcon() : label}
                </div>
                {value}
            </div>
        );
    }
}

MetaData.propTypes = {
    value: PropTypes.number,
    label: PropTypes.string,
    withIcon: PropTypes.bool
};

MetaData.defaultProps = {
    value: null,
    label: '',
    withIcon: false
};

export default MetaData;
