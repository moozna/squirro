import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import MetaData from './MetaData';

describe('<MetaData />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<MetaData />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <MetaData value={100} label="pages" withIcon />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when label is year', () => {
        const wrapper = shallow(
            <MetaData value={2010} label="year" withIcon />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when label is pages', () => {
        const wrapper = shallow(
            <MetaData value={100} label="pages" withIcon />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected icon when label is read', () => {
        const wrapper = shallow(<MetaData value={2} label="read" withIcon />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does mot render icon when label is null', () => {
        const wrapper = shallow(<MetaData value={2} label={null} withIcon />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
