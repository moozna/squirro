import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FilterLink from './FilterLink';

describe('<FilterLink />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<FilterLink />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<FilterLink>{testChildren}</FilterLink>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a link when filterable is true', () => {
        const wrapper = shallow(<FilterLink filterable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleClick()', () => {
        test('fires the onFiltering callback', () => {
            const mockValue = 'Stephen King';
            const handleOnFiltering = jest.fn();

            const wrapper = shallow(
                <FilterLink
                    value={mockValue}
                    filterable
                    onFiltering={handleOnFiltering}
                />
            );
            wrapper.simulate('click');

            expect(handleOnFiltering.mock.calls.length).toBe(1);
            expect(handleOnFiltering.mock.calls[0][0]).toBe(mockValue);
        });
    });
});
