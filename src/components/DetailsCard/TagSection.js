import React from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import Tags from '../Tags/Tags';
import noop from '../../utils/noop';

const TagSection = ({ genres, tags, updateGenres, updateTags }) => (
    <div className={styles.tagSection}>
        <Tags
            value={genres}
            color="orange"
            filterable
            onFiltering={updateGenres}
        />
        <Tags value={tags} filterable onFiltering={updateTags} />
        {genres.length || tags.length ? <hr /> : null}
    </div>
);

TagSection.propTypes = {
    genres: PropTypes.arrayOf(PropTypes.string),
    tags: PropTypes.arrayOf(PropTypes.string),
    updateGenres: PropTypes.func,
    updateTags: PropTypes.func
};

TagSection.defaultProps = {
    genres: [],
    tags: [],
    updateGenres: noop,
    updateTags: noop
};

export default TagSection;
