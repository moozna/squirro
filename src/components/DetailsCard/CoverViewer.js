import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './DetailsCard.css';
import { coversType } from '../../types/types';
import Carousel from '../Carousel/Carousel';
import Cover from '../Cover/Cover';

class CoverViewer extends Component {
    renderImage(url) {
        const { asin } = this.props;

        return (
            <Cover
                key={url}
                imagePath={url}
                altText={asin}
                placeholderClass={styles.placeholder}
            />
        );
    }

    render() {
        const { covers } = this.props;

        if (!covers || !covers.length) {
            return this.renderImage('');
        }

        return (
            <div className={styles.coverViewer}>
                <Carousel>
                    {covers.map(item => this.renderImage(item.url))}
                </Carousel>
            </div>
        );
    }
}

CoverViewer.propTypes = {
    covers: coversType,
    asin: PropTypes.string
};

CoverViewer.defaultProps = {
    covers: [],
    asin: ''
};

export default CoverViewer;
