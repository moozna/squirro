import React from 'react';
import PropTypes from 'prop-types';

const Description = ({ value, className }) => (
    <div className={className} dangerouslySetInnerHTML={{ __html: value }} />
);

Description.propTypes = {
    value: PropTypes.string,
    className: PropTypes.string
};

Description.defaultProps = {
    value: '',
    className: ''
};

export default Description;
