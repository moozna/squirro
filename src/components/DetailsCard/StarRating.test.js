import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import StarRating from './StarRating';

describe('<StarRating />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<StarRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when rating is specified', () => {
        const wrapper = shallow(<StarRating value={3.5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
