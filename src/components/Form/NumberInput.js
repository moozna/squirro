import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Input.css';
import noop from '../../utils/noop';
import {
    isNotCompleteNumber,
    isNumeric,
    toFixedDecimals,
    getRatio,
    getPrecision,
    getMinValue,
    getMaxValue
} from './NumberUtils';
import InputField from './InputField';

class NumberInput extends Component {
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            this.inputNode.setSelectionRange(snapshot.start, snapshot.end);
        }
    }

    getSnapshotBeforeUpdate() {
        if (this.isActiveElement()) {
            return {
                start: this.inputNode.selectionStart,
                end: this.inputNode.selectionEnd
            };
        }

        return null;
    }

    getInputValue() {
        const { value } = this.props;

        if (value === null || value === undefined) {
            return '';
        }

        if (isNotCompleteNumber(value)) {
            return value;
        }

        let inputValue = this.getValidValue(value);
        const precision = Math.abs(this.getMaxPrecision(value));
        inputValue = Number(toFixedDecimals(inputValue, precision));

        return inputValue;
    }

    getValidValue(value) {
        const { min, max } = this.props;
        const minValue = getMinValue(min);
        const maxValue = getMaxValue(max);

        if (value < minValue) {
            return minValue;
        }

        if (value > maxValue) {
            return maxValue;
        }

        return value;
    }

    getMaxPrecision(currentValue, ratio = 1) {
        const { precision, step } = this.props;

        if (isNumeric(precision)) {
            return getPrecision(precision);
        }

        const ratioPrecision = getPrecision(ratio);
        const stepPrecision = getPrecision(step);

        if (!currentValue) {
            return ratioPrecision + stepPrecision;
        }

        const currentValuePrecision = getPrecision(currentValue);
        return Math.max(currentValuePrecision, ratioPrecision + stepPrecision);
    }

    getDisplayValue() {
        const { format } = this.props;
        const value = this.getInputValue();

        if (format) {
            return format(value);
        }

        return value;
    }

    handleChange = (name, value) => {
        const { onChange } = this.props;
        const numericValue = value.replace(/[^0-9.]/g, '');
        onChange(name, this.parseForOutput(numericValue));
    };

    handleKeyDown = event => {
        if (event.key === 'ArrowUp') {
            event.preventDefault();
            const ratio = getRatio(event);
            this.step(ratio);
        } else if (event.key === 'ArrowDown') {
            event.preventDefault();
            const ratio = getRatio(event);
            this.step(-ratio);
        }
    };

    saveRef = node => {
        this.inputNode = node;
    };

    step(ratio) {
        const { step, name, onChange } = this.props;
        const value = this.getInputValue();

        const precision = Math.abs(this.getMaxPrecision(value, ratio));
        const newValue = ((value || 0) + step * ratio).toFixed(precision);
        onChange(name, this.parseForOutput(newValue));
    }

    parseForOutput(value) {
        const { parse } = this.props;
        let inputValue = value;

        if (parse) {
            inputValue = parse(inputValue);
        }

        if (inputValue === '') {
            return null;
        }

        if (isNotCompleteNumber(inputValue)) {
            return inputValue;
        }

        inputValue = this.getValidValue(inputValue);
        const precision = Math.abs(this.getMaxPrecision(value));
        inputValue = Number(toFixedDecimals(inputValue, precision));

        return inputValue;
    }

    isActiveElement() {
        return document.activeElement === this.inputNode;
    }

    render() {
        const { name, label, touched, error, align, onBlur } = this.props;

        return (
            <InputField
                saveRef={this.saveRef}
                inputElementClass={styles.numberInput}
                value={this.getDisplayValue()}
                name={name}
                label={label}
                touched={touched}
                error={error}
                align={align}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
                onBlur={onBlur}
            />
        );
    }
}

NumberInput.propTypes = {
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
    label: PropTypes.string,
    precision: PropTypes.number,
    step: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    touched: PropTypes.bool,
    error: PropTypes.string,
    align: PropTypes.oneOf(['center', 'left']),
    format: PropTypes.func,
    parse: PropTypes.func,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

NumberInput.defaultProps = {
    value: '',
    name: '',
    label: '',
    precision: null,
    step: 1,
    min: null,
    max: null,
    touched: false,
    error: '',
    align: 'center',
    format: null,
    parse: null,
    onChange: noop,
    onBlur: noop
};

export default NumberInput;
