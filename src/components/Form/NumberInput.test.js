import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import NumberInput from './NumberInput';
import { formatNumber, parseNumber } from './NumberUtils';

describe('<NumberInput />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<NumberInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the input', () => {
        const wrapper = mount(<NumberInput />);

        expect(wrapper.instance().inputNode).toBeDefined();
    });

    describe('getInputValue()', () => {
        test('returns empty string when value is null', () => {
            const wrapper = shallow(<NumberInput value={null} />);

            expect(wrapper.instance().getInputValue()).toBe('');
        });

        test('returns empty string when value is undefined', () => {
            const wrapper = shallow(<NumberInput value={undefined} />);

            expect(wrapper.instance().getInputValue()).toBe('');
        });

        test('returns 0 when value is 0', () => {
            const wrapper = shallow(<NumberInput value={0} />);

            expect(wrapper.instance().getInputValue()).toBe(0);
        });

        test('returns max value when value is larger', () => {
            const wrapper = shallow(<NumberInput value={10} max={5} />);

            expect(wrapper.instance().getInputValue()).toBe(5);
        });

        test('returns min value when value is smaller', () => {
            const wrapper = shallow(<NumberInput value={-2} min={0} />);

            expect(wrapper.instance().getInputValue()).toBe(0);
        });

        test('returns an integer when precision is 0', () => {
            const wrapper = shallow(
                <NumberInput value={12.78} precision={0} />
            );

            expect(wrapper.instance().getInputValue()).toBe(12);
        });

        test('returns a number with one decimal when precision is 0.1', () => {
            const wrapper = shallow(
                <NumberInput value={12.78} precision={0.1} />
            );

            expect(wrapper.instance().getInputValue()).toBe(12.7);
        });

        test('returns a number with two decimals when precision is 0.01', () => {
            const wrapper = shallow(
                <NumberInput value={12.78} precision={0.01} />
            );

            expect(wrapper.instance().getInputValue()).toBe(12.78);
        });

        test('uses step when precision is not provided', () => {
            const wrapper = shallow(<NumberInput value={12.78} step={0.01} />);

            expect(wrapper.instance().getInputValue()).toBe(12.78);
        });

        test('returns the original value when precision and step are not provided', () => {
            const wrapper = shallow(<NumberInput value={12.78564} />);

            expect(wrapper.instance().getInputValue()).toBe(12.78564);
        });

        test('does not throw when value is string', () => {
            const wrapper = shallow(
                <NumberInput value="12.78" precision={0.01} />
            );

            expect(wrapper.instance().getInputValue()).toBe(12.78);
        });
    });

    describe('update', () => {
        test('saves previous cursor position before update when the field is focused', () => {
            const wrapper = mount(
                <NumberInput value={12.78} precision={0.1} />
            );
            wrapper.instance().inputNode.focus();
            wrapper.instance().inputNode.selectionStart = 2;
            wrapper.instance().inputNode.selectionEnd = 3;

            const result = wrapper.instance().getSnapshotBeforeUpdate();

            expect(result.start).toBe(2);
            expect(result.end).toBe(3);
        });

        test('does not save previous cursor position when the field is not focused', () => {
            const wrapper = mount(
                <NumberInput value={12.78} precision={0.1} />
            );
            wrapper.instance().inputNode.selectionStart = 2;
            wrapper.instance().inputNode.selectionEnd = 3;

            const result = wrapper.instance().getSnapshotBeforeUpdate();

            expect(result).toBeNull();
        });

        test('restores previous cursor position when it is available', () => {
            const wrapper = mount(
                <NumberInput value={12.78} precision={0.1} />
            );
            wrapper.instance().inputNode.focus();
            wrapper.instance().inputNode.selectionStart = 2;
            wrapper.instance().inputNode.selectionEnd = 3;
            const mockSetSelectionRange = jest.spyOn(
                wrapper.instance().inputNode,
                'setSelectionRange'
            );
            wrapper.setProps({ value: 12.8 });

            expect(mockSetSelectionRange.mock.calls.length).toBe(1);
            expect(mockSetSelectionRange.mock.calls[0][0]).toBe(2);
            expect(mockSetSelectionRange.mock.calls[0][1]).toBe(3);
        });

        test('does not try to restore previous cursor position when it is not available', () => {
            const wrapper = mount(
                <NumberInput value={12.78} precision={0.1} />
            );
            wrapper.instance().inputNode.selectionStart = 2;
            wrapper.instance().inputNode.selectionEnd = 3;
            const mockSetSelectionRange = jest.spyOn(
                wrapper.instance().inputNode,
                'setSelectionRange'
            );
            wrapper.setProps({ value: 12.8 });

            expect(mockSetSelectionRange.mock.calls.length).toBe(0);
        });
    });

    describe('formatting', () => {
        test('formats numbers over 4 digits', () => {
            const wrapper = shallow(
                <NumberInput value={1000} format={formatNumber} />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('formats longer numbers', () => {
            const wrapper = shallow(
                <NumberInput value={1000000} format={formatNumber} />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('formats numbers with decimal part', () => {
            const wrapper = shallow(
                <NumberInput value={1000.3435} format={formatNumber} />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('formats numbers with decimal part based on precision', () => {
            const wrapper = shallow(
                <NumberInput
                    value={1000.3435}
                    format={formatNumber}
                    precision={0.1}
                />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('formats not complete numbers', () => {
            const wrapper = shallow(
                <NumberInput value="0." format={formatNumber} precision={0.1} />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('formats negative numbers', () => {
            const wrapper = shallow(
                <NumberInput
                    value="-0.6"
                    format={formatNumber}
                    precision={0.1}
                />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('onChange', () => {
        test('fires the onChange callback', () => {
            const mockEvent = {
                target: {
                    value: '12.3'
                }
            };
            const handleOnChange = jest.fn();

            const wrapper = mount(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    onChange={handleOnChange}
                />
            );
            wrapper.find('input').simulate('change', mockEvent);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('rating');
            expect(handleOnChange.mock.calls[0][1]).toBe(12.3);
        });

        test('removes non-numeric characters from the input value', () => {
            const mockEvent = {
                target: {
                    value: 'zzy12.3t'
                }
            };
            const handleOnChange = jest.fn();

            const wrapper = mount(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    onChange={handleOnChange}
                />
            );
            wrapper.find('input').simulate('change', mockEvent);

            expect(handleOnChange.mock.calls[0][1]).toBe(12.3);
        });

        test('formats the input value based on the provided precision', () => {
            const mockValue = '12.3456665';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    precision={0.1}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleChange(undefined, mockValue);

            expect(handleOnChange.mock.calls[0][1]).toBe(12.3);
        });

        test('parses formatted input value', () => {
            const mockValue = '1 000 000';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    parse={parseNumber}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleChange(undefined, mockValue);

            expect(handleOnChange.mock.calls[0][1]).toBe(1000000);
        });

        test('parses formatted input value and formats it based on the provided precision', () => {
            const mockValue = '1 000 000.256876';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    precision={0.1}
                    parse={parseNumber}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleChange(undefined, mockValue);

            expect(handleOnChange.mock.calls[0][1]).toBe(1000000.2);
        });

        test('parses not complete numbers', () => {
            const mockValue = '12 000.';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    parse={parseNumber}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleChange(undefined, mockValue);

            expect(handleOnChange.mock.calls[0][1]).toBe('12000.');
        });

        test('parses empty value as null', () => {
            const mockValue = '';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={1000.3435}
                    parse={parseNumber}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleChange(undefined, mockValue);

            expect(handleOnChange.mock.calls[0][1]).toBe(null);
        });
    });

    describe('onKeyDown', () => {
        describe('ArrowUp', () => {
            test('fires the onChange callback', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = mount(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        onChange={handleOnChange}
                    />
                );
                wrapper.find('input').simulate('keydown', mockEvent);

                expect(handleOnChange.mock.calls.length).toBe(1);
                expect(handleOnChange.mock.calls[0][0]).toBe('rating');
                expect(handleOnChange.mock.calls[0][1]).toBe(13.525);
            });

            test('formats the input value based on the provided precision', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(13.5);
            });

            test('uses 0.1 as step when ctrl key is pressed', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    ctrlKey: true,
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(12.6);
            });

            test('uses 0.1 as step when meta key is pressed', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    metaKey: true,
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(12.6);
            });

            test('uses 10 as step when shift key is pressed', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    shiftKey: true,
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(22.5);
            });

            test('uses 0 as initial value when it is not provided', () => {
                const mockEvent = {
                    key: 'ArrowUp',
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(1);
            });
        });

        describe('ArrowUp', () => {
            test('fires the onChange callback', () => {
                const mockEvent = {
                    key: 'ArrowDown',
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = mount(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        onChange={handleOnChange}
                    />
                );
                wrapper.find('input').simulate('keydown', mockEvent);

                expect(handleOnChange.mock.calls.length).toBe(1);
                expect(handleOnChange.mock.calls[0][0]).toBe('rating');
                expect(handleOnChange.mock.calls[0][1]).toBe(11.525);
            });

            test('formats the input value based on the provided precision', () => {
                const mockEvent = {
                    key: 'ArrowDown',
                    preventDefault: jest.fn()
                };
                const handleOnChange = jest.fn();

                const wrapper = shallow(
                    <NumberInput
                        name="rating"
                        value={12.525}
                        precision={0.1}
                        onChange={handleOnChange}
                    />
                );
                wrapper.instance().handleKeyDown(mockEvent);

                expect(handleOnChange.mock.calls[0][1]).toBe(11.5);
            });
        });

        test('other keys work as default', () => {
            const mockEvent = {
                key: 'x',
                preventDefault: jest.fn()
            };
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <NumberInput
                    name="rating"
                    value={12.525}
                    precision={0.1}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().handleKeyDown(mockEvent);

            expect(mockEvent.preventDefault.mock.calls.length).toBe(0);
            expect(handleOnChange.mock.calls.length).toBe(0);
        });
    });
});
