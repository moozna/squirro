import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ErrorMessage from './ErrorMessage';

describe('<ErrorMessage />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ErrorMessage />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders nothing when touched is false', () => {
        const wrapper = shallow(
            <ErrorMessage touched={false} error="Error message" />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders nothing when error message is not provided', () => {
        const wrapper = shallow(<ErrorMessage touched={false} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the expected html when it is touched and error message is provided', () => {
        const wrapper = shallow(<ErrorMessage touched error="Error message" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
