import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InputField from './InputField';

describe('<InputField />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<InputField />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a label when it is specified', () => {
        const wrapper = shallow(<InputField label="label" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an error message when it is specified', () => {
        const wrapper = shallow(<InputField touched error="Error message" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a flexible input with the specified minimum width', () => {
        const minWidth = 100;
        const wrapper = shallow(
            <InputField flexibleWidth minWidth={minWidth} />
        );
        const input = wrapper.find('Input');

        expect(input.props().flexibleWidth).toBe(true);
        expect(input.props().minWidth).toBe(minWidth);
    });

    test('renders a flexible input without minimum width', () => {
        const minWidth = null;
        const wrapper = shallow(
            <InputField flexibleWidth minWidth={minWidth} />
        );
        const input = wrapper.find('Input');

        expect(input.props().flexibleWidth).toBe(true);
        expect(input.props().minWidth).toBe(minWidth);
    });

    test('renders a multiline input', () => {
        const wrapper = shallow(<InputField multiline />);
        const input = wrapper.find('Input');

        expect(input.props().multiline).toBe(true);
    });

    test('renders a center aligned input', () => {
        const wrapper = shallow(<InputField align="center" />);
        const input = wrapper.find('Input');

        expect(input.prop('inputElementClass')).toEqual(
            expect.stringContaining('center')
        );
    });

    test('renders a left aligned input', () => {
        const wrapper = shallow(<InputField align="left" />);
        const input = wrapper.find('Input');

        expect(input.prop('inputElementClass')).toEqual(
            expect.stringContaining('left')
        );
    });

    test('animates the bar from left', () => {
        const wrapper = shallow(<InputField align="left" />);
        const bar = wrapper.find('.bar');

        expect(bar.hasClass('leftAnimation')).toBe(true);
    });

    test('animates the bar from the center', () => {
        const wrapper = shallow(<InputField align="center" />);
        const bar = wrapper.find('.bar');

        expect(bar.hasClass('centerAnimation')).toBe(true);
    });
});
