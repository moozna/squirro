import * as NumberUtils from './NumberUtils';

describe('NumberUtils', () => {
    describe('isNumeric()', () => {
        describe('returns false', () => {
            test('when value is null', () => {
                expect(NumberUtils.isNumeric(null)).toBe(false);
            });

            test('when value is undefined', () => {
                expect(NumberUtils.isNumeric(undefined)).toBe(false);
            });

            test('when value is empty string', () => {
                expect(NumberUtils.isNumeric('')).toBe(false);
            });

            test('when value is NaN', () => {
                expect(NumberUtils.isNumeric(NaN)).toBe(false);
            });

            test('when value is not complete number', () => {
                expect(NumberUtils.isNumeric('1.')).toBe(false);
            });
        });

        describe('returns true', () => {
            test('when value is integer', () => {
                expect(NumberUtils.isNumeric(123)).toBe(true);
            });

            test('when value is floating point number', () => {
                expect(NumberUtils.isNumeric(12.3)).toBe(true);
            });

            test('when value is 0', () => {
                expect(NumberUtils.isNumeric(0)).toBe(true);
            });
        });
    });

    describe('isNotCompleteNumber()', () => {
        describe('returns true', () => {
            test('when value is NaN', () => {
                expect(NumberUtils.isNotCompleteNumber(NaN)).toBe(true);
            });

            test('when value is empty string', () => {
                expect(NumberUtils.isNotCompleteNumber('')).toBe(true);
            });

            test('when value is negative sign', () => {
                expect(NumberUtils.isNotCompleteNumber('-')).toBe(true);
            });

            test('when last value is decimal point', () => {
                expect(NumberUtils.isNotCompleteNumber('123.')).toBe(true);
            });
        });
    });

    describe('toFixedDecimals()', () => {
        test('formats values with dot as decimal point', () => {
            expect(NumberUtils.toFixedDecimals(12.66666, 2)).toBe('12.66');
        });

        test('formats values with comma as decimal point', () => {
            expect(NumberUtils.toFixedDecimals('12,66666', 2)).toBe('12,66');
        });

        test('formats negative values', () => {
            expect(NumberUtils.toFixedDecimals('-12,66666', 2)).toBe('-12,66');
        });

        test('formats value as integers when precision is 0', () => {
            expect(NumberUtils.toFixedDecimals(12.66666, 0)).toBe('12');
        });

        test('formats value with one decimal point when precision is 1', () => {
            expect(NumberUtils.toFixedDecimals(12.66666, 1)).toBe('12.6');
        });
    });

    describe('getRatio()', () => {
        test('returns 1 when modifiers are not specified', () => {
            const mockEvent = {};
            expect(NumberUtils.getRatio(mockEvent)).toBe(1);
        });

        test('returns 0.1 when meta key is pressed', () => {
            const mockEvent = {
                metaKey: true
            };
            expect(NumberUtils.getRatio(mockEvent)).toBe(0.1);
        });

        test('returns 0.1 when ctrl key is pressed', () => {
            const mockEvent = {
                ctrlKey: true
            };
            expect(NumberUtils.getRatio(mockEvent)).toBe(0.1);
        });

        test('returns 10 when shift key is pressed', () => {
            const mockEvent = {
                shiftKey: true
            };
            expect(NumberUtils.getRatio(mockEvent)).toBe(10);
        });
    });

    describe('getMinValue()', () => {
        test('returns min value when it is larger than the default min', () => {
            const min = -1000;
            expect(NumberUtils.getMinValue(min)).toBe(min);
        });

        test('returns the default min when min value is smaller than the default min', () => {
            const min = -10000000000000000000;
            expect(NumberUtils.getMinValue(min)).toBe(Number.MIN_SAFE_INTEGER);
        });

        test('returns the default min when min value is not specified', () => {
            expect(NumberUtils.getMinValue(null)).toBe(Number.MIN_SAFE_INTEGER);
        });
    });

    describe('getMaxValue()', () => {
        test('returns max value when it is smaller than the default max', () => {
            const max = 1000;
            expect(NumberUtils.getMaxValue(max)).toBe(max);
        });

        test('returns the default max when max value is smaller than the default max', () => {
            const max = 10000000000000000000;
            expect(NumberUtils.getMaxValue(max)).toBe(Number.MAX_SAFE_INTEGER);
        });

        test('returns the default max when max value is not specified', () => {
            expect(NumberUtils.getMaxValue(null)).toBe(Number.MAX_SAFE_INTEGER);
        });
    });

    describe('getPrecision()', () => {
        test('returns 0 when value is an integer', () => {
            expect(NumberUtils.getPrecision(13)).toBe(0);
        });

        test('returns 1 when value is a number with one decimal', () => {
            expect(NumberUtils.getPrecision(13.6)).toBe(1);
        });

        test('returns 2 when value is a number with two decimals', () => {
            expect(NumberUtils.getPrecision(13.68)).toBe(2);
        });
    });

    describe('formatNumber()', () => {
        test('returns the original value when it is not a complete number', () => {
            expect(NumberUtils.formatNumber('7.')).toBe('7.');
        });

        test('formats numbers over 4 digits', () => {
            expect(NumberUtils.formatNumber('3243')).toBe('3 243');
        });

        test('formats longer numbers', () => {
            expect(NumberUtils.formatNumber('5644564564')).toBe(
                '5 644 564 564'
            );
        });

        test('formats numbers with decimal part', () => {
            expect(NumberUtils.formatNumber('15000.26')).toBe('15 000.26');
        });

        test('formats negative numbers', () => {
            expect(NumberUtils.formatNumber('-324356.676')).toBe(
                '-324 356.676'
            );
        });
    });

    describe('parseNumber()', () => {
        test('parses not complete numbers', () => {
            expect(NumberUtils.parseNumber('7.')).toBe('7.');
        });

        test('parses numbers over 4 digits', () => {
            expect(NumberUtils.parseNumber('3 243')).toBe('3243');
        });

        test('parses longer numbers', () => {
            expect(NumberUtils.parseNumber('5 644 564 564')).toBe('5644564564');
        });

        test('parses numbers with decimal part', () => {
            expect(NumberUtils.parseNumber('15 000.26')).toBe('15000.26');
        });

        test('parses negative numbers', () => {
            expect(NumberUtils.parseNumber('-324 356.676')).toBe('-324356.676');
        });
    });
});
