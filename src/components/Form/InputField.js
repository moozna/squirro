import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Input.css';
import noop from '../../utils/noop';
import ErrorMessage from './ErrorMessage';
import Input from './Input';

const InputField = ({
    label,
    align,
    error,
    touched,
    inputElementClass,
    multiline,
    ...restProps
}) => {
    const isErrorDisplayed = touched && error;

    const inputClasses = classnames(styles.input, {
        [styles.withLabel]: label
    });

    const inputFieldClasses = classnames(styles.inputField, {
        [styles.multiline]: multiline
    });

    const inputElementClasses = classnames(inputElementClass, {
        [styles[align]]: align,
        [styles.withError]: isErrorDisplayed
    });

    const barClasses = classnames(styles.bar, {
        [styles.leftAnimation]: align === 'left',
        [styles.centerAnimation]: align === 'center'
    });

    return (
        <span className={inputClasses}>
            <label className={inputFieldClasses}>
                <Input
                    inputElementClass={inputElementClasses}
                    multiline={multiline}
                    {...restProps}
                />
                {label ? <span className={styles.label}>{label}</span> : null}
                <div className={barClasses} />
            </label>
            <ErrorMessage touched={touched} error={error} />
        </span>
    );
};

InputField.propTypes = {
    saveRef: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
    type: PropTypes.string,
    label: PropTypes.string,
    touched: PropTypes.bool,
    error: PropTypes.string,
    multiline: PropTypes.bool,
    flexibleWidth: PropTypes.bool,
    minWidth: PropTypes.number,
    align: PropTypes.oneOf(['center', 'left']),
    inputElementClass: PropTypes.string,
    onChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func
};

InputField.defaultProps = {
    saveRef: null,
    value: '',
    name: '',
    type: 'text',
    label: '',
    touched: false,
    error: '',
    multiline: false,
    flexibleWidth: false,
    minWidth: 150,
    align: 'left',
    inputElementClass: '',
    onChange: noop,
    onKeyDown: noop,
    onFocus: noop,
    onBlur: noop
};

export default InputField;
