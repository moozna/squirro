import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Input, { isValuePresent } from './Input';

describe('<Input />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Input />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a flexible input with the default minimum width', () => {
        const wrapper = mount(
            <Input value="Title of the book" flexibleWidth />
        );
        const input = wrapper.find('input');

        expect(input.prop('style').minWidth).toBe('150px');
        expect(wrapper.instance().inputNode.style.width).toBe('16px');
    });

    test('renders a flexible input with the specified minimum width', () => {
        const wrapper = shallow(
            <Input value="Title of the book" flexibleWidth minWidth={100} />
        );
        const input = wrapper.find('input');

        expect(input.prop('style').minWidth).toBe('100px');
    });

    test('renders a flexible input without minimum width', () => {
        const wrapper = shallow(
            <Input value="Title of the book" flexibleWidth minWidth={null} />
        );
        const input = wrapper.find('input');

        expect(input.prop('style').minWidth).toBeUndefined();
    });

    test('updates the size of the flexible input', () => {
        const wrapper = shallow(
            <Input value="Title of the book" flexibleWidth />
        );
        const mockHandleAutoresize = jest.spyOn(
            wrapper.instance(),
            'handleAutoresize'
        );
        const newValue = 'Title of the new book';
        wrapper.setProps({ value: newValue });

        expect(mockHandleAutoresize.mock.calls.length).toBe(1);
    });

    test('renders a not flexible input', () => {
        const wrapper = shallow(
            <Input value="Title of the book" flexibleWidth={false} />
        );
        const input = wrapper.find('input');

        expect(input.prop('style').width).toBe('100%');
    });

    test('renders a multiline input', () => {
        const wrapper = shallow(<Input multiline />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a flexible multiline input', () => {
        const wrapper = mount(
            <Input value="Title of the book" flexibleWidth multiline />
        );

        expect(wrapper.instance().inputNode.style.height).toBe('0px');
    });

    test('renders a filled input when the input field has a value', () => {
        const wrapper = shallow(
            <Input value="Title of the book" label="label" />
        );
        const input = wrapper.find('input');

        expect(input.hasClass('filled')).toBe(true);
    });

    test('has a ref on the input', () => {
        const wrapper = mount(<Input />);

        expect(wrapper.instance().inputNode).toBeDefined();
    });

    test('has a ref on the ghostInput', () => {
        const wrapper = mount(<Input />);

        expect(wrapper.instance().ghost).toBeDefined();
    });

    describe('onChange', () => {
        test('fires the onChange callback', () => {
            const mockEvent = {
                target: {
                    value: 'Title of the book'
                }
            };
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <Input name="title" value="Title" onChange={handleOnChange} />
            );
            wrapper.find('input').simulate('change', mockEvent);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('title');
            expect(handleOnChange.mock.calls[0][1]).toBe('Title of the book');
        });
    });

    describe('onKeyDown', () => {
        test('fires the onKeyDown callback', () => {
            const mockEvent = {};
            const handleOnKeyDown = jest.fn();
            const wrapper = shallow(<Input onKeyDown={handleOnKeyDown} />);

            wrapper.find('input').simulate('keydown', mockEvent);

            expect(handleOnKeyDown.mock.calls.length).toBe(1);
        });

        test('stops keydown event when Enter is pressed', () => {
            const mockEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<Input />);

            wrapper.find('input').simulate('keydown', mockEvent);
            expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('does not stop Enter keydown event when the input is multiline', () => {
            const mockEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<Input multiline />);

            wrapper.find('textarea').simulate('keydown', mockEvent);
            expect(mockEvent.preventDefault.mock.calls.length).toBe(0);
        });

        test('pressing escape calls onChange callback with the saved original value', () => {
            const mockEvent = {
                key: 'Escape'
            };
            const handleOnChange = jest.fn();
            const wrapper = mount(
                <Input name="title" value="Title" onChange={handleOnChange} />
            );

            wrapper.instance().originalValue = 'Title of the book';
            wrapper.find('input').simulate('keydown', mockEvent);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('title');
            expect(handleOnChange.mock.calls[0][1]).toBe('Title of the book');
        });

        test('does not call onChange callback after pressing escape when disableValueReset is true', () => {
            const mockEvent = {
                key: 'Escape'
            };
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <Input
                    name="title"
                    value="Title"
                    onChange={handleOnChange}
                    disableValueReset
                />
            );

            wrapper.find('input').simulate('keydown', mockEvent);

            expect(handleOnChange.mock.calls.length).toBe(0);
        });
    });

    describe('onFocus', () => {
        test('fires the onFocus callback', () => {
            const handleOnFocus = jest.fn();
            const wrapper = shallow(
                <Input name="title" onFocus={handleOnFocus} />
            );

            wrapper.find('input').simulate('focus');

            expect(handleOnFocus.mock.calls.length).toBe(1);
        });

        test('updates the saved original value', () => {
            const wrapper = shallow(<Input name="title" value="Title" />);

            wrapper.find('input').simulate('focus');

            expect(wrapper.instance().originalValue).toBe('Title');
        });

        test('saves the original value as string', () => {
            const wrapper = shallow(<Input name="title" value={5.2} />);

            wrapper.find('input').simulate('focus');

            expect(wrapper.instance().originalValue).toBe('5.2');
        });
    });

    describe('onBlur', () => {
        test('fires the onBlur callback', () => {
            const handleOnBlur = jest.fn();
            const wrapper = shallow(
                <Input name="title" onBlur={handleOnBlur} />
            );

            wrapper.find('input').simulate('blur');

            expect(handleOnBlur.mock.calls.length).toBe(1);
            expect(handleOnBlur.mock.calls[0][0]).toBe('title');
        });
    });

    describe('isValuePresent()', () => {
        test('returns false when value is undefined', () => {
            expect(isValuePresent(undefined)).toBe(false);
        });

        test('returns false when value is null', () => {
            expect(isValuePresent(null)).toBe(false);
        });

        test('returns false when value is empty string', () => {
            expect(isValuePresent('')).toBe(false);
        });

        test('returns false when value is NaN', () => {
            expect(isValuePresent(NaN)).toBe(false);
        });

        test('returns true when value is 0', () => {
            expect(isValuePresent(0)).toBe(true);
        });
    });
});
