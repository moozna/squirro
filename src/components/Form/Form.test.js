import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Form from './Form';

describe('<Form />', () => {
    const MockComponent = () => <div className="testDiv">Test div</div>;
    const Component = Form(MockComponent);

    test('has the expected html structure', () => {
        const wrapper = shallow(<Component />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('InitialValues', () => {
        test('saves the provided initial values', () => {
            const mockInitialValues = {
                title: 'my title',
                asin: 'asin'
            };
            const wrapper = shallow(
                <Component initialValues={mockInitialValues} />
            );

            expect(wrapper.state('values')).toEqual(mockInitialValues);
        });
    });

    describe('onBlur', () => {
        test('saves the visited input field to the state', () => {
            const wrapper = shallow(<Component />);
            wrapper.instance().setTouched('title');

            expect(wrapper.state().touched.title).toBe(true);
        });

        test('works with nested properties', () => {
            const mockPropertyName = 'rating.amazonRating';
            const wrapper = shallow(<Component />);
            wrapper.instance().setTouched('rating.amazonRating');

            expect(wrapper.state().touched[mockPropertyName]).toBe(true);
        });

        test('updates validation after every blur', async () => {
            const mockErrors = { title: 'Error' };
            const mockValidation = jest.fn().mockReturnValue(mockErrors);

            const wrapper = shallow(<Component validate={mockValidation} />);
            await wrapper.instance().setTouched('title');

            expect(mockValidation.mock.calls.length).toBe(1);
            expect(wrapper.state('errors')).toEqual(mockErrors);
        });
    });

    describe('handleChange()', () => {
        test('updates field value on the state', () => {
            const mockInitialValues = {
                title: 'my title',
                asin: 'asin'
            };
            const newValue = 'new title';

            const wrapper = shallow(
                <Component initialValues={mockInitialValues} />
            );
            wrapper.instance().handleChange('title', newValue);

            expect(wrapper.state().values.title).toBe(newValue);
        });

        test('works with nested properties', () => {
            const mockInitialValues = {
                title: 'my title',
                asin: 'asin'
            };
            const mockPropertyName = 'rating.amazonRating';
            const newValue = 2.6;

            const wrapper = shallow(
                <Component initialValues={mockInitialValues} />
            );
            wrapper.instance().handleChange(mockPropertyName, newValue);

            expect(wrapper.state().values[mockPropertyName]).toBe(newValue);
        });

        test('sets field to dirty on the state', () => {
            const mockInitialValues = {
                title: 'my title',
                asin: 'asin'
            };
            const newValue = 'new title';

            const wrapper = shallow(
                <Component initialValues={mockInitialValues} />
            );
            wrapper.instance().handleChange('title', newValue);

            expect(wrapper.state().dirty.title).toBe(true);
        });

        test('setting dirty state works with nested properties', () => {
            const mockInitialValues = {
                title: 'my title',
                asin: 'asin'
            };
            const mockPropertyName = 'rating.amazonRating';
            const newValue = 2.6;

            const wrapper = shallow(
                <Component initialValues={mockInitialValues} />
            );
            wrapper.instance().handleChange(mockPropertyName, newValue);

            expect(wrapper.state().dirty[mockPropertyName]).toBe(true);
        });

        test('updates validation after every change', async () => {
            const mockErrors = { title: 'Error' };
            const mockValidation = jest.fn().mockReturnValue(mockErrors);

            const wrapper = shallow(<Component validate={mockValidation} />);
            await wrapper.instance().handleChange('title', 'new title');

            expect(mockValidation.mock.calls.length).toBe(1);
            expect(wrapper.state('errors')).toEqual(mockErrors);
        });
    });

    describe('saveUploadedFiles()', () => {
        test('updates uploaded images on the state', () => {
            const initialValue = ['file1.jpg'];
            const newValue = ['file2.jpg', 'file3.jpg'];
            const expectedValue = ['file1.jpg', 'file2.jpg', 'file3.jpg'];

            const wrapper = shallow(<Component />);
            wrapper.setState({ uploaded: initialValue });
            wrapper.instance().saveUploadedFiles(newValue);

            expect(wrapper.state('uploaded')).toEqual(expectedValue);
        });
    });

    describe('saveDeletedFiles()', () => {
        test('updates deleted images on the state', () => {
            const initialValue = ['file1.jpg', 'file2.jpg'];
            const newValue = 'file3.jpg';
            const expectedValue = ['file1.jpg', 'file2.jpg', 'file3.jpg'];

            const wrapper = shallow(<Component />);
            wrapper.setState({ deleted: initialValue });
            wrapper.instance().saveDeletedFiles(newValue);

            expect(wrapper.state('deleted')).toEqual(expectedValue);
        });
    });

    describe('handleSubmit()', () => {
        test('updates validation', async () => {
            const mockErrors = { title: 'Error' };
            const mockValidation = jest.fn().mockReturnValue(mockErrors);

            const wrapper = shallow(<Component validate={mockValidation} />);
            await wrapper.instance().handleSubmit();

            expect(mockValidation.mock.calls.length).toBe(1);
            expect(wrapper.state('errors')).toEqual(mockErrors);
            expect(wrapper.state('isSubmitting')).toEqual(false);
        });

        test('fires the onSubmit callback when form is valid', async () => {
            const mockErrors = {};
            const handleOnSubmit = jest.fn();
            const mockValidation = jest.fn().mockReturnValue(mockErrors);

            const wrapper = shallow(
                <Component
                    validate={mockValidation}
                    onSubmit={handleOnSubmit}
                />
            );
            await wrapper.instance().handleSubmit();

            expect(wrapper.state('isSubmitting')).toEqual(true);
            expect(handleOnSubmit.mock.calls.length).toBe(1);
        });

        test('does not fire the onSubmit callback when form is inValid', async () => {
            const mockErrors = { title: 'Error' };
            const handleOnSubmit = jest.fn();
            const mockValidation = jest.fn().mockReturnValue(mockErrors);

            const wrapper = shallow(
                <Component
                    validate={mockValidation}
                    onSubmit={handleOnSubmit}
                />
            );
            await wrapper.instance().handleSubmit();

            expect(wrapper.state('isSubmitting')).toEqual(false);
            expect(handleOnSubmit.mock.calls.length).toBe(0);
        });

        test('fires the deleteImages callback', async () => {
            const mockErrors = {};
            const mockValidation = jest.fn().mockReturnValue(mockErrors);
            const mockDeleteImages = jest.fn();
            const files = ['file1.jpg', 'file2.jpg', 'file3.jpg'];

            const wrapper = shallow(
                <Component
                    validate={mockValidation}
                    deleteImages={mockDeleteImages}
                />
            );
            wrapper.setState({ deleted: files });
            await wrapper.instance().handleSubmit();

            expect(mockDeleteImages.mock.calls.length).toBe(1);
            expect(mockDeleteImages.mock.calls[0][0]).toBe(files);
        });

        test('does not fire the deleteImages callback when deleted files is empty', async () => {
            const mockErrors = {};
            const mockValidation = jest.fn().mockReturnValue(mockErrors);
            const mockDeleteImages = jest.fn();

            const wrapper = shallow(
                <Component
                    validate={mockValidation}
                    deleteImages={mockDeleteImages}
                />
            );
            wrapper.setState({ deleted: [] });
            await wrapper.instance().handleSubmit();

            expect(mockDeleteImages.mock.calls.length).toBe(0);
        });

        test('fires the onSubmit callback when validation is not provided', () => {
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(<Component onSubmit={handleOnSubmit} />);
            wrapper.instance().handleSubmit();

            expect(wrapper.state('isSubmitting')).toEqual(true);
            expect(handleOnSubmit.mock.calls.length).toBe(1);
        });
    });

    describe('handleCancel()', () => {
        test('fires the deleteImages callback', () => {
            const mockDeleteImages = jest.fn();
            const files = ['file1.jpg', 'file2.jpg', 'file3.jpg'];

            const wrapper = shallow(
                <Component deleteImages={mockDeleteImages} />
            );
            wrapper.setState({ uploaded: files });
            wrapper.instance().handleCancel();

            expect(mockDeleteImages.mock.calls.length).toBe(1);
            expect(mockDeleteImages.mock.calls[0][0]).toBe(files);
        });

        test('does not fire the deleteImages callback when uploaded files is empty', () => {
            const mockDeleteImages = jest.fn();

            const wrapper = shallow(
                <Component deleteImages={mockDeleteImages} />
            );
            wrapper.setState({ uploaded: [] });
            wrapper.instance().handleCancel();

            expect(mockDeleteImages.mock.calls.length).toBe(0);
        });

        test('fires the onCancel callback', () => {
            const mockOnCancel = jest.fn();

            const wrapper = shallow(<Component onCancel={mockOnCancel} />);
            wrapper.instance().handleCancel();

            expect(mockOnCancel.mock.calls.length).toBe(1);
        });
    });

    describe('handleKeyDown()', () => {
        test('fires handleSubmit when Ctrl + Enter is pressed', () => {
            const mockKeydownEvent = {
                ctrlKey: true,
                key: 'Enter'
            };

            const wrapper = shallow(<Component />);
            const mockHandleSubmit = jest.spyOn(
                wrapper.instance(),
                'handleSubmit'
            );

            wrapper.find('form').simulate('keydown', mockKeydownEvent);

            expect(mockHandleSubmit.mock.calls.length).toBe(1);
        });

        test('does not fire handleSubmit when not Ctrl + Enter is pressed', () => {
            const mockKeydownEvent = {
                ctrlKey: false,
                key: 'Enter'
            };

            const wrapper = shallow(<Component />);
            const mockHandleSubmit = jest.spyOn(
                wrapper.instance(),
                'handleSubmit'
            );

            wrapper.find('form').simulate('keydown', mockKeydownEvent);

            expect(mockHandleSubmit.mock.calls.length).toBe(0);
        });

        test('fires handleCancel when Alt + c is pressed', () => {
            const mockKeydownEvent = {
                altKey: true,
                key: 'c'
            };

            const wrapper = shallow(<Component />);
            const mockHandleCancel = jest.spyOn(
                wrapper.instance(),
                'handleCancel'
            );

            wrapper.find('form').simulate('keydown', mockKeydownEvent);

            expect(mockHandleCancel.mock.calls.length).toBe(1);
        });

        test('does not fire handleCancel when not Alt + c is pressed', () => {
            const mockKeydownEvent = {
                altKey: false,
                key: 'c'
            };

            const wrapper = shallow(<Component />);
            const mockHandleCancel = jest.spyOn(
                wrapper.instance(),
                'handleCancel'
            );

            wrapper.find('form').simulate('keydown', mockKeydownEvent);

            expect(mockHandleCancel.mock.calls.length).toBe(0);
        });
    });
});
