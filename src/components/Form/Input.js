import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Input.css';
import noop from '../../utils/noop';

export const isValuePresent = value =>
    value !== null &&
    value !== undefined &&
    value !== '' &&
    !(typeof value === 'number' && Number.isNaN(value));

class Input extends Component {
    componentDidMount() {
        this.handleAutoresize();
    }

    componentDidUpdate() {
        this.handleAutoresize();
    }

    setTextareaHeight() {
        const element = this.inputNode;
        const ghostElement = this.ghost;

        if (element) {
            element.style.height = `${ghostElement.clientHeight +
                ghostElement.offsetTop}px`;
        }
    }

    setInputWidth() {
        const { defaultPadding } = this.props;
        const element = this.inputNode;
        const ghostElement = this.ghost;

        if (element) {
            element.style.width = `${ghostElement.clientWidth +
                defaultPadding}px`;
        }
    }

    getWidthStyle() {
        const { flexibleWidth, minWidth } = this.props;
        const styles = {};

        if (flexibleWidth) {
            if (minWidth) {
                styles.minWidth = `${minWidth}px`;
            }
        } else {
            styles.width = '100%';
        }

        return styles;
    }

    handleChange = event => {
        const { onChange, name } = this.props;
        const { target } = event;
        onChange(name, target.value);
    };

    handleKeyDown = event => {
        const {
            name,
            multiline,
            disableValueReset,
            onChange,
            onKeyDown
        } = this.props;

        if (!multiline && event.key === 'Enter') {
            event.preventDefault();
        }

        if (event.key === 'Escape' && !disableValueReset) {
            onChange(name, this.originalValue);
            this.inputNode.blur();
        }

        onKeyDown(event);
    };

    handleFocus = event => {
        const { value, onFocus } = this.props;
        this.originalValue = String(value);
        onFocus(event);
    };

    handleBlur = () => {
        const { onBlur, name } = this.props;
        onBlur(name);
    };

    saveRef = node => {
        const { saveRef } = this.props;
        if (saveRef) {
            saveRef(node);
        }

        this.inputNode = node;
    };

    saveGhostRef = node => {
        this.ghost = node;
    };

    handleAutoresize() {
        const { multiline, flexibleWidth } = this.props;

        if (multiline) {
            this.setTextareaHeight();
        } else if (flexibleWidth) {
            this.setInputWidth();
        }
    }

    renderGhostField() {
        const { value, multiline } = this.props;

        const ghostClasses = classnames(styles.ghostInputElement, {
            [styles.multiline]: multiline
        });

        return (
            <div
                className={ghostClasses}
                ref={this.saveGhostRef}
                aria-hidden="true"
            >
                {value}
            </div>
        );
    }

    render() {
        const { value, name, type, multiline, inputElementClass } = this.props;

        const valuePresent = isValuePresent(value);
        const inputElementClasses = classnames(
            styles.inputElement,
            {
                [styles.filled]: valuePresent
            },
            inputElementClass
        );

        const InputElementType = multiline ? 'textarea' : 'input';

        return (
            <Fragment>
                <InputElementType
                    ref={this.saveRef}
                    className={inputElementClasses}
                    style={this.getWidthStyle()}
                    value={value}
                    name={name}
                    type={type}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    data-lpignore
                    autoComplete="off"
                    spellCheck="false"
                />
                {this.renderGhostField()}
            </Fragment>
        );
    }
}

Input.propTypes = {
    saveRef: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    name: PropTypes.string,
    type: PropTypes.string,
    multiline: PropTypes.bool,
    flexibleWidth: PropTypes.bool,
    minWidth: PropTypes.number,
    inputElementClass: PropTypes.string,
    defaultPadding: PropTypes.number,
    disableValueReset: PropTypes.bool,
    onChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func
};

Input.defaultProps = {
    saveRef: null,
    value: '',
    name: '',
    type: 'text',
    multiline: false,
    flexibleWidth: false,
    minWidth: 150,
    inputElementClass: '',
    defaultPadding: 16,
    disableValueReset: false,
    onChange: noop,
    onKeyDown: noop,
    onFocus: noop,
    onBlur: noop
};

export default Input;
