import React from 'react';
import PropTypes from 'prop-types';
import styles from './Input.css';

const ErrorMessage = ({ touched, error }) => {
    if (touched && error) {
        return (
            <div className={styles.errorMessage}>
                <span className={styles.errorText}>{error}</span>
            </div>
        );
    }

    return null;
};

ErrorMessage.propTypes = {
    touched: PropTypes.bool,
    error: PropTypes.string
};

ErrorMessage.defaultProps = {
    touched: false,
    error: ''
};

export default ErrorMessage;
