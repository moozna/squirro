export function isNumeric(n) {
    return !Number.isNaN(parseFloat(n)) && Number.isFinite(n);
}

// '1.' '1x' 'xx' '' => are not complete numbers
export function isNotCompleteNumber(num) {
    return (
        Number.isNaN(num) ||
        num === '' ||
        num === '-' ||
        num.toString().indexOf('.') === num.toString().length - 1
    );
}

export function toFixedDecimals(num, digits) {
    const re = new RegExp(`^-?\\d+(?:(.|,)\\d{0,${digits || -1}})?`);
    return num.toString().match(re)[0];
}

export function getRatio(event) {
    let ratio = 1;
    if (event.metaKey || event.ctrlKey) {
        ratio = 0.1;
    } else if (event.shiftKey) {
        ratio = 10;
    }
    return ratio;
}

export function getMinValue(min) {
    const defaultMin = Number.MIN_SAFE_INTEGER;

    if (!isNumeric(min)) {
        return defaultMin;
    }

    return Math.max(min, defaultMin);
}

export function getMaxValue(max) {
    const defaultMax = Number.MAX_SAFE_INTEGER;

    if (!isNumeric(max)) {
        return defaultMax;
    }

    return Math.min(max, defaultMax);
}

export function getPrecision(value) {
    const valueString = value.toString();
    let precision = 0;

    if (valueString.indexOf('.') >= 0) {
        precision = valueString.length - valueString.indexOf('.') - 1;
    }

    return precision;
}

export function formatNumber(num, thousandSeparator = ' ') {
    if (isNotCompleteNumber(num)) {
        return num;
    }
    let isNegative = false;
    let number = num;

    if (num < 0) {
        number = Math.abs(num);
        isNegative = true;
    }

    const numberString = number.toString();
    const [integers, fractional] = numberString.split('.');
    const rest = integers.length % 3;
    let result = integers.substr(0, rest);
    const thousands = integers.substr(rest).match(/\d{3}/gi);

    if (thousands) {
        const first = rest ? thousandSeparator : '';
        result += first + thousands.join(thousandSeparator);
    }

    if (fractional) {
        result += `.${fractional}`;
    }

    if (isNegative) {
        result = `-${result}`;
    }

    return result;
}

export function parseNumber(value, thousandSeparator = ' ') {
    const re = new RegExp(`${thousandSeparator}`, 'g');
    return value.replace(re, '');
}
