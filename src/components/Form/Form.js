import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bookType } from '../../types/types';
import noop from '../../utils/noop';
import flatten from '../../utils/flatten';
import unflatten from '../../utils/unflatten';

function withForm(WrappedComponent) {
    return class Form extends Component {
        static propTypes = {
            initialValues: bookType,
            validate: PropTypes.func,
            onSubmit: PropTypes.func,
            onCancel: PropTypes.func,
            deleteImages: PropTypes.func
        };

        static defaultProps = {
            initialValues: {},
            validate: null,
            onSubmit: noop,
            onCancel: noop,
            deleteImages: noop
        };

        state = {
            values: flatten(this.props.initialValues),
            uploaded: [],
            deleted: [],
            touched: {},
            dirty: {},
            errors: {},
            isSubmitting: false
        };

        setTouched = name => {
            this.setState(
                prevState => ({
                    touched: {
                        ...prevState.touched,
                        [name]: true
                    }
                }),
                this.runValidations
            );
        };

        handleChange = (name, value) => {
            this.setState(
                prevState => ({
                    values: {
                        ...prevState.values,
                        [name]: value
                    },
                    dirty: {
                        ...prevState.dirty,
                        [name]: true
                    }
                }),
                this.runValidations
            );
        };

        saveUploadedFiles = files => {
            this.setState(prevState => ({
                uploaded: [...prevState.uploaded, ...files]
            }));
        };

        saveDeletedFiles = file => {
            this.setState(prevState => ({
                deleted: prevState.deleted.concat(file)
            }));
        };

        handleKeyDown = event => {
            if (event.ctrlKey && event.key === 'Enter') {
                this.handleSubmit();
            }

            if (event.altKey && event.key === 'c') {
                this.handleCancel();
            }
        };

        handleSubmit = async () => {
            const { validate } = this.props;
            const { values } = this.state;

            if (validate) {
                const errors = await validate(values);
                const isValid = Object.keys(errors).length === 0;
                this.setState({
                    errors,
                    isSubmitting: isValid
                });

                if (isValid) {
                    this.submitForm();
                }
            } else {
                this.setState({ isSubmitting: true });
                this.submitForm();
            }
        };

        handleCancel = () => {
            const { onCancel } = this.props;
            const { uploaded } = this.state;
            this.deleteImagesIfNeeded(uploaded);
            onCancel();
        };

        runValidations = async () => {
            const { validate } = this.props;
            const { values } = this.state;

            if (validate) {
                const errors = await validate(values);
                this.setState({ errors });
            }
        };

        submitForm() {
            const { onSubmit } = this.props;
            const { values, deleted } = this.state;
            onSubmit(unflatten(values));
            this.deleteImagesIfNeeded(deleted);
        }

        deleteImagesIfNeeded(images) {
            const { deleteImages } = this.props;

            if (images.length) {
                deleteImages(images);
            }
        }

        render() {
            const { dirty, errors } = this.state;

            const isFormDirty = Object.values(dirty).filter(Boolean).length > 0;
            const isFormValid = Object.keys(errors).length === 0;

            const newProps = {
                ...this.state,
                onChange: this.handleChange,
                onBlur: this.setTouched,
                onSubmit: this.handleSubmit,
                onCancel: this.handleCancel,
                saveUploadedFiles: this.saveUploadedFiles,
                saveDeletedFiles: this.saveDeletedFiles,
                isFormValid,
                isFormDirty
            };

            return (
                <form
                    onSubmit={this.handleSubmit}
                    onKeyDown={this.handleKeyDown}
                >
                    <WrappedComponent {...this.props} {...newProps} />
                </form>
            );
        }
    };
}

export default withForm;
