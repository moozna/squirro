import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import styles from './Carousel.css';
import SLIDE_ANIMATION_CLASSES from './animation';

const Slides = ({ images, selectedIndex, direction, timeout }) => {
    const imageClasses = SLIDE_ANIMATION_CLASSES[direction];
    return (
        <ul className={styles.slides}>
            {React.Children.map(images, (child, index) => (
                <CSSTransition
                    key={index}
                    in={index === selectedIndex}
                    classNames={imageClasses}
                    timeout={timeout}
                    unmountOnExit
                >
                    <div className={styles.image}>{child}</div>
                </CSSTransition>
            ))}
        </ul>
    );
};

Slides.propTypes = {
    images: PropTypes.node,
    selectedIndex: PropTypes.number,
    direction: PropTypes.string,
    timeout: PropTypes.number
};

Slides.defaultProps = {
    images: null,
    selectedIndex: 0,
    direction: 'next',
    timeout: 350
};

export default Slides;
