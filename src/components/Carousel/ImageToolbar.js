import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Carousel.css';
import noop from '../../utils/noop';
import HeartFull from '../svg/HeartFull';
import HeartEmpty from '../svg/HeartEmpty';
import Trash from '../svg/Trash';
import Button from '../Button/Button';

class ImageToolbar extends Component {
    handleSetToDefault = event => {
        const { selectedIndex, setToDefault } = this.props;
        event.stopPropagation();
        setToDefault(selectedIndex);
    };

    handleDelete = event => {
        const { selectedIndex, deleteImage } = this.props;
        event.stopPropagation();
        deleteImage(selectedIndex);
    };

    renderHeart() {
        const { isDefault } = this.props;

        if (isDefault) {
            return (
                <div className={styles.heartIcon}>
                    <HeartFull />
                </div>
            );
        }

        return (
            <Button
                color="white"
                shape="square"
                shadow="outer"
                onClick={this.handleSetToDefault}
            >
                <HeartEmpty />
            </Button>
        );
    }

    render() {
        return (
            <div className={styles.imageToolbar}>
                {this.renderHeart()}
                <Button
                    color="white"
                    shape="square"
                    shadow="outer"
                    onClick={this.handleDelete}
                >
                    <Trash />
                </Button>
            </div>
        );
    }
}

ImageToolbar.propTypes = {
    selectedIndex: PropTypes.number,
    isDefault: PropTypes.bool,
    setToDefault: PropTypes.func,
    deleteImage: PropTypes.func
};

ImageToolbar.defaultProps = {
    selectedIndex: 0,
    isDefault: false,
    setToDefault: noop,
    deleteImage: noop
};

export default ImageToolbar;
