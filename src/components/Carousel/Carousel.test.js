import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Carousel from './Carousel';

describe('<Carousel />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Carousel />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Carousel>{testChildren}</Carousel>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a toolbar when readOnly is false and there is at least one children', () => {
        const testChildren = <div />;
        const wrapper = shallow(
            <Carousel readOnly={false}>{testChildren}</Carousel>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders controls when there are two or more children', () => {
        const wrapper = shallow(
            <Carousel>
                <div />
                <div />
                <div />
            </Carousel>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('updating children', () => {
        test('updates selectedIndex when it is invalid with the current children', () => {
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );
            wrapper.setState({ selectedIndex: 2 });
            wrapper.setProps({ children: [<div />, <div />] });

            expect(wrapper.state('selectedIndex')).toBe(1);
        });

        test('does not update selectedIndex when it is not invalid with the current children', () => {
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );
            wrapper.setState({ selectedIndex: 2 });
            wrapper.setProps({
                children: [<div />, <div />, <div />, <div />]
            });

            expect(wrapper.state('selectedIndex')).toBe(2);
        });
    });

    describe('prevSlide()', () => {
        describe('switches to the previous slide', () => {
            test('subtracts one from the selected index', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1 });
                wrapper.instance().prevSlide();

                expect(wrapper.state('selectedIndex')).toBe(0);
            });

            test('updates the animation direction on the state', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1 });
                wrapper.instance().prevSlide();

                expect(wrapper.state('direction')).toBe('prev');
            });

            test('does not update state when animating is already in progress', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1, animating: true });
                wrapper.instance().prevSlide();

                expect(wrapper.state('selectedIndex')).toBe(1);
            });
        });

        describe('switches to the last slide', () => {
            test('sets selected index to the last one', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 0 });
                wrapper.instance().prevSlide();

                expect(wrapper.state('selectedIndex')).toBe(2);
            });

            test('updates the animation direction on the state', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 0 });
                wrapper.instance().prevSlide();

                expect(wrapper.state('direction')).toBe('prevTolast');
            });
        });
    });

    describe('nextSlide()', () => {
        describe('switches to the next slide', () => {
            test('adds one to the selected index', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1 });
                wrapper.instance().nextSlide();

                expect(wrapper.state('selectedIndex')).toBe(2);
            });

            test('updates the animation direction on the state', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1 });
                wrapper.instance().nextSlide();

                expect(wrapper.state('direction')).toBe('next');
            });

            test('does not update state when animating is already in progress', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 1, animating: true });
                wrapper.instance().nextSlide();

                expect(wrapper.state('selectedIndex')).toBe(1);
            });
        });

        describe('switches to the first slide', () => {
            test('sets selected index to the first one', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 2 });
                wrapper.instance().nextSlide();

                expect(wrapper.state('selectedIndex')).toBe(0);
            });

            test('updates the animation direction on the state', () => {
                const wrapper = shallow(
                    <Carousel>
                        <div />
                        <div />
                        <div />
                    </Carousel>
                );

                wrapper.setState({ selectedIndex: 2 });
                wrapper.instance().nextSlide();

                expect(wrapper.state('direction')).toBe('nextToFirst');
            });
        });
    });

    describe('selectSlide()', () => {
        test('updates selected index', () => {
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );

            wrapper.setState({ selectedIndex: 1 });
            wrapper.instance().selectSlide(2);

            expect(wrapper.state('selectedIndex')).toBe(2);
        });

        test('updates the animation direction to prev when new index is smaller', () => {
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );

            wrapper.setState({ selectedIndex: 1 });
            wrapper.instance().selectSlide(0);

            expect(wrapper.state('direction')).toBe('prev');
        });

        test('updates the animation direction to next when new index is larger', () => {
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );

            wrapper.setState({ selectedIndex: 1 });
            wrapper.instance().selectSlide(2);

            expect(wrapper.state('direction')).toBe('next');
        });

        test('does not update state when the same slide is selected', () => {
            jest.useFakeTimers();
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );

            const mockDoSliding = jest.spyOn(wrapper.instance(), 'doSliding');

            wrapper.setState({ selectedIndex: 1 });
            wrapper.instance().selectSlide(1);
            jest.runAllTimers();

            expect(mockDoSliding.mock.calls.length).toBe(0);
            mockDoSliding.mockRestore();
        });
    });

    describe('doSliding()', () => {
        test('manages animating after every update', () => {
            jest.useFakeTimers();
            const wrapper = shallow(
                <Carousel>
                    <div />
                    <div />
                    <div />
                </Carousel>
            );

            wrapper.instance().doSliding(1);
            expect(wrapper.state('animating')).toBe(true);

            jest.runAllTimers();
            expect(wrapper.state('animating')).toBe(false);
        });
    });

    describe('handleKeyDown()', () => {
        test('calls prevSlide when left arrow is pressed', () => {
            const mockKeydownEvent = {
                key: 'ArrowLeft'
            };
            const wrapper = shallow(<Carousel />);
            const mockPrevSlide = jest.spyOn(wrapper.instance(), 'prevSlide');

            wrapper.find('.carousel').simulate('keydown', mockKeydownEvent);

            expect(mockPrevSlide.mock.calls.length).toBe(1);
        });

        test('calls prevSlide when left arrow is pressed in IE/Edge', () => {
            const mockKeydownEvent = {
                key: 'Left'
            };
            const wrapper = shallow(<Carousel />);
            const mockPrevSlide = jest.spyOn(wrapper.instance(), 'prevSlide');

            wrapper.find('.carousel').simulate('keydown', mockKeydownEvent);

            expect(mockPrevSlide.mock.calls.length).toBe(1);
        });

        test('calls nextSlide when right arrow is pressed', () => {
            const mockKeydownEvent = {
                key: 'ArrowRight'
            };
            const wrapper = shallow(<Carousel />);
            const mockNextSlide = jest.spyOn(wrapper.instance(), 'nextSlide');

            wrapper.find('.carousel').simulate('keydown', mockKeydownEvent);

            expect(mockNextSlide.mock.calls.length).toBe(1);
        });

        test('calls nextSlide when right arrow is pressed in IE/Edge', () => {
            const mockKeydownEvent = {
                key: 'Right'
            };
            const wrapper = shallow(<Carousel />);
            const mockNextSlide = jest.spyOn(wrapper.instance(), 'nextSlide');

            wrapper.find('.carousel').simulate('keydown', mockKeydownEvent);

            expect(mockNextSlide.mock.calls.length).toBe(1);
        });

        test('does not switch slides when not one of the arrows is pressed', () => {
            const mockKeydownEvent = {
                key: 'Escape'
            };
            const wrapper = shallow(<Carousel />);
            const mockPrevSlide = jest.spyOn(wrapper.instance(), 'prevSlide');
            const mockNextSlide = jest.spyOn(wrapper.instance(), 'nextSlide');

            wrapper.find('.carousel').simulate('keydown', mockKeydownEvent);

            expect(mockPrevSlide.mock.calls.length).toBe(0);
            expect(mockNextSlide.mock.calls.length).toBe(0);
        });
    });
});
