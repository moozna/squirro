import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ControlButton from './ControlButton';

describe('<ControlButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ControlButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a prev button', () => {
        const wrapper = shallow(<ControlButton direction="prev" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a next button', () => {
        const wrapper = shallow(<ControlButton direction="next" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleClick()', () => {
        test('stops click event', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };

            const wrapper = shallow(<ControlButton />);
            wrapper.find('.controlButton').simulate('click', mockClickEvent);

            expect(mockClickEvent.stopPropagation.mock.calls.length).toBe(1);
        });

        test('fires the onButtonClick callback', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };
            const handleOnClick = jest.fn();

            const wrapper = shallow(
                <ControlButton onButtonClick={handleOnClick} />
            );
            wrapper.find('.controlButton').simulate('click', mockClickEvent);

            expect(handleOnClick.mock.calls.length).toBe(1);
        });
    });
});
