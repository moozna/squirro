import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import styles from './Carousel.css';
import noop from '../../utils/noop';
import Slides from './Slides';
import ImageToolbar from './ImageToolbar';
import ControlButton from './ControlButton';
import Indicator from './Indicator';

const TIMEOUT = 350;

class Carousel extends Component {
    state = {
        selectedIndex: 0,
        animating: false
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (
            nextProps.children &&
            nextProps.children.length <= prevState.selectedIndex
        ) {
            return {
                selectedIndex: nextProps.children.length - 1,
                direction: 'prev'
            };
        }

        return null;
    }

    get length() {
        const { children } = this.props;
        return React.Children.count(children);
    }

    prevSlide = () => {
        const { selectedIndex, animating } = this.state;

        if (animating) {
            return;
        }

        const numberOfSlides = this.length;
        let newIndex;
        let direction;

        if (selectedIndex - 1 >= 0) {
            newIndex = (selectedIndex - 1) % numberOfSlides;
            direction = 'prev';
        } else {
            newIndex = numberOfSlides - 1;
            direction = 'prevTolast';
        }

        this.doSliding(newIndex, direction);
    };

    nextSlide = () => {
        const { selectedIndex, animating } = this.state;

        if (animating) {
            return;
        }

        const numberOfSlides = this.length;
        const newIndex = (selectedIndex + 1) % numberOfSlides;
        const direction = selectedIndex < newIndex ? 'next' : 'nextToFirst';

        this.doSliding(newIndex, direction);
    };

    selectSlide = newIndex => {
        const { selectedIndex, animating } = this.state;

        if (animating || newIndex === selectedIndex) {
            return;
        }

        const direction = newIndex < selectedIndex ? 'prev' : 'next';

        this.doSliding(newIndex, direction);
    };

    doSliding = (index, direction) => {
        this.setState({
            selectedIndex: index,
            direction,
            animating: true
        });

        setTimeout(() => {
            this.setState({
                animating: false
            });
        }, TIMEOUT);
    };

    handleKeyDown = event => {
        switch (event.key) {
            case 'Left':
            case 'ArrowLeft':
                this.prevSlide();
                break;
            case 'Right':
            case 'ArrowRight':
                this.nextSlide();
                break;
            default:
                break;
        }
    };

    renderToolbar() {
        const { readOnly, setToDefault, deleteImage } = this.props;
        const { selectedIndex } = this.state;

        if (readOnly || this.length < 1) {
            return null;
        }

        return (
            <ImageToolbar
                isDefault={selectedIndex === 0}
                selectedIndex={selectedIndex}
                setToDefault={setToDefault}
                deleteImage={deleteImage}
            />
        );
    }

    renderControls() {
        const { selectedIndex } = this.state;

        if (this.length <= 1) {
            return null;
        }

        return (
            <Fragment>
                <ControlButton
                    direction="prev"
                    onButtonClick={this.prevSlide}
                />
                <ControlButton
                    direction="next"
                    onButtonClick={this.nextSlide}
                />
                <Indicator
                    length={this.length}
                    selectedIndex={selectedIndex}
                    onClick={this.selectSlide}
                />
            </Fragment>
        );
    }

    render() {
        const { children } = this.props;
        const { selectedIndex, direction } = this.state;

        return (
            <div
                tabIndex="0"
                className={styles.carousel}
                onKeyDown={this.handleKeyDown}
            >
                <Slides
                    images={children}
                    selectedIndex={selectedIndex}
                    direction={direction}
                    timeout={TIMEOUT}
                />
                {this.renderToolbar()}
                {this.renderControls()}
            </div>
        );
    }
}

Carousel.propTypes = {
    children: PropTypes.node,
    readOnly: PropTypes.bool,
    setToDefault: PropTypes.func,
    deleteImage: PropTypes.func
};

Carousel.defaultProps = {
    children: null,
    readOnly: true,
    setToDefault: noop,
    deleteImage: noop
};

export default Carousel;
