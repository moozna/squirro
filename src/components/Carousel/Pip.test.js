import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Pip from './Pip';

describe('<Pip />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Pip />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a selected pip', () => {
        const wrapper = shallow(<Pip selected />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleClick()', () => {
        test('stops click event', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };

            const wrapper = shallow(<Pip />);
            wrapper.find('li').simulate('click', mockClickEvent);

            expect(mockClickEvent.stopPropagation.mock.calls.length).toBe(1);
        });

        test('fires the onClick callback', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };
            const handleOnClick = jest.fn();

            const wrapper = shallow(<Pip index={3} onClick={handleOnClick} />);
            wrapper.find('li').simulate('click', mockClickEvent);

            expect(handleOnClick.mock.calls.length).toBe(1);
            expect(handleOnClick.mock.calls[0][0]).toBe(3);
        });
    });
});
