import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Carousel.css';
import noop from '../../utils/noop';

class Pip extends Component {
    handleClick = event => {
        const { index, onClick } = this.props;
        event.stopPropagation();
        onClick(index);
    };

    render() {
        const { selected } = this.props;

        const classes = classnames(styles.pip, {
            [styles.selected]: selected
        });

        return (
            <li className={styles.navigationItem} onClick={this.handleClick}>
                <span className={classes} />
            </li>
        );
    }
}

Pip.propTypes = {
    index: PropTypes.number,
    selected: PropTypes.bool,
    onClick: PropTypes.func
};

Pip.defaultProps = {
    index: null,
    selected: false,
    onClick: noop
};

export default Pip;
