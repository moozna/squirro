import styles from './Carousel.css';

const SLIDE_ANIMATION_CLASSES = {
    prev: {
        enter: styles.prevEnter,
        enterActive: styles.prevEnterActive,
        exit: styles.prevLeave,
        exitActive: styles.prevLeaveActive
    },
    next: {
        enter: styles.nextEnter,
        enterActive: styles.nextEnterActive,
        exit: styles.nextLeave,
        exitActive: styles.nextLeaveActive
    },
    prevTolast: {
        enter: styles.lastEnter,
        enterActive: styles.lastEnterActive,
        exit: styles.lastLeave,
        exitActive: styles.lastLeaveActive
    },
    nextToFirst: {
        enter: styles.firstEnter,
        enterActive: styles.firstEnterActive,
        exit: styles.firstLeave,
        exitActive: styles.firstLeaveActive
    }
};

export default SLIDE_ANIMATION_CLASSES;
