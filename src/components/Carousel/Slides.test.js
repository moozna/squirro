import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Slides from './Slides';

describe('<Slides />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Slides />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when images are specified', () => {
        const images = [<div />, <div />, <div />];
        const wrapper = shallow(<Slides images={images} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('Animation', () => {
        const images = [<div />, <div />, <div />];

        test('renders the correct classes when direction is prev', () => {
            const wrapper = shallow(
                <Slides images={images} direction="prev" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders the correct classes when direction is next', () => {
            const wrapper = shallow(
                <Slides images={images} direction="next" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders the correct classes when direction is prevTolast', () => {
            const wrapper = shallow(
                <Slides images={images} direction="prevTolast" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders the correct classes when direction is nextToFirst', () => {
            const wrapper = shallow(
                <Slides images={images} direction="nextToFirst" />
            );

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });
});
