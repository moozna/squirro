import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Carousel.css';
import noop from '../../utils/noop';
import ArrowLeft from '../svg/ArrowLeft';
import ArrowRight from '../svg/ArrowRight';

class ControlButton extends Component {
    handleClick = event => {
        const { onButtonClick } = this.props;
        event.stopPropagation();
        onButtonClick();
    };

    renderIcon() {
        const { direction } = this.props;
        const iconProps = {
            className: styles.icon,
            iconSize: 30
        };

        switch (direction) {
            case 'prev':
                return <ArrowLeft {...iconProps} />;
            case 'next':
                return <ArrowRight {...iconProps} />;
            default:
                return null;
        }
    }

    render() {
        const { direction } = this.props;

        const buttonClasses = classnames(
            styles.controlButton,
            styles[direction]
        );

        return (
            <button
                type="button"
                className={buttonClasses}
                onClick={this.handleClick}
            >
                {this.renderIcon()}
            </button>
        );
    }
}

ControlButton.propTypes = {
    onButtonClick: PropTypes.func,
    direction: PropTypes.string
};

ControlButton.defaultProps = {
    onButtonClick: noop,
    direction: ''
};

export default ControlButton;
