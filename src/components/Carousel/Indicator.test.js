import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Indicator from './Indicator';

describe('<Indicator />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Indicator />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<Indicator length={3} selectedIndex={1} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
