import React from 'react';
import PropTypes from 'prop-types';
import styles from './Carousel.css';
import noop from '../../utils/noop';
import Pip from './Pip';

const getIndexes = number => [...Array(number).keys()];

const Indicator = ({ length, selectedIndex, onClick }) => {
    const indexes = getIndexes(length);

    return (
        <ol className={styles.indicator}>
            {indexes.map(index => (
                <Pip
                    key={index}
                    index={index}
                    selected={index === selectedIndex}
                    onClick={onClick}
                />
            ))}
        </ol>
    );
};

Indicator.propTypes = {
    length: PropTypes.number,
    selectedIndex: PropTypes.number,
    onClick: PropTypes.func
};

Indicator.defaultProps = {
    length: 0,
    selectedIndex: 0,
    onClick: noop
};

export default Indicator;
