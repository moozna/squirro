import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImageToolbar from './ImageToolbar';

describe('<ImageToolbar />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ImageToolbar />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a toolbar for a default image', () => {
        const wrapper = shallow(<ImageToolbar isDefault />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleSetToDefault()', () => {
        test('stops click event', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };

            const wrapper = shallow(<ImageToolbar />);
            wrapper.instance().handleSetToDefault(mockClickEvent);

            expect(mockClickEvent.stopPropagation.mock.calls.length).toBe(1);
        });

        test('fires the setToDefault callback', () => {
            const selectedIndex = 1;
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };
            const handleSetToDefault = jest.fn();

            const wrapper = shallow(
                <ImageToolbar
                    selectedIndex={selectedIndex}
                    setToDefault={handleSetToDefault}
                />
            );
            wrapper.instance().handleSetToDefault(mockClickEvent);

            expect(handleSetToDefault.mock.calls.length).toBe(1);
            expect(handleSetToDefault.mock.calls[0][0]).toEqual(selectedIndex);
        });
    });

    describe('handleDelete()', () => {
        test('stops click event', () => {
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };

            const wrapper = shallow(<ImageToolbar />);
            wrapper.instance().handleDelete(mockClickEvent);

            expect(mockClickEvent.stopPropagation.mock.calls.length).toBe(1);
        });

        test('fires the deleteImage callback', () => {
            const selectedIndex = 1;
            const mockClickEvent = {
                stopPropagation: jest.fn()
            };
            const handleDeleteImage = jest.fn();

            const wrapper = shallow(
                <ImageToolbar
                    selectedIndex={selectedIndex}
                    deleteImage={handleDeleteImage}
                />
            );
            wrapper.instance().handleDelete(mockClickEvent);

            expect(handleDeleteImage.mock.calls.length).toBe(1);
            expect(handleDeleteImage.mock.calls[0][0]).toEqual(selectedIndex);
        });
    });
});
