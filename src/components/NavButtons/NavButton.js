import React from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import Button from '../Button/Button';

const NavButton = ({ children, ...otherProps }) => (
    <Button
        component={otherProps.to ? Link : 'button'}
        shadow="inner"
        {...otherProps}
    >
        {children}
    </Button>
);

NavButton.propTypes = {
    children: PropTypes.node,
    to: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
};

NavButton.defaultProps = {
    children: null,
    to: ''
};

export default NavButton;
