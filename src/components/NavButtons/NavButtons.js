import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './NavButtons.css';

const NavButtons = ({ children, buttonsClass }) => {
    const classes = classnames(styles.navButtons, buttonsClass);

    return <div className={classes}>{children}</div>;
};

NavButtons.propTypes = {
    children: PropTypes.node,
    buttonsClass: PropTypes.string
};

NavButtons.defaultProps = {
    children: null,
    buttonsClass: ''
};

export default NavButtons;
