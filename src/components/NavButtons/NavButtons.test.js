import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NavButtons from './NavButtons';

describe('<NavButtons />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<NavButtons />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<NavButtons>{testChildren}</NavButtons>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided className', () => {
        const wrapper = shallow(<NavButtons buttonsClass="myClass" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
