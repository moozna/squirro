import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NavButton from './NavButton';

describe('<NavButton />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<NavButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(<NavButton to={{ type: 'BOOKS' }} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(
            <NavButton>{testChildren}</NavButton>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
