import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import CloseButton from './CloseButton';

describe('<CloseButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<CloseButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the open button', () => {
        const wrapper = mount(<CloseButton />);

        expect(wrapper.instance().openButtonNode).toBeDefined();
    });

    describe('isDialogOpen', () => {
        test('when there are unsaved changes clicking on the open button sets isDialogOpen to true', () => {
            const wrapper = shallow(<CloseButton hasUnsavedChanges />);
            wrapper.find('NavButton').simulate('click');

            expect(wrapper.state('isDialogOpen')).toBe(true);
        });

        test('without unsaved changes clicking on the open button fires the onClose callback ', () => {
            const handleOnClose = jest.fn();
            const wrapper = shallow(<CloseButton onClose={handleOnClose} />);
            wrapper.find('NavButton').simulate('click');

            expect(wrapper.state('isDialogOpen')).toBe(false);
            expect(handleOnClose.mock.calls.length).toBe(1);
        });

        test('renders an open dialog when isDialogOpen is true', () => {
            const wrapper = shallow(<CloseButton />);
            wrapper.instance().setState({ isDialogOpen: true });

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('handleClose()', () => {
        test('sets isDialogOpen to false', () => {
            const wrapper = shallow(<CloseButton />);

            wrapper.instance().handleClose();
            expect(wrapper.state('isDialogOpen')).toBe(false);
        });

        test('sets open button to focused', () => {
            const mockFocus = jest.fn();
            const wrapper = mount(<CloseButton />);
            wrapper.instance().openButtonNode.focus = mockFocus;

            wrapper.instance().handleClose();
            expect(mockFocus.mock.calls.length).toBe(1);
        });
    });

    describe('handleSubmit()', () => {
        test('fires the onClose callback', () => {
            const handleOnClose = jest.fn();
            const wrapper = shallow(<CloseButton onClose={handleOnClose} />);

            wrapper.instance().handleSubmit();
            expect(handleOnClose.mock.calls.length).toBe(1);
        });

        test('closes the dialog', () => {
            const mockHandleClose = jest.fn();
            const wrapper = shallow(<CloseButton />);
            wrapper.instance().handleClose = mockHandleClose;

            wrapper.instance().handleSubmit();
            expect(mockHandleClose.mock.calls.length).toBe(1);
        });
    });
});
