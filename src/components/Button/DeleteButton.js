import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import Button from './Button';
import Trash from '../svg/Trash';
import Dialog from '../Dialog/Dialog';
import DialogCard from '../Dialog/DialogCard';

class DeleteButton extends Component {
    state = {
        isDialogOpen: false
    };

    handleOpen = () => {
        this.setState({ isDialogOpen: true });
    };

    handleClose = () => {
        this.setState({ isDialogOpen: false });
        if (this.openButtonNode) {
            this.openButtonNode.focus();
        }
    };

    handleSubmit = () => {
        const { onDelete } = this.props;
        onDelete();
        this.handleClose();
    };

    saveOpenButtonRef = node => {
        this.openButtonNode = node;
    };

    renderDialog() {
        const { name } = this.props;

        const actions = [
            { label: 'Yes, delete it!', onClick: this.handleSubmit },
            { label: 'Cancel', onClick: this.handleClose }
        ];

        return (
            <Dialog
                role="alertdialog"
                ariaLabel="delete"
                onClose={this.handleClose}
            >
                <DialogCard title="Are you sure?" actions={actions}>
                    <p>{`You will not be able to recover this ${name}.`}</p>
                </DialogCard>
            </Dialog>
        );
    }

    render() {
        const { name, onDelete, ...buttonProps } = this.props;
        const { isDialogOpen } = this.state;

        return (
            <Fragment>
                <Button
                    onClick={this.handleOpen}
                    saveRef={this.saveOpenButtonRef}
                    {...buttonProps}
                >
                    <Trash />
                </Button>
                {isDialogOpen && this.renderDialog()}
            </Fragment>
        );
    }
}

DeleteButton.propTypes = {
    name: PropTypes.string,
    onDelete: PropTypes.func
};

DeleteButton.defaultProps = {
    name: 'item',
    onDelete: noop
};

export default DeleteButton;
