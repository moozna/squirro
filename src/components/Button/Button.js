import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Button.css';

class Button extends Component {
    render() {
        const {
            children,
            className,
            component,
            saveRef,
            type,
            color,
            disabledColor,
            shape,
            shadow,
            active,
            disabled,
            ...otherProps
        } = this.props;

        let ElementType = component;

        if (ElementType === 'button' && otherProps.href) {
            ElementType = 'a';
        }

        const buttonProps = {};
        if (ElementType === 'button') {
            buttonProps.type = type;
            buttonProps.disabled = disabled;
        } else {
            buttonProps.role = 'button';
        }

        const classes = classnames(
            styles.button,
            {
                [styles[color]]: color !== 'none',
                [styles[disabledColor]]: disabledColor,
                [styles[shape]]: shape,
                [styles.shadowInner]: shadow === 'inner',
                [styles.shadowOuter]: shadow === 'outer',
                [styles.active]: active,
                [styles.disabled]: disabled
            },
            className
        );

        return (
            <ElementType
                className={classes}
                ref={saveRef}
                {...buttonProps}
                {...otherProps}
            >
                <span className={styles.label}>{children}</span>
            </ElementType>
        );
    }
}

Button.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    component: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
        PropTypes.object
    ]),
    saveRef: PropTypes.func,
    type: PropTypes.string,
    href: PropTypes.string,
    color: PropTypes.oneOf([
        'none',
        'magenta',
        'orange',
        'white',
        'transparent'
    ]),
    disabledColor: PropTypes.oneOf(['dark', 'light']),
    shape: PropTypes.oneOf(['circle', 'square', 'stadium']),
    shadow: PropTypes.oneOf(['none', 'inner', 'outer']),
    active: PropTypes.bool,
    disabled: PropTypes.bool
};

Button.defaultProps = {
    children: null,
    className: '',
    component: 'button',
    saveRef: null,
    type: 'button',
    href: null,
    color: 'magenta',
    disabledColor: 'dark',
    shape: 'circle',
    shadow: 'none',
    active: false,
    disabled: false
};

export default Button;
