import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import NavButton from '../NavButtons/NavButton';
import Close from '../svg/Close';
import Dialog from '../Dialog/Dialog';
import DialogCard from '../Dialog/DialogCard';

class CloseButton extends Component {
    state = {
        isDialogOpen: false
    };

    handleOpen = () => {
        const { hasUnsavedChanges, onClose } = this.props;

        if (hasUnsavedChanges) {
            this.setState({ isDialogOpen: true });
        } else {
            onClose();
        }
    };

    handleClose = () => {
        this.setState({ isDialogOpen: false });
        if (this.openButtonNode) {
            this.openButtonNode.focus();
        }
    };

    handleSubmit = () => {
        const { onClose } = this.props;
        onClose();
        this.handleClose();
    };

    saveOpenButtonRef = node => {
        this.openButtonNode = node;
    };

    renderDialog() {
        const actions = [
            { label: 'Yes, discard them!', onClick: this.handleSubmit },
            { label: 'Cancel', onClick: this.handleClose }
        ];

        return (
            <Dialog
                role="alertdialog"
                ariaLabel="discard"
                onClose={this.handleClose}
            >
                <DialogCard title="Are you sure?" actions={actions}>
                    <p>
                        You have unsaved changes. You will not be able to
                        recover these.
                    </p>
                </DialogCard>
            </Dialog>
        );
    }

    render() {
        const { hasUnsavedChanges, onClose, ...buttonProps } = this.props;
        const { isDialogOpen } = this.state;

        return (
            <Fragment>
                <NavButton
                    onClick={this.handleOpen}
                    saveRef={this.saveOpenButtonRef}
                    {...buttonProps}
                >
                    <Close />
                </NavButton>
                {isDialogOpen && this.renderDialog()}
            </Fragment>
        );
    }
}

CloseButton.propTypes = {
    hasUnsavedChanges: PropTypes.bool,
    onClose: PropTypes.func
};

CloseButton.defaultProps = {
    hasUnsavedChanges: false,
    onClose: noop
};

export default CloseButton;
