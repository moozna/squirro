import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Button from './Button';

const testComponent = () => <div />;

describe('<Button />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Button />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Button>{testChildren}</Button>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided component', () => {
        const wrapper = shallow(<Button component={testComponent} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an "a" tag when href is provided', () => {
        const wrapper = shallow(<Button type="button" href="/books" />);

        expect(wrapper.type()).toBe('a');
    });

    test('renders a submit button', () => {
        const mockType = 'submit';
        const wrapper = shallow(<Button component="button" type={mockType} />);

        expect(wrapper.prop('type')).toBe(mockType);
    });

    test('renders an active button', () => {
        const wrapper = shallow(<Button component="button" active />);

        expect(wrapper.hasClass('active')).toBe(true);
    });

    test('renders a disabled button', () => {
        const wrapper = shallow(<Button component="button" disabled />);

        expect(wrapper.hasClass('disabled')).toBe(true);
        expect(wrapper.prop('disabled')).toBe(true);
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<Button className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    describe('variants', () => {
        test('renders an orange button', () => {
            const wrapper = shallow(<Button color="orange" />);

            expect(wrapper.hasClass('orange')).toBe(true);
        });

        test('renders a white button', () => {
            const wrapper = shallow(<Button color="white" />);

            expect(wrapper.hasClass('white')).toBe(true);
        });

        test('renders a transparent button', () => {
            const wrapper = shallow(<Button color="transparent" />);

            expect(wrapper.hasClass('transparent')).toBe(true);
        });

        test('renders a square button', () => {
            const wrapper = shallow(<Button shape="square" />);

            expect(wrapper.hasClass('square')).toBe(true);
        });

        test('renders a stadium button', () => {
            const wrapper = shallow(<Button shape="stadium" />);

            expect(wrapper.hasClass('stadium')).toBe(true);
        });

        test('renders a button with inner shadow', () => {
            const wrapper = shallow(<Button shadow="inner" />);

            expect(wrapper.hasClass('shadowInner')).toBe(true);
        });

        test('renders a button with outer shadow', () => {
            const wrapper = shallow(<Button shadow="outer" />);

            expect(wrapper.hasClass('shadowOuter')).toBe(true);
        });
    });
});
