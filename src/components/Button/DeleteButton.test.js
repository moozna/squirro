import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import DeleteButton from './DeleteButton';

describe('<DeleteButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DeleteButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the open button', () => {
        const wrapper = mount(<DeleteButton />);

        expect(wrapper.instance().openButtonNode).toBeDefined();
    });

    describe('isDialogOpen', () => {
        test('clicking on the open button sets isDialogOpen to true', () => {
            const wrapper = shallow(<DeleteButton />);
            wrapper.find('Button').simulate('click');

            expect(wrapper.state('isDialogOpen')).toBe(true);
        });

        test('renders an open dialog when isDialogOpen is true', () => {
            const wrapper = shallow(<DeleteButton />);
            wrapper.instance().setState({ isDialogOpen: true });

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('handleClose()', () => {
        test('sets isDialogOpen to false', () => {
            const wrapper = shallow(<DeleteButton />);

            wrapper.instance().handleClose();
            expect(wrapper.state('isDialogOpen')).toBe(false);
        });

        test('sets open button to focused', () => {
            const mockFocus = jest.fn();
            const wrapper = mount(<DeleteButton />);
            wrapper.instance().openButtonNode.focus = mockFocus;

            wrapper.instance().handleClose();
            expect(mockFocus.mock.calls.length).toBe(1);
        });
    });

    describe('handleSubmit()', () => {
        test('fires the onDelete callback', () => {
            const handleOnDelete = jest.fn();
            const wrapper = shallow(<DeleteButton onDelete={handleOnDelete} />);

            wrapper.instance().handleSubmit();
            expect(handleOnDelete.mock.calls.length).toBe(1);
        });

        test('closes the dialog', () => {
            const mockHandleClose = jest.fn();
            const wrapper = shallow(<DeleteButton />);
            wrapper.instance().handleClose = mockHandleClose;

            wrapper.instance().handleSubmit();
            expect(mockHandleClose.mock.calls.length).toBe(1);
        });
    });
});
