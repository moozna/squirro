import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Chip from './Chip';

describe('<Chip />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Chip />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Chip>{testChildren}</Chip>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a tag with the specified value', () => {
        const wrapper = shallow(<Chip value="Fantasy" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a chip which can be toggled', () => {
        const wrapper = shallow(<Chip checkable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an orange chip', () => {
        const wrapper = shallow(<Chip color="orange" />);

        expect(wrapper.hasClass('orange')).toBe(true);
    });

    test('renders a checked chip', () => {
        const wrapper = shallow(<Chip checked />);

        expect(wrapper.hasClass('checked')).toBe(true);
    });

    test('renders a disabled chip', () => {
        const wrapper = shallow(<Chip disabled />);

        expect(wrapper.hasClass('disabled')).toBe(true);
    });

    test('renders a chip without borders', () => {
        const wrapper = shallow(<Chip inputStyle />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a chip without borders and with square close icon', () => {
        const wrapper = shallow(<Chip inputStyle deletable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a chip with close icon', () => {
        const wrapper = shallow(<Chip deletable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled chip with close icon', () => {
        const wrapper = shallow(<Chip deletable disabled />);
        const deleteButton = wrapper.find('.deleteButton');

        expect(deleteButton.hasClass('disabled')).toBe(true);
    });

    test('renders a chip with the provided close icon', () => {
        const testComponent = () => <div />;
        const wrapper = shallow(<Chip icon={testComponent} deletable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onDeleteClick()', () => {
        test('fires the onDeleteClick callback', () => {
            const mockEvent = {
                preventDefault: jest.fn()
            };
            const handleOnDeleteClick = jest.fn();

            const wrapper = shallow(
                <Chip deletable onDeleteClick={handleOnDeleteClick} />
            );

            wrapper.find('.deleteButton').simulate('click', mockEvent);
            expect(handleOnDeleteClick.mock.calls.length).toBe(1);
        });

        test('calls the onDeleteClick callback with the value of the tag', () => {
            const testTag = 'Fantasy';
            const mockEvent = {
                preventDefault: jest.fn()
            };
            const handleOnDeleteClick = jest.fn();

            const wrapper = shallow(
                <Chip
                    value={testTag}
                    deletable
                    onDeleteClick={handleOnDeleteClick}
                />
            );

            wrapper.find('.deleteButton').simulate('click', mockEvent);
            expect(handleOnDeleteClick.mock.calls[0][0]).toBe(testTag);
        });
    });
});
