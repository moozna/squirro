import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Chip.css';
import noop from '../../utils/noop';
import ToggleInput from '../Checkbox/ToggleInput';
import Delete from '../svg/Delete';

class Chip extends Component {
    handleDeleteClick = event => {
        const { value, onDeleteClick } = this.props;
        event.preventDefault();
        onDeleteClick(value);
    };

    renderToggleInput() {
        const {
            fieldId,
            name,
            checked,
            disabled,
            checkable,
            onToggle
        } = this.props;

        if (!checkable) {
            return null;
        }

        return (
            <ToggleInput
                fieldId={fieldId}
                type="checkbox"
                name={name}
                checked={checked}
                disabled={disabled}
                onChange={onToggle}
            />
        );
    }

    renderDeleteButton() {
        const { inputStyle, icon: Icon, disabled, deletable } = this.props;

        if (!deletable) {
            return null;
        }

        const buttonClasses = classnames(styles.deleteButton, {
            [styles.square]: inputStyle,
            [styles.disabled]: disabled
        });

        return (
            <span className={buttonClasses} onClick={this.handleDeleteClick}>
                <Icon className={styles.deleteIcon} iconSize={16} />
            </span>
        );
    }

    render() {
        const {
            children,
            value,
            color,
            checked,
            disabled,
            inputStyle,
            checkable,
            deletable,
            filterable
        } = this.props;

        const classes = classnames(styles.chip, {
            [styles[color]]: color,
            [styles.checked]: checked,
            [styles.disabled]: disabled,
            [styles.inputStyle]: inputStyle,
            [styles.hoverable]: filterable || checkable,
            [styles.deletable]: !!deletable
        });

        const ElementType = checkable ? 'label' : 'div';
        const label = children || value;

        return (
            <ElementType className={classes}>
                {this.renderToggleInput()}
                {label ? <span className={styles.label}>{label}</span> : null}
                {this.renderDeleteButton()}
            </ElementType>
        );
    }
}

Chip.propTypes = {
    children: PropTypes.node,
    fieldId: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    color: PropTypes.oneOf(['magenta', 'orange']),
    inputStyle: PropTypes.bool,
    icon: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
        PropTypes.object
    ]),
    checkable: PropTypes.bool,
    deletable: PropTypes.bool,
    filterable: PropTypes.bool,
    onToggle: PropTypes.func,
    onDeleteClick: PropTypes.func
};

Chip.defaultProps = {
    children: null,
    fieldId: '',
    name: '',
    value: '',
    checked: false,
    disabled: false,
    color: 'magenta',
    inputStyle: false,
    icon: Delete,
    checkable: false,
    deletable: false,
    filterable: false,
    onToggle: noop,
    onDeleteClick: noop
};

export default Chip;
