import React from 'react';
import Link from 'redux-first-router-link';
import { booksPage } from '../../data/actions/pageActions';
import styles from './Sidebar.css';
import Squirrel from '../svg/Squirrel';

const SidebarHeader = () => (
    <div className={styles.header}>
        <Link to={booksPage()} className={styles.logo}>
            <Squirrel imageClass={styles.image} />
        </Link>
    </div>
);

export default SidebarHeader;
