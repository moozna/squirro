import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SidebarHeader from './SidebarHeader';

describe('<SidebarHeader />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<SidebarHeader />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
