import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Sidebar from './Sidebar';

describe('<Sidebar />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Sidebar />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Sidebar>{testChildren}</Sidebar>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an opened sidebar', () => {
        const wrapper = shallow(<Sidebar opened />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('triggers the toggleButton', () => {
        const handleToggleClick = jest.fn();
        const wrapper = shallow(<Sidebar onToggleClick={handleToggleClick} />);

        wrapper.find('.toggleButton').simulate('click');
        expect(handleToggleClick.mock.calls.length).toBe(1);
    });
});
