import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Sidebar.css';
import noop from '../../utils/noop';
import SidebarHeader from './SidebarHeader';

const Sidebar = ({ children, opened, onToggleClick }) => {
    const classes = classnames(styles.sidebar, {
        [styles.opened]: opened
    });

    return (
        <aside className={classes}>
            <SidebarHeader />
            <div className={styles.content}>{children}</div>
            <button
                type="button"
                className={styles.toggleButton}
                onClick={onToggleClick}
            >
                &#8942;
            </button>
        </aside>
    );
};

Sidebar.propTypes = {
    children: PropTypes.node,
    opened: PropTypes.bool,
    onToggleClick: PropTypes.func
};

Sidebar.defaultProps = {
    children: null,
    opened: false,
    onToggleClick: noop
};

export default Sidebar;
