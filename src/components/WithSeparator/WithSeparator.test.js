import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import WithSeparator from './WithSeparator';

describe('<WithSeparator />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<WithSeparator />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<WithSeparator>{testChildren}</WithSeparator>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children with the provided separator', () => {
        const testChildren = [
            <div key="1">first</div>,
            <div key="2">
                <div>second</div>
            </div>,
            'third',
            <div key="3">fourth</div>
        ];
        const wrapper = shallow(
            <WithSeparator separator=", ">{testChildren}</WithSeparator>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('skips falsy children', () => {
        const testChildren = ['first', null, '', 'fourth'];
        const wrapper = shallow(
            <WithSeparator separator=", ">{testChildren}</WithSeparator>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
