import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

class WithSeparator extends Component {
    concatenateWithSeparator(text) {
        const { separator } = this.props;
        return `${text}${separator}`;
    }

    addSeparator(child) {
        if (typeof child === 'string') {
            return this.concatenateWithSeparator(child);
        }

        if (typeof child.props.children === 'string') {
            return React.cloneElement(
                child,
                [],
                this.concatenateWithSeparator(child.props.children)
            );
        }

        return React.cloneElement(
            child,
            [],
            this.addSeparator(child.props.children)
        );
    }

    isLastChild(index) {
        const { children } = this.props;
        return index === React.Children.count(children) - 1;
    }

    joinChildren() {
        const { children } = this.props;

        return React.Children.map(children, (child, index) => {
            if (!child) {
                return null;
            }

            if (this.isLastChild(index)) {
                return child;
            }

            return this.addSeparator(child);
        });
    }

    render() {
        return <Fragment>{this.joinChildren()}</Fragment>;
    }
}

WithSeparator.propTypes = {
    children: PropTypes.node,
    separator: PropTypes.string
};

WithSeparator.defaultProps = {
    children: null,
    separator: ''
};

export default WithSeparator;
