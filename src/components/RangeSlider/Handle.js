import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './RangeSlider.css';
import noop from '../../utils/noop';
import Tooltip from '../Tooltip/Tooltip';

class Handle extends Component {
    getAriaProps() {
        const { value, min, max, disabled } = this.props;
        return {
            role: 'slider',
            tabIndex: 0,
            'aria-valuemin': min,
            'aria-valuemax': max,
            'aria-valuenow': value,
            'aria-disabled': !!disabled
        };
    }

    handleMouseDown = event => {
        const { disabled, handleId, onValueChange } = this.props;

        if (disabled) {
            return;
        }

        onValueChange(event, handleId);
    };

    renderCircle() {
        const { isHalf, handleId } = this.props;
        const classes = classnames(styles.circle, {
            [styles.firstHalf]: isHalf && handleId === 0,
            [styles.secondHalf]: isHalf && handleId === 1
        });

        return (
            <div
                {...this.getAriaProps()}
                className={classes}
                onMouseDown={this.handleMouseDown}
            />
        );
    }

    renderHandle() {
        const { value, withTooltip, tooltipLabels } = this.props;
        const circle = this.renderCircle();

        if (withTooltip) {
            const label = tooltipLabels[value] || value.toString();
            return (
                <Tooltip label={label} className={styles.tooltip}>
                    {circle}
                </Tooltip>
            );
        }

        return circle;
    }

    render() {
        const { offset } = this.props;
        const positionStyle = {
            left: `${offset}%`
        };

        return (
            <div className={styles.handle} style={positionStyle}>
                {this.renderHandle()}
            </div>
        );
    }
}

Handle.propTypes = {
    value: PropTypes.number,
    min: PropTypes.number,
    max: PropTypes.number,
    offset: PropTypes.number,
    handleId: PropTypes.number,
    isHalf: PropTypes.bool,
    disabled: PropTypes.bool,
    withTooltip: PropTypes.bool,
    tooltipLabels: PropTypes.object,
    onValueChange: PropTypes.func
};

Handle.defaultProps = {
    value: 0,
    min: 0,
    max: 5,
    offset: 0,
    handleId: null,
    isHalf: false,
    disabled: false,
    withTooltip: false,
    tooltipLabels: {},
    onValueChange: noop
};

export default Handle;
