import React from 'react';
import PropTypes from 'prop-types';
import styles from './RangeSlider.css';

const Track = ({ offset, length }) => {
    const positionStyle = {
        left: `${offset}%`,
        width: `${length}%`
    };

    return <div className={styles.track} style={positionStyle} />;
};

Track.propTypes = {
    offset: PropTypes.number,
    length: PropTypes.number
};

Track.defaultProps = {
    offset: 0,
    length: 0
};

export default Track;
