import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './RangeSlider.css';
import noop from '../../utils/noop';

import Track from './Track';
import Marks from './Marks';
import Handle from './Handle';

const getHandleCenterPosition = handle => {
    const coords = handle.getBoundingClientRect();
    return coords.left + coords.width * 0.5;
};

const getPrecision = step => {
    const stepString = step.toString();
    let precision = 0;
    if (stepString.indexOf('.') >= 0) {
        precision = stepString.length - stepString.indexOf('.') - 1;
    }
    return precision;
};

class RangeSlider extends Component {
    state = {
        values: this.getInitialValues(),
        activeHandle: null
    };

    onMouseDown = (event, activeHandle) => {
        event.stopPropagation();
        event.preventDefault();

        if (event.button !== 0) {
            return;
        }

        let position = event.pageX;
        const handlePosition = getHandleCenterPosition(event.target);
        this.dragOffset = position - handlePosition;
        position = handlePosition;

        this.setState({ activeHandle });
        this.addDocumentMouseEvents();
    };

    onMouseMove = event => {
        if (!this.sliderRef) {
            this.onEnd();
            return;
        }
        const position = event.pageX;
        this.onMove(event, position - this.dragOffset);
    };

    onMove(event, position) {
        const { values, activeHandle } = this.state;
        event.stopPropagation();
        event.preventDefault();

        const value = this.calcValueByPos(position);
        const oldValue = values[activeHandle];

        if (value !== oldValue) {
            const nextValues = [...values];
            nextValues[activeHandle] = value;

            this.setState({
                values: nextValues
            });
        }
    }

    onEnd = () => {
        const { onAfterChange } = this.props;
        const { values } = this.state;

        this.removeDocumentEvents();
        this.dragOffset = 0;
        this.setState({
            activeHandle: null
        });

        onAfterChange(values);
    };

    getInitialValues() {
        const { values, defaultValues } = this.props;
        if (values && values.length) {
            return values;
        }
        return defaultValues;
    }

    getSliderStart() {
        const slider = this.sliderRef;
        const { left } = slider.getBoundingClientRect();
        return left;
    }

    getSliderLength() {
        const slider = this.sliderRef;
        const { width } = slider.getBoundingClientRect();
        return width;
    }

    getClosestPoint(value) {
        const { marks, step, min } = this.props;
        const points = marks.map(parseFloat);

        if (step !== null) {
            const closestStep = Math.round((value - min) / step) * step + min;
            points.push(closestStep);
        }

        const diffs = points.map(point => Math.abs(value - point));
        return points[diffs.indexOf(Math.min(...diffs))];
    }

    saveSlider = slider => {
        this.sliderRef = slider;
    };

    calcOffset(value) {
        const { min, max } = this.props;
        const ratio = (value - min) / (max - min);
        return ratio * 100;
    }

    calcValueByPos(position) {
        const pixelOffset = position - this.getSliderStart();
        const value = this.calcValueByOffset(pixelOffset);
        const nextValue = this.trimAlignValue(value);
        return nextValue;
    }

    calcValueByOffset(offset) {
        const { min, max } = this.props;
        const ratio = Math.abs(Math.max(offset, 0) / this.getSliderLength());
        const value = ratio * (max - min) + min;
        return value;
    }

    trimAlignValue(value) {
        const valueInRange = this.ensureValueInRange(value);
        const valNotConflict = this.ensureValueNotConflict(valueInRange);
        return this.ensureValuePrecision(valNotConflict);
    }

    ensureValueInRange(value) {
        const { min, max } = this.props;

        if (value <= min) {
            return min;
        }
        if (value >= max) {
            return max;
        }
        return value;
    }

    ensureValueNotConflict(value) {
        const { values, activeHandle } = this.state;

        const handle = activeHandle;
        const rangeStartValue = values[0];
        const rangeEndValue = values[1];

        if (handle === 0 && value > rangeEndValue) {
            return rangeEndValue;
        }

        if (handle === 1 && value < rangeStartValue) {
            return rangeStartValue;
        }

        return value;
    }

    ensureValuePrecision(value) {
        const { step } = this.props;
        const closestPoint = this.getClosestPoint(value);
        return step === null
            ? closestPoint
            : parseFloat(closestPoint.toFixed(getPrecision(step)));
    }

    addDocumentMouseEvents() {
        document.addEventListener('mousemove', this.onMouseMove);
        document.addEventListener('mouseup', this.onEnd);
    }

    removeDocumentEvents() {
        document.removeEventListener('mousemove', this.onMouseMove);
        document.removeEventListener('mouseup', this.onEnd);
    }

    render() {
        const {
            min,
            max,
            marks,
            disabled,
            withTooltip,
            tooltipLabels
        } = this.props;
        const {
            values: [rangeStartValue, rangeEndValue]
        } = this.state;

        const classes = classnames(styles.rangeSlider, {
            [styles.disabled]: disabled
        });

        const isBothValueEqual = rangeStartValue === rangeEndValue;
        const startPosition = this.calcOffset(rangeStartValue);
        const length = this.calcOffset(rangeEndValue) - startPosition;
        const endPosition = this.calcOffset(rangeEndValue);

        return (
            <div className={classes} ref={this.saveSlider}>
                <div className={styles.rail} />
                <Track offset={startPosition} length={length} />
                <Handle
                    offset={startPosition}
                    value={rangeStartValue}
                    min={min}
                    max={max}
                    disabled={disabled}
                    onValueChange={this.onMouseDown}
                    handleId={0}
                    isHalf={isBothValueEqual}
                    withTooltip={withTooltip}
                    tooltipLabels={tooltipLabels}
                />
                <Handle
                    offset={endPosition}
                    value={rangeEndValue}
                    min={min}
                    max={max}
                    disabled={disabled}
                    onValueChange={this.onMouseDown}
                    handleId={1}
                    isHalf={isBothValueEqual}
                    withTooltip={withTooltip}
                    tooltipLabels={tooltipLabels}
                />
                <Marks min={min} max={max} marks={marks} />
            </div>
        );
    }
}

RangeSlider.propTypes = {
    defaultValues: PropTypes.arrayOf(PropTypes.number),
    values: PropTypes.arrayOf(PropTypes.number),
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    marks: PropTypes.arrayOf(PropTypes.number),
    disabled: PropTypes.bool,
    withTooltip: PropTypes.bool,
    tooltipLabels: PropTypes.object,
    onAfterChange: PropTypes.func
};

RangeSlider.defaultProps = {
    defaultValues: [0, 5],
    values: [],
    min: 0,
    max: 5,
    step: 1,
    marks: [],
    disabled: false,
    withTooltip: false,
    tooltipLabels: {},
    onAfterChange: noop
};

export default RangeSlider;
