import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './RangeSlider.css';

class Marks extends Component {
    renderMarks() {
        const { min, max, marks } = this.props;
        const range = max - min || 1;

        return marks.map(point => {
            const positionStyle = {
                left: `${((point - min) / range) * 100}%`
            };

            return (
                <span
                    className={styles.markText}
                    style={positionStyle}
                    key={point}
                >
                    {point}
                </span>
            );
        });
    }

    render() {
        const { centered } = this.props;

        const classes = classnames(styles.mark, {
            [styles.centered]: centered
        });

        return <div className={classes}>{this.renderMarks()}</div>;
    }
}

Marks.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    marks: PropTypes.arrayOf(PropTypes.number),
    centered: PropTypes.bool
};

Marks.defaultProps = {
    min: 0,
    max: 5,
    marks: [],
    centered: false
};

export default Marks;
