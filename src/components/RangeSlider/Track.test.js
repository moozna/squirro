import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Track from './Track';

describe('<Track />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Track />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('uses the provided offset', () => {
        const testOffset = 50;
        const wrapper = shallow(<Track offset={testOffset} />);

        expect(wrapper.find('.track').prop('style').left).toBe(
            `${testOffset}%`
        );
    });

    test('uses the provided length', () => {
        const testLength = 50;
        const wrapper = shallow(<Track length={testLength} />);

        expect(wrapper.find('.track').prop('style').width).toBe(
            `${testLength}%`
        );
    });
});
