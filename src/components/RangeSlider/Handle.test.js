import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Handle from './Handle';

describe('<Handle />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Handle />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the handle with tooltip', () => {
        const wrapper = shallow(<Handle value={15} withTooltip />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the handle with customized tooltip labels', () => {
        const mockTooltipLabels = {
            1: 'First value',
            2: 'Second value',
            3: 'Third value'
        };

        const wrapper = shallow(
            <Handle value={2} tooltipLabels={mockTooltipLabels} withTooltip />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a half handle', () => {
        const wrapper = shallow(<Handle handleId={0} isHalf />);

        expect(wrapper.find('.circle').hasClass('firstHalf')).toBe(true);
    });

    test('fires the onValueChange callback', () => {
        const handleMouseDown = jest.fn();
        const wrapper = shallow(<Handle onValueChange={handleMouseDown} />);

        wrapper.find('.circle').simulate('mousedown');
        expect(handleMouseDown.mock.calls.length).toBe(1);
    });

    test('does not fires the onValueChange callback when disabled', () => {
        const handleMouseDown = jest.fn();
        const wrapper = shallow(
            <Handle onValueChange={handleMouseDown} disabled />
        );

        wrapper.find('.circle').simulate('mousedown');
        expect(handleMouseDown.mock.calls.length).toBe(0);
    });

    test('uses the provided min value', () => {
        const testMin = 1;
        const wrapper = shallow(<Handle min={testMin} />);

        expect(wrapper.find('.circle').prop('aria-valuemin')).toBe(testMin);
    });

    test('uses the provided max value', () => {
        const testMax = 10;
        const wrapper = shallow(<Handle max={testMax} />);

        expect(wrapper.find('.circle').prop('aria-valuemax')).toBe(testMax);
    });

    test('uses the provided value', () => {
        const testValue = 5;
        const wrapper = shallow(<Handle value={testValue} />);

        expect(wrapper.find('.circle').prop('aria-valuenow')).toBe(testValue);
    });

    test('uses the provided offset', () => {
        const testOffset = 50;
        const wrapper = shallow(<Handle offset={testOffset} />);

        expect(wrapper.find('.handle').prop('style').left).toBe(
            `${testOffset}%`
        );
    });
});
