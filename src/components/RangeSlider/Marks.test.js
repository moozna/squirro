import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Marks from './Marks';

describe('<Marks />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Marks />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided marks', () => {
        const marks = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];
        const wrapper = shallow(<Marks marks={marks} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a single mark at the start', () => {
        const marks = [12];
        const wrapper = shallow(<Marks min={12} max={12} marks={marks} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders two marks at the ends', () => {
        const marks = [12, 50];
        const wrapper = shallow(<Marks min={12} max={50} marks={marks} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders centered marks', () => {
        const wrapper = shallow(<Marks centered />);
        expect(wrapper.hasClass('centered')).toBe(true);
    });
});
