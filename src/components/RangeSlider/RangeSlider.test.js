import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import RangeSlider from './RangeSlider';

describe('<RangeSlider />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<RangeSlider />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the slider', () => {
        const wrapper = mount(<RangeSlider />);

        expect(wrapper.instance().sliderRef).toBeDefined();
    });

    test('renders a disabled slider', () => {
        const wrapper = shallow(<RangeSlider disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('uses the provided values', () => {
        const testValues = [2, 5];
        const wrapper = shallow(<RangeSlider values={testValues} />);

        expect(wrapper.state('values')).toEqual(testValues);
    });

    test('uses the default values when values are not provided', () => {
        const testDefaultValues = [1, 4];
        const wrapper = shallow(
            <RangeSlider defaultValues={testDefaultValues} />
        );

        expect(wrapper.state('values')).toEqual(testDefaultValues);
    });

    test('fires the onAfterChange callback', () => {
        const handleOnAfterChange = jest.fn();
        const wrapper = shallow(
            <RangeSlider onAfterChange={handleOnAfterChange} />
        );

        wrapper.instance().onEnd();
        expect(handleOnAfterChange.mock.calls.length).toBe(1);
    });

    describe('event listeners', () => {
        let mockAddEventListener;
        let mockRemoveEventListener;
        let mockMouseDownEvent;

        beforeEach(() => {
            mockAddEventListener = jest.spyOn(document, 'addEventListener');
            mockRemoveEventListener = jest.spyOn(
                document,
                'removeEventListener'
            );
            mockMouseDownEvent = {
                stopPropagation: jest.fn(),
                preventDefault: jest.fn(),
                button: 0,
                target: {
                    getBoundingClientRect: () => ({ left: 0, width: 100 })
                }
            };
        });

        afterEach(() => {
            mockAddEventListener.mockRestore();
            mockRemoveEventListener.mockRestore();
        });

        test('stops mousedown event', () => {
            const wrapper = shallow(<RangeSlider />);
            wrapper.instance().onMouseDown(mockMouseDownEvent, 0);

            expect(mockMouseDownEvent.stopPropagation.mock.calls.length).toBe(
                1
            );
            expect(mockMouseDownEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('attaches event listeners after a mousedown event fired with the left button', () => {
            const wrapper = shallow(<RangeSlider />);
            wrapper.instance().onMouseDown(mockMouseDownEvent, 0);

            expect(mockAddEventListener.mock.calls[0][0]).toBe('mousemove');
            expect(mockAddEventListener.mock.calls[1][0]).toBe('mouseup');
        });

        test('does not attaches event listeners when mousedown event fired with the wrong button', () => {
            const mockRightButtonMouseDownEvent = {
                ...mockMouseDownEvent,
                button: 1
            };

            const wrapper = shallow(<RangeSlider />);
            wrapper.instance().onMouseDown(mockRightButtonMouseDownEvent, 0);

            expect(mockAddEventListener.mock.calls.length).toBe(0);
        });

        test('detaches event listeners after a mouseup event fired', () => {
            const wrapper = shallow(<RangeSlider />);
            wrapper.instance().onEnd();

            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('mousemove');
            expect(mockRemoveEventListener.mock.calls[1][0]).toBe('mouseup');
        });
    });

    describe('moving of handlers', () => {
        const initialMockMouseMoveEvent = {
            stopPropagation: jest.fn(),
            preventDefault: jest.fn(),
            button: 0
        };

        const sliderRef = {
            getBoundingClientRect: () => ({ left: 0, width: 100 })
        };

        describe('handles half values', () => {
            test('sets start value of range to 0.5 when first handler is moved with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 10
                };

                const wrapper = shallow(<RangeSlider step={0.5} />);

                wrapper.setState({ activeHandle: 0 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([0.5, 5]);
            });

            test('sets end value of range to 4.5 when second handler is moved back with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 90
                };

                const wrapper = shallow(<RangeSlider step={0.5} />);

                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([0, 4.5]);
            });
        });

        describe('handles whole values', () => {
            test('sets start value of range to 1 when first handler is moved with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 20
                };

                const wrapper = shallow(<RangeSlider step={1} />);

                wrapper.setState({ activeHandle: 0 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([1, 5]);
            });

            test('sets end value of range to 4 when second handler is moved back with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 80
                };

                const wrapper = shallow(<RangeSlider step={1} />);

                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([0, 4]);
            });
        });

        describe('handles values based on the given marks', () => {
            test('sets start value of range when first handler is moved with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 25
                };

                const wrapper = shallow(
                    <RangeSlider
                        values={[7, 33]}
                        step={null}
                        marks={[2, 9, 27, 33]}
                        max={35}
                    />
                );

                wrapper.setState({ activeHandle: 0 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([9, 33]);
            });

            test('sets end value of range when second handler is moved back with one step', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 81
                };

                const wrapper = shallow(
                    <RangeSlider
                        values={[7, 33]}
                        step={null}
                        marks={[2, 9, 27, 33]}
                        max={35}
                    />
                );

                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([7, 27]);
            });
        });

        describe('out of range values', () => {
            test('sets the handler to the min value when it is moved to smaller value than min value', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: -20
                };

                const wrapper = shallow(<RangeSlider min={1} />);
                wrapper.setState({ activeHandle: 0 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([1, 5]);
            });

            test('sets the handler to the max value when it is moved to larger value than max value', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 150
                };

                const wrapper = shallow(<RangeSlider max={4} />);
                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([0, 4]);
            });
        });

        describe('crossing handlers is not possible', () => {
            test('sets the start handler to the end value when it is moved to a larger value than the end handler', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 80
                };

                const wrapper = shallow(<RangeSlider values={[2, 3]} />);
                wrapper.setState({ activeHandle: 0 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([3, 3]);
            });

            test('sets the end handler to the start value when it is moved to a smaller value than the start handler', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 20
                };

                const wrapper = shallow(<RangeSlider values={[2, 3]} />);
                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = -1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([2, 2]);
            });
        });

        describe('remains the same value', () => {
            test('does not changes the value when handler is moved to a position that represents the same value', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 60
                };

                const wrapper = shallow(<RangeSlider values={[2, 3]} />);
                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().sliderRef = sliderRef;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([2, 3]);
            });

            test('does not changes the value when sliderRef is undefined', () => {
                const mockMouseMoveEvent = {
                    ...initialMockMouseMoveEvent,
                    pageX: 80
                };

                const wrapper = shallow(<RangeSlider values={[2, 3]} />);
                wrapper.setState({ activeHandle: 1 });
                wrapper.instance().dragOffset = 1;
                wrapper.instance().onMouseMove(mockMouseMoveEvent);

                expect(wrapper.state('values')).toEqual([2, 3]);
            });
        });
    });
});
