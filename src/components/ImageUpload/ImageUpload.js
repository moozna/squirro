import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ImageUpload.css';
import { coversType } from '../../types/types';
import noop from '../../utils/noop';
import DropZone from '../DropZone/DropZone';
import Carousel from '../Carousel/Carousel';
import Cover from '../Cover/Cover';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';

class ImageUpload extends Component {
    state = {
        loading: false
    };

    setToDefault = index => {
        const { value, name, onChange } = this.props;
        const selectedImage = value[index];
        const newOrder = [
            selectedImage,
            ...value.filter(item => item !== selectedImage)
        ];
        onChange(name, newOrder);
        this.carousel.doSliding(0, 'prev');
    };

    handleDeleteImage = index => {
        const { value, name, onChange, saveDeletedFiles } = this.props;
        const selectedImage = value[index];
        const newValues = value.filter(item => item !== selectedImage);
        saveDeletedFiles(selectedImage);
        onChange(name, newValues);
    };

    handleDrop = newImages => {
        const {
            asin,
            name,
            value,
            onChange,
            uploadCoverImage,
            saveUploadedFiles
        } = this.props;

        this.setState({ loading: true });

        uploadCoverImage(asin, newImages).then(response => {
            const savedImages = response.images;
            const newValues = [...value, ...savedImages];
            this.setState({ loading: false });
            onChange(name, newValues);
            saveUploadedFiles(savedImages);
            this.carousel.doSliding(newValues.length - 1, 'next');
        });
    };

    saveRef = node => {
        this.carousel = node;
    };

    renderImages() {
        const { value, name } = this.props;

        if (!value || !value.length) {
            return <DropZone name={name} onChange={this.handleDrop} />;
        }

        return (
            <DropZone name={name} onChange={this.handleDrop}>
                <Carousel
                    ref={this.saveRef}
                    readOnly={false}
                    setToDefault={this.setToDefault}
                    deleteImage={this.handleDeleteImage}
                >
                    {value.map((item, index) => (
                        <Cover key={index} imagePath={item.url} />
                    ))}
                </Carousel>
            </DropZone>
        );
    }

    render() {
        const { loading } = this.state;

        return (
            <div
                className={styles.imageUpload}
                onFocus={this.handleFocus}
                onBlur={this.handleBlur}
            >
                {this.renderImages()}
                {loading && <LoadingIndicator />}
            </div>
        );
    }
}

ImageUpload.propTypes = {
    value: coversType,
    name: PropTypes.string,
    asin: PropTypes.string,
    onChange: PropTypes.func,
    uploadCoverImage: PropTypes.func,
    saveUploadedFiles: PropTypes.func,
    saveDeletedFiles: PropTypes.func
};

ImageUpload.defaultProps = {
    value: [],
    name: '',
    asin: '',
    onChange: noop,
    uploadCoverImage: noop,
    saveUploadedFiles: noop,
    saveDeletedFiles: noop
};

export default ImageUpload;
