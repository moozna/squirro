import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImageUpload from './ImageUpload';
import noop from '../../utils/noop';

describe('<ImageUpload />', () => {
    const image1 = { url: 'image1' };
    const image2 = { url: 'image2' };
    const image3 = { url: 'image3' };
    const image4 = { url: 'image4' };

    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ImageUpload />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when images are specified', () => {
        const mockImages = [image1, image2, image3];
        const wrapper = shallow(<ImageUpload value={mockImages} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on carousel', () => {
        const mockImages = [image1, image2, image3];
        const wrapper = mount(<ImageUpload value={mockImages} />);

        expect(wrapper.instance().carousel).toBeDefined();
    });

    describe('setToDefault', () => {
        test('fires the onChange callback', () => {
            const mockImages = [image1, image2, image3];
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={mockImages}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().carousel = {
                doSliding: noop
            };
            const expectedImages = [image2, image1, image3];

            wrapper.instance().setToDefault(1);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('cover');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedImages);
        });

        test('slides the carousel to the first image', () => {
            const mockImages = [image1, image2, image3];
            const mockDoSliding = jest.fn();
            const wrapper = shallow(
                <ImageUpload name="cover" value={mockImages} onChange={noop} />
            );
            wrapper.instance().carousel = {
                doSliding: mockDoSliding
            };

            wrapper.instance().setToDefault(1);

            expect(mockDoSliding.mock.calls.length).toBe(1);
            expect(mockDoSliding.mock.calls[0][0]).toBe(0);
            expect(mockDoSliding.mock.calls[0][1]).toEqual('prev');
        });
    });

    describe('handleDeleteImage', () => {
        test('fires the onChange callback', () => {
            const mockImages = [image1, image2, image3];
            const handleOnChange = jest.fn();
            const wrapper = mount(
                <ImageUpload
                    name="cover"
                    value={mockImages}
                    onChange={handleOnChange}
                />
            );
            const expectedImages = [image1, image3];

            wrapper.instance().handleDeleteImage(1);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('cover');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedImages);
        });

        test('fires the saveDeletedFiles callback', () => {
            const mockImages = [image1, image2, image3];
            const mockSaveDeletedFiles = jest.fn();

            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={mockImages}
                    saveDeletedFiles={mockSaveDeletedFiles}
                />
            );
            const expectedImage = mockImages[1];

            wrapper.instance().handleDeleteImage(1);

            expect(mockSaveDeletedFiles.mock.calls.length).toBe(1);
            expect(mockSaveDeletedFiles.mock.calls[0][0]).toEqual(
                expectedImage
            );
        });
    });

    describe('handleDrop', () => {
        test('fires the onChange callback', async () => {
            const currentImages = [image1, image2];
            const newImages = [image3, image4];
            const handleOnChange = jest.fn();
            const mockUploadCoverImage = jest.fn().mockReturnValue(
                Promise.resolve({
                    images: newImages
                })
            );

            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={currentImages}
                    onChange={handleOnChange}
                    uploadCoverImage={mockUploadCoverImage}
                    saveUploadedFiles={noop}
                />
            );
            wrapper.instance().carousel = {
                doSliding: noop
            };
            const expectedValue = [image1, image2, image3, image4];

            await wrapper.instance().handleDrop(newImages);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('cover');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedValue);
        });

        test('fires the saveUploadedFiles callback', async () => {
            const currentImages = [image1, image2];
            const newImages = [image3, image4];
            const mockSaveUploadFiles = jest.fn();
            const mockUploadCoverImage = jest.fn().mockReturnValue(
                Promise.resolve({
                    images: newImages
                })
            );

            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={currentImages}
                    onChange={noop}
                    uploadCoverImage={mockUploadCoverImage}
                    saveUploadedFiles={mockSaveUploadFiles}
                />
            );
            wrapper.instance().carousel = {
                doSliding: noop
            };

            await wrapper.instance().handleDrop(newImages);

            expect(mockSaveUploadFiles.mock.calls.length).toBe(1);
            expect(mockSaveUploadFiles.mock.calls[0][0]).toEqual(newImages);
        });

        test('slides the carousel to the last image', async () => {
            const currentImages = [image1, image2];
            const newImages = [image3, image4];
            const mockDoSliding = jest.fn();
            const mockUploadCoverImage = jest.fn().mockReturnValue(
                Promise.resolve({
                    images: newImages
                })
            );

            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={currentImages}
                    onChange={noop}
                    uploadCoverImage={mockUploadCoverImage}
                    saveUploadedFiles={noop}
                />
            );
            wrapper.instance().carousel = {
                doSliding: mockDoSliding
            };

            await wrapper.instance().handleDrop(newImages);

            expect(mockDoSliding.mock.calls.length).toBe(1);
            expect(mockDoSliding.mock.calls[0][0]).toBe(3);
            expect(mockDoSliding.mock.calls[0][1]).toEqual('next');
        });
    });

    describe('Loading', () => {
        test('renders a loading indicator', () => {
            const mockImages = [image1, image2, image3];
            const wrapper = shallow(<ImageUpload value={mockImages} />);
            wrapper.setState({ loading: true });

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('does not render loading indicator after images were received', async () => {
            const currentImages = [image1, image2];
            const newImages = [image3, image4];
            const mockUploadCoverImage = jest.fn().mockReturnValue(
                Promise.resolve({
                    images: newImages
                })
            );
            const wrapper = shallow(
                <ImageUpload
                    name="cover"
                    value={currentImages}
                    uploadCoverImage={mockUploadCoverImage}
                    saveUploadedFiles={noop}
                />
            );
            wrapper.instance().carousel = {
                doSliding: noop
            };

            await wrapper.instance().handleDrop(newImages);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });
});
