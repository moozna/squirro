import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './SearchInput.css';
import noop from '../../utils/noop';
import Input from '../Form/Input';
import Close from '../svg/Close';

class SearchInput extends Component {
    handleKeyDown = event => {
        const { clearSearch } = this.props;

        if (event.key === 'Enter') {
            clearSearch();
        }
    };

    render() {
        const {
            value,
            name,
            focused,
            onChange,
            onFocus,
            onBlur,
            clearSearch
        } = this.props;

        const hasValue = value.length > 0;
        const isActive = hasValue || focused;
        const classes = classnames(styles.searchField, {
            [styles.active]: isActive
        });

        return (
            <label className={classes}>
                <Input
                    inputElementClass={styles.searchInput}
                    type="text"
                    name={name}
                    value={value}
                    onChange={onChange}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    flexibleWidth
                    minWidth={0}
                    defaultPadding={0}
                    disableValueReset
                />
                {hasValue && (
                    <button
                        type="button"
                        className={styles.closeButton}
                        onClick={clearSearch}
                        onKeyDown={this.handleKeyDown}
                    >
                        <Close iconSize={16} />
                    </button>
                )}
            </label>
        );
    }
}

SearchInput.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    focused: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    clearSearch: PropTypes.func
};

SearchInput.defaultProps = {
    value: '',
    name: '',
    focused: false,
    onChange: noop,
    onFocus: noop,
    onBlur: noop,
    clearSearch: noop
};

export default SearchInput;
