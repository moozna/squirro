import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SearchInput from './SearchInput';

describe('<SearchInput />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<SearchInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an open component when value is not empty', () => {
        const wrapper = shallow(<SearchInput value="king" />);

        expect(wrapper.hasClass('active')).toBe(true);
    });

    test('renders an open component when it is focused', () => {
        const wrapper = shallow(<SearchInput focused />);

        expect(wrapper.hasClass('active')).toBe(true);
    });

    describe('close button', () => {
        test('rendered when value is provided', () => {
            const wrapper = shallow(<SearchInput value="king" focused />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('fires the clearSearch callback when Enter is pressed', () => {
            const mockEvent = {
                key: 'Enter'
            };
            const handleClearSearch = jest.fn();

            const wrapper = shallow(
                <SearchInput value="king" clearSearch={handleClearSearch} />
            );
            wrapper.find('.closeButton').simulate('keydown', mockEvent);

            expect(handleClearSearch.mock.calls.length).toBe(1);
        });

        test('does not fire the clearSearch callback when not Enter is pressed', () => {
            const mockEvent = {
                key: 'x'
            };
            const handleClearSearch = jest.fn();

            const wrapper = shallow(
                <SearchInput value="king" clearSearch={handleClearSearch} />
            );
            wrapper.find('.closeButton').simulate('keydown', mockEvent);

            expect(handleClearSearch.mock.calls.length).toBe(0);
        });
    });
});
