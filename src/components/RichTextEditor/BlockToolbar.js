import React from 'react';
import PropTypes from 'prop-types';
import { RichUtils } from 'draft-js';
import styles from './RichTextEditor.css';
import StyleButton from './StyleButton';
import noop from '../../utils/noop';

const BlockToolbar = ({ buttons, editorState, onToggle }) => {
    if (buttons.length < 1) {
        return null;
    }

    const blockType = RichUtils.getCurrentBlockType(editorState);

    return (
        <div className={styles.controls}>
            {buttons.map(type => (
                <StyleButton
                    key={type.style}
                    label={type.label}
                    icon={type.icon}
                    active={type.style === blockType}
                    onToggle={onToggle}
                    style={type.style}
                />
            ))}
        </div>
    );
};

BlockToolbar.propTypes = {
    buttons: PropTypes.arrayOf(PropTypes.object),
    editorState: PropTypes.object,
    onToggle: PropTypes.func
};

BlockToolbar.defaultProps = {
    buttons: [],
    editorState: {},
    onToggle: noop
};

export default BlockToolbar;
