import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RichTextEditor.css';
import noop from '../../utils/noop';
import IconButton from './IconButton';
import InputPopup from './InputPopup';

class PopupButton extends Component {
    state = {
        showURLInput: false
    };

    componentDidUpdate(prevProps, prevState) {
        if (!prevState.showURLInput && this.state.showURLInput) {
            document.addEventListener('click', this.handleDocumentClick);
        }

        if (prevState.showURLInput && !this.state.showURLInput) {
            document.removeEventListener('click', this.handleDocumentClick);
        }
    }

    componentWillUnmount() {
        if (this.state.showURLInput) {
            document.removeEventListener('click', this.handleDocumentClick);
        }
    }

    handleTogglePopup = () => {
        const { focusEditor } = this.props;
        const { showURLInput: isShowing } = this.state;

        if (isShowing) {
            setTimeout(() => focusEditor(), 0);
        }

        this.setState({
            showURLInput: !isShowing
        });
    };

    handleSubmit = url => {
        const { onSubmit } = this.props;

        let newUrl = url;

        if (url.indexOf('http') === -1) {
            newUrl = `http://${newUrl}`;
        }

        onSubmit(newUrl);
        this.closePopup();
    };

    handleCancel = () => {
        this.closePopup();
    };

    handleDocumentClick = event => {
        const { showURLInput } = this.state;

        if (
            showURLInput &&
            this.rootNode &&
            !this.rootNode.contains(event.target)
        ) {
            this.closePopup();
        }
    };

    saveRef = node => {
        this.rootNode = node;
    };

    closePopup() {
        this.setState({ showURLInput: false });
    }

    renderPopup = () => {
        const { url } = this.props;
        const { showURLInput } = this.state;

        if (!showURLInput) {
            return null;
        }

        return (
            <InputPopup
                value={url}
                onSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
            />
        );
    };

    render() {
        const { label, icon, disabled } = this.props;
        return (
            <span className={styles.popupButton} ref={this.saveRef}>
                <IconButton
                    label={label}
                    icon={icon}
                    disabled={disabled}
                    onClick={this.handleTogglePopup}
                />
                {this.renderPopup()}
            </span>
        );
    }
}

PopupButton.propTypes = {
    url: PropTypes.string,
    label: PropTypes.string,
    icon: PropTypes.node,
    disabled: PropTypes.bool,
    focusEditor: PropTypes.func,
    onSubmit: PropTypes.func
};

PopupButton.defaultProps = {
    url: '',
    label: '',
    icon: null,
    disabled: false,
    focusEditor: noop,
    onSubmit: noop
};

export default PopupButton;
