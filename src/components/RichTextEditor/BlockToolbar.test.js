import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EditorState } from 'draft-js';
import BlockToolbar from './BlockToolbar';
import { BLOCK_BUTTONS } from './buttons';

describe('<BlockToolbar />', () => {
    const mockEditorState = EditorState.createEmpty();

    test('has the expected html structure', () => {
        const wrapper = shallow(<BlockToolbar editorState={mockEditorState} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when buttons are specified', () => {
        const wrapper = shallow(
            <BlockToolbar
                buttons={BLOCK_BUTTONS}
                editorState={mockEditorState}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
