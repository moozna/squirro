import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import PopupButton from './PopupButton';

describe('<PopupButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<PopupButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an open input', () => {
        const wrapper = shallow(<PopupButton />);

        wrapper.setState({ showURLInput: true });
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled popup button', () => {
        const wrapper = shallow(<PopupButton disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the root node', () => {
        const wrapper = mount(<PopupButton />);

        expect(wrapper.instance().rootNode).toBeDefined();
    });

    describe('handleSubmit()', () => {
        test('fires the onSubmit callback', () => {
            const mockUrl = 'http://www.url';
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(<PopupButton onSubmit={handleOnSubmit} />);

            wrapper.instance().handleSubmit(mockUrl);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(mockUrl);
        });

        test('adds http to the url if its missing', () => {
            const mockUrl = 'www.url';
            const exoectedUrl = 'http://www.url';
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(<PopupButton onSubmit={handleOnSubmit} />);

            wrapper.instance().handleSubmit(mockUrl);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(exoectedUrl);
        });

        test('closes the input popup', () => {
            const mockUrl = 'www.url';
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(<PopupButton onSubmit={handleOnSubmit} />);
            wrapper.setState({ showURLInput: true });

            wrapper.instance().handleSubmit(mockUrl);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(wrapper.state('showURLInput')).toBe(false);
        });
    });

    describe('handleCancel()', () => {
        test('closes the input popup', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.instance().handleCancel();

            expect(wrapper.state('showURLInput')).toBe(false);
        });
    });

    describe('handleTogglePopup()', () => {
        test('opens the input popup when it is closed', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.instance().handleTogglePopup();

            expect(wrapper.state('showURLInput')).toBe(true);
        });

        test('closes the input popup when it is opened', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.instance().handleTogglePopup();

            expect(wrapper.state('showURLInput')).toBe(false);
        });

        test('fires the focusEditor callback when input popup is closed', () => {
            jest.useFakeTimers();
            const handleFocusEditor = jest.fn();
            const wrapper = shallow(
                <PopupButton focusEditor={handleFocusEditor} />
            );
            wrapper.setState({ showURLInput: true });
            wrapper.instance().handleTogglePopup();
            jest.runAllTimers();

            expect(handleFocusEditor.mock.calls.length).toBe(1);
        });
    });

    describe('handleDocumentClick()', () => {
        test('closes the opened input popup by clicking outside the component', () => {
            const wrapper = mount(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.instance().handleDocumentClick({
                target: document.createElement('div')
            });

            expect(wrapper.state('showURLInput')).toBe(false);
        });

        test('does not close the opened input popup by clicking inside the component', () => {
            const wrapper = mount(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.instance().handleDocumentClick({
                target: wrapper.getDOMNode()
            });

            expect(wrapper.state('showURLInput')).toBe(true);
        });

        test('does not close the closed input popup by clicking outside the component', () => {
            const wrapper = mount(<PopupButton />);
            wrapper.instance().handleDocumentClick({
                target: document.createElement('div')
            });

            expect(wrapper.state('showURLInput')).toBe(false);
        });
    });

    describe('event listeners', () => {
        let mockAddEventListener;
        let mockRemoveEventListener;

        beforeEach(() => {
            mockAddEventListener = jest.spyOn(document, 'addEventListener');
            mockRemoveEventListener = jest.spyOn(
                document,
                'removeEventListener'
            );
        });

        afterEach(() => {
            mockAddEventListener.mockRestore();
            mockRemoveEventListener.mockRestore();
        });

        test('attaches event listeners after input popup is opened', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.setState({ showURLInput: true });

            expect(mockAddEventListener.mock.calls.length).toBe(1);
            expect(mockAddEventListener.mock.calls[0][0]).toBe('click');
        });

        test('detaches event listeners after input popup is closed', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.setState({ showURLInput: false });

            expect(mockRemoveEventListener.mock.calls.length).toBe(1);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('click');
        });

        test('detaches event listeners after unmount', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.setState({ showURLInput: true });
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(1);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('click');
        });

        test('does not detach event listeners when input popup is not opened', () => {
            const wrapper = shallow(<PopupButton />);
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(0);
        });
    });
});
