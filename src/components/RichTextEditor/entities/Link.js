import PropTypes from 'prop-types';
import React from 'react';
import styles from '../RichTextEditor.css';
import Tooltip from '../../Tooltip/Tooltip';

export const findLinkEntities = (contentBlock, callback, contentState) => {
    contentBlock.findEntityRanges(character => {
        const entityKey = character.getEntity();
        return (
            entityKey !== null &&
            contentState.getEntity(entityKey).getType() === 'LINK'
        );
    }, callback);
};

const Link = ({ children, contentState, entityKey }) => {
    const { url } = contentState.getEntity(entityKey).getData();
    return (
        <Tooltip label={url} position="bottom">
            <a
                className={styles.link}
                href={url}
                rel="noopener noreferrer"
                target="_blank"
                aria-label={url}
            >
                {children}
            </a>
        </Tooltip>
    );
};

Link.propTypes = {
    children: PropTypes.node,
    entityKey: PropTypes.string,
    contentState: PropTypes.object.isRequired
};

Link.defaultProps = {
    children: null,
    entityKey: ''
};

export default Link;
