import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Link from './Link';

describe('<Link />', () => {
    const mockContentState = {
        getEntity: () => ({
            getData: jest.fn().mockReturnValue({ url: 'url' })
        })
    };

    test('has the expected html structure', () => {
        const wrapper = shallow(<Link contentState={mockContentState} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(
            <Link contentState={mockContentState}>{testChildren}</Link>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
