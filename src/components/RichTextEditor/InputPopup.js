import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RichTextEditor.css';
import noop from '../../utils/noop';
import IconButton from './IconButton';
import { INPUT_BUTTONS } from './buttons';

class InputPopup extends Component {
    state = {
        urlInputValue: this.props.value
    };

    getWidthStyle() {
        const { urlInputValue } = this.state;

        return {
            width: `${urlInputValue.length + 1}ch`,
            minWidth: '170px',
            maxWidth: '30vw'
        };
    }

    handleInputChange = event => {
        const { target } = event;
        this.setState({
            urlInputValue: target.value
        });
    };

    handleSubmit = () => {
        const { onSubmit } = this.props;
        const { urlInputValue } = this.state;
        onSubmit(urlInputValue);
    };

    handleKeydown = event => {
        const { onCancel } = this.props;

        if (event.key === 'Enter') {
            this.handleSubmit();
        }

        if (event.key === 'Escape') {
            onCancel();
        }
    };

    render() {
        const { urlInputValue } = this.state;
        const { onCancel } = this.props;

        return (
            <div className={styles.inputPopup}>
                <input
                    style={this.getWidthStyle()}
                    value={urlInputValue}
                    onChange={this.handleInputChange}
                    onKeyDown={this.handleKeydown}
                    data-lpignore
                    autoFocus
                    autoComplete="off"
                    spellCheck={false}
                />
                <IconButton
                    {...INPUT_BUTTONS.ok}
                    buttonClass={styles.inputButton}
                    onMouseDown={this.handleSubmit}
                />
                <IconButton
                    {...INPUT_BUTTONS.cancel}
                    buttonClass={styles.inputButton}
                    onMouseDown={onCancel}
                />
            </div>
        );
    }
}

InputPopup.propTypes = {
    value: PropTypes.string,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func
};

InputPopup.defaultProps = {
    value: '',
    onSubmit: noop,
    onCancel: noop
};

export default InputPopup;
