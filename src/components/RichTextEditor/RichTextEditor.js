import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Editor,
    EditorState,
    RichUtils,
    SelectionState,
    Modifier
} from 'draft-js';
import classnames from 'classnames';
import styles from './RichTextEditor.css';
import noop from '../../utils/noop';
import {
    createEditorState,
    stateToHTML,
    getEntityAtCursor,
    clearEntityForRange,
    blockStyleFn,
    keyBindingFn
} from './draftUtils';
import Toolbar from './Toolbar';

class RichTextEditor extends Component {
    state = {
        editorState: createEditorState(this.props.value),
        focused: false
    };

    setLink = url => {
        const { editorState } = this.state;
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity(
            'LINK',
            'MUTABLE',
            { url }
        );

        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

        const newEditorState = EditorState.push(
            editorState,
            contentStateWithEntity
        );

        let selection = editorState.getSelection();

        if (selection.isCollapsed()) {
            const entity = getEntityAtCursor(editorState);

            if (entity != null) {
                const { blockKey, startOffset, endOffset } = entity;
                selection = new SelectionState({
                    anchorKey: blockKey,
                    focusKey: blockKey,
                    anchorOffset: startOffset,
                    focusOffset: endOffset
                });
            }
        }

        this.handleChange(
            RichUtils.toggleLink(newEditorState, selection, entityKey)
        );
        setTimeout(() => this.focus(), 0);
    };

    removeLink = () => {
        const { editorState } = this.state;
        const entity = getEntityAtCursor(editorState);

        if (entity != null) {
            const { blockKey, startOffset, endOffset } = entity;
            this.handleChange(
                clearEntityForRange(
                    editorState,
                    blockKey,
                    startOffset,
                    endOffset
                )
            );
        }
    };

    insertEmptyLine = () => {
        const { editorState } = this.state;

        const contentState = Modifier.insertText(
            editorState.getCurrentContent(),
            editorState.getSelection(),
            '\n ',
            editorState.getCurrentInlineStyle(),
            null
        );

        const newEditorState = EditorState.push(
            editorState,
            contentState,
            'insert-characters'
        );

        const newEditorStateWithSelection = EditorState.forceSelection(
            newEditorState,
            contentState.getSelectionAfter()
        );

        this.handleChange(newEditorStateWithSelection);
    };

    removeAllInlineStyles = () => {
        const { editorState } = this.state;
        const styles = ['BOLD', 'ITALIC', 'UNDERLINE', 'STRIKETHROUGH', 'CODE'];

        const contentWithoutStyles = styles.reduce(
            (newContentState, style) =>
                Modifier.removeInlineStyle(
                    newContentState,
                    editorState.getSelection(),
                    style
                ),
            editorState.getCurrentContent()
        );

        const newEditorState = EditorState.push(
            editorState,
            contentWithoutStyles,
            'change-inline-style'
        );

        this.handleChange(newEditorState);
    };

    toggleInlineStyle = inlineStyle => {
        const { editorState } = this.state;
        this.handleChange(
            RichUtils.toggleInlineStyle(editorState, inlineStyle)
        );
    };

    toggleBlockType = blockType => {
        const { editorState } = this.state;
        this.handleChange(RichUtils.toggleBlockType(editorState, blockType));
    };

    handleUndo = () => {
        const { editorState } = this.state;
        this.handleChange(EditorState.undo(editorState));
    };

    handleRedo = () => {
        const { editorState } = this.state;
        this.handleChange(EditorState.redo(editorState));
    };

    handleChange = editorState => {
        const { name, onChange } = this.props;
        const currentHtml = stateToHTML(editorState);

        this.setState({ editorState });
        onChange(name, currentHtml);
    };

    handleKeyCommand = (command, editorState) => {
        if (command === 'insert-empty-line') {
            this.insertEmptyLine();
            return 'handled';
        }

        const newState = RichUtils.handleKeyCommand(editorState, command);

        if (newState) {
            this.handleChange(newState);
            return 'handled';
        }
        return 'not-handled';
    };

    focus = () => {
        this.editor.focus();
        this.setState({ focused: true });
    };

    handleBlur = () => {
        this.setState({ focused: false });
    };

    saveRef = node => {
        this.editor = node;
    };

    isValuePresent() {
        const { editorState } = this.state;
        const contentState = editorState.getCurrentContent();
        return contentState.hasText();
    }

    render() {
        const { editorState, focused } = this.state;
        const { label } = this.props;

        const labelClasses = classnames(styles.label, {
            [styles.focused]: focused,
            [styles.filled]: this.isValuePresent()
        });

        const barClasses = classnames(styles.bar, styles.leftAnimation, {
            [styles.focused]: focused
        });

        return (
            <div className={styles.content}>
                <Toolbar
                    editorState={editorState}
                    focusEditor={this.focus}
                    toggleInlineStyle={this.toggleInlineStyle}
                    toggleBlockType={this.toggleBlockType}
                    setLink={this.setLink}
                    removeLink={this.removeLink}
                    insertEmptyLine={this.insertEmptyLine}
                    removeStyles={this.removeAllInlineStyles}
                    onUndo={this.handleUndo}
                    onRedo={this.handleRedo}
                />
                <div
                    className={styles.editor}
                    onClick={this.focus}
                    onFocus={this.focus}
                >
                    <Editor
                        className={styles.editorRoot}
                        ref={this.saveRef}
                        editorState={editorState}
                        onChange={this.handleChange}
                        blockStyleFn={blockStyleFn}
                        keyBindingFn={keyBindingFn}
                        handleKeyCommand={this.handleKeyCommand}
                        onBlur={this.handleBlur}
                    />
                    {label ? (
                        <span className={labelClasses}>{label}</span>
                    ) : null}
                    <div className={barClasses} />
                </div>
            </div>
        );
    }
}

RichTextEditor.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

RichTextEditor.defaultProps = {
    value: '',
    name: '',
    label: '',
    onChange: noop
};

export default RichTextEditor;
