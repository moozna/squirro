import React from 'react';
import Bold from '../svg/Bold';
import Italic from '../svg/Italic';
import Underline from '../svg/Underline';
import Strikethrough from '../svg/Strikethrough';
import Blockquote from '../svg/Blockquote';
import UnorderedList from '../svg/UnorderedList';
import OrderedList from '../svg/OrderedList';
import Link from '../svg/Link';
import Unlink from '../svg/Unlink';
import Check from '../svg/Check';
import Close from '../svg/Close';
import Undo from '../svg/Undo';
import Redo from '../svg/Redo';
import NewLine from '../svg/NewLine';
import Rubber from '../svg/Rubber';

export const INLINE_BUTTONS = [
    {
        label: 'Bold (Ctrl + B)',
        icon: <Bold />,
        style: 'BOLD'
    },
    {
        label: 'Italic (Ctrl + I)',
        icon: <Italic />,
        style: 'ITALIC'
    },
    {
        label: 'Underline (Ctrl + U)',
        icon: <Underline />,
        style: 'UNDERLINE'
    },
    {
        label: 'Strikethrough',
        icon: <Strikethrough />,
        style: 'STRIKETHROUGH'
    }
];

export const BLOCK_BUTTONS = [
    {
        label: 'Blockquote',
        icon: <Blockquote />,
        style: 'blockquote'
    },
    {
        label: 'Unordered List',
        icon: <UnorderedList />,
        style: 'unordered-list-item'
    },
    {
        label: 'Ordered List',
        icon: <OrderedList />,
        style: 'ordered-list-item'
    }
];

export const LINK_BUTTONS = {
    link: {
        label: 'Link',
        icon: <Link />
    },
    unlink: {
        label: 'Remove Link',
        icon: <Unlink />
    }
};

export const INPUT_BUTTONS = {
    ok: {
        label: 'OK',
        icon: <Check />
    },
    cancel: {
        label: 'Cancel',
        icon: <Close />
    }
};

export const EMPTY_LINE_BUTTON = {
    label: 'Empty Line (Shift + Enter)',
    icon: <NewLine />
};

export const CLEAR_BUTTON = {
    label: 'Clear Styles',
    icon: <Rubber />
};

export const HYSTORY_BUTTONS = {
    undo: {
        label: 'Undo (Ctrl + Z)',
        icon: <Undo />
    },
    redo: {
        label: 'Redo (Ctrl + Y)',
        icon: <Redo />
    }
};
