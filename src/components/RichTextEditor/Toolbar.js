import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './RichTextEditor.css';
import noop from '../../utils/noop';
import { hasSelection, isCursorOnLink } from './draftUtils';
import InlineToolbar from './InlineToolbar';
import BlockToolbar from './BlockToolbar';
import PopupButton from './PopupButton';
import IconButton from './IconButton';
import {
    INLINE_BUTTONS,
    BLOCK_BUTTONS,
    LINK_BUTTONS,
    CLEAR_BUTTON,
    EMPTY_LINE_BUTTON,
    HYSTORY_BUTTONS
} from './buttons';

class Toolbar extends Component {
    renderLinkButtons() {
        const { editorState, focusEditor, setLink, removeLink } = this.props;
        const linkEntity = isCursorOnLink(editorState);
        const url = linkEntity ? linkEntity.getData().url : '';
        const shouldShowLinkButton = hasSelection(editorState) || linkEntity;

        return (
            <span>
                <PopupButton
                    {...LINK_BUTTONS.link}
                    url={url}
                    disabled={!shouldShowLinkButton}
                    focusEditor={focusEditor}
                    onSubmit={setLink}
                />
                <IconButton
                    {...LINK_BUTTONS.unlink}
                    disabled={!linkEntity}
                    onMouseDown={removeLink}
                />
            </span>
        );
    }

    renderUndoRedo() {
        const { editorState, onUndo, onRedo } = this.props;
        const canUndo = editorState.getUndoStack().size !== 0;
        const canRedo = editorState.getRedoStack().size !== 0;

        return (
            <span className={styles.historyButtons}>
                <IconButton
                    {...HYSTORY_BUTTONS.undo}
                    disabled={!canUndo}
                    onMouseDown={onUndo}
                />
                <IconButton
                    {...HYSTORY_BUTTONS.redo}
                    disabled={!canRedo}
                    onMouseDown={onRedo}
                />
            </span>
        );
    }

    render() {
        const {
            editorState,
            inlineButtons,
            blockButtons,
            toggleInlineStyle,
            toggleBlockType,
            insertEmptyLine,
            removeStyles
        } = this.props;

        return (
            <div className={styles.toolbar}>
                <InlineToolbar
                    editorState={editorState}
                    buttons={inlineButtons}
                    onToggle={toggleInlineStyle}
                />
                <IconButton
                    {...CLEAR_BUTTON}
                    disabled={!hasSelection(editorState)}
                    onMouseDown={removeStyles}
                />
                {this.renderLinkButtons()}
                <BlockToolbar
                    editorState={editorState}
                    buttons={blockButtons}
                    onToggle={toggleBlockType}
                />
                <IconButton
                    {...EMPTY_LINE_BUTTON}
                    onMouseDown={insertEmptyLine}
                />
                {this.renderUndoRedo()}
            </div>
        );
    }
}

Toolbar.propTypes = {
    editorState: PropTypes.object.isRequired,
    inlineButtons: PropTypes.arrayOf(PropTypes.object),
    blockButtons: PropTypes.arrayOf(PropTypes.object),
    focusEditor: PropTypes.func,
    toggleInlineStyle: PropTypes.func,
    toggleBlockType: PropTypes.func,
    insertEmptyLine: PropTypes.func,
    setLink: PropTypes.func,
    removeLink: PropTypes.func,
    onUndo: PropTypes.func,
    onRedo: PropTypes.func,
    removeStyles: PropTypes.func
};

Toolbar.defaultProps = {
    inlineButtons: INLINE_BUTTONS,
    blockButtons: BLOCK_BUTTONS,
    focusEditor: noop,
    toggleInlineStyle: noop,
    toggleBlockType: noop,
    insertEmptyLine: noop,
    setLink: noop,
    removeLink: noop,
    onUndo: noop,
    onRedo: noop,
    removeStyles: noop
};

export default Toolbar;
