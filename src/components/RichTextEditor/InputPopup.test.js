import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import InputPopup from './InputPopup';

describe('<InputPopup />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<InputPopup />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('saves the provided initial value', () => {
        const mockInputValue = 'initialValue';
        const wrapper = shallow(<InputPopup value={mockInputValue} />);

        expect(wrapper.state('urlInputValue')).toBe(mockInputValue);
    });

    describe('onChange', () => {
        test('updates the input value on the state', () => {
            const mockEvent = {
                target: {
                    value: 'Input url'
                }
            };
            const wrapper = shallow(<InputPopup />);

            wrapper.find('input').simulate('change', mockEvent);

            expect(wrapper.state('urlInputValue')).toBe(mockEvent.target.value);
        });
    });

    describe('onSubmit', () => {
        test('fires the onSubmit callback', () => {
            const mockInputValue = 'inputValue';
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(<InputPopup onSubmit={handleOnSubmit} />);

            wrapper.setState({ urlInputValue: mockInputValue });
            wrapper.instance().handleSubmit();

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(mockInputValue);
        });
    });

    describe('onKeydown', () => {
        test('fires the onSubmit callback when Enter is pressed', () => {
            const mockKeydownEvent = {
                key: 'Enter'
            };
            const mockInputValue = 'inputValue';
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(<InputPopup onSubmit={handleOnSubmit} />);

            wrapper.setState({ urlInputValue: mockInputValue });
            wrapper.find('input').simulate('keydown', mockKeydownEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(mockInputValue);
        });

        test('fires the onCancel callback when Escape is pressed', () => {
            const mockKeydownEvent = {
                key: 'Escape'
            };
            const handleOnCancel = jest.fn();
            const wrapper = shallow(<InputPopup onCancel={handleOnCancel} />);

            wrapper.find('input').simulate('keydown', mockKeydownEvent);

            expect(handleOnCancel.mock.calls.length).toBe(1);
        });
    });
});
