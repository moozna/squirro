import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EditorState } from 'draft-js';
import InlineToolbar from './InlineToolbar';
import { INLINE_BUTTONS } from './buttons';

describe('<InlineToolbar />', () => {
    const mockEditorState = EditorState.createEmpty();

    test('has the expected html structure', () => {
        const wrapper = shallow(
            <InlineToolbar editorState={mockEditorState} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when buttons are specified', () => {
        const wrapper = shallow(
            <InlineToolbar
                buttons={INLINE_BUTTONS}
                editorState={mockEditorState}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
