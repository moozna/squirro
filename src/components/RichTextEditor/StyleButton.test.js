import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import StyleButton from './StyleButton';

describe('<StyleButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<StyleButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onToggle()', () => {
        test('fires the onToggle callback', () => {
            const handleOnToggle = jest.fn();
            const wrapper = shallow(<StyleButton onToggle={handleOnToggle} />);

            wrapper.instance().handleToggle();
            expect(handleOnToggle.mock.calls.length).toBe(1);
        });
    });
});
