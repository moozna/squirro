import React from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import Tooltip from '../Tooltip/Tooltip';
import Button from '../Button/Button';

const IconButton = ({
    buttonClass,
    label,
    icon,
    active,
    disabled,
    onClick,
    onMouseDown
}) => (
    <Tooltip label={label}>
        <Button
            className={buttonClass}
            active={active}
            disabled={disabled}
            onClick={onClick}
            onMouseDown={onMouseDown}
            color="transparent"
            shape="square"
        >
            {icon || label}
        </Button>
    </Tooltip>
);

IconButton.propTypes = {
    buttonClass: PropTypes.string,
    label: PropTypes.string,
    icon: PropTypes.node,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    onMouseDown: PropTypes.func
};

IconButton.defaultProps = {
    buttonClass: '',
    label: '',
    icon: null,
    active: false,
    disabled: false,
    onClick: noop,
    onMouseDown: noop
};

export default IconButton;
