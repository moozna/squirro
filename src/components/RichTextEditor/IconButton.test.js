import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import IconButton from './IconButton';

describe('<IconButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<IconButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an icon', () => {
        const wrapper = shallow(<IconButton icon={<div />} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a label, when icon is not specified', () => {
        const wrapper = shallow(<IconButton label="label" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
