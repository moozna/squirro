import {
    EditorState,
    ContentState,
    convertToRaw,
    convertFromHTML,
    CompositeDecorator,
    CharacterMetadata,
    getDefaultKeyBinding
} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import Link, { findLinkEntities } from './entities/Link';
import styles from './RichTextEditor.css';

const decorator = new CompositeDecorator([
    {
        strategy: findLinkEntities,
        component: Link
    }
]);

export const createEditorState = (value = '') => {
    const blocksFromHTML = convertFromHTML(value);

    if (!blocksFromHTML.contentBlocks || !value) {
        return EditorState.createEmpty(decorator);
    }

    const content = ContentState.createFromBlockArray(
        blocksFromHTML.contentBlocks,
        blocksFromHTML.entityMap
    );

    return EditorState.createWithContent(content, decorator);
};

export const stateToHTML = editorState => {
    const rawContentState = convertToRaw(editorState.getCurrentContent());
    return draftToHtml(rawContentState);
};

const getEntityAtOffset = (block, offset) => {
    const entityKey = block.getEntityAt(offset);
    if (entityKey == null) {
        return null;
    }

    let startOffset = offset;

    while (
        startOffset > 0 &&
        block.getEntityAt(startOffset - 1) === entityKey
    ) {
        startOffset -= 1;
    }

    let endOffset = startOffset;
    const blockLength = block.getLength();

    while (
        endOffset < blockLength &&
        block.getEntityAt(endOffset + 1) === entityKey
    ) {
        endOffset += 1;
    }

    return {
        entityKey,
        blockKey: block.getKey(),
        startOffset,
        endOffset: endOffset + 1
    };
};

export const getEntityAtCursor = editorState => {
    const selection = editorState.getSelection();
    const startKey = selection.getStartKey();
    const startBlock = editorState.getCurrentContent().getBlockForKey(startKey);
    const startOffset = selection.getStartOffset();

    if (selection.isCollapsed()) {
        return getEntityAtOffset(
            startBlock,
            startOffset === 0 ? startOffset : startOffset - 1
        );
    }

    if (startKey !== selection.getEndKey()) {
        return null;
    }

    const endOffset = selection.getEndOffset();
    const startEntityKey = startBlock.getEntityAt(startOffset);

    for (let i = startOffset; i < endOffset; i++) {
        const entityKey = startBlock.getEntityAt(i);
        if (entityKey == null || entityKey !== startEntityKey) {
            return null;
        }
    }

    return {
        entityKey: startEntityKey,
        blockKey: startBlock.getKey(),
        startOffset,
        endOffset
    };
};

export const hasSelection = editorState => {
    const selection = editorState.getSelection();
    return !selection.isCollapsed();
};

export const isCursorOnLink = editorState => {
    const contentState = editorState.getCurrentContent();
    const entity = getEntityAtCursor(editorState);
    return entity == null ? null : contentState.getEntity(entity.entityKey);
};

export const clearEntityForRange = (
    editorState,
    blockKey,
    startOffset,
    endOffset
) => {
    const contentState = editorState.getCurrentContent();
    const blockMap = contentState.getBlockMap();
    const block = blockMap.get(blockKey);
    const charList = block.getCharacterList();
    const newCharList = charList.map((char, i) => {
        if (i >= startOffset && i < endOffset) {
            return CharacterMetadata.applyEntity(char, null);
        }
        return char;
    });
    const newBlock = block.set('characterList', newCharList);
    const newBlockMap = blockMap.set(blockKey, newBlock);
    const newContentState = contentState.set('blockMap', newBlockMap);
    return EditorState.push(editorState, newContentState, 'apply-entity');
};

export const blockStyleFn = contentBlock => {
    const type = contentBlock.getType();

    if (type === 'unstyled') {
        return styles.paragraph;
    }
    return null;
};

export const keyBindingFn = event => {
    if (event.shiftKey && event.key === 'Enter') {
        return 'insert-empty-line';
    }
    return getDefaultKeyBinding(event);
};
