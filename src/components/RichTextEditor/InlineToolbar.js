import React from 'react';
import PropTypes from 'prop-types';
import styles from './RichTextEditor.css';
import StyleButton from './StyleButton';
import noop from '../../utils/noop';

const InlineToolbar = ({ buttons, editorState, onToggle }) => {
    if (buttons.length < 1) {
        return null;
    }

    const currentStyle = editorState.getCurrentInlineStyle();

    return (
        <div className={styles.controls}>
            {buttons.map(type => (
                <StyleButton
                    key={type.label}
                    label={type.label}
                    icon={type.icon}
                    active={currentStyle.has(type.style)}
                    onToggle={onToggle}
                    style={type.style}
                />
            ))}
        </div>
    );
};

InlineToolbar.propTypes = {
    buttons: PropTypes.arrayOf(PropTypes.object),
    editorState: PropTypes.object,
    onToggle: PropTypes.func
};

InlineToolbar.defaultProps = {
    buttons: [],
    editorState: {},
    onToggle: noop
};

export default InlineToolbar;
