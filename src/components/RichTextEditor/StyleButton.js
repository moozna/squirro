import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import IconButton from './IconButton';

class StyleButton extends Component {
    handleToggle = () => {
        const { onToggle, style } = this.props;
        onToggle(style);
    };

    render() {
        const { label, icon, active, disabled } = this.props;
        return (
            <IconButton
                label={label}
                icon={icon}
                active={active}
                disabled={disabled}
                onMouseDown={this.handleToggle}
            />
        );
    }
}

StyleButton.propTypes = {
    style: PropTypes.string,
    label: PropTypes.string,
    icon: PropTypes.node,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    onToggle: PropTypes.func
};

StyleButton.defaultProps = {
    style: '',
    label: '',
    icon: null,
    active: false,
    disabled: false,
    onToggle: noop
};

export default StyleButton;
