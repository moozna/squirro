import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import FileInput from './FileInput';

describe('<FileInput />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<FileInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <FileInput name="fileInput" accept=".png" multiple />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled file input', () => {
        const wrapper = shallow(<FileInput disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
