import React from 'react';
import PropTypes from 'prop-types';
import styles from './DropZone.css';
import noop from '../../utils/noop';

const FileInput = ({
    saveRef,
    name,
    multiple,
    accept,
    disabled,
    onChange,
    onFocus,
    onBlur
}) => (
    <input
        ref={saveRef}
        type="file"
        name={name}
        className={styles.fileInput}
        multiple={multiple}
        accept={accept}
        disabled={!!disabled}
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        autoComplete="off"
    />
);

FileInput.propTypes = {
    saveRef: PropTypes.func,
    name: PropTypes.string,
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func
};

FileInput.defaultProps = {
    saveRef: noop,
    name: '',
    multiple: false,
    accept: '.jpg, .jpeg, .png',
    disabled: false,
    onChange: noop,
    onFocus: noop,
    onBlur: noop
};

export default FileInput;
