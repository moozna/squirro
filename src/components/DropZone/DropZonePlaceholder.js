import React from 'react';
import styles from './DropZone.css';
import ImageIcon from '../svg/ImageIcon';

const DropZonePlaceholder = () => (
    <div className={styles.placeholder}>
        <div>
            <ImageIcon />
            <p>Choose a file or drag it here</p>
        </div>
    </div>
);

export default DropZonePlaceholder;
