import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DropZonePlaceholder from './DropZonePlaceholder';

describe('<DropZonePlaceholder />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DropZonePlaceholder />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
