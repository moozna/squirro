import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import DropZone from './DropZone';

describe('<DropZone />', () => {
    window.URL.createObjectURL = jest.fn().mockReturnValue('url');

    test('has the expected html structure', () => {
        const wrapper = shallow(<DropZone />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<DropZone>{testChildren}</DropZone>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a focused DropZone', () => {
        const wrapper = shallow(<DropZone />);
        wrapper.instance().handleFileInputFocus();

        expect(wrapper.hasClass('focused')).toBe(true);
    });

    test('renders an active DropZone', () => {
        const mockDragEnterEvent = {
            preventDefault: jest.fn()
        };

        const wrapper = shallow(<DropZone />);
        wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

        expect(wrapper.hasClass('active')).toBe(true);
    });

    test('renders a rejected DropZone when files are rejected', () => {
        const mockFiles = [
            { name: 'cover1.jpg', type: 'image/jpeg' },
            { name: 'cover2.svg', type: 'image/svg+xml' }
        ];
        const mockDragEnterEvent = {
            preventDefault: jest.fn(),
            dataTransfer: {
                items: mockFiles
            }
        };

        const wrapper = shallow(<DropZone />);
        wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

        expect(wrapper.hasClass('rejected')).toBe(true);
    });

    test('renders a rejected DropZone when multiple is disabled and more files are dragged', () => {
        const mockFiles = [
            { name: 'cover1.jpg', type: 'image/jpeg' },
            { name: 'cover2.png', type: 'image/png' }
        ];
        const mockDragEnterEvent = {
            preventDefault: jest.fn(),
            dataTransfer: {
                items: mockFiles
            }
        };

        const wrapper = shallow(<DropZone multiple={false} />);
        wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

        expect(wrapper.hasClass('rejected')).toBe(true);
    });

    test('renders a disabled DropZone', () => {
        const wrapper = shallow(<DropZone disabled />);

        expect(wrapper.hasClass('disabled')).toBe(true);
    });

    test('has a ref on the root node', () => {
        const wrapper = mount(<DropZone />);

        expect(wrapper.instance().node).toBeDefined();
    });

    test('has a ref on the file input', () => {
        const wrapper = mount(<DropZone />);

        expect(wrapper.instance().fileInput).toBeDefined();
    });

    describe('handleDragEnter()', () => {
        test('stops dragenter event', () => {
            const mockDragEnterEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<DropZone />);
            wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

            expect(mockDragEnterEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('saves dragTarget', () => {
            const currentTarget = {};
            const mockDragEnterEvent = {
                preventDefault: jest.fn(),
                target: currentTarget
            };

            const wrapper = shallow(<DropZone />);
            wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

            expect(wrapper.instance().dragTargets.includes(currentTarget)).toBe(
                true
            );
        });

        test('does not saves dragTarget if it is already saved', () => {
            const currentTarget = {};
            const mockDragEnterEvent = {
                preventDefault: jest.fn(),
                target: currentTarget
            };

            const wrapper = shallow(<DropZone />);
            wrapper.instance().dragTargets = [currentTarget];
            wrapper.find('.dropZone').simulate('dragenter', mockDragEnterEvent);

            expect(wrapper.instance().dragTargets.length).toBe(1);
        });
    });

    describe('handleDragLeave()', () => {
        test('stops dragleave event', () => {
            const mockDragLeaveEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<DropZone />);
            wrapper.find('.dropZone').simulate('dragleave', mockDragLeaveEvent);

            expect(mockDragLeaveEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('sets isDragActive to false', () => {
            const mockDragLeaveEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = mount(<DropZone />);

            wrapper.find('.dropZone').simulate('dragleave', mockDragLeaveEvent);
            expect(wrapper.state('isDragActive')).toBe(false);
        });

        test('does not sets isDragActive to false when drag is happening over a child element', () => {
            const currentTarget = {};
            const mockDragLeaveEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = mount(<DropZone />);

            wrapper.instance().node.contains = jest.fn().mockReturnValue(true);
            wrapper.setState({ isDragActive: true });
            wrapper.instance().dragTargets = [currentTarget];

            wrapper.find('.dropZone').simulate('dragleave', mockDragLeaveEvent);
            expect(wrapper.state('isDragActive')).toBe(true);
        });
    });

    describe('handleDrop()', () => {
        test('stops drop event', () => {
            const mockDropEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<DropZone />);
            wrapper.find('.dropZone').simulate('drop', mockDropEvent);

            expect(mockDropEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('fires the onChange callback', () => {
            const mockChangeEvent = {
                preventDefault: jest.fn()
            };
            const handleOnChange = jest.fn();

            const wrapper = shallow(<DropZone onChange={handleOnChange} />);
            wrapper.find('.dropZone').simulate('drop', mockChangeEvent);

            expect(handleOnChange.mock.calls.length).toBe(1);
        });

        test('calls onChange callback with the correct values', () => {
            const mockFiles = [
                { name: 'cover1.jpg', type: 'image/jpeg' },
                { name: 'cover2.svg', type: 'image/svg+xml' }
            ];

            const mockChangeEvent = {
                preventDefault: jest.fn(),
                dataTransfer: {
                    items: mockFiles
                }
            };
            const handleOnChange = jest.fn();

            const wrapper = shallow(<DropZone onChange={handleOnChange} />);
            wrapper.find('.dropZone').simulate('drop', mockChangeEvent);

            const expectedAcceptedFiles = [
                { name: 'cover1.jpg', type: 'image/jpeg', url: 'url' }
            ];

            const expectedRejectedFiles = [
                {
                    name: 'cover2.svg',
                    type: 'image/svg+xml',
                    url: 'url'
                }
            ];

            expect(handleOnChange.mock.calls[0][0]).toEqual(
                expectedAcceptedFiles
            );
            expect(handleOnChange.mock.calls[0][1]).toEqual(
                expectedRejectedFiles
            );
        });

        test('accepts only one file when multiple is false', () => {
            const mockFiles = [
                { name: 'cover1.jpg', type: 'image/jpeg' },
                { name: 'cover2.png', type: 'image/png' }
            ];

            const mockChangeEvent = {
                preventDefault: jest.fn(),
                dataTransfer: {
                    items: mockFiles
                }
            };
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <DropZone multiple={false} onChange={handleOnChange} />
            );
            wrapper.find('.dropZone').simulate('drop', mockChangeEvent);

            const expectedAcceptedFiles = [
                { name: 'cover1.jpg', type: 'image/jpeg', url: 'url' }
            ];

            const expectedRejectedFiles = [
                { name: 'cover2.png', type: 'image/png', url: 'url' }
            ];

            expect(handleOnChange.mock.calls[0][0]).toEqual(
                expectedAcceptedFiles
            );
            expect(handleOnChange.mock.calls[0][1]).toEqual(
                expectedRejectedFiles
            );
        });
    });

    describe('handleDocumentDrop()', () => {
        test('stops document drop event', () => {
            const mockDropEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = shallow(<DropZone />);
            wrapper.instance().handleDocumentDrop(mockDropEvent);

            expect(mockDropEvent.preventDefault.mock.calls.length).toBe(1);
        });

        test('does not stop document drop event when it happens on a children', () => {
            const mockDropEvent = {
                preventDefault: jest.fn()
            };

            const wrapper = mount(<DropZone />);
            wrapper.instance().node.contains = jest.fn().mockReturnValue(true);
            wrapper.instance().handleDocumentDrop(mockDropEvent);

            expect(mockDropEvent.preventDefault.mock.calls.length).toBe(0);
        });
    });

    describe('handleFileInputClick()', () => {
        test('calls click method of the input', () => {
            const wrapper = mount(<DropZone />);
            const mockInputClick = jest.spyOn(
                wrapper.instance().fileInput,
                'click'
            );
            wrapper.find('.dropZone').simulate('click');

            expect(mockInputClick.mock.calls.length).toBe(1);
        });
    });

    describe('tracks focused state', () => {
        test('sets focused to true on focus', () => {
            const wrapper = shallow(<DropZone />);

            wrapper.instance().handleFileInputFocus();
            expect(wrapper.state('focused')).toBe(true);
        });

        test('sets focused to false on blur', () => {
            const wrapper = shallow(<DropZone />);
            wrapper.setState({ focused: true });

            wrapper.instance().handleFileInputBlur();
            expect(wrapper.state('focused')).toBe(false);
        });
    });

    describe('event listeners', () => {
        let mockAddEventListener;
        let mockRemoveEventListener;

        beforeEach(() => {
            mockAddEventListener = jest.spyOn(document, 'addEventListener');
            mockRemoveEventListener = jest.spyOn(
                document,
                'removeEventListener'
            );
        });

        afterEach(() => {
            mockAddEventListener.mockRestore();
            mockRemoveEventListener.mockRestore();
        });

        test('attaches event listeners', () => {
            shallow(<DropZone />);

            expect(mockAddEventListener.mock.calls.length).toBe(2);
            expect(mockAddEventListener.mock.calls[0][0]).toBe('dragover');
            expect(mockAddEventListener.mock.calls[1][0]).toBe('drop');
        });

        test('detaches event listeners after unmount', () => {
            const wrapper = shallow(<DropZone />);
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(2);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('dragover');
            expect(mockRemoveEventListener.mock.calls[1][0]).toBe('drop');
        });
    });
});
