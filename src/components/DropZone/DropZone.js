import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './DropZone.css';
import noop from '../../utils/noop';
import {
    getDataTransferItems,
    handleDocumentDragOver,
    isFileAccepted,
    isAllFileAccepted,
    isFileMatchSize
} from './fileInputUtils';
import FileInput from './FileInput';
import DropZonePlaceHolder from './DropZonePlaceholder';

class DropZone extends Component {
    state = {
        focused: false,
        isDragActive: false,
        draggedFiles: []
    };

    componentDidMount() {
        this.dragTargets = [];
        document.addEventListener('dragover', handleDocumentDragOver, false);
        document.addEventListener('drop', this.handleDocumentDrop, false);
    }

    componentWillUnmount() {
        document.removeEventListener('dragover', handleDocumentDragOver);
        document.removeEventListener('drop', this.handleDocumentDrop);
    }

    handleDragEnter = event => {
        event.preventDefault();

        if (this.dragTargets.indexOf(event.target) === -1) {
            this.dragTargets.push(event.target);
        }

        this.setState({
            isDragActive: true,
            draggedFiles: getDataTransferItems(event)
        });
    };

    handleDragLeave = event => {
        event.preventDefault();

        this.dragTargets = this.dragTargets.filter(
            elem => elem !== event.target && this.node.contains(elem)
        );

        if (this.dragTargets.length > 0) {
            return;
        }

        this.setState({
            isDragActive: false,
            draggedFiles: []
        });
    };

    handleDrop = event => {
        const { accept, multiple, maxSize, minSize, onChange } = this.props;
        const fileList = getDataTransferItems(event);
        const acceptedFiles = [];
        const rejectedFiles = [];

        event.preventDefault();

        fileList.forEach(file => {
            const currentFile = file;
            currentFile.url = window.URL.createObjectURL(currentFile);

            if (
                isFileAccepted(currentFile, accept) &&
                isFileMatchSize(currentFile, minSize, maxSize)
            ) {
                acceptedFiles.push(currentFile);
            } else {
                rejectedFiles.push(currentFile);
            }
        });

        if (!multiple) {
            rejectedFiles.push(...acceptedFiles.splice(1));
        }

        onChange(acceptedFiles, rejectedFiles);
        this.draggedFiles = null;

        this.setState({
            isDragActive: false,
            draggedFiles: []
        });
    };

    handleFileInputFocus = () => {
        this.setState({ focused: true });
    };

    handleFileInputBlur = () => {
        this.setState({ focused: false });
    };

    handleFileInputClick = () => {
        this.fileInput.value = null;
        this.fileInput.click();
    };

    saveRef = node => {
        this.node = node;
    };

    saveInputRef = node => {
        this.fileInput = node;
    };

    handleDocumentDrop(event) {
        if (this.node && this.node.contains(event.target)) {
            return;
        }

        event.preventDefault();
        this.dragTargets = [];
    }

    render() {
        const { children, name, accept, multiple, disabled } = this.props;
        const { focused, isDragActive, draggedFiles } = this.state;

        const fileCount = draggedFiles.length;
        const isDragAccepted =
            fileCount > 0 && isAllFileAccepted(draggedFiles, accept);
        const isDragRejected = fileCount > 0 && (!isDragAccepted || !multiple);
        const showPlaceholder = !children;

        const classes = classnames(styles.dropZone, {
            [styles.focused]: focused,
            [styles.active]: isDragActive,
            [styles.filled]: !showPlaceholder,
            [styles.rejected]: isDragRejected,
            [styles.disabled]: disabled
        });

        return (
            <div
                className={classes}
                ref={this.saveRef}
                onDragEnter={this.handleDragEnter}
                onDragLeave={this.handleDragLeave}
                onDrop={this.handleDrop}
                onClick={this.handleFileInputClick}
            >
                <FileInput
                    name={name}
                    accept={accept}
                    multiple={multiple}
                    saveRef={this.saveInputRef}
                    onChange={this.handleDrop}
                    onFocus={this.handleFileInputFocus}
                    onBlur={this.handleFileInputBlur}
                />
                {showPlaceholder ? <DropZonePlaceHolder /> : children}
            </div>
        );
    }
}

DropZone.propTypes = {
    children: PropTypes.node,
    name: PropTypes.string,
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    minSize: PropTypes.number,
    maxSize: PropTypes.number,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
};

DropZone.defaultProps = {
    children: null,
    name: '',
    multiple: true,
    accept: 'image/jpg, image/jpeg, image/png',
    minSize: null,
    maxSize: null,
    disabled: false,
    onChange: noop
};

export default DropZone;
