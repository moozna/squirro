import * as fileInputUtils from './fileInputUtils';

describe('fileInputUtils', () => {
    describe('getDataTransferItems()', () => {
        test('returns an empty list when event does not contain files', () => {
            const mockEvent = {};

            expect(fileInputUtils.getDataTransferItems(mockEvent)).toEqual([]);
        });

        test('returns files from the files property of the dataTransfer object', () => {
            const mockFiles = [{ name: 'cover1.jpg' }, { name: 'cover2.png' }];
            const mockEvent = {
                dataTransfer: {
                    files: mockFiles
                }
            };

            expect(fileInputUtils.getDataTransferItems(mockEvent)).toEqual(
                mockFiles
            );
        });

        test('returns files from the items property of the dataTransfer object', () => {
            const mockFiles = [{ name: 'cover1.jpg' }, { name: 'cover2.png' }];
            const mockEvent = {
                dataTransfer: {
                    files: [],
                    items: mockFiles
                }
            };

            expect(fileInputUtils.getDataTransferItems(mockEvent)).toEqual(
                mockFiles
            );
        });

        test('returns files from the files property of the target object', () => {
            const mockFiles = [{ name: 'cover1.jpg' }, { name: 'cover2.png' }];
            const mockEvent = {
                dataTransfer: {},
                target: {
                    files: mockFiles
                }
            };

            expect(fileInputUtils.getDataTransferItems(mockEvent)).toEqual(
                mockFiles
            );
        });
    });

    describe('isFileAccepted()', () => {
        test('returns true when arguments are not specified', () => {
            expect(fileInputUtils.isFileAccepted()).toBe(true);
        });

        test('returns true when file type informations are missing', () => {
            const acceptedFiles = 'image/jpg, image/jpeg, image/png';
            const mockFile = {};

            expect(fileInputUtils.isFileAccepted(mockFile, acceptedFiles)).toBe(
                false
            );
        });

        test('returns true when accepted files contains the type of the file', () => {
            const acceptedFiles = 'image/jpg, image/jpeg, image/png';
            const mockFile = {
                name: 'cover1.jpg',
                type: 'image/jpeg'
            };

            expect(fileInputUtils.isFileAccepted(mockFile, acceptedFiles)).toBe(
                true
            );
        });

        test('returns false when accepted files contains the type of the file', () => {
            const acceptedFiles = 'image/jpg, image/jpeg, image/png';
            const mockFile = {
                name: 'cover1.svg',
                type: 'image/svg+xml'
            };

            expect(fileInputUtils.isFileAccepted(mockFile, acceptedFiles)).toBe(
                false
            );
        });

        describe('acceptedFiles', () => {
            test('can be specified as a string of mime types', () => {
                const acceptedFiles = 'image/jpg, image/jpeg, image/png';
                const mockFile = {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                };

                expect(
                    fileInputUtils.isFileAccepted(mockFile, acceptedFiles)
                ).toBe(true);
            });

            test('can be specified as an array of mime types', () => {
                const acceptedFiles = ['image/jpg', 'image/jpeg', 'image/png'];
                const mockFile = {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                };

                expect(
                    fileInputUtils.isFileAccepted(mockFile, acceptedFiles)
                ).toBe(true);
            });

            test('can be specified as a string of file extensions', () => {
                const acceptedFiles = '.jpg, .jpeg, .png';
                const mockFile = {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                };

                expect(
                    fileInputUtils.isFileAccepted(mockFile, acceptedFiles)
                ).toBe(true);
            });

            test('can be specified as an array of file extensions', () => {
                const acceptedFiles = ['.jpg', '.jpeg', '.png'];
                const mockFile = {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                };

                expect(
                    fileInputUtils.isFileAccepted(mockFile, acceptedFiles)
                ).toBe(true);
            });

            test('mime types can be specified with wildcard', () => {
                const acceptedFiles = ['image/*'];
                const mockFile = {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                };

                expect(
                    fileInputUtils.isFileAccepted(mockFile, acceptedFiles)
                ).toBe(true);
            });
        });
    });

    describe('isAllFileAccepted()', () => {
        test('returns true when file type is application/x-moz-file', () => {
            const acceptedFiles = 'image/jpg, image/jpeg, image/png';
            const mockFiles = [
                {
                    name: 'cover1.jpg',
                    type: 'image/jpeg'
                },
                {
                    name: 'cover2.png',
                    type: 'image/png'
                }
            ];

            expect(
                fileInputUtils.isAllFileAccepted(mockFiles, acceptedFiles)
            ).toBe(true);
        });

        test('returns true when file type is application/x-moz-file', () => {
            const acceptedFiles = 'image/jpg, image/jpeg, image/png';
            const mockFiles = [
                {
                    name: 'cover1.svg',
                    type: 'image/svg+xml'
                },
                {
                    name: 'cover2.png',
                    type: 'image/png'
                }
            ];

            expect(
                fileInputUtils.isAllFileAccepted(mockFiles, acceptedFiles)
            ).toBe(false);
        });
    });

    describe('isFileMatchSize()', () => {
        test('returns true when size limits are not specified', () => {
            const mockFile = {
                size: 0
            };

            expect(fileInputUtils.isFileMatchSize(mockFile)).toBe(true);
        });

        test('returns true when file size is larger than minSize', () => {
            const mockFile = {
                size: 10
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, 5)).toBe(true);
        });

        test('returns true when file size is equal with minSize', () => {
            const mockFile = {
                size: 5
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, 5)).toBe(true);
        });

        test('returns false when file size is smaller than minSize', () => {
            const mockFile = {
                size: 5
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, 10)).toBe(false);
        });

        test('returns true when file size is smaller than maxSize', () => {
            const mockFile = {
                size: 10
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, null, 20)).toBe(
                true
            );
        });

        test('returns true when file size is equal with maxSize', () => {
            const mockFile = {
                size: 20
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, null, 20)).toBe(
                true
            );
        });

        test('returns false when file size is larger than maxSize', () => {
            const mockFile = {
                size: 15
            };

            expect(fileInputUtils.isFileMatchSize(mockFile, null, 10)).toBe(
                false
            );
        });
    });

    describe('handleDocumentDragOver()', () => {
        test('calls preventDefault', () => {
            const mockEvent = {
                preventDefault: jest.fn()
            };

            fileInputUtils.handleDocumentDragOver(mockEvent);

            expect(mockEvent.preventDefault.mock.calls.length).toBe(1);
        });
    });
});
