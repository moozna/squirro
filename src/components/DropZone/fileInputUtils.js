export function getDataTransferItems(event) {
    let dataTransferItemsList = [];
    const { dataTransfer } = event;

    if (dataTransfer) {
        if (dataTransfer.files && dataTransfer.files.length) {
            dataTransferItemsList = dataTransfer.files;
        } else if (dataTransfer.items && dataTransfer.items.length) {
            dataTransferItemsList = dataTransfer.items;
        }
    }

    if (event.target && event.target.files) {
        dataTransferItemsList = event.target.files;
    }

    return [...dataTransferItemsList];
}

export function isFileAccepted(file, acceptedFiles) {
    if (file && acceptedFiles) {
        const acceptedFilesArray = Array.isArray(acceptedFiles)
            ? acceptedFiles
            : acceptedFiles.split(',');
        const fileName = file.name || '';
        const mimeType = file.type || '';
        const baseMimeType = mimeType.replace(/\/.*$/, '');

        return acceptedFilesArray.some(type => {
            const validType = type.trim();

            if (validType.charAt(0) === '.') {
                return fileName.toLowerCase().endsWith(validType.toLowerCase());
            }

            if (/\/\*$/.test(validType)) {
                // image/* mime type
                return baseMimeType === validType.replace(/\/.*$/, '');
            }

            return mimeType === validType;
        });
    }

    return true;
}

export function isAllFileAccepted(files, accept) {
    return files.every(file => isFileAccepted(file, accept));
}

export function isFileMatchSize(file, minSize, maxSize) {
    const minValid = minSize ? file.size >= minSize : true;
    const maxValid = maxSize ? file.size <= maxSize : true;

    return minValid && maxValid;
}

export function handleDocumentDragOver(event) {
    event.preventDefault();
}
