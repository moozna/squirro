import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ImportEditForm.css';
import { goodreadsDataType, amazonDataType } from '../../types/types';
import Tabs from '../Tabs/Tabs';
import Tab from '../Tabs/Tab';
import ImportedData from '../ImportedData/ImportedData';
import GoodreadsLogo from '../svg/GoodreadsLogo';
import AmazonLogo from '../svg/AmazonLogo';

class DataTabs extends Component {
    state = {
        activeTabIndex: 1
    };

    handleTabChange = index => {
        this.setState({ activeTabIndex: index });
    };

    render() {
        const { goodreadsData, amazonData, duplicates } = this.props;

        return (
            <Tabs
                activeTab={this.state.activeTabIndex}
                onTabChange={this.handleTabChange}
                headerClass={styles.tabsHeader}
            >
                <Tab label={<GoodreadsLogo />}>
                    <ImportedData
                        goodreadsData={goodreadsData}
                        duplicates={duplicates}
                        columns={1}
                    />
                </Tab>
                <Tab label={<AmazonLogo />}>
                    <ImportedData
                        amazonData={amazonData}
                        duplicates={duplicates}
                        columns={1}
                    />
                </Tab>
            </Tabs>
        );
    }
}

DataTabs.propTypes = {
    goodreadsData: goodreadsDataType,
    amazonData: amazonDataType,
    duplicates: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    })
};

DataTabs.defaultProps = {
    goodreadsData: null,
    amazonData: null,
    duplicates: {}
};

export default DataTabs;
