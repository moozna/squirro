import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './ImportEditForm.css';
import {
    valuesType,
    touchedType,
    errorsType,
    suggestionGroupType
} from '../../types/types';
import noop from '../../utils/noop';
import withForm from '../Form/Form';
import Tabs from '../Tabs/Tabs';
import Tab from '../Tabs/Tab';
import CardHeaderFields from '../DetailsForm/CardHeaderFields';
import TagSectionFields from '../DetailsForm/TagSectionFields';
import StarRatingInput from '../DetailsForm/StarRatingInput';
import InterestLevelInput from '../DetailsForm/InterestLevelInput';
import BooksitesRatingFields from '../DetailsForm/BooksitesRatingFields';
import InfoGroupFields from '../DetailsForm/InfoGroupFields';
import HtmlInput from '../HtmlInput/HtmlInput';
import ShelfTagFields from '../DetailsForm/ShelfTagFields';
import ImageUpload from '../ImageUpload/ImageUpload';
import FormNavigation from '../DetailsForm/FormNavigation';

export class ImportEditForm extends Component {
    state = {
        activeTabIndex: 0
    };

    handleTabChange = index => {
        this.setState({ activeTabIndex: index });
    };

    render() {
        const {
            values,
            touched,
            errors,
            suggestions,
            isFormValid,
            onChange,
            onSubmit,
            onCancel,
            onBlur,
            uploadCoverImage,
            saveUploadedFiles,
            saveDeletedFiles
        } = this.props;

        const inputProps = {
            values,
            touched,
            errors,
            onChange,
            onBlur
        };

        return (
            <div>
                <Tabs
                    activeTab={this.state.activeTabIndex}
                    onTabChange={this.handleTabChange}
                    headerClass={styles.tabsHeader}
                >
                    <Tab label="Main">
                        <CardHeaderFields
                            suggestions={suggestions}
                            className={styles.formHeader}
                            {...inputProps}
                        />
                        <TagSectionFields
                            suggestions={suggestions}
                            {...inputProps}
                        />
                        <div className={styles.formImageUpload}>
                            <ImageUpload
                                name="covers"
                                value={values.covers}
                                asin={values.asin}
                                onChange={onChange}
                                uploadCoverImage={uploadCoverImage}
                                saveUploadedFiles={saveUploadedFiles}
                                saveDeletedFiles={saveDeletedFiles}
                            />
                        </div>
                        <div className={styles.formShelfTags}>
                            <ShelfTagFields
                                suggestions={suggestions}
                                {...inputProps}
                            />
                        </div>
                    </Tab>
                    <Tab label="Details">
                        <div className={styles.ratings}>
                            <StarRatingInput
                                name="rating"
                                value={values.rating}
                                onChange={onChange}
                            />
                            <InterestLevelInput
                                name="interestLevel"
                                value={values.interestLevel}
                                onChange={onChange}
                            />
                            <BooksitesRatingFields {...inputProps} />
                        </div>
                        <div className={styles.formInfoGroup}>
                            <InfoGroupFields {...inputProps} />
                        </div>
                        <div className={styles.formDescription}>
                            <HtmlInput
                                name="description"
                                label="Description"
                                value={values.description}
                                onChange={onChange}
                            />
                        </div>
                    </Tab>
                </Tabs>
                <FormNavigation
                    onSubmit={onSubmit}
                    onCancel={onCancel}
                    isSubmittable={isFormValid}
                />
            </div>
        );
    }
}

ImportEditForm.propTypes = {
    values: valuesType,
    touched: touchedType,
    errors: errorsType,
    suggestions: suggestionGroupType,
    isFormValid: PropTypes.bool,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    onBlur: PropTypes.func,
    uploadCoverImage: PropTypes.func,
    saveUploadedFiles: PropTypes.func,
    saveDeletedFiles: PropTypes.func
};

ImportEditForm.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    suggestions: {},
    isFormValid: null,
    onChange: noop,
    onSubmit: noop,
    onCancel: noop,
    onBlur: noop,
    uploadCoverImage: noop,
    saveUploadedFiles: noop,
    saveDeletedFiles: noop
};

export default withForm(ImportEditForm);
