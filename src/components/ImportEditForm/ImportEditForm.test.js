import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImportEditFormWithForm, { ImportEditForm } from './ImportEditForm';

describe('<ImportEditForm />', () => {
    describe('<ImportEditFormWithForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ImportEditFormWithForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<ImportEditForm />', () => {
        test('has the expected html structure by default', () => {
            const wrapper = shallow(<ImportEditForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('sets the first tab to active by default', () => {
            const wrapper = shallow(<ImportEditForm />);

            expect(wrapper.state('activeTabIndex')).toBe(0);
        });

        test('sets the specified tab to active', () => {
            const wrapper = shallow(<ImportEditForm />);

            wrapper.instance().handleTabChange(2);
            expect(wrapper.state('activeTabIndex')).toBe(2);
        });
    });
});
