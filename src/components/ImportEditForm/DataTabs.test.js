import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DataTabs from './DataTabs';

describe('<DataTabs />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DataTabs />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are specified', () => {
        const amazonData = { asin: 'asin', title: 'title' };
        const goodreadsData = { goodreadsId: 12, title: 'title' };
        const duplicates = { title: true };

        const wrapper = shallow(
            <DataTabs
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                duplicates={duplicates}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets the second tab to active by default', () => {
        const wrapper = shallow(<DataTabs />);

        expect(wrapper.state('activeTabIndex')).toBe(1);
    });

    test('sets the specified tab to active', () => {
        const wrapper = shallow(<DataTabs />);

        wrapper.instance().handleTabChange(0);
        expect(wrapper.state('activeTabIndex')).toBe(0);
    });
});
