import React from 'react';
import PropTypes from 'prop-types';

const DisplayNumber = ({ value }) => {
    if (value !== 0 && !value) {
        return null;
    }

    const formattedNumber = value.toLocaleString(navigator.language, {
        minimumFractionDigits: 0,
        maximumFractionDigits: 1
    });

    return <span>{formattedNumber}</span>;
};

DisplayNumber.propTypes = {
    value: PropTypes.number
};

DisplayNumber.defaultProps = {
    value: null
};

export default DisplayNumber;
