import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DisplayNumber from './DisplayNumber';

describe('<DisplayNumber />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<DisplayNumber />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('render zero', () => {
        const wrapper = shallow(<DisplayNumber value={0} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders whole numbers without decimal places', () => {
        const wrapper = shallow(<DisplayNumber value={5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders numbers with one decimal place', () => {
        const wrapper = shallow(<DisplayNumber value={5.2} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render more than one decimal place', () => {
        const wrapper = shallow(<DisplayNumber value={5.8} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('rounds numbers to one decimal place', () => {
        const wrapper = shallow(<DisplayNumber value={5.88} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
