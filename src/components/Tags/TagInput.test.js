import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import TagInput from './TagInput';

describe('<TagInput />', () => {
    const data = ['read', 'want to read', 'whishlist'];

    test('has the expected html structure', () => {
        const wrapper = shallow(<TagInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<TagInput>{testChildren}</TagInput>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has a ref on the input', () => {
        const wrapper = mount(<TagInput />);

        expect(wrapper.instance().input).toBeDefined();
    });

    describe('handleClickFauxInput()', () => {
        test('sets input to focused', () => {
            const mockFocus = jest.fn();
            const wrapper = mount(<TagInput />);
            wrapper.instance().input.focus = mockFocus;

            wrapper.find('.tagInput').simulate('click');

            expect(mockFocus.mock.calls.length).toBe(1);
        });
    });

    describe('addTag()', () => {
        test('saves new tag to the state', () => {
            const tagName = 'on Kindle';
            const wrapper = shallow(<TagInput name="tags" data={data} />);
            const expectedTags = [
                'read',
                'want to read',
                'whishlist',
                'on Kindle'
            ];

            wrapper.instance().addTag(tagName);

            expect(wrapper.instance().tags).toEqual(expectedTags);
        });

        test('does not save duplicate tag to the state', () => {
            const tagName = 'read';
            const wrapper = shallow(<TagInput name="tags" data={data} />);
            const expectedTags = ['read', 'want to read', 'whishlist'];

            wrapper.instance().addTag(tagName);

            expect(wrapper.instance().tags).toEqual(expectedTags);
        });

        test('resets the value on the state', () => {
            const tagName = 'on Kindle';
            const wrapper = shallow(<TagInput name="tags" data={data} />);

            wrapper.instance().addTag(tagName);

            expect(wrapper.state('value')).toBe('');
        });

        test('fires the onChange callback', () => {
            const tagName = 'owned';
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <TagInput name="tags" data={data} onChange={handleOnChange} />
            );
            const expectedTags = ['read', 'want to read', 'whishlist', 'owned'];

            wrapper.instance().addTag(tagName);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('tags');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedTags);
        });
    });

    describe('deleteTag()', () => {
        test('deletes the specified tag from the state', () => {
            const tagName = 'on Kindle';
            const wrapper = shallow(<TagInput name="tags" data={data} />);
            const expectedTags = ['read', 'want to read', 'whishlist'];

            wrapper.instance().deleteTag(tagName);

            expect(wrapper.instance().tags).toEqual(expectedTags);
        });

        test('fires the onChange callback', () => {
            const tagName = 'whishlist';
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <TagInput name="tags" data={data} onChange={handleOnChange} />
            );
            const expectedTags = ['read', 'want to read'];

            wrapper.instance().deleteTag(tagName);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('tags');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedTags);
        });
    });

    describe('deleteLastTag()', () => {
        test('deletes the last tag from the state', () => {
            const wrapper = shallow(<TagInput name="tags" data={data} />);
            const expectedTags = ['read', 'want to read'];

            wrapper.instance().deleteLastTag();

            expect(wrapper.instance().tags).toEqual(expectedTags);
        });

        test('fires the onChange callback', () => {
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <TagInput name="tags" data={data} onChange={handleOnChange} />
            );
            const expectedTags = ['read', 'want to read'];

            wrapper.instance().deleteLastTag();

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe('tags');
            expect(handleOnChange.mock.calls[0][1]).toEqual(expectedTags);
        });
    });

    describe('handleInputChange()', () => {
        test('saves the value of the input field to the state', () => {
            const mockChangeEvent = {
                target: {
                    value: 'newValue'
                }
            };
            const wrapper = mount(<TagInput name="tags" data={data} />);

            wrapper.find('input').simulate('change', mockChangeEvent);

            expect(wrapper.state('value')).toBe('newValue');
        });
    });

    describe('handleBlurInput()', () => {
        test('fires the onBlur callback', () => {
            const handleOnBlur = jest.fn();
            const wrapper = mount(
                <TagInput name="tags" data={data} onBlur={handleOnBlur} />
            );

            wrapper.find('input').simulate('blur');

            expect(handleOnBlur.mock.calls.length).toBe(1);
            expect(handleOnBlur.mock.calls[0][0]).toBe('tags');
        });

        test('resets the value of the input field', () => {
            const wrapper = mount(<TagInput />);
            wrapper.setState({ value: 'fant' });

            wrapper.find('input').simulate('blur');

            expect(wrapper.state('value')).toBe('');
        });

        test('resets suggestions', () => {
            const mockSuggestions = [
                { name: 'on Kindle' },
                { name: 'owned' },
                { name: 'favourite' }
            ];
            const wrapper = mount(<TagInput />);
            wrapper.setState({ suggestions: mockSuggestions });

            wrapper.find('input').simulate('blur');

            expect(wrapper.state('suggestions')).toEqual([]);
        });
    });

    describe('suggestions', () => {
        test('are empty on the state by default', () => {
            const mockSuggestions = [
                { name: 'on Kindle' },
                { name: 'owned' },
                { name: 'favourite' }
            ];
            const wrapper = shallow(<TagInput suggestions={mockSuggestions} />);

            expect(wrapper.state('suggestions')).toEqual([]);
        });

        test('populates suggestions based on the provided input value', () => {
            const mockSuggestions = [
                { name: 'on Kindle' },
                { name: 'owned' },
                { name: 'favourite' }
            ];
            const expected = [{ name: 'favourite' }];
            const wrapper = shallow(<TagInput suggestions={mockSuggestions} />);
            wrapper.instance().fetchSuggestions('fav');

            expect(wrapper.state('suggestions')).toEqual(expected);
        });

        test('populates only with suggestions that are not present in saved tags', () => {
            const mockSuggestions = [
                { name: 'favourite' },
                { name: 'read' },
                { name: 'want to read' },
                { name: 'currently reading' },
                { name: 'whishlist' }
            ];
            const expected = [{ name: 'currently reading' }];
            const wrapper = shallow(
                <TagInput
                    name="tags"
                    data={data}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.instance().fetchSuggestions('read');

            expect(wrapper.state('suggestions')).toEqual(expected);
        });
    });

    describe('clearSuggestions()', () => {
        test('resets suggestions on the state', () => {
            const mockSuggestions = [
                { name: 'on Kindle' },
                { name: 'owned' },
                { name: 'favourite' }
            ];
            const wrapper = shallow(<TagInput suggestions={mockSuggestions} />);
            wrapper.setState({ suggestions: mockSuggestions });
            wrapper.instance().clearSuggestions();

            expect(wrapper.state('suggestions')).toEqual([]);
        });
    });
});
