import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './TagInput.css';
import { suggestionsType } from '../../types/types';
import noop from '../../utils/noop';
import AutoComplete from '../AutoComplete/AutoComplete';
import InputField from '../Form/InputField';
import { filterSuggestions } from '../AutoComplete/findMatches';

class TagInput extends Component {
    state = {
        tags: new Set(this.props.data),
        value: '',
        suggestions: []
    };

    get tags() {
        return [...this.state.tags];
    }

    get suggestionsWithoutDuplication() {
        const { suggestions } = this.props;
        return suggestions.filter(item => !this.tags.includes(item.name));
    }

    handleClickFauxInput = () => {
        this.input.focus();
    };

    handleInputChange = (name, value) => this.setState({ value });

    fetchSuggestions = (nextValue, suggestionsLength) => {
        this.setState({
            suggestions: filterSuggestions(
                this.suggestionsWithoutDuplication,
                nextValue,
                suggestionsLength
            )
        });
    };

    clearSuggestions = () => {
        this.setState({ suggestions: [] });
    };

    addTag = newTag => {
        const { name, onChange } = this.props;
        this.state.tags.add(newTag);
        this.setState({ value: '' });
        onChange(name, this.tags);
    };

    deleteTag = tag => {
        const { name, onChange } = this.props;
        this.state.tags.delete(tag);
        onChange(name, this.tags);
    };

    deleteLastTag = () => {
        const allTags = this.tags;
        const lastTag = allTags[allTags.length - 1];
        this.deleteTag(lastTag);
    };

    handleBlurInput = () => {
        const { name, onBlur } = this.props;
        this.setState({ value: '', suggestions: [] });
        onBlur(name);
    };

    saveInputRef = node => {
        this.input = node;
    };

    renderTags() {
        const { children } = this.props;
        return React.Children.map(children, child =>
            React.cloneElement(child, {
                value: this.tags,
                deletable: true,
                onDeleteClick: this.deleteTag
            })
        );
    }

    render() {
        const { name, label, touched, error } = this.props;
        const { value, suggestions } = this.state;

        return (
            <div
                className={styles.tagInput}
                onClick={this.handleClickFauxInput}
            >
                {this.renderTags()}
                <div className={styles.autoCompleteField}>
                    <AutoComplete
                        name={name}
                        value={value}
                        suggestions={suggestions}
                        onInputChange={this.handleInputChange}
                        fetchSuggestions={this.fetchSuggestions}
                        clearSuggestions={this.clearSuggestions}
                        onSubmit={this.addTag}
                        onBlur={this.handleBlurInput}
                        onCtrlBackspace={this.deleteLastTag}
                    >
                        <InputField
                            saveRef={this.saveInputRef}
                            value={value}
                            label={label}
                            touched={touched}
                            error={error}
                            flexibleWidth
                        />
                    </AutoComplete>
                </div>
            </div>
        );
    }
}

TagInput.propTypes = {
    children: PropTypes.node,
    data: PropTypes.arrayOf(PropTypes.string),
    name: PropTypes.string,
    label: PropTypes.string,
    suggestions: suggestionsType,
    touched: PropTypes.bool,
    error: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

TagInput.defaultProps = {
    children: null,
    data: [],
    name: '',
    label: '',
    suggestions: [],
    touched: false,
    error: '',
    onChange: noop,
    onBlur: noop
};

export default TagInput;
