import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import Chip from '../Chip/Chip';
import FilterLink from '../DetailsCard/FilterLink';

const Tags = ({
    value,
    color,
    inputStyle,
    inline,
    deletable,
    filterable,
    onDeleteClick,
    onFiltering
}) => {
    const ElementType = inline ? Fragment : 'div';
    return (
        <ElementType>
            {value.map(tag => (
                <Chip
                    key={tag}
                    value={tag}
                    color={color}
                    inputStyle={inputStyle}
                    deletable={deletable}
                    onDeleteClick={onDeleteClick}
                    filterable={filterable}
                >
                    <FilterLink
                        value={tag}
                        filterable={filterable}
                        onFiltering={onFiltering}
                    >
                        {tag}
                    </FilterLink>
                </Chip>
            ))}
        </ElementType>
    );
};

Tags.propTypes = {
    value: PropTypes.arrayOf(PropTypes.string),
    color: PropTypes.string,
    inputStyle: PropTypes.bool,
    inline: PropTypes.bool,
    deletable: PropTypes.bool,
    filterable: PropTypes.bool,
    onDeleteClick: PropTypes.func,
    onFiltering: PropTypes.func
};

Tags.defaultProps = {
    value: [],
    color: 'magenta',
    inputStyle: false,
    inline: false,
    deletable: false,
    filterable: false,
    onDeleteClick: noop,
    onFiltering: noop
};

export default Tags;
