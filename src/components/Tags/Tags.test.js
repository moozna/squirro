import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Tags from './Tags';

describe('<Tags />', () => {
    const data = ['read', 'want to read', 'whishlist'];

    test('has the expected html structure', () => {
        const wrapper = shallow(<Tags />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when data is specified', () => {
        const wrapper = shallow(<Tags value={data} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders orange tags', () => {
        const wrapper = shallow(<Tags value={data} color="orange" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders inline tags', () => {
        const wrapper = shallow(<Tags value={data} inline />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders tags with close icon', () => {
        const wrapper = shallow(<Tags value={data} deletable />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders tags with inputStyle', () => {
        const wrapper = shallow(<Tags value={data} inputStyle />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
