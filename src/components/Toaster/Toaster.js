import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import styles from './Toaster.css';
import noop from '../../utils/noop';
import Toast from './Toast';

const ANIMATION_CLASSES = {
    enter: styles.toastEnter,
    enterActive: styles.toastEnterActive,
    exit: styles.toastLeave,
    exitActive: styles.toastLeaveActive
};

class Toaster extends Component {
    renderToasts() {
        const {
            messages,
            autoClose,
            pauseOnFocusLoss,
            clearMessage
        } = this.props;

        return messages.map(item => (
            <CSSTransition
                key={item.id}
                classNames={ANIMATION_CLASSES}
                timeout={800}
                appear
                unmountOnExit
            >
                <Toast
                    toastId={item.id}
                    autoClose={autoClose}
                    pauseOnFocusLoss={pauseOnFocusLoss}
                    closeToast={clearMessage}
                >
                    {item.title ? <div>{item.title}</div> : null}
                    <div>{item.message}</div>
                </Toast>
            </CSSTransition>
        ));
    }

    render() {
        return (
            <div className={styles.toaster}>
                <TransitionGroup>{this.renderToasts()}</TransitionGroup>
            </div>
        );
    }
}

Toaster.propTypes = {
    autoClose: PropTypes.number,
    pauseOnFocusLoss: PropTypes.bool,
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            message: PropTypes.string,
            title: PropTypes.string
        })
    ),
    clearMessage: PropTypes.func
};

Toaster.defaultProps = {
    autoClose: 8000,
    pauseOnFocusLoss: true,
    messages: [],
    clearMessage: noop
};

export default Toaster;
