import React from 'react';
import PropTypes from 'prop-types';
import styles from './Toaster.css';
import noop from '../../utils/noop';

const ProgressBar = ({ delay, isRunning, closeToast }) => {
    const animationStyle = {
        animationDuration: `${delay}ms`,
        animationPlayState: isRunning ? 'running' : 'paused'
    };

    return (
        <div
            className={styles.progressBar}
            style={animationStyle}
            onAnimationEnd={closeToast}
        />
    );
};

ProgressBar.propTypes = {
    delay: PropTypes.number,
    isRunning: PropTypes.bool,
    closeToast: PropTypes.func
};

ProgressBar.defaultProps = {
    delay: 8000,
    isRunning: false,
    closeToast: noop
};

export default ProgressBar;
