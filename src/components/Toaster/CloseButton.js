import React from 'react';
import PropTypes from 'prop-types';
import styles from './Toaster.css';
import noop from '../../utils/noop';
import Close from '../svg/Close';

const CloseButton = ({ closeToast }) => (
    <button className={styles.closeButton} type="button" onClick={closeToast}>
        <Close iconSize={18} />
    </button>
);

CloseButton.propTypes = {
    closeToast: PropTypes.func
};

CloseButton.defaultProps = {
    closeToast: noop
};

export default CloseButton;
