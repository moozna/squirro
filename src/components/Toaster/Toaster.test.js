import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Toaster from './Toaster';

describe('<Toaster />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Toaster />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when messages are specified', () => {
        const mockMessages = [
            {
                id: '1',
                message: 'Books error message',
                title: 'Error title'
            },
            {
                id: '2',
                message: 'Import error message'
            },
            {
                id: '3',
                message: 'Filters error message',
                title: 'Error title'
            }
        ];
        const wrapper = shallow(<Toaster messages={mockMessages} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
