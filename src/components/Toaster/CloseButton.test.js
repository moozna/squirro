import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import CloseButton from './CloseButton';

describe('<CloseButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<CloseButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onClick()', () => {
        test('fires the closeToast callback', () => {
            const handleCloseToast = jest.fn();
            const wrapper = shallow(
                <CloseButton closeToast={handleCloseToast} />
            );

            wrapper.find('.closeButton').simulate('click');
            expect(handleCloseToast.mock.calls.length).toBe(1);
        });
    });
});
