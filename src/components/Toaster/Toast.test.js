import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Toast from './Toast';

describe('<Toast />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Toast />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Toast>{testChildren}</Toast>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleClose()', () => {
        test('fires the closeToast callback', () => {
            const toastId = '1';
            const handleCloseToast = jest.fn();

            const wrapper = shallow(
                <Toast toastId={toastId} closeToast={handleCloseToast} />
            );
            wrapper.instance().handleClose();

            expect(handleCloseToast.mock.calls.length).toBe(1);
            expect(handleCloseToast.mock.calls[0][0]).toBe(toastId);
        });
    });

    describe('onMouseEnter', () => {
        test('stops animation when autoClose is enabled and toast is hovered', () => {
            const wrapper = shallow(<Toast autoClose={5000} />);
            wrapper.find('.toast').simulate('mouseenter');

            expect(wrapper.state('isRunning')).toBeFalsy();
        });

        test('does not set isRunning when autoClose is not enabled', () => {
            const wrapper = shallow(<Toast autoClose={null} />);
            expect(wrapper.state('isRunning')).toBeTruthy();
            wrapper.find('.toast').simulate('mouseenter');

            expect(wrapper.state('isRunning')).toBeTruthy();
        });
    });

    describe('onMouseLeave', () => {
        test('restarts animation when autoClose is enabled and toast is not hovered', () => {
            const wrapper = shallow(<Toast autoClose={5000} />);
            wrapper.setState({ isRunning: false });
            wrapper.find('.toast').simulate('mouseleave');

            expect(wrapper.state('isRunning')).toBeTruthy();
        });

        test('does not set isRunning when autoClose is not enabled', () => {
            const wrapper = shallow(<Toast autoClose={null} />);
            wrapper.setState({ isRunning: false });
            wrapper.find('.toast').simulate('mouseleave');

            expect(wrapper.state('isRunning')).toBeFalsy();
        });
    });

    describe('event listeners', () => {
        let mockAddEventListener;
        let mockRemoveEventListener;

        beforeEach(() => {
            mockAddEventListener = jest.spyOn(window, 'addEventListener');
            mockRemoveEventListener = jest.spyOn(window, 'removeEventListener');
        });

        afterEach(() => {
            mockAddEventListener.mockRestore();
            mockRemoveEventListener.mockRestore();
        });

        test('attaches event listeners when pauseOnFocusLoss is enabled', () => {
            shallow(<Toast pauseOnFocusLoss />);

            expect(mockAddEventListener.mock.calls.length).toBe(2);
            expect(mockAddEventListener.mock.calls[0][0]).toBe('focus');
            expect(mockAddEventListener.mock.calls[1][0]).toBe('blur');
        });

        test('does not attaches event listeners when pauseOnFocusLoss is not enabled', () => {
            shallow(<Toast pauseOnFocusLoss={false} />);

            expect(mockAddEventListener.mock.calls.length).toBe(0);
        });

        test('detaches event listeners after unmount when pauseOnFocusLoss is enabled', () => {
            const wrapper = shallow(<Toast pauseOnFocusLoss />);
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(2);
            expect(mockRemoveEventListener.mock.calls[0][0]).toBe('focus');
            expect(mockRemoveEventListener.mock.calls[1][0]).toBe('blur');
        });

        test('does not detaches event listeners after unmount when pauseOnFocusLoss is not enabled', () => {
            const wrapper = shallow(<Toast pauseOnFocusLoss={false} />);
            wrapper.unmount();

            expect(mockRemoveEventListener.mock.calls.length).toBe(0);
        });
    });
});
