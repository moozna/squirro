import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ProgressBar from './ProgressBar';

describe('<ProgressBar />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<ProgressBar />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('uses the provided delay', () => {
        const testDelay = 3000;
        const wrapper = shallow(<ProgressBar delay={testDelay} />);

        expect(
            wrapper.find('.progressBar').prop('style').animationDuration
        ).toBe(`${testDelay}ms`);
    });

    test('renders the component with running animation', () => {
        const wrapper = shallow(<ProgressBar isRunning />);

        expect(
            wrapper.find('.progressBar').prop('style').animationPlayState
        ).toBe('running');
    });

    test('renders the component with paused animation', () => {
        const wrapper = shallow(<ProgressBar isRunning={false} />);

        expect(
            wrapper.find('.progressBar').prop('style').animationPlayState
        ).toBe('paused');
    });
});
