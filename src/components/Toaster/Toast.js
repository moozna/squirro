import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Toaster.css';
import noop from '../../utils/noop';
import ProgressBar from './ProgressBar';
import CloseButton from './CloseButton';

class Toast extends Component {
    state = {
        isRunning: true
    };

    componentDidMount() {
        const { pauseOnFocusLoss } = this.props;
        if (pauseOnFocusLoss) {
            this.addFocusEvents();
        }
    }

    componentWillUnmount() {
        const { pauseOnFocusLoss } = this.props;
        if (pauseOnFocusLoss) {
            this.removeFocusEvents();
        }
    }

    pauseToast = () => {
        const { autoClose } = this.props;
        if (autoClose) {
            this.setState({ isRunning: false });
        }
    };

    playToast = () => {
        const { autoClose } = this.props;
        if (autoClose) {
            this.setState({ isRunning: true });
        }
    };

    handleClose = () => {
        const { toastId, closeToast } = this.props;
        closeToast(toastId);
    };

    addFocusEvents() {
        window.addEventListener('focus', this.playToast);
        window.addEventListener('blur', this.pauseToast);
    }

    removeFocusEvents() {
        window.removeEventListener('focus', this.playToast);
        window.removeEventListener('blur', this.pauseToast);
    }

    render() {
        const { children, autoClose } = this.props;
        const { isRunning } = this.state;

        return (
            <div
                className={styles.toast}
                onMouseEnter={this.pauseToast}
                onMouseLeave={this.playToast}
            >
                <div className={styles.toastBody}>{children}</div>
                <CloseButton closeToast={this.handleClose} />
                {autoClose && (
                    <ProgressBar
                        delay={autoClose}
                        isRunning={isRunning}
                        closeToast={this.handleClose}
                    />
                )}
            </div>
        );
    }
}

Toast.propTypes = {
    children: PropTypes.node,
    toastId: PropTypes.string,
    autoClose: PropTypes.number,
    pauseOnFocusLoss: PropTypes.bool,
    closeToast: PropTypes.func
};

Toast.defaultProps = {
    children: null,
    toastId: null,
    autoClose: 8000,
    pauseOnFocusLoss: true,
    closeToast: noop
};

export default Toast;
