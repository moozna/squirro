import { escapeForRegExp, markIt, filterSuggestions } from './findMatches';

describe('findMatches', () => {
    describe('escapeForRegExp()', () => {
        test('is a function', () => {
            expect(typeof escapeForRegExp).toBe('function');
        });

        describe('does not throw', () => {
            test('when value is undefined', () => {
                expect(escapeForRegExp(undefined)).toBe(undefined);
            });

            test('when value is null', () => {
                expect(escapeForRegExp(null)).toBe(null);
            });

            test('when value is an empty string', () => {
                expect(escapeForRegExp('')).toBe('');
            });
        });

        test('returns the original string when it not contains special characters', () => {
            expect(escapeForRegExp('something')).toBe('something');
        });

        test('escapes "-"', () => {
            expect(escapeForRegExp('-')).toBe('\\-');
        });

        test('escapes "^"', () => {
            expect(escapeForRegExp('^')).toBe('\\^');
        });

        test('escapes "$"', () => {
            expect(escapeForRegExp('$')).toBe('\\$');
        });

        test('escapes "*"', () => {
            expect(escapeForRegExp('*')).toBe('\\*');
        });

        test('escapes "+"', () => {
            expect(escapeForRegExp('+')).toBe('\\+');
        });

        test('escapes "?"', () => {
            expect(escapeForRegExp('?')).toBe('\\?');
        });

        test('escapes "."', () => {
            expect(escapeForRegExp('.')).toBe('\\.');
        });

        test('escapes "("', () => {
            expect(escapeForRegExp('(')).toBe('\\(');
        });

        test('escapes ")"', () => {
            expect(escapeForRegExp(')')).toBe('\\)');
        });

        test('escapes "{"', () => {
            expect(escapeForRegExp('{')).toBe('\\{');
        });

        test('escapes "}"', () => {
            expect(escapeForRegExp('}')).toBe('\\}');
        });

        test('escapes "["', () => {
            expect(escapeForRegExp('[')).toBe('\\[');
        });

        test('escapes "]"', () => {
            expect(escapeForRegExp(']')).toBe('\\]');
        });

        test('escapes "|"', () => {
            expect(escapeForRegExp('|')).toBe('\\|');
        });

        test('escapes "\\"', () => {
            expect(escapeForRegExp('\\')).toBe('\\\\');
        });
    });

    describe('markIt()', () => {
        test('is a function', () => {
            expect(typeof markIt).toBe('function');
        });

        test('returns the original text when input value is missing', () => {
            const mockText = 'Science Fiction Fantasy';
            const expected = { __html: 'Science Fiction Fantasy' };
            expect(markIt(mockText)).toEqual(expected);
        });

        test('returns the original text when matching is not found', () => {
            const mockText = 'Science Fiction Fantasy';
            const mockInputValue = 'Something';
            const expected = { __html: 'Science Fiction Fantasy' };
            expect(markIt(mockText, mockInputValue)).toEqual(expected);
        });

        test('indicates the matched input value in the text', () => {
            const mockText = 'Science Fiction Fantasy';
            const mockInputValue = 'Fant';
            const expected = { __html: 'Science Fiction <mark>Fant</mark>asy' };
            expect(markIt(mockText, mockInputValue)).toEqual(expected);
        });

        test('indicates the matched input value everywhere in the text', () => {
            const mockText = 'Fantasy and Science Fiction Fantasy';
            const mockInputValue = 'Fant';
            const expected = {
                __html:
                    '<mark>Fant</mark>asy and Science Fiction <mark>Fant</mark>asy'
            };
            expect(markIt(mockText, mockInputValue)).toEqual(expected);
        });

        test('indicates the matched input value based on case insensitive search', () => {
            const mockText = 'Science Fiction Fantasy';
            const mockInputValue = 'fant';
            const expected = {
                __html: 'Science Fiction <mark>Fant</mark>asy'
            };
            expect(markIt(mockText, mockInputValue)).toEqual(expected);
        });
    });

    describe('filterSuggestions()', () => {
        test('is a function', () => {
            expect(typeof filterSuggestions).toBe('function');
        });

        test('returns the suggestions that match the input value', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockInputValue = 'fant';
            const expected = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Fantasy' }
            ];

            expect(filterSuggestions(mockSuggestions, mockInputValue)).toEqual(
                expected
            );
        });

        test('returns an empty array when nothing is matched', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockInputValue = 'something';
            const expected = [];

            expect(filterSuggestions(mockSuggestions, mockInputValue)).toEqual(
                expected
            );
        });

        test('returns an empty array when suggestions are not provided', () => {
            const mockInputValue = 'something';
            const expected = [];

            expect(filterSuggestions(undefined, mockInputValue)).toEqual(
                expected
            );
        });

        test('returns up to the specified number of suggestions', () => {
            const maxNumberOfSuggestion = 1;
            const testSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockInputValue = 'fant';

            expect(
                filterSuggestions(
                    testSuggestions,
                    mockInputValue,
                    maxNumberOfSuggestion
                ).length
            ).toBe(maxNumberOfSuggestion);
        });
    });
});
