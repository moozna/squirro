import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Suggestions from './Suggestions';

describe('<Suggestions />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Suggestions />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders nothing when expandable is false', () => {
        const wrapper = shallow(<Suggestions expandable={false} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders nothing when suggestions are empty', () => {
        const wrapper = shallow(<Suggestions expandable suggestions={[]} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders suggestions', () => {
        const mockSuggestions = [
            { name: 'Science Fiction Fantasy' },
            { name: 'Horror' },
            { name: 'Fantasy' }
        ];
        const wrapper = shallow(
            <Suggestions expandable suggestions={mockSuggestions} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders selected suggestion', () => {
        const mockSuggestions = [
            { name: 'Science Fiction Fantasy' },
            { name: 'Horror' },
            { name: 'Fantasy' }
        ];
        const wrapper = shallow(
            <Suggestions
                expandable
                suggestions={mockSuggestions}
                selectedIndex={1}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
