import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { suggestionsType } from '../../types/types';
import noop from '../../utils/noop';
import AutoComplete from './AutoComplete';
import InputField from '../Form/InputField';
import { filterSuggestions } from './findMatches';

class AutoCompleteField extends Component {
    state = {
        suggestions: []
    };

    fetchSuggestions = (nextValue, suggestionsLength) => {
        this.setState({
            suggestions: filterSuggestions(
                this.suggestionWithoutExactMatch(nextValue),
                nextValue,
                suggestionsLength
            )
        });
    };

    selectSuggestion = value => {
        const { name, onChange } = this.props;
        onChange(name, value);
    };

    clearSuggestions = () => {
        this.setState({ suggestions: [] });
    };

    suggestionWithoutExactMatch(value) {
        const { suggestions } = this.props;
        return suggestions.filter(item => value !== item.name);
    }

    render() {
        const {
            value,
            name,
            label,
            touched,
            error,
            onChange,
            onBlur
        } = this.props;
        const { suggestions } = this.state;

        return (
            <AutoComplete
                value={value}
                name={name}
                suggestions={suggestions}
                delimiterChars={['Enter']}
                onInputChange={onChange}
                fetchSuggestions={this.fetchSuggestions}
                clearSuggestions={this.clearSuggestions}
                onSubmit={this.selectSuggestion}
                onBlur={onBlur}
            >
                <InputField
                    value={value}
                    label={label}
                    touched={touched}
                    error={error}
                    flexibleWidth
                />
            </AutoComplete>
        );
    }
}

AutoCompleteField.propTypes = {
    value: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    suggestions: suggestionsType,
    touched: PropTypes.bool,
    error: PropTypes.string,
    onChange: PropTypes.func,
    onBlur: PropTypes.func
};

AutoCompleteField.defaultProps = {
    value: '',
    name: '',
    label: '',
    suggestions: [],
    touched: false,
    error: '',
    onChange: noop,
    onBlur: noop
};

export default AutoCompleteField;
