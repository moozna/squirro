import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AutoCompleteField from './AutoCompleteField';

describe('<AutoCompleteField />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<AutoCompleteField />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <AutoCompleteField
                value="A Song of"
                name="seriesTitle"
                label="label"
                touched={false}
                error="Error message"
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('suggestions', () => {
        test('are empty on the state by default', () => {
            const mockSuggestions = [
                { name: 'A Song of Ice and Fire' },
                { name: 'Arc of a Scythe' },
                { name: 'Not a Drop to Drink' }
            ];

            const wrapper = shallow(
                <AutoCompleteField suggestions={mockSuggestions} />
            );

            expect(wrapper.state('suggestions')).toEqual([]);
        });

        test('populates suggestions based on the provided input value', () => {
            const mockSuggestions = [
                { name: 'A Song of Ice and Fire' },
                { name: 'Arc of a Scythe' },
                { name: 'Not a Drop to Drink' }
            ];
            const expected = [
                { name: 'A Song of Ice and Fire' },
                { name: 'Arc of a Scythe' }
            ];

            const wrapper = shallow(
                <AutoCompleteField suggestions={mockSuggestions} />
            );
            wrapper.instance().fetchSuggestions('A s');

            expect(wrapper.state('suggestions')).toEqual(expected);
        });

        test('populates only with suggestions that are not exactly the same as the input value', () => {
            const mockSuggestions = [
                { name: 'A Song of Ice and Fire' },
                { name: 'A Song of Ice and Fire 2' },
                { name: 'Arc of a Scythe' },
                { name: 'Not a Drop to Drink' }
            ];
            const expected = [{ name: 'A Song of Ice and Fire 2' }];

            const wrapper = shallow(
                <AutoCompleteField suggestions={mockSuggestions} />
            );
            wrapper.instance().fetchSuggestions('A Song of Ice and Fire');

            expect(wrapper.state('suggestions')).toEqual(expected);
        });
    });

    describe('selectSuggestion()', () => {
        test('fires the onChange callback', () => {
            const fieldName = 'seriesTitle';
            const mockSelection = 'Graceling Realm';
            const handleOnChange = jest.fn();

            const wrapper = shallow(
                <AutoCompleteField
                    value="Grace"
                    name={fieldName}
                    onChange={handleOnChange}
                />
            );
            wrapper.instance().selectSuggestion(mockSelection);

            expect(handleOnChange.mock.calls.length).toBe(1);
            expect(handleOnChange.mock.calls[0][0]).toBe(fieldName);
            expect(handleOnChange.mock.calls[0][1]).toEqual(mockSelection);
        });
    });

    describe('clearSuggestions()', () => {
        test('resets suggestions on the state', () => {
            const mockSuggestions = [
                { name: 'A Song of Ice and Fire' },
                { name: 'Arc of a Scythe' },
                { name: 'Not a Drop to Drink' }
            ];

            const wrapper = shallow(
                <AutoCompleteField suggestions={mockSuggestions} />
            );
            wrapper.setState({ suggestions: mockSuggestions });
            wrapper.instance().clearSuggestions();

            expect(wrapper.state('suggestions')).toEqual([]);
        });
    });
});
