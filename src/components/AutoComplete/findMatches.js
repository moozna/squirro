export const escapeForRegExp = value =>
    value ? value.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&') : value;

export const markIt = (text, inputValue) => {
    let markedText = text;

    if (inputValue) {
        const regex = RegExp(escapeForRegExp(inputValue), 'gi');
        markedText = text.replace(regex, '<mark>$&</mark>');
    }

    return {
        __html: markedText
    };
};

export const filterSuggestions = (suggestions, inputValue, length) => {
    const regex = new RegExp(`(?:^|\\s)${escapeForRegExp(inputValue)}`, 'i');

    return suggestions
        ? suggestions.filter(item => regex.test(item.name)).slice(0, length)
        : [];
};
