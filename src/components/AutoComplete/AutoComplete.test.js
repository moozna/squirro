import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import AutoComplete from './AutoComplete';

describe('<AutoComplete />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<AutoComplete />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <input />;
        const wrapper = shallow(<AutoComplete>{testChildren}</AutoComplete>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('suggestion list', () => {
        test('is closed when input is not focused', () => {
            const wrapper = shallow(<AutoComplete />);
            const suggestions = wrapper.find('Suggestions');

            expect(suggestions.props().expandable).toBe(false);
        });

        test('is closed when suggestions is empty', () => {
            const mockSuggestions = [];

            const wrapper = shallow(
                <AutoComplete suggestions={mockSuggestions} />
            );
            const suggestions = wrapper.find('Suggestions');

            expect(suggestions.props().expandable).toBe(false);
        });

        test('is open when input is focused and suggestions are provided', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];

            const wrapper = shallow(
                <AutoComplete suggestions={mockSuggestions} />
            );
            wrapper.setState({ inputFocused: true });
            const suggestions = wrapper.find('Suggestions');

            expect(suggestions.props().expandable).toBe(true);
        });
    });

    describe('handleFocus()', () => {
        test('sets inputFocused on the state to true', () => {
            const testChildren = <input />;

            const wrapper = shallow(
                <AutoComplete>{testChildren}</AutoComplete>
            );
            wrapper.find('input').simulate('focus');

            expect(wrapper.state('inputFocused')).toBe(true);
        });

        test('fires the onFocus callback', () => {
            const testChildren = <input />;
            const handleOnFocus = jest.fn();

            const wrapper = shallow(
                <AutoComplete onFocus={handleOnFocus}>
                    {testChildren}
                </AutoComplete>
            );
            wrapper.find('input').simulate('focus');

            expect(handleOnFocus.mock.calls.length).toBe(1);
        });
    });

    describe('handleBlur()', () => {
        test('sets inputFocused on the state to false', () => {
            const testChildren = <input />;

            const wrapper = shallow(
                <AutoComplete>{testChildren}</AutoComplete>
            );
            wrapper.find('input').simulate('focus');

            expect(wrapper.state('inputFocused')).toBe(true);
        });

        test('fires the onBlur callback', () => {
            const testChildren = <input />;
            const handleOnBlur = jest.fn();

            const wrapper = shallow(
                <AutoComplete onBlur={handleOnBlur}>
                    {testChildren}
                </AutoComplete>
            );
            wrapper.find('input').simulate('blur');

            expect(handleOnBlur.mock.calls.length).toBe(1);
        });
    });

    describe('onInputChange()', () => {
        test('resets selectedIndex', () => {
            const mockInputName = 'genre';
            const mockInputValue = 'Fantasy';

            const wrapper = shallow(<AutoComplete />);
            wrapper.setState({ selectedIndex: 2 });
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(wrapper.state('selectedIndex')).toBe(-1);
        });

        test('fires the onInputChange callback', () => {
            const handleOnInputChange = jest.fn();
            const mockInputName = 'genre';
            const mockInputValue = 'Fantasy';

            const wrapper = shallow(
                <AutoComplete onInputChange={handleOnInputChange} />
            );
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(handleOnInputChange.mock.calls.length).toBe(1);
            expect(handleOnInputChange.mock.calls[0][0]).toBe(mockInputName);
            expect(handleOnInputChange.mock.calls[0][1]).toBe(mockInputValue);
        });

        test('fires the fetchSuggestions callback', () => {
            const handleFetchSuggestions = jest.fn();
            const mockInputName = 'genre';
            const mockInputValue = 'Fantasy';
            const mockSuggestionsLength = 5;

            const wrapper = shallow(
                <AutoComplete
                    maxSuggestionsLength={mockSuggestionsLength}
                    fetchSuggestions={handleFetchSuggestions}
                />
            );
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(handleFetchSuggestions.mock.calls.length).toBe(1);
            expect(handleFetchSuggestions.mock.calls[0][0]).toBe(
                mockInputValue
            );
            expect(handleFetchSuggestions.mock.calls[0][1]).toBe(
                mockSuggestionsLength
            );
        });

        test('does not fire the fetchSuggestions callback when input value is too short', () => {
            const handleFetchSuggestions = jest.fn();
            const mockInputName = 'genre';
            const mockInputValue = 'F';
            const mockSuggestionsLength = 5;

            const wrapper = shallow(
                <AutoComplete
                    maxSuggestionsLength={mockSuggestionsLength}
                    fetchSuggestions={handleFetchSuggestions}
                />
            );
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(handleFetchSuggestions.mock.calls.length).toBe(0);
        });

        test('fires the clearSuggestions callback when input value is too short and suggestions are available', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const handleClearSuggestions = jest.fn();
            const mockInputName = 'genre';
            const mockInputValue = 'F';

            const wrapper = shallow(
                <AutoComplete
                    suggestions={mockSuggestions}
                    clearSuggestions={handleClearSuggestions}
                />
            );
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(handleClearSuggestions.mock.calls.length).toBe(1);
        });

        test('does not fire the clearSuggestions callback when input value is long enough', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const handleClearSuggestions = jest.fn();
            const mockInputName = 'genre';
            const mockInputValue = 'Fant';

            const wrapper = shallow(
                <AutoComplete
                    suggestions={mockSuggestions}
                    clearSuggestions={handleClearSuggestions}
                />
            );
            wrapper.instance().handleInputChange(mockInputName, mockInputValue);

            expect(handleClearSuggestions.mock.calls.length).toBe(0);
        });
    });

    describe('onSubmit()', () => {
        test('fires the onSubmit callback on "Enter"', () => {
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete value="Fantasy" onSubmit={handleOnSubmit} />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe('Fantasy');
        });

        test('fires the onSubmit callback on ","', () => {
            const mockChangeEvent = {
                key: ',',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete value="Fantasy" onSubmit={handleOnSubmit} />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe('Fantasy');
        });

        test('fires the onSubmit callback on custom delimiter char', () => {
            const mockChangeEvent = {
                key: 'x',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="Fantasy"
                    delimiterChars={['x']}
                    onSubmit={handleOnSubmit}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe('Fantasy');
        });

        test('does not fire the onSubmit callback when value length is smaller than the minimum length', () => {
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    minValueLength={7}
                    onSubmit={handleOnSubmit}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(0);
        });

        test('fires the onSubmit callback with the selected suggestion text', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    onSubmit={handleOnSubmit}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.setState({ selectedIndex: 0 });
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(
                'Science Fiction Fantasy'
            );
        });

        test('fires the onSubmit callback with the current input value if suggestion is not selected', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    onSubmit={handleOnSubmit}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe('fant');
        });

        test('resets selectedIndex on the state', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(
                <AutoComplete value="fant" suggestions={mockSuggestions} />
            );
            wrapper.setState({ selectedIndex: 2 });
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(wrapper.state('selectedIndex')).toBe(-1);
        });

        test('stops keydown event when delimiter char is pressed', () => {
            const mockChangeEvent = {
                key: 'Enter',
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();

            const wrapper = shallow(
                <AutoComplete value="" onSubmit={handleOnSubmit} />
            );
            wrapper.setState({ selectedIndex: 0 });
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(mockChangeEvent.preventDefault.mock.calls.length).toBe(1);
        });
    });

    describe('ArrowUp', () => {
        test('selects the last suggestion when nothing was selected', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'ArrowUp',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(
                <AutoComplete suggestions={mockSuggestions} />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(wrapper.state('selectedIndex')).toBe(2);
        });

        test('selects the suggestion before the selected one', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'ArrowUp',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(
                <AutoComplete suggestions={mockSuggestions} />
            );
            wrapper.setState({ selectedIndex: 2 });
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(wrapper.state('selectedIndex')).toBe(1);
        });
    });

    describe('ArrowDown', () => {
        test('selects the suggestion after the selected one', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'ArrowDown',
                preventDefault: jest.fn()
            };

            const wrapper = shallow(
                <AutoComplete suggestions={mockSuggestions} />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(wrapper.state('selectedIndex')).toBe(0);
        });
    });

    describe('Backspace', () => {
        test('fires the onCtrlBackspace callback on Ctrl + Backspace', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Backspace',
                ctrlKey: true,
                preventDefault: jest.fn()
            };
            const handleOnCtrlBackspace = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    onCtrlBackspace={handleOnCtrlBackspace}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnCtrlBackspace.mock.calls.length).toBe(1);
        });

        test('does not fire the onCtrlBackspace callback when Ctrl is not pressed', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Backspace',
                ctrlKey: false,
                preventDefault: jest.fn()
            };
            const handleOnCtrlBackspace = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    onCtrlBackspace={handleOnCtrlBackspace}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnCtrlBackspace.mock.calls.length).toBe(0);
        });
    });

    describe('Escape', () => {
        test('fires the onBlur callback', () => {
            const mockSuggestions = [
                { name: 'Science Fiction Fantasy' },
                { name: 'Horror' },
                { name: 'Fantasy' }
            ];
            const mockChangeEvent = {
                key: 'Escape',
                preventDefault: jest.fn()
            };
            const handleOnBlur = jest.fn();

            const wrapper = shallow(
                <AutoComplete
                    value="fant"
                    onBlur={handleOnBlur}
                    suggestions={mockSuggestions}
                />
            );
            wrapper.find('.autoComplete').simulate('keydown', mockChangeEvent);

            expect(handleOnBlur.mock.calls.length).toBe(1);
        });
    });
});
