import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AutoComplete.css';
import { suggestionsType } from '../../types/types';
import noop from '../../utils/noop';
import Suggestions from './Suggestions';

class AutoComplete extends Component {
    state = {
        inputFocused: false,
        selectedIndex: -1
    };

    handleInputFocus = () => {
        const { onFocus } = this.props;
        this.setState({ inputFocused: true });
        onFocus();
    };

    handleInputBlur = () => {
        const { onBlur } = this.props;
        this.setState({ inputFocused: false });
        onBlur();
    };

    handleInputChange = (name, value) => {
        const {
            maxSuggestionsLength,
            onInputChange,
            fetchSuggestions
        } = this.props;

        this.setState({ selectedIndex: -1 });
        onInputChange(name, value);

        if (this.isValueLongEnough(value)) {
            fetchSuggestions(value, maxSuggestionsLength);
        } else {
            this.clearSuggestions();
        }
    };

    handleKeyDown = event => {
        const { selectedIndex } = this.state;
        const {
            value,
            suggestions,
            delimiterChars,
            onBlur,
            onEscape,
            onCtrlBackspace
        } = this.props;

        if (delimiterChars.indexOf(event.key) > -1) {
            event.preventDefault();

            if (this.isValueLongEnough(value)) {
                if (selectedIndex > -1) {
                    this.submitValue(suggestions[selectedIndex].name);
                } else {
                    this.submitValue(value);
                }
            }
        }

        if (event.key === 'ArrowUp') {
            event.preventDefault();

            if (selectedIndex <= 0) {
                this.setState({ selectedIndex: suggestions.length - 1 });
            } else {
                this.setState(prevState => ({
                    selectedIndex: prevState.selectedIndex - 1
                }));
            }
        }

        if (event.key === 'ArrowDown') {
            event.preventDefault();

            this.setState(prevState => ({
                selectedIndex:
                    (prevState.selectedIndex + 1) % suggestions.length
            }));
        }

        if (event.key === 'Backspace' && event.ctrlKey) {
            onCtrlBackspace();
        }

        if (event.key === 'Escape') {
            onEscape();
            onBlur();
            this.clearSuggestions();
        }
    };

    submitValue = value => {
        const { onSubmit } = this.props;
        onSubmit(value);
        this.setState({ selectedIndex: -1 });
        this.clearSuggestions();
    };

    clearSuggestions() {
        const { suggestions, clearSuggestions } = this.props;

        if (suggestions.length) {
            clearSuggestions();
        }
    }

    isValueLongEnough(value) {
        const { minValueLength } = this.props;
        return value.length >= minValueLength;
    }

    renderInput() {
        const { children, name } = this.props;
        const { inputFocused } = this.state;
        return React.Children.map(children, child =>
            React.cloneElement(child, {
                name,
                focused: inputFocused,
                onChange: this.handleInputChange,
                onFocus: this.handleInputFocus,
                onBlur: this.handleInputBlur
            })
        );
    }

    render() {
        const { value, suggestions } = this.props;
        const { inputFocused, selectedIndex } = this.state;

        const expandable = !!(inputFocused && suggestions.length);

        return (
            <div className={styles.autoComplete} onKeyDown={this.handleKeyDown}>
                {this.renderInput()}
                <Suggestions
                    inputValue={value}
                    suggestions={suggestions}
                    selectedIndex={selectedIndex}
                    expandable={expandable}
                    onSelection={this.submitValue}
                />
            </div>
        );
    }
}

AutoComplete.propTypes = {
    children: PropTypes.node,
    value: PropTypes.string,
    name: PropTypes.string,
    suggestions: suggestionsType,
    minValueLength: PropTypes.number,
    maxSuggestionsLength: PropTypes.number,
    delimiterChars: PropTypes.arrayOf(PropTypes.string),
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onInputChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onCtrlBackspace: PropTypes.func,
    onEscape: PropTypes.func,
    fetchSuggestions: PropTypes.func,
    clearSuggestions: PropTypes.func
};

AutoComplete.defaultProps = {
    children: null,
    value: '',
    name: '',
    suggestions: [],
    minValueLength: 2,
    maxSuggestionsLength: 10,
    delimiterChars: [',', 'Enter'],
    onFocus: noop,
    onBlur: noop,
    onInputChange: noop,
    onSubmit: noop,
    onCtrlBackspace: noop,
    onEscape: noop,
    fetchSuggestions: noop,
    clearSuggestions: noop
};

export default AutoComplete;
