import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './AutoComplete.css';
import noop from '../../utils/noop';
import { markIt } from './findMatches';

class SuggestionItem extends Component {
    handleMouseDown = event => {
        const { name, onSubmit } = this.props;
        event.preventDefault();
        onSubmit(name);
    };

    render() {
        const { name, inputValue, active } = this.props;
        const classes = classnames(styles.suggestionItem, {
            [styles.active]: active
        });

        return (
            <li className={classes} onMouseDown={this.handleMouseDown}>
                <span dangerouslySetInnerHTML={markIt(name, inputValue)} />
            </li>
        );
    }
}

SuggestionItem.propTypes = {
    name: PropTypes.string,
    inputValue: PropTypes.string,
    active: PropTypes.bool,
    onSubmit: PropTypes.func
};

SuggestionItem.defaultProps = {
    name: '',
    inputValue: '',
    active: false,
    onSubmit: noop
};

export default SuggestionItem;
