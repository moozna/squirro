import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AutoComplete.css';
import { suggestionsType } from '../../types/types';
import noop from '../../utils/noop';
import SuggestionItem from './SuggestionItem';

class Suggestions extends Component {
    renderOptions() {
        const {
            inputValue,
            suggestions,
            selectedIndex,
            onSelection
        } = this.props;

        return suggestions.map((item, index) => (
            <SuggestionItem
                key={index}
                name={item.name}
                active={selectedIndex === index}
                disabled={item.disabled}
                inputValue={inputValue}
                onSubmit={onSelection}
            />
        ));
    }

    render() {
        const { expandable, suggestions } = this.props;

        if (!expandable || !suggestions.length) {
            return null;
        }

        return (
            <div className={styles.suggestions}>
                <ul role="listbox" className={styles.listBox}>
                    {this.renderOptions()}
                </ul>
            </div>
        );
    }
}

Suggestions.propTypes = {
    inputValue: PropTypes.string,
    suggestions: suggestionsType,
    selectedIndex: PropTypes.number,
    expandable: PropTypes.bool,
    onSelection: PropTypes.func
};

Suggestions.defaultProps = {
    inputValue: '',
    suggestions: [],
    selectedIndex: null,
    expandable: false,
    onSelection: noop
};

export default Suggestions;
