import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SuggestionItem from './SuggestionItem';

describe('<SuggestionItem />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<SuggestionItem />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a suggestion item with the specified text', () => {
        const wrapper = shallow(
            <SuggestionItem name="Science Fiction Fantasy" />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a suggestion item and indicates the matched input value in its text', () => {
        const wrapper = shallow(
            <SuggestionItem name="Science Fiction Fantasy" inputValue="Fant" />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a selected suggestion item', () => {
        const wrapper = shallow(
            <SuggestionItem name="Science Fiction Fantasy" active />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onSubmit()', () => {
        test('fires the onSubmit callback', () => {
            const mockMouseDownEvent = {
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(
                <SuggestionItem
                    name="Science Fiction Fantasy"
                    onSubmit={handleOnSubmit}
                />
            );
            wrapper
                .find('.suggestionItem')
                .simulate('mousedown', mockMouseDownEvent);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(
                'Science Fiction Fantasy'
            );
        });

        test('stops mouseDown event', () => {
            const mockMouseDownEvent = {
                preventDefault: jest.fn()
            };
            const handleOnSubmit = jest.fn();
            const wrapper = shallow(
                <SuggestionItem
                    name="Science Fiction Fantasy"
                    onSubmit={handleOnSubmit}
                />
            );
            wrapper
                .find('.suggestionItem')
                .simulate('mousedown', mockMouseDownEvent);

            expect(mockMouseDownEvent.preventDefault.mock.calls.length).toBe(1);
        });
    });
});
