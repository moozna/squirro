import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Shelf from './Shelf';

describe('<Shelf />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Shelf />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <Shelf shelfName="Best Book Cover Art" count={52} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an input field when it is the edited shelf', () => {
        const wrapper = shallow(
            <Shelf shelfName="Best Book Cover Art" isEditedShelf />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleSelectShelf()', () => {
        test('fires the onSelectShelf callback', () => {
            const handleOnSelectShelf = jest.fn();
            const shelfName = 'Best Book Cover Art';

            const wrapper = shallow(
                <Shelf
                    shelfName={shelfName}
                    onSelectShelf={handleOnSelectShelf}
                />
            );
            wrapper.instance().handleSelectShelf();

            expect(handleOnSelectShelf.mock.calls.length).toBe(1);
            expect(handleOnSelectShelf.mock.calls[0][0]).toBe(shelfName);
        });
    });

    describe('handleEdit()', () => {
        test('fires the onStartEditing callback', () => {
            const handleOnStartEditing = jest.fn();
            const shelfName = 'Best Book Cover Art';

            const wrapper = shallow(
                <Shelf
                    shelfName={shelfName}
                    onStartEditing={handleOnStartEditing}
                />
            );
            wrapper.instance().handleEdit();

            expect(handleOnStartEditing.mock.calls.length).toBe(1);
            expect(handleOnStartEditing.mock.calls[0][0]).toBe(shelfName);
        });
    });

    describe('handleSubmit()', () => {
        test('fires the onSubmit callback', () => {
            const handleOnSubmit = jest.fn();
            const shelfName = 'Best Book Cover Art';
            const shelfData = {
                shelfName: 'Best Books'
            };

            const wrapper = shallow(
                <Shelf shelfName={shelfName} onSubmit={handleOnSubmit} />
            );
            wrapper.instance().handleSubmit(shelfData);

            expect(handleOnSubmit.mock.calls.length).toBe(1);
            expect(handleOnSubmit.mock.calls[0][0]).toBe(shelfName);
            expect(handleOnSubmit.mock.calls[0][1]).toEqual(shelfData);
        });
    });

    describe('handleDelete()', () => {
        test('fires the onDelete callback', () => {
            const handleOnDelete = jest.fn();
            const shelfName = 'Best Book Cover Art';

            const wrapper = shallow(
                <Shelf shelfName={shelfName} onDelete={handleOnDelete} />
            );
            wrapper.instance().handleDelete();

            expect(handleOnDelete.mock.calls.length).toBe(1);
            expect(handleOnDelete.mock.calls[0][0]).toBe(shelfName);
        });
    });
});
