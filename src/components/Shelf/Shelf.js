import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import ShelfItem from './ShelfItem';
import ShelfInput from './ShelfInput';

class Shelf extends Component {
    handleSelectShelf = () => {
        const { shelfName, onSelectShelf } = this.props;
        onSelectShelf(shelfName);
    };

    handleEdit = () => {
        const { shelfName, onStartEditing } = this.props;
        onStartEditing(shelfName);
    };

    handleSubmit = shelfData => {
        const { shelfName, onSubmit } = this.props;
        onSubmit(shelfName, shelfData);
    };

    handleDelete = () => {
        const { shelfName, onDelete } = this.props;
        onDelete(shelfName);
    };

    render() {
        const {
            shelfName,
            count,
            isEditedShelf,
            isEditingProgress,
            onCancel
        } = this.props;

        if (isEditedShelf) {
            return (
                <ShelfInput
                    initialValues={{ shelfName }}
                    onSubmit={this.handleSubmit}
                    onCancel={onCancel}
                />
            );
        }

        return (
            <ShelfItem
                shelfName={shelfName}
                count={count}
                isEditingProgress={isEditingProgress}
                onSelectShelf={this.handleSelectShelf}
                onStartEditing={this.handleEdit}
                onDelete={this.handleDelete}
            />
        );
    }
}

Shelf.propTypes = {
    shelfName: PropTypes.string,
    count: PropTypes.number,
    isEditedShelf: PropTypes.bool,
    isEditingProgress: PropTypes.bool,
    onSelectShelf: PropTypes.func,
    onStartEditing: PropTypes.func,
    onSubmit: PropTypes.func,
    onDelete: PropTypes.func,
    onCancel: PropTypes.func
};

Shelf.defaultProps = {
    shelfName: '',
    count: null,
    isEditedShelf: false,
    isEditingProgress: false,
    onSelectShelf: noop,
    onStartEditing: noop,
    onSubmit: noop,
    onDelete: noop,
    onCancel: noop
};

export default Shelf;
