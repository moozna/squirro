import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link';
import { booksPage } from '../../data/actions/pageActions';
import styles from './Shelf.css';
import noop from '../../utils/noop';
import ListItem from '../List/ListItem';
import Button from '../Button/Button';
import Edit from '../svg/Edit';
import DeleteButton from '../Button/DeleteButton';

class ShelfItem extends Component {
    state = {
        isHovered: false
    };

    handleMouseEnter = () => {
        this.setState({ isHovered: true });
    };

    handleMouseLeave = () => {
        this.setState({ isHovered: false });
    };

    render() {
        const {
            shelfName,
            count,
            isEditingProgress,
            onSelectShelf,
            onStartEditing,
            onDelete
        } = this.props;
        const { isHovered } = this.state;
        const isActionsVisible = !isEditingProgress && isHovered;

        return (
            <ListItem
                itemId={shelfName}
                title={shelfName}
                sideContent={isHovered && !isEditingProgress ? null : count}
                linkComponent={Link}
                to={booksPage()}
                disabled={isEditingProgress}
                onItemClick={onSelectShelf}
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
            >
                {isActionsVisible && (
                    <div className={styles.actionButtons}>
                        <Button onClick={onStartEditing}>
                            <Edit />
                        </Button>
                        <DeleteButton name="shelf" onDelete={onDelete} />
                    </div>
                )}
            </ListItem>
        );
    }
}

ShelfItem.propTypes = {
    shelfName: PropTypes.string,
    count: PropTypes.number,
    isEditingProgress: PropTypes.bool,
    onSelectShelf: PropTypes.func,
    onStartEditing: PropTypes.func,
    onDelete: PropTypes.func
};

ShelfItem.defaultProps = {
    shelfName: '',
    count: null,
    isEditingProgress: false,
    onSelectShelf: noop,
    onStartEditing: noop,
    onDelete: noop
};

export default ShelfItem;
