import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ShelfItem from './ShelfItem';

describe('<ShelfItem />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<ShelfItem />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <ShelfItem shelfName="Best Book Cover Art" count={35} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders action buttons on hover', () => {
        const wrapper = shallow(<ShelfItem />);
        wrapper.setState({ isHovered: true });

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render action buttons when editing is in progress', () => {
        const wrapper = shallow(<ShelfItem isEditingProgress />);
        wrapper.setState({ isHovered: true });

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets isHovered to true on mouseEnter', () => {
        const wrapper = shallow(<ShelfItem />);

        wrapper.instance().handleMouseEnter();
        expect(wrapper.state('isHovered')).toBe(true);
    });

    test('sets isHovered to false on mouseLeave', () => {
        const wrapper = shallow(<ShelfItem />);
        wrapper.setState({ isHovered: true });

        wrapper.instance().handleMouseLeave();
        expect(wrapper.state('isHovered')).toBe(false);
    });
});
