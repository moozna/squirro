import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Shelf.css';
import noop from '../../utils/noop';
import withForm from '../Form/Form';
import ListItem from '../List/ListItem';
import InputField from '../Form/InputField';
import Button from '../Button/Button';
import Check from '../svg/Check';
import Close from '../svg/Close';

export class ShelfInput extends Component {
    componentDidMount() {
        if (this.inputField) {
            this.inputField.focus();
        }
    }

    saveRef = node => {
        this.inputField = node;
    };

    render() {
        const {
            values,
            touched,
            errors,
            isFormValid,
            isFormDirty,
            onChange,
            onBlur,
            onSubmit,
            onCancel
        } = this.props;

        const name = 'shelfName';
        const inputProps = {
            value: values[name],
            name,
            touched: touched[name],
            error: errors[name],
            onChange,
            onBlur
        };

        const isSubmittable = isFormDirty && isFormValid;

        return (
            <ListItem disabled>
                <InputField
                    {...inputProps}
                    saveRef={this.saveRef}
                    flexibleWidth
                />
                <div className={styles.actionButtons}>
                    <Button disabled={!isSubmittable} onClick={onSubmit}>
                        <Check />
                    </Button>
                    <Button onClick={onCancel}>
                        <Close />
                    </Button>
                </div>
            </ListItem>
        );
    }
}

ShelfInput.propTypes = {
    values: PropTypes.shape({ shelfName: PropTypes.string }),
    touched: PropTypes.shape({ shelfName: PropTypes.bool }),
    errors: PropTypes.shape({ shelfName: PropTypes.string }),
    isFormValid: PropTypes.bool,
    isFormDirty: PropTypes.bool,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func,
    onBlur: PropTypes.func
};

ShelfInput.defaultProps = {
    values: {},
    touched: {},
    errors: {},
    isFormValid: null,
    isFormDirty: false,
    onChange: noop,
    onSubmit: noop,
    onCancel: noop,
    onBlur: noop
};

export default withForm(ShelfInput);
