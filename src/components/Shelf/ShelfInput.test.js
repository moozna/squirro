import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import ShelfInputWithForm, { ShelfInput } from './ShelfInput';

describe('<ShelfInput />', () => {
    describe('<ShelfInputWithForm />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ShelfInputWithForm />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });

    describe('<ShelfInput />', () => {
        test('has the expected html structure with default props', () => {
            const wrapper = shallow(<ShelfInput />);

            expect(toJson(wrapper)).toMatchSnapshot();
        });

        test('renders a disabled button when the form is not dirty', () => {
            const wrapper = shallow(
                <ShelfInput isFormDirty={false} isFormValid={true} />
            );
            const button = wrapper.find('Button').first();

            expect(button.prop('disabled')).toBe(true);
        });

        test('renders a disabled button when the form is invalid', () => {
            const wrapper = shallow(
                <ShelfInput isFormDirty={true} isFormValid={false} />
            );
            const button = wrapper.find('Button').first();

            expect(button.prop('disabled')).toBe(true);
        });

        test('has a ref on the input field', () => {
            const wrapper = mount(<ShelfInput />);

            expect(wrapper.instance().inputField).toBeDefined();
        });

        test('sets the input field to focused', () => {
            const mockFocus = jest.fn();
            const wrapper = shallow(<ShelfInput />);
            const instance = wrapper.instance();
            instance.inputField = {
                focus: mockFocus
            };
            instance.componentDidMount();

            expect(mockFocus.mock.calls.length).toBe(1);
        });
    });
});
