import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './LoadingIndicator.css';

const LoadingIndicator = ({ className }) => {
    const classes = classnames(styles.loadingIndicator, className);
    return (
        <div className={classes}>
            <div className={styles.loader}>
                <div className={styles.loaderOutter} />
                <div className={styles.loaderInner} />
            </div>
        </div>
    );
};

LoadingIndicator.propTypes = {
    className: PropTypes.string
};

LoadingIndicator.defaultProps = {
    className: ''
};

export default LoadingIndicator;
