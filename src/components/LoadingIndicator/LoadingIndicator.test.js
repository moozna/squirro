import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import LoadingIndicator from './LoadingIndicator';

describe('<LoadingIndicator />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<LoadingIndicator />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a custom class', () => {
        const wrapper = shallow(<LoadingIndicator className="myClass" />);

        expect(wrapper.hasClass('myClass')).toBe(true);
    });
});
