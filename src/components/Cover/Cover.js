import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Cover.css';
import Placeholder from './Placeholder';

class Cover extends Component {
    state = {
        isLoading: true,
        isFailed: false
    };

    onLinkError = () => {
        this.setState({ isLoading: false, isFailed: true });
    };

    onLoad = () => {
        this.setState({ isLoading: false, isFailed: false });
    };

    render() {
        const { imagePath, altText, placeholderClass } = this.props;
        const { isLoading, isFailed } = this.state;
        const isPlaceholderActive = isLoading || isFailed;

        const classes = classnames(styles.cover, {
            [styles.hidden]: isPlaceholderActive
        });

        return (
            <Fragment>
                <img
                    className={classes}
                    src={imagePath}
                    onLoad={this.onLoad}
                    onError={this.onLinkError}
                    alt={altText}
                />
                {isPlaceholderActive ? (
                    <Placeholder className={placeholderClass} />
                ) : null}
            </Fragment>
        );
    }
}

Cover.propTypes = {
    imagePath: PropTypes.string,
    altText: PropTypes.string,
    placeholderClass: PropTypes.string
};

Cover.defaultProps = {
    imagePath: '',
    altText: '',
    placeholderClass: ''
};

export default Cover;
