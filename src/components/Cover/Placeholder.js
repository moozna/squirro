import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Cover.css';
import Butterfly from '../svg/Butterfly';

const Placeholder = ({ className }) => {
    const classes = classnames(styles.placeholder, className);

    return (
        <div className={classes}>
            <Butterfly
                outlineClass={styles.imageOutline}
                innerClass={styles.imageInner}
            />
        </div>
    );
};

Placeholder.propTypes = {
    className: PropTypes.string
};

Placeholder.defaultProps = {
    className: ''
};

export default Placeholder;
