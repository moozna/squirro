import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Cover from './Cover';

describe('<Cover />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Cover />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <Cover imagePath="imagePath" altText="title" />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure after image error', () => {
        const wrapper = shallow(
            <Cover imagePath="imagePath" altText="title" />
        );
        wrapper.find('.cover').simulate('error');

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure after successful image download', () => {
        const wrapper = shallow(
            <Cover imagePath="imagePath" altText="title" />
        );
        wrapper.find('.cover').simulate('load');

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('state changes', () => {
        test('has correct values by default', () => {
            const wrapper = shallow(<Cover />);
            expect(wrapper.state('isLoading')).toBe(true);
            expect(wrapper.state('isFailed')).toBe(false);
        });

        test('has correct values after image error', () => {
            const wrapper = shallow(<Cover />);
            wrapper.find('.cover').simulate('error');
            expect(wrapper.state('isLoading')).toBe(false);
            expect(wrapper.state('isFailed')).toBe(true);
        });

        test('has correct values after successful image download', () => {
            const wrapper = shallow(<Cover />);
            wrapper.find('.cover').simulate('load');
            expect(wrapper.state('isLoading')).toBe(false);
            expect(wrapper.state('isFailed')).toBe(false);
        });
    });
});
