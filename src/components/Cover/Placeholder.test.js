import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Placeholder from './Placeholder';

describe('<Placeholder />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Placeholder />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a placeholder with custom class', () => {
        const wrapper = shallow(<Placeholder className="myClass" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
