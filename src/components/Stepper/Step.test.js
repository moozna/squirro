import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Step from './Step';

describe('<Step />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Step />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an icon', () => {
        const wrapper = shallow(<Step icon={<div className="icon" />} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label', () => {
        const wrapper = shallow(<Step label="myLabel" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an active step', () => {
        const wrapper = shallow(<Step active />);

        expect(wrapper.hasClass('active')).toBe(true);
    });

    describe('handleClick()', () => {
        test('fires the onClick callback', () => {
            const handleOnClick = jest.fn();

            const wrapper = shallow(<Step onClick={handleOnClick} />);
            wrapper.find('.step').simulate('click');

            expect(handleOnClick.mock.calls.length).toBe(1);
        });
    });
});
