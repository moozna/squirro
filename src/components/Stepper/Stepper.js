import React from 'react';
import PropTypes from 'prop-types';
import styles from './Stepper.css';

const Stepper = ({ children, activeIndex }) => (
    <ul className={styles.stepper}>
        {React.Children.map(children, (child, index) =>
            React.cloneElement(child, {
                key: index,
                active: index <= activeIndex
            })
        )}
    </ul>
);

Stepper.propTypes = {
    children: PropTypes.node,
    activeIndex: PropTypes.number
};

Stepper.defaultProps = {
    children: null,
    activeIndex: -1
};

export default Stepper;
