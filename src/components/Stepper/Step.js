import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Stepper.css';
import noop from '../../utils/noop';

class Step extends Component {
    handleClick = () => {
        const { onClick } = this.props;
        onClick();
    };

    render() {
        const { icon, label, active } = this.props;

        const classes = classnames(styles.step, {
            [styles.active]: active
        });

        return (
            <li className={classes} onClick={this.handleClick}>
                <button type="button" className={styles.stepIcon}>
                    {icon}
                </button>
                {label}
            </li>
        );
    }
}

Step.propTypes = {
    icon: PropTypes.node,
    label: PropTypes.string,
    active: PropTypes.bool,
    onClick: PropTypes.func
};

Step.defaultProps = {
    icon: null,
    label: '',
    active: false,
    onClick: noop
};

export default Step;
