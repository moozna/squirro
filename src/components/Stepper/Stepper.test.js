import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Stepper from './Stepper';
import Step from './Step';

describe('<Stepper />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Stepper />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Stepper>{testChildren}</Stepper>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders active children', () => {
        const testChildren = [
            <Step key="1" />,
            <Step key="2" />,
            <Step key="3" />
        ];
        const wrapper = shallow(
            <Stepper activeIndex={1}>{testChildren}</Stepper>
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
