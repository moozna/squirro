import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Link from 'redux-first-router-link';
import styles from './Pagination.css';
import noop from '../../utils/noop';
import Button from '../Button/Button';

class PaginationItem extends Component {
    getLinkProps() {
        const { pageNumber, createHref } = this.props;
        const to = createHref(pageNumber);
        return to ? { component: Link, to } : {};
    }

    handleClick = () => {
        const { pageNumber, active, disabled, onClick } = this.props;

        if (!active && !disabled) {
            onClick(pageNumber);
        }
    };

    render() {
        const { label, active, disabled } = this.props;

        const classes = classnames(styles.halo, styles.magenta, {
            [styles.active]: active,
            [styles.disabled]: disabled
        });
        const linkProps = this.getLinkProps();

        return (
            <li className={styles.paginationItem}>
                <Button
                    className={classes}
                    color="none"
                    active={active}
                    disabled={disabled}
                    onClick={this.handleClick}
                    {...linkProps}
                >
                    {label}
                </Button>
            </li>
        );
    }
}

PaginationItem.propTypes = {
    pageNumber: PropTypes.number,
    label: PropTypes.string,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    createHref: PropTypes.func,
    onClick: PropTypes.func
};

PaginationItem.defaultProps = {
    pageNumber: 1,
    label: '',
    active: false,
    disabled: false,
    createHref: noop,
    onClick: noop
};

export default PaginationItem;
