import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PrevButton from './PrevButton';

describe('<PrevButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<PrevButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with the previous page number', () => {
        const wrapper = shallow(<PrevButton activePage={3} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('pageNumber')).toBe(2);
    });

    test('renders a disabled button when activePage is the first page', () => {
        const wrapper = shallow(<PrevButton activePage={1} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('disabled')).toBe(true);
    });

    test('renders a not disabled button when there are more pages before activePage', () => {
        const wrapper = shallow(<PrevButton activePage={2} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('disabled')).toBe(false);
    });
});
