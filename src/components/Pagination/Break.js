import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import PaginationItem from './PaginationItem';

class Break extends Component {
    getPageNumber() {
        const { startPageOfBreak, activePage } = this.props;

        return activePage < startPageOfBreak
            ? this.jumpForward()
            : this.jumpBackward();
    }

    jumpForward() {
        const { activePage, pageCount, numberOfDisplayedPages } = this.props;
        const newPageNumber = activePage + numberOfDisplayedPages;
        return Math.min(newPageNumber, pageCount);
    }

    jumpBackward() {
        const { activePage, numberOfDisplayedPages } = this.props;
        const newPageNumber = activePage - numberOfDisplayedPages;
        return Math.max(1, newPageNumber);
    }

    render() {
        const { createHref, onPageSelect } = this.props;

        return (
            <PaginationItem
                pageNumber={this.getPageNumber()}
                label="..."
                createHref={createHref}
                onClick={onPageSelect}
            />
        );
    }
}

Break.propTypes = {
    startPageOfBreak: PropTypes.number,
    pageCount: PropTypes.number,
    numberOfDisplayedPages: PropTypes.number,
    activePage: PropTypes.number,
    createHref: PropTypes.func,
    onPageSelect: PropTypes.func
};

Break.defaultProps = {
    startPageOfBreak: 0,
    pageCount: 0,
    numberOfDisplayedPages: 5,
    activePage: 0,
    createHref: noop,
    onPageSelect: noop
};

export default Break;
