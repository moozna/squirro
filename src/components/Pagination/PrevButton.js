import React from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import PaginationItem from './PaginationItem';

const PrevButton = ({ activePage, createHref, onPageSelect }) => {
    const isFirstPageActive = activePage <= 1;

    return (
        <PaginationItem
            pageNumber={activePage - 1}
            label="&lt;"
            createHref={createHref}
            onClick={onPageSelect}
            disabled={isFirstPageActive}
        />
    );
};

PrevButton.propTypes = {
    activePage: PropTypes.number,
    createHref: PropTypes.func,
    onPageSelect: PropTypes.func
};

PrevButton.defaultProps = {
    activePage: 0,
    createHref: noop,
    onPageSelect: noop
};

export default PrevButton;
