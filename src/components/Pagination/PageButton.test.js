import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PageButton from './PageButton';

describe('<PageButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<PageButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with the specified page number', () => {
        const wrapper = shallow(<PageButton pageNumber={7} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('pageNumber')).toBe(7);
        expect(button.prop('label')).toBe('7');
    });

    test('renders an active button when the specified page number is the active page', () => {
        const wrapper = shallow(<PageButton activePage={7} pageNumber={7} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('active')).toBe(true);
    });
});
