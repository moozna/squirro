import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Pagination.css';
import noop from '../../utils/noop';
import PageButton from './PageButton';
import PrevButton from './PrevButton';
import NextButton from './NextButton';
import Break from './Break';

class Pagination extends Component {
    createBreakElement = page => {
        const {
            pageCount,
            activePage,
            numberOfDisplayedPages,
            createHref,
            onPageSelect
        } = this.props;

        return (
            <Break
                key={page}
                startPageOfBreak={page}
                pageCount={pageCount}
                numberOfDisplayedPages={numberOfDisplayedPages}
                activePage={activePage}
                createHref={createHref}
                onPageSelect={onPageSelect}
            />
        );
    };

    createPageElement(page) {
        const { activePage, createHref, onPageSelect } = this.props;

        return (
            <PageButton
                key={page}
                pageNumber={page}
                activePage={activePage}
                createHref={createHref}
                onPageSelect={onPageSelect}
            />
        );
    }

    hasOnlyRightSideBreak(neighbours) {
        const { activePage } = this.props;
        return activePage < neighbours;
    }

    hasOnlyLeftSideBreak(neighbours) {
        const { pageCount, activePage } = this.props;
        return activePage > pageCount - neighbours;
    }

    isFixedStartPage(page) {
        const { numberOfEndPages } = this.props;
        return page <= numberOfEndPages;
    }

    isFixedEndPage(page) {
        const { pageCount, numberOfEndPages } = this.props;
        return page > pageCount - numberOfEndPages;
    }

    isPageInDefinedRange(page, leftNeigbours, rightNeigbours) {
        const { activePage } = this.props;
        return (
            page > activePage - leftNeigbours &&
            page <= activePage + rightNeigbours
        );
    }

    isDisplayedPage(page, leftNeigbours, rightNeigbours) {
        return (
            this.isFixedStartPage(page) ||
            this.isFixedEndPage(page) ||
            this.isPageInDefinedRange(page, leftNeigbours, rightNeigbours)
        );
    }

    shouldDisplayBreak(items, breakView) {
        const { numberOfEndPages } = this.props;
        return numberOfEndPages > 0 && items[items.length - 1] !== breakView;
    }

    renderPagination() {
        const { pageCount, activePage, numberOfDisplayedPages } = this.props;

        const items = [];

        const neighbours = numberOfDisplayedPages / 2;
        let leftNeigbours = neighbours;
        let rightNeigbours = numberOfDisplayedPages - leftNeigbours;

        if (this.hasOnlyLeftSideBreak(neighbours)) {
            rightNeigbours = pageCount - activePage;
            leftNeigbours = numberOfDisplayedPages - rightNeigbours;
        } else if (this.hasOnlyRightSideBreak(neighbours)) {
            leftNeigbours = activePage;
            rightNeigbours = numberOfDisplayedPages - leftNeigbours;
        }
        let breakView;

        for (let page = 1; page <= pageCount; page++) {
            if (this.isDisplayedPage(page, leftNeigbours, rightNeigbours)) {
                items.push(this.createPageElement(page));
            } else if (this.shouldDisplayBreak(items, breakView)) {
                breakView = this.createBreakElement(page);
                items.push(breakView);
            }
        }
        return items;
    }

    render() {
        const { activePage, pageCount, createHref, onPageSelect } = this.props;

        return (
            <ul className={styles.pagination}>
                <PrevButton
                    activePage={activePage}
                    createHref={createHref}
                    onPageSelect={onPageSelect}
                />

                {this.renderPagination()}

                <NextButton
                    pageCount={pageCount}
                    activePage={activePage}
                    createHref={createHref}
                    onPageSelect={onPageSelect}
                />
            </ul>
        );
    }
}

Pagination.propTypes = {
    pageCount: PropTypes.number,
    numberOfDisplayedPages: PropTypes.number,
    numberOfEndPages: PropTypes.number,
    activePage: PropTypes.number,
    createHref: PropTypes.func,
    onPageSelect: PropTypes.func
};

Pagination.defaultProps = {
    pageCount: 0,
    numberOfDisplayedPages: 5,
    numberOfEndPages: 1,
    activePage: 0,
    createHref: noop,
    onPageSelect: noop
};

export default Pagination;
