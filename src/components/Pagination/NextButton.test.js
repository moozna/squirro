import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import NextButton from './NextButton';

describe('<NextButton />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<NextButton />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with the next page number', () => {
        const wrapper = shallow(<NextButton activePage={2} pageCount={3} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('pageNumber')).toBe(3);
    });

    test('renders a disabled button when activePage is the last page', () => {
        const wrapper = shallow(<NextButton activePage={3} pageCount={3} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('disabled')).toBe(true);
    });

    test('renders a not disabled button when there are more pages after activePage', () => {
        const wrapper = shallow(<NextButton activePage={2} pageCount={3} />);
        const button = wrapper.find('PaginationItem');

        expect(button.prop('disabled')).toBe(false);
    });
});
