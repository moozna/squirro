import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import PaginationItem from './PaginationItem';

describe('<PaginationItem />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<PaginationItem />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a button with the specified label', () => {
        const wrapper = shallow(<PaginationItem label="label" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an active pagination button', () => {
        const wrapper = shallow(<PaginationItem active />);
        const button = wrapper.find('Button');

        expect(button.hasClass('active')).toBe(true);
        expect(button.prop('active')).toBe(true);
    });

    test('renders a disabled pagination button', () => {
        const wrapper = shallow(<PaginationItem disabled />);
        const button = wrapper.find('Button');

        expect(button.hasClass('disabled')).toBe(true);
        expect(button.prop('disabled')).toBe(true);
    });

    test('calls createHref with pageNumber to construct url for a link', () => {
        const mockPageNumber = 2;
        const mockCreateHref = jest.fn();
        shallow(
            <PaginationItem
                pageNumber={mockPageNumber}
                createHref={mockCreateHref}
            />
        );

        expect(mockCreateHref.mock.calls[0][0]).toBe(mockPageNumber);
    });

    test('renders a link when createHref is provided', () => {
        const mockUrl = '/test?page=2';
        const mockCreateHref = jest.fn().mockReturnValue(mockUrl);
        const wrapper = shallow(<PaginationItem createHref={mockCreateHref} />);

        const button = wrapper.find('Button');
        expect(button.prop('to')).toBe(mockUrl);
    });

    test('fires the onClick callback', () => {
        const handleClick = jest.fn();
        const wrapper = shallow(<PaginationItem onClick={handleClick} />);

        wrapper.find('Button').simulate('click');
        expect(handleClick.mock.calls.length).toBe(1);
    });

    test('does not fire the onClick callback when already active', () => {
        const handleClick = jest.fn();
        const wrapper = shallow(
            <PaginationItem onClick={handleClick} active />
        );

        wrapper.find('Button').simulate('click');
        expect(handleClick.mock.calls.length).toBe(0);
    });

    test('does not fire the onClick callback when disabled', () => {
        const handleClick = jest.fn();
        const wrapper = shallow(
            <PaginationItem onClick={handleClick} disabled />
        );

        wrapper.find('Button').simulate('click');
        expect(handleClick.mock.calls.length).toBe(0);
    });
});
