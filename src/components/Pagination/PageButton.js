import React from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import PaginationItem from './PaginationItem';

const PageButton = ({ pageNumber, activePage, createHref, onPageSelect }) => (
    <PaginationItem
        pageNumber={pageNumber}
        label={String(pageNumber)}
        active={activePage === pageNumber}
        createHref={createHref}
        onClick={onPageSelect}
    />
);

PageButton.propTypes = {
    pageNumber: PropTypes.number,
    activePage: PropTypes.number,
    createHref: PropTypes.func,
    onPageSelect: PropTypes.func
};

PageButton.defaultProps = {
    pageNumber: 1,
    activePage: 0,
    createHref: noop,
    onPageSelect: noop
};

export default PageButton;
