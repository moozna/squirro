import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Break from './Break';

describe('<Break />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Break />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('forward jump', () => {
        const pageCount = 10;
        const numberOfDisplayedPages = 3;

        test('renders a button with the next page number when it is smaller than the last page', () => {
            const activePage = 5;

            const wrapper = shallow(
                <Break
                    startPageOfBreak={6}
                    pageCount={pageCount}
                    numberOfDisplayedPages={numberOfDisplayedPages}
                    activePage={activePage}
                />
            );

            const button = wrapper.find('PaginationItem');
            expect(button.prop('pageNumber')).toBe(
                activePage + numberOfDisplayedPages
            );
        });

        test('renders a button with the last page number when the next page would be bigger than the last page', () => {
            const wrapper = shallow(
                <Break
                    startPageOfBreak={9}
                    pageCount={pageCount}
                    numberOfDisplayedPages={numberOfDisplayedPages}
                    activePage={8}
                />
            );

            const button = wrapper.find('PaginationItem');
            expect(button.prop('pageNumber')).toBe(pageCount);
        });
    });

    describe('backward jump', () => {
        const pageCount = 10;
        const numberOfDisplayedPages = 3;

        test('renders a button with the previous page number when it is bigger then the first page', () => {
            const activePage = 5;

            const wrapper = shallow(
                <Break
                    startPageOfBreak={3}
                    pageCount={pageCount}
                    numberOfDisplayedPages={numberOfDisplayedPages}
                    activePage={activePage}
                />
            );

            const button = wrapper.find('PaginationItem');
            expect(button.prop('pageNumber')).toBe(
                activePage - numberOfDisplayedPages
            );
        });

        test('renders a button with the first page number when the next page would be smaller than the first page', () => {
            const wrapper = shallow(
                <Break
                    startPageOfBreak={1}
                    pageCount={pageCount}
                    numberOfDisplayedPages={numberOfDisplayedPages}
                    activePage={2}
                />
            );

            const button = wrapper.find('PaginationItem');
            expect(button.prop('pageNumber')).toBe(1);
        });
    });
});
