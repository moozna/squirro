import React from 'react';
import PropTypes from 'prop-types';
import noop from '../../utils/noop';
import PaginationItem from './PaginationItem';

const NextButton = ({ activePage, pageCount, createHref, onPageSelect }) => {
    const isLastPageActive = activePage >= pageCount;

    return (
        <PaginationItem
            pageNumber={activePage + 1}
            label="&gt;"
            createHref={createHref}
            onClick={onPageSelect}
            disabled={isLastPageActive}
        />
    );
};

NextButton.propTypes = {
    pageCount: PropTypes.number,
    activePage: PropTypes.number,
    createHref: PropTypes.func,
    onPageSelect: PropTypes.func
};

NextButton.defaultProps = {
    pageCount: 0,
    activePage: 0,
    createHref: noop,
    onPageSelect: noop
};

export default NextButton;
