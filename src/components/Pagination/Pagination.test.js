import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Pagination from './Pagination';

describe('<Pagination />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Pagination />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when pageCount is specified', () => {
        const wrapper = shallow(<Pagination pageCount={5} activePage={5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when number of displayed pages is specified', () => {
        const wrapper = shallow(
            <Pagination
                pageCount={10}
                activePage={2}
                numberOfDisplayedPages={2}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when end pages are specified', () => {
        const wrapper = shallow(
            <Pagination pageCount={10} activePage={2} numberOfEndPages={3} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when end pages are zero', () => {
        const wrapper = shallow(
            <Pagination pageCount={10} activePage={2} numberOfEndPages={0} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when break is displayed on the right side', () => {
        const wrapper = shallow(<Pagination pageCount={10} activePage={2} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when break is displayed on the left side', () => {
        const wrapper = shallow(<Pagination pageCount={10} activePage={8} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when break is displayed both side', () => {
        const wrapper = shallow(<Pagination pageCount={10} activePage={5} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
