import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import DataRow from './DataRow';

describe('<DataRow />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<DataRow />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are specified', () => {
        const amazonData = { asin: 'asin', title: 'title' };
        const goodreadsData = { goodreadsId: 12, title: 'title' };

        const wrapper = shallow(
            <DataRow
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                propName="title"
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label', () => {
        const wrapper = shallow(<DataRow label="myLabel" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the component with a single column', () => {
        const wrapper = shallow(<DataRow columns={1} />);

        expect(wrapper.hasClass('single')).toBe(true);
    });

    test('forwards values with multiple property names', () => {
        const amazonData = { asin: 'asin', title: 'title' };
        const goodreadsData = { goodreadsId: 12, title: 'title' };

        const wrapper = shallow(
            <DataRow
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                propName={{ goodreads: 'goodreadsId', amazon: 'asin' }}
            />
        );

        const goodreadsColumn = wrapper.find('Data').get(0);
        const amazonColumn = wrapper.find('Data').get(1);

        expect(goodreadsColumn.props.value).toBe(12);
        expect(amazonColumn.props.value).toBe('asin');
    });

    test('forwards values when propName is series', () => {
        const amazonData = {
            asin: 'asin',
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        };
        const goodreadsData = {
            goodreadsId: 12,
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        };

        const wrapper = shallow(
            <DataRow
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                propName="series"
            />
        );

        const goodreadsColumn = wrapper.find('Data').get(0);
        const amazonColumn = wrapper.find('Data').get(1);

        expect(goodreadsColumn.props.value).toEqual({
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        });
        expect(amazonColumn.props.value).toEqual({
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        });
    });

    test('forwards isDuplicate when propName is duplicated', () => {
        const amazonData = { asin: 'asin', title: 'title' };
        const goodreadsData = { goodreadsId: 12, title: 'title' };
        const duplicates = { title: true };

        const wrapper = shallow(
            <DataRow
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                duplicates={duplicates}
                propName="title"
            />
        );

        const goodreadsColumn = wrapper.find('Data').get(0);
        const amazonColumn = wrapper.find('Data').get(1);

        expect(goodreadsColumn.props.isDuplicate).toBe(false);
        expect(amazonColumn.props.isDuplicate).toBe(true);
    });

    test('forwards isDuplicate when propName is series and it is duplicated', () => {
        const amazonData = {
            asin: 'asin',
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        };
        const goodreadsData = {
            goodreadsId: 12,
            seriesTitle: 'seriesTitle',
            seriesIndex: '1'
        };
        const duplicates = { seriesTitle: true, seriesIndex: true };

        const wrapper = shallow(
            <DataRow
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                duplicates={duplicates}
                propName="series"
            />
        );

        const goodreadsColumn = wrapper.find('Data').get(0);
        const amazonColumn = wrapper.find('Data').get(1);

        expect(goodreadsColumn.props.isDuplicate).toBe(false);
        expect(amazonColumn.props.isDuplicate).toBe(true);
    });
});
