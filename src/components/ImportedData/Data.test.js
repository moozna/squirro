import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Data from './Data';

const TestComponent = ({ value }) => (
    <div className="testComponent">{value}</div>
);

describe('<Data />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Data />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when value is specified', () => {
        const wrapper = shallow(<Data value="value" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the value with the specified component', () => {
        const wrapper = shallow(
            <Data
                value="value"
                siteName="amazon"
                component={<TestComponent />}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a missing data', () => {
        const wrapper = shallow(<Data />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a duplicate data', () => {
        const wrapper = shallow(<Data value="value" isDuplicate />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
