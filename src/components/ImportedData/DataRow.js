import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './ImportedData.css';
import { goodreadsDataType, amazonDataType } from '../../types/types';
import Data from './Data';
import get from '../../utils/get';
import isObject from '../../utils/isObject';
import isEmptyObject from '../../utils/isEmptyObject';

const getSeriesData = values => {
    const seriesTitle = get(values, 'seriesTitle');
    const seriesIndex = get(values, 'seriesIndex');
    return {
        seriesTitle,
        seriesIndex
    };
};

class DataRow extends Component {
    getPropName(siteName) {
        const { propName } = this.props;
        return isObject(propName) ? propName[siteName] : propName;
    }

    getDataValue(values, siteName) {
        const propName = this.getPropName(siteName);
        return propName === 'series'
            ? getSeriesData(values)
            : get(values, propName);
    }

    isDuplicate() {
        const { duplicates } = this.props;
        const propName = this.getPropName('amazon');
        return propName === 'series'
            ? this.isDuplicateSeries()
            : get(duplicates, propName);
    }

    isDuplicateSeries() {
        const { duplicates } = this.props;
        return get(duplicates, 'seriesTitle') && get(duplicates, 'seriesIndex');
    }

    render() {
        const {
            label,
            goodreadsData,
            amazonData,
            columns,
            component
        } = this.props;

        const classes = classnames(styles.dataRow, {
            [styles.single]: columns === 1
        });

        return (
            <div className={classes}>
                <div className={styles.dataRowLabel}>{label}</div>
                {goodreadsData && !isEmptyObject(goodreadsData) && (
                    <div className={styles.goodreadsData}>
                        <Data
                            value={this.getDataValue(
                                goodreadsData,
                                'goodreads'
                            )}
                            component={component}
                            siteName="goodreads"
                        />
                    </div>
                )}
                {amazonData && !isEmptyObject(amazonData) && (
                    <div className={styles.amazonData}>
                        <Data
                            value={this.getDataValue(amazonData, 'amazon')}
                            isDuplicate={this.isDuplicate()}
                            component={component}
                            siteName="amazon"
                        />
                    </div>
                )}
            </div>
        );
    }
}

DataRow.propTypes = {
    label: PropTypes.string,
    propName: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.shape({
            goodreads: PropTypes.string,
            amazon: PropTypes.string
        })
    ]),
    goodreadsData: goodreadsDataType,
    amazonData: amazonDataType,
    duplicates: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    }),
    columns: PropTypes.number,
    component: PropTypes.node
};

DataRow.defaultProps = {
    label: '',
    propName: '',
    goodreadsData: null,
    amazonData: null,
    duplicates: {},
    columns: 2,
    component: null
};

export default DataRow;
