import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '../Tooltip/Tooltip';
import Duplicate from '../svg/Duplicate';

class Data extends Component {
    renderValue() {
        const { value, component, siteName } = this.props;

        if (component) {
            return (
                <div>{React.cloneElement(component, { value, siteName })}</div>
            );
        }
        return <div>{value}</div>;
    }

    render() {
        const { value, isDuplicate } = this.props;

        if (!value) {
            return <div>&mdash;</div>;
        }

        if (isDuplicate) {
            return (
                <div>
                    <Tooltip label="Duplicate">
                        <Duplicate iconSize={20} />
                    </Tooltip>
                </div>
            );
        }

        return this.renderValue();
    }
}

Data.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
        PropTypes.shape({
            seriesTitle: PropTypes.string,
            seriesIndex: PropTypes.string
        })
    ]),
    isDuplicate: PropTypes.bool,
    component: PropTypes.node,
    siteName: PropTypes.oneOf(['', 'amazon', 'goodreads'])
};

Data.defaultProps = {
    value: '',
    isDuplicate: false,
    component: null,
    siteName: ''
};

export default Data;
