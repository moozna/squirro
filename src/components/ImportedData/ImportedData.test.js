import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ImportedData from './ImportedData';

describe('<ImportedData />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ImportedData />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when values are specified', () => {
        const amazonData = { asin: 'asin', title: 'title' };
        const goodreadsData = { goodreadsId: 12, title: 'title' };
        const duplicates = { title: true };

        const wrapper = shallow(
            <ImportedData
                amazonData={amazonData}
                goodreadsData={goodreadsData}
                duplicates={duplicates}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders loading indicator when data import is in progress', () => {
        const wrapper = shallow(
            <ImportedData isLoading={{ amazon: true, goodreads: true }} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
