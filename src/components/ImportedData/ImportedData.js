import React from 'react';
import PropTypes from 'prop-types';
import styles from './ImportedData.css';
import { goodreadsDataType, amazonDataType } from '../../types/types';
import DataRow from './DataRow';
import Identifier from '../DetailsCard/Identifier';
import Authors from '../DetailsCard/Authors';
import SeriesTitle from '../DetailsCard/SeriesTitle';
import Votes from '../DetailsCard/Votes';
import Tags from '../Tags/Tags';
import Description from '../DetailsCard/Description';
import LoadingIndicator from '../LoadingIndicator/LoadingIndicator';

const ImportedData = ({
    goodreadsData,
    amazonData,
    duplicates,
    columns,
    isLoading
}) => {
    const data = {
        goodreadsData,
        amazonData,
        duplicates,
        columns
    };
    return (
        <div className={styles.importedData}>
            <DataRow
                label="ID/ASIN"
                propName={{ goodreads: 'goodreadsId', amazon: 'asin' }}
                {...data}
                component={<Identifier />}
            />
            <DataRow label="Title" propName="title" {...data} />
            <DataRow
                label="Series"
                propName="series"
                {...data}
                component={<SeriesTitle />}
            />
            <DataRow
                label="Author"
                propName="author"
                component={<Authors />}
                {...data}
            />
            <DataRow label="Year" propName="publicationYear" {...data} />
            <DataRow label="Pages" propName="pages" {...data} />
            <DataRow
                label="Rating"
                propName={{
                    goodreads: 'goodreadsRating.rating',
                    amazon: 'amazonRating.rating'
                }}
                {...data}
            />
            <DataRow
                label="Votes"
                propName={{
                    goodreads: 'goodreadsRating.numberOfRatings',
                    amazon: 'amazonRating.numberOfRatings'
                }}
                component={<Votes />}
                {...data}
            />
            <DataRow
                label="Genres"
                propName="originalGenres"
                component={<Tags color="orange" />}
                {...data}
            />
            <DataRow
                label="Description"
                propName="description"
                component={<Description />}
                {...data}
            />
            <div className={styles.importLoaders}>
                {isLoading.goodreads && (
                    <LoadingIndicator className={styles.goodreadsLoader} />
                )}
                {isLoading.amazon && (
                    <LoadingIndicator className={styles.amazonLoader} />
                )}
            </div>
        </div>
    );
};

ImportedData.propTypes = {
    goodreadsData: goodreadsDataType,
    amazonData: amazonDataType,
    duplicates: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    }),
    isLoading: PropTypes.shape({
        amazon: PropTypes.bool,
        goodreads: PropTypes.bool
    }),
    columns: PropTypes.number
};

ImportedData.defaultProps = {
    goodreadsData: null,
    amazonData: null,
    duplicates: {},
    isLoading: {},
    columns: 2
};

export default ImportedData;
