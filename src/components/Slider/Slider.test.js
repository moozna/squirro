import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Slider from './Slider';

describe('<Slider />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Slider />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const wrapper = shallow(
            <Slider
                name="test"
                value={4}
                label="Test slider"
                min={0}
                max={8}
                step={2}
                marks={[0, 2, 4, 6, 8]}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a readOnly slider', () => {
        const wrapper = shallow(<Slider readOnly />);
        const input = wrapper.find('input');

        expect(input.props().disabled).toBe(true);
    });

    test('fires the onChange callback', () => {
        const mockEvent = {
            target: {
                value: '3'
            }
        };
        const handleOnChange = jest.fn();

        const wrapper = shallow(
            <Slider name="test" onChange={handleOnChange} />
        );
        wrapper.find('input').simulate('change', mockEvent);

        expect(handleOnChange.mock.calls.length).toBe(1);
        expect(handleOnChange.mock.calls[0][0]).toBe('test');
        expect(handleOnChange.mock.calls[0][1]).toBe(3);
    });

    test('does not fire the onChange callback when readOnly', () => {
        const mockEvent = {
            target: {
                value: '3'
            }
        };
        const handleOnChange = jest.fn();
        const wrapper = shallow(
            <Slider name="test" onChange={handleOnChange} readOnly />
        );
        wrapper.find('input').simulate('change', mockEvent);

        expect(handleOnChange.mock.calls.length).toBe(0);
    });
});
