import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SliderLabel from './SliderLabel';

describe('<SliderLabel />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<SliderLabel />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label when value is 0', () => {
        const wrapper = shallow(<SliderLabel label="Test label" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render label when value is greater than 0', () => {
        const wrapper = shallow(<SliderLabel value={3} />);
        const title = wrapper.find('CSSTransition');

        expect(title.props().in).toBe(false);
    });
});
