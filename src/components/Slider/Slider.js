import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Slider.css';
import noop from '../../utils/noop';
import SliderLabel from './SliderLabel';
import SliderOutput from './SliderOutput';
import Marks from '../RangeSlider/Marks';

class Slider extends Component {
    handleChange = event => {
        const { name, readOnly, onChange } = this.props;
        const { value } = event.target;

        if (!readOnly) {
            onChange(name, Number(value));
        }
    };

    renderMarks() {
        const { min, max, marks } = this.props;

        if (!marks.length) {
            return null;
        }

        return (
            <div className={styles.marksLine}>
                <Marks min={min} max={max} marks={marks} centered />
            </div>
        );
    }

    render() {
        const {
            value,
            label,
            min,
            max,
            step,
            outputLabels,
            readOnly
        } = this.props;

        const sliderClasses = classnames(styles.sliderInput, {
            [styles.readOnly]: readOnly
        });

        return (
            <div className={styles.slider}>
                <div className={styles.header}>
                    <SliderLabel value={value} label={label} />
                    <SliderOutput value={value} outputLabels={outputLabels} />
                </div>
                <input
                    className={sliderClasses}
                    type="range"
                    value={value}
                    min={min}
                    max={max}
                    step={step}
                    disabled={readOnly}
                    onChange={this.handleChange}
                />
                {this.renderMarks()}
            </div>
        );
    }
}

Slider.propTypes = {
    name: PropTypes.string,
    value: PropTypes.number,
    label: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    outputLabels: PropTypes.object,
    marks: PropTypes.arrayOf(PropTypes.number),
    readOnly: PropTypes.bool,
    onChange: PropTypes.func
};

Slider.defaultProps = {
    name: '',
    value: 0,
    label: '',
    min: 0,
    max: 5,
    step: 1,
    outputLabels: {},
    marks: [],
    readOnly: false,
    onChange: noop
};

export default Slider;
