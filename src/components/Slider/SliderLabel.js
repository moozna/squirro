import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import styles from './Slider.css';
import SLIDER_HEADER_ANIMATION_CLASSES from './animation';

const SliderLabel = ({ value, label }) => (
    <CSSTransition
        in={!value}
        classNames={SLIDER_HEADER_ANIMATION_CLASSES}
        timeout={350}
        unmountOnExit
    >
        <div className={styles.label}>{label}</div>
    </CSSTransition>
);

SliderLabel.propTypes = {
    value: PropTypes.number,
    label: PropTypes.string
};

SliderLabel.defaultProps = {
    value: 0,
    label: ''
};

export default SliderLabel;
