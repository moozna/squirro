import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import SliderOutput from './SliderOutput';

describe('<SliderOutput />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<SliderOutput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label based on the value when value is greater than 0', () => {
        const mockOutputLabels = {
            1: 'First value',
            2: 'Second value',
            3: 'Third value'
        };
        const wrapper = shallow(
            <SliderOutput value={3} outputLabels={mockOutputLabels} />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render option text when value is 0', () => {
        const wrapper = shallow(<SliderOutput value={0} />);
        const output = wrapper.find('CSSTransition');

        expect(output.props().in).toBe(false);
    });
});
