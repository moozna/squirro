import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import styles from './Slider.css';
import SLIDER_HEADER_ANIMATION_CLASSES from './animation';

const SliderOutput = ({ value, outputLabels }) => {
    const textValue = outputLabels[value] || value;

    return (
        <CSSTransition
            in={!!value}
            classNames={SLIDER_HEADER_ANIMATION_CLASSES}
            timeout={350}
            unmountOnExit
        >
            <div className={styles.output}>{textValue}</div>
        </CSSTransition>
    );
};

SliderOutput.propTypes = {
    value: PropTypes.number,
    outputLabels: PropTypes.object
};

SliderOutput.defaultProps = {
    value: 0,
    outputLabels: {}
};

export default SliderOutput;
