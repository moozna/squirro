import styles from './Slider.css';

const SLIDER_HEADER_ANIMATION_CLASSES = {
    enter: styles.headerEnter,
    enterActive: styles.headerEnterActive,
    exit: styles.headerLeave,
    exitActive: styles.headerLeaveActive
};

export default SLIDER_HEADER_ANIMATION_CLASSES;
