import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Checkbox.css';
import noop from '../../utils/noop';
import ToggleInput from './ToggleInput';
import Check from './Check';

const Checkbox = ({
    children,
    fieldId,
    name,
    label,
    checked,
    disabled,
    labelClass,
    onChange
}) => {
    const classes = classnames(
        styles.field,
        {
            [styles.disabled]: disabled,
            [styles.checked]: checked
        },
        labelClass
    );

    return (
        <label className={classes}>
            <ToggleInput
                fieldId={fieldId}
                name={name}
                icon={<Check checked={checked} />}
                checked={checked}
                disabled={disabled}
                onChange={onChange}
            />
            {label ? <span className={styles.label}>{label}</span> : null}
            {children}
        </label>
    );
};

Checkbox.propTypes = {
    children: PropTypes.node,
    fieldId: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    labelClass: PropTypes.string,
    onChange: PropTypes.func
};

Checkbox.defaultProps = {
    children: null,
    fieldId: '',
    name: '',
    label: '',
    checked: false,
    disabled: false,
    labelClass: '',
    onChange: noop
};

export default Checkbox;
