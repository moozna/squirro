import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import ToggleInput from './ToggleInput';

describe('<ToggleInput />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ToggleInput />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a checked ToggleInput', () => {
        const wrapper = shallow(<ToggleInput checked />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled ToggleInput', () => {
        const wrapper = shallow(<ToggleInput disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an icon', () => {
        const wrapper = shallow(<ToggleInput icon={<div />} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('onChange()', () => {
        test('fires the onChange callback', () => {
            const handleOnChange = jest.fn();
            const wrapper = shallow(<ToggleInput onChange={handleOnChange} />);

            wrapper.find('.input').simulate('change');
            expect(handleOnChange.mock.calls.length).toBe(1);
        });

        test('does not fire the onChange callback when the ToggleInput is disabled', () => {
            const handleOnChange = jest.fn();
            const wrapper = shallow(
                <ToggleInput onChange={handleOnChange} disabled />
            );

            wrapper.find('.input').simulate('change');
            expect(handleOnChange.mock.calls.length).toBe(0);
        });
    });
});
