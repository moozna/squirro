import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Checkbox from './Checkbox';

describe('<Checkbox />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Checkbox />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Checkbox>{testChildren}</Checkbox>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders the provided label', () => {
        const wrapper = shallow(<Checkbox label="myLabel" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a checked checkbox', () => {
        const wrapper = shallow(<Checkbox checked />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a disabled checkbox', () => {
        const wrapper = shallow(<Checkbox disabled />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders label with a custom label class', () => {
        const wrapper = shallow(<Checkbox labelClass="myClass" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
