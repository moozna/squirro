import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Checkbox.css';
import noop from '../../utils/noop';

class ToggleInput extends Component {
    handleToggle = () => {
        const { fieldId, disabled, onChange } = this.props;

        if (!disabled) {
            onChange(fieldId);
        }
    };

    render() {
        const { name, type, icon, toggleClass, checked, disabled } = this.props;

        return (
            <span className={toggleClass}>
                {icon}
                <input
                    className={styles.input}
                    name={name}
                    type={type}
                    checked={checked}
                    disabled={disabled}
                    onChange={this.handleToggle}
                />
            </span>
        );
    }
}

ToggleInput.propTypes = {
    fieldId: PropTypes.string,
    name: PropTypes.string,
    type: PropTypes.string,
    icon: PropTypes.node,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    toggleClass: PropTypes.string,
    onChange: PropTypes.func
};

ToggleInput.defaultProps = {
    fieldId: '',
    name: '',
    type: 'checkbox',
    icon: null,
    checked: false,
    disabled: false,
    toggleClass: '',
    onChange: noop
};

export default ToggleInput;
