import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './Checkbox.css';

const Check = ({ children, checked }) => {
    const classes = classnames(styles.check, {
        [styles.checked]: checked
    });

    return <div className={classes}>{children}</div>;
};

Check.propTypes = {
    children: PropTypes.node,
    checked: PropTypes.bool
};

Check.defaultProps = {
    children: null,
    checked: false
};

export default Check;
