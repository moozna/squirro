import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Check from './Check';

describe('<Check />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Check />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders children by default', () => {
        const testChildren = <div />;
        const wrapper = shallow(<Check>{testChildren}</Check>);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a checked checkbox', () => {
        const wrapper = shallow(<Check checked />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
