import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { importBookPage, editImportPage } from '../../data/actions/pageActions';
import { getCurrentPage } from '../../data/reducers/pageReducer';
import styles from './ImportPage.css';
import noop from '../../utils/noop';
import ImportBook from '../../containers/ImportBook/ImportBook';
import EditImport from '../../containers/EditImport/EditImport';
import Stepper from '../../components/Stepper/Stepper';
import Step from '../../components/Stepper/Step';
import OpenBook from '../../components/svg/OpenBook';
import Import from '../../components/svg/Import';
import Edit from '../../components/svg/Edit';

export const ImportPage = ({ currentPage, importBookPage, editImportPage }) => {
    const components = {
        ImportBook: {
            component: ImportBook,
            index: 0
        },
        EditImport: {
            component: EditImport,
            index: 1
        }
    };

    const Page = components[currentPage].component;
    const activeStep = components[currentPage].index;

    return (
        <div className={styles.detailsCard}>
            <div className={styles.importHeader}>
                <OpenBook className={styles.icon} />
                <Stepper activeIndex={activeStep}>
                    <Step
                        icon={<Import iconSize={20} />}
                        label="Import Data"
                        onClick={importBookPage}
                    />
                    <Step
                        icon={<Edit iconSize={20} />}
                        label="Edit Imported Data"
                        onClick={editImportPage}
                    />
                </Stepper>
            </div>
            <Page />
        </div>
    );
};

ImportPage.propTypes = {
    currentPage: PropTypes.string,
    importBookPage: PropTypes.func,
    editImportPage: PropTypes.func
};

ImportPage.defaultProps = {
    currentPage: 'ImportBook',
    importBookPage: noop,
    editImportPage: noop
};

const mapStateToProps = state => ({
    currentPage: getCurrentPage(state)
});

const mapDispatchToProps = {
    importBookPage,
    editImportPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImportPage);
