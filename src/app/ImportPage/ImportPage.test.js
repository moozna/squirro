import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ImportPage } from './ImportPage';

describe('<ImportPage />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ImportPage />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when ImportBook page is active', () => {
        const wrapper = shallow(<ImportPage currentPage="ImportBook" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when EditImport page is active', () => {
        const wrapper = shallow(<ImportPage currentPage="EditImport" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
