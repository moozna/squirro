import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Page } from './Page';

describe('<Page />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Page />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when Books page is active', () => {
        const wrapper = shallow(<Page currentPage="Books" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when BookDetails page is active', () => {
        const wrapper = shallow(<Page currentPage="BookDetails" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when the url does not exist', () => {
        const wrapper = shallow(<Page currentPage="NotFound" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders a shrinked page', () => {
        const wrapper = shallow(<Page shrinked />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
