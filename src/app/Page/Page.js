import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { getCurrentPage } from '../../data/reducers/pageReducer';
import styles from './Page.css';
import AppBar from '../../components/AppBar/AppBar';
import Books from '../../containers/Books/Books';
import BookDetails from '../../containers/BookDetails/BookDetails';
import EditBook from '../../containers/EditBook/EditBook';
import AddBook from '../../containers/AddBook/AddBook';
import ImportPage from '../ImportPage/ImportPage';
import BookShelves from '../../containers/BookShelves/BookShelves';
import NotFound from '../NotFound/NotFound';
import Messages from '../../containers/Messages/Messages';
import ScrollToTop from '../../components/ScrollToTop/ScrollToTop';

export const Page = ({ currentPage, shrinked }) => {
    const classes = classnames(styles.page, {
        [styles.shrinked]: shrinked
    });

    const components = {
        Books,
        BookDetails,
        EditBook,
        AddBook,
        ImportBook: ImportPage,
        EditImport: ImportPage,
        BookShelves,
        NotFound
    };

    const Page = components[currentPage];

    return (
        <div className={classes}>
            <AppBar />
            <main className={styles.main}>
                <ScrollToTop>
                    <Page />
                </ScrollToTop>
            </main>
            <Messages />
        </div>
    );
};

Page.propTypes = {
    currentPage: PropTypes.string,
    shrinked: PropTypes.bool
};

Page.defaultProps = {
    currentPage: 'Books',
    shrinked: false
};

const mapStateToProps = state => ({
    currentPage: getCurrentPage(state)
});

export default connect(mapStateToProps)(Page);
