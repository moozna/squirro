import { connectRoutes } from 'redux-first-router';
import queryString from 'query-string';
import {
    navigateToHome,
    navigateToBooks,
    navigateToBookDetails,
    navigateToEditBook,
    navigateToEditImport,
    navigateToShelves
} from '../data/actions/routingActions';

const routesMap = {
    HOME: {
        path: '/',
        thunk: navigateToHome()
    },
    BOOKS: {
        path: '/books/:page?',
        thunk: navigateToBooks()
    },
    ADD: {
        path: '/book/add'
    },
    IMPORT: {
        path: '/book/import'
    },
    EDIT_IMPORT: {
        path: '/book/import/edit',
        thunk: navigateToEditImport()
    },
    BOOK: {
        path: '/book/:id',
        thunk: navigateToBookDetails()
    },
    EDIT: {
        path: '/book/:id/edit',
        thunk: navigateToEditBook()
    },
    SHELVES: {
        path: '/shelves',
        thunk: navigateToShelves()
    }
};

export const { reducer, middleware, enhancer } = connectRoutes(routesMap, {
    querySerializer: queryString
});
