import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import configureStore from '../../data/configureStore';
import App from './App';

describe('<App />', () => {
    test('renders without crashing', () => {
        const store = configureStore();
        const div = document.createElement('div');
        ReactDOM.render(
            <Provider store={store}>
                <App />
            </Provider>,
            div
        );
        ReactDOM.unmountComponentAtNode(div);
    });

    test('renders a closed sidebar by default', () => {
        const wrapper = shallow(<App />);

        expect(wrapper.state('isSidebarOpen')).toBe(false);
    });

    test('opens the sidebar', () => {
        const wrapper = shallow(<App />);

        wrapper.instance().toggleSidebarOpened();
        expect(wrapper.state('isSidebarOpen')).toBe(true);
    });
});
