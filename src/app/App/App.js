import React, { Component } from 'react';
import styles from './App.css';
import Sidebar from '../../components/Sidebar/Sidebar';
import Page from '../Page/Page';
import TabNavigation from '../../containers/TabNavigation/TabNavigation';

class App extends Component {
    state = {
        isSidebarOpen: false
    };

    toggleSidebarOpened = () => {
        this.setState(prevState => ({
            isSidebarOpen: !prevState.isSidebarOpen
        }));
    };

    render() {
        const { isSidebarOpen } = this.state;

        return (
            <div className={styles.app}>
                <Sidebar
                    opened={isSidebarOpen}
                    onToggleClick={this.toggleSidebarOpened}
                >
                    <TabNavigation />
                </Sidebar>
                <Page shrinked={isSidebarOpen} />
            </div>
        );
    }
}

export default App;
