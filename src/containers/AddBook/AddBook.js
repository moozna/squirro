import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    uploadCoverImage,
    deleteCoverImage
} from '../../data/actions/imageActions';
import { saveAddedBook } from '../../data/actions/newBookActions';
import { booksPage } from '../../data/actions/pageActions';
import { getSuggestions } from '../../data/reducers/suggestionsReducer';
import { suggestionGroupType } from '../../types/types';
import noop from '../../utils/noop';
import DetailsForm from '../../components/DetailsForm/DetailsForm';
import bookValidation from '../../components/DetailsForm/bookValidation';

export class AddBook extends Component {
    handleSubmit = bookData => {
        const { saveAddedBook } = this.props;
        saveAddedBook(bookData);
    };

    handleCancel = () => {
        const { booksPage } = this.props;
        booksPage();
    };

    render() {
        const { suggestions, uploadCoverImage, deleteCoverImage } = this.props;

        return (
            <DetailsForm
                suggestions={suggestions}
                validate={bookValidation}
                uploadCoverImage={uploadCoverImage}
                deleteImages={deleteCoverImage}
                onSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
            />
        );
    }
}

AddBook.propTypes = {
    suggestions: suggestionGroupType,
    uploadCoverImage: PropTypes.func,
    deleteCoverImage: PropTypes.func,
    booksPage: PropTypes.func,
    saveAddedBook: PropTypes.func
};

AddBook.defaultProps = {
    suggestions: {},
    uploadCoverImage: noop,
    deleteCoverImage: noop,
    booksPage: noop,
    saveAddedBook: noop
};

const mapStateToProps = state => ({
    suggestions: getSuggestions(state)
});

const mapDispatchToProps = {
    uploadCoverImage,
    deleteCoverImage,
    saveAddedBook,
    booksPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddBook);
