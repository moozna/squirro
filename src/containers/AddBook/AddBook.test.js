import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { AddBook } from './AddBook';

describe('<AddBook />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<AddBook />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleSubmit()', () => {
        test('calls saveAddedBook', () => {
            const mockBookData = {
                _id: '1',
                asin: 'asin',
                title: 'title'
            };
            const mockSaveAddedBook = jest.fn();

            const wrapper = shallow(
                <AddBook saveAddedBook={mockSaveAddedBook} />
            );
            wrapper.instance().handleSubmit(mockBookData);

            expect(mockSaveAddedBook.mock.calls.length).toBe(1);
            expect(mockSaveAddedBook.mock.calls[0][0]).toEqual(mockBookData);
        });
    });

    describe('handleCancel()', () => {
        test('navigates to books page', () => {
            const mockBooksPage = jest.fn();

            const wrapper = shallow(<AddBook booksPage={mockBooksPage} />);
            wrapper.instance().handleCancel();

            expect(mockBooksPage.mock.calls.length).toBe(1);
        });
    });
});
