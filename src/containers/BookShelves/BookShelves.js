import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    selectShelfByName,
    saveEditedShelf,
    deleteSelectedShelf
} from '../../data/actions/shelvesActions';
import { getShelves } from '../../data/reducers/shelvesReducer';
import styles from './BookShelves.css';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import List from '../../components/List/List';
import Shelf from '../../components/Shelf/Shelf';

export class BookShelves extends Component {
    state = {
        editedShelf: null
    };

    handleStartEditing = shelfName => {
        this.setState({ editedShelf: shelfName });
    };

    handleSaveEditedShelf = (shelfName, data) => {
        const { saveEditedShelf } = this.props;
        saveEditedShelf(shelfName, data).then(this.clearEditedShelf());
    };

    handleCancel = () => {
        this.clearEditedShelf();
    };

    clearEditedShelf() {
        this.setState({ editedShelf: null });
    }

    renderListItems() {
        const { editedShelf } = this.state;
        const { selectShelfByName, deleteSelectedShelf } = this.props;

        return this.props.shelves.map(shelf => (
            <Shelf
                key={shelf._id}
                shelfName={shelf._id}
                count={shelf.count}
                isEditedShelf={shelf._id === editedShelf}
                isEditingProgress={!!editedShelf}
                onSelectShelf={selectShelfByName}
                onStartEditing={this.handleStartEditing}
                onDelete={deleteSelectedShelf}
                onSubmit={this.handleSaveEditedShelf}
                onCancel={this.handleCancel}
            />
        ));
    }

    render() {
        return (
            <div className={styles.bookShelves}>
                <SubHeader caption="Shelves" />
                <List>{this.renderListItems()}</List>
            </div>
        );
    }
}

BookShelves.propTypes = {
    shelves: PropTypes.arrayOf(
        PropTypes.shape({
            _id: PropTypes.string,
            count: PropTypes.number
        })
    ),
    selectShelfByName: PropTypes.func,
    saveEditedShelf: PropTypes.func,
    deleteSelectedShelf: PropTypes.func
};

BookShelves.defaultProps = {
    shelves: [],
    selectShelfByName: noop,
    saveEditedShelf: noop,
    deleteSelectedShelf: noop
};

const mapStateToProps = state => ({
    shelves: getShelves(state)
});

const mapDispatchToProps = {
    selectShelfByName,
    saveEditedShelf,
    deleteSelectedShelf
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BookShelves);
