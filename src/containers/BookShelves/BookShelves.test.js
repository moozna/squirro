import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BookShelves } from './BookShelves';

describe('<BookShelves />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<BookShelves />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when shelves is specified', () => {
        const shelves = [
            {
                _id: 'All Time Favorite Romance Novels',
                count: 37
            },
            {
                _id: 'Best Book Cover Art',
                count: 45
            },
            {
                _id: 'Best Books',
                count: 41
            }
        ];

        const wrapper = shallow(<BookShelves shelves={shelves} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleStartEditing()', () => {
        test('sets the shelfName as editedShelf', () => {
            const shelfName = 'Best Book Cover Art';
            const wrapper = shallow(<BookShelves />);

            wrapper.instance().handleStartEditing(shelfName);
            expect(wrapper.state('editedShelf')).toBe(shelfName);
        });
    });

    describe('handleSaveEditedShelf()', () => {
        test('fires the saveEditedShelf callback', () => {
            const handlesSveEditedShelf = jest
                .fn()
                .mockReturnValue(Promise.resolve());
            const shelfName = 'Best Book Cover Art';
            const shelfData = {
                shelfName: 'Best Books'
            };

            const wrapper = shallow(
                <BookShelves saveEditedShelf={handlesSveEditedShelf} />
            );
            wrapper.instance().handleSaveEditedShelf(shelfName, shelfData);

            expect(handlesSveEditedShelf.mock.calls.length).toBe(1);
            expect(handlesSveEditedShelf.mock.calls[0][0]).toBe(shelfName);
            expect(handlesSveEditedShelf.mock.calls[0][1]).toEqual(shelfData);
        });

        test('clears editedShelf', async () => {
            const handlesSveEditedShelf = jest
                .fn()
                .mockReturnValue(Promise.resolve());
            const shelfName = 'Best Book Cover Art';
            const shelfData = {
                shelfName: 'Best Books'
            };

            const wrapper = shallow(
                <BookShelves saveEditedShelf={handlesSveEditedShelf} />
            );
            await wrapper
                .instance()
                .handleSaveEditedShelf(shelfName, shelfData);

            expect(wrapper.state('editedShelf')).toBe(null);
        });
    });

    describe('handleCancel()', () => {
        test('clears editedShelf', () => {
            const shelfName = 'Best Book Cover Art';
            const wrapper = shallow(<BookShelves />);
            wrapper.setState({ editedShelf: shelfName });

            expect(wrapper.state('editedShelf')).toBe(shelfName);
            wrapper.instance().handleCancel();
            expect(wrapper.state('editedShelf')).toBe(null);
        });
    });
});
