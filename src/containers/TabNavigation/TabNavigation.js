import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchAllFilters } from '../../data/actions/rangeActions';
import styles from './TabNavigation.css';
import noop from '../../utils/noop';
import Tabs from '../../components/Tabs/Tabs';
import Tab from '../../components/Tabs/Tab';
import GenreList from '../Filtering/GenreList';
import RatingFilters from '../RatingFilters/RatingFilters';
import InterestRange from '../Filtering/InterestRange';
import ReadStatus from '../Filtering/ReadStatus';
import TagList from '../Filtering/TagList';
import Year from '../Filtering/Year';
import Pages from '../Filtering/Pages';
import BookList from '../BookList/BookList';
import BooksPagination from '../BooksPagination/BooksPagination';

const tabProps = {
    className: styles.tabLabel,
    buttonStyle: 'halo'
};

export class TabNavigation extends Component {
    state = {
        activeTabIndex: 1
    };

    componentDidMount() {
        this.props.fetchAllFilters();
    }

    handleTabChange = index => {
        this.setState({ activeTabIndex: index });
    };

    render() {
        return (
            <Tabs
                activeTab={this.state.activeTabIndex}
                onTabChange={this.handleTabChange}
                headerClass={styles.tabHeader}
            >
                <Tab label="g" {...tabProps}>
                    <GenreList />
                </Tab>
                <Tab label="f" {...tabProps}>
                    <InterestRange />
                    <ReadStatus />
                    <TagList />
                    <RatingFilters />
                    <Year />
                    <Pages />
                </Tab>
                <Tab label="l" {...tabProps}>
                    <BookList />
                    <BooksPagination
                        className={styles.booksPagination}
                        numberOfDisplayedPages={1}
                        numberOfEndPages={0}
                    />
                </Tab>
            </Tabs>
        );
    }
}

TabNavigation.propTypes = {
    fetchAllFilters: PropTypes.func
};

TabNavigation.defaultProps = {
    fetchAllFilters: noop
};

const mapDispatchToProps = {
    fetchAllFilters
};

export default connect(null, mapDispatchToProps)(TabNavigation);
