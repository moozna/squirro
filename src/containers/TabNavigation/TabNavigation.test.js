import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { TabNavigation } from './TabNavigation';

describe('<TabNavigation />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<TabNavigation />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets the second tab to active by default', () => {
        const wrapper = shallow(<TabNavigation />);

        expect(wrapper.state('activeTabIndex')).toBe(1);
    });

    test('sets the specified tab to active', () => {
        const wrapper = shallow(<TabNavigation />);

        wrapper.instance().handleTabChange(2);
        expect(wrapper.state('activeTabIndex')).toBe(2);
    });
});
