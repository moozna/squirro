import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Books } from './Books';

describe('<Books />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<Books />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when books are specified', () => {
        const books = [
            {
                _id: '1',
                title: 'title1',
                author: ['Stephen King'],
                covers: [
                    {
                        url: '/images/covers/B003BKZW4U.jpg',
                        thumbnail: '/images/smallcovers/B003BKZW4U.jpg'
                    }
                ]
            },
            {
                _id: '2',
                title: 'title2',
                author: ['Mark Lukens', 'Stephen King'],
                covers: [
                    {
                        url: '/images/covers/B00FD4SP8M.jpg',
                        thumbnail: '/images/smallcovers/B00FD4SP8M.jpg'
                    }
                ]
            }
        ];
        const wrapper = shallow(<Books books={books} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders selectedShelf when it is specified', () => {
        const selectedShelf = 'Best Horror Books';
        const wrapper = shallow(<Books selectedShelf={selectedShelf} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('marks the previously selected GridElement', () => {
        const mockPreviousBookId = '2';
        const books = [
            {
                _id: '1',
                title: 'title1'
            },
            {
                _id: '2',
                title: 'title2'
            }
        ];
        const wrapper = shallow(
            <Books
                books={books}
                previouslySelectedBookId={mockPreviousBookId}
            />
        );

        const gridElement = wrapper.find('[title="title2"]');
        expect(gridElement.prop('isSelected')).toBe(true);
    });
});
