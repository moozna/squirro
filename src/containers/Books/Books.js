import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Link from 'redux-first-router-link';
import { bookDetailsPage } from '../../data/actions/pageActions';
import { loadMoreBooks } from '../../data/actions/paginationActions';
import { removeShelfSelection } from '../../data/actions/shelvesActions';
import { getBooks } from '../../data/reducers/booksReducer';
import { getHasMoreForPage } from '../../data/reducers/paginationReducer';
import { getSelectedShelf } from '../../data/reducers/shelvesReducer';
import { getPreviouslySelectedBookId } from '../../data/reducers/pageReducer';
import { bookType } from '../../types/types';
import styles from './Books.css';
import noop from '../../utils/noop';
import get from '../../utils/get';
import Chip from '../../components/Chip/Chip';
import Grid from '../../components/Grid/Grid';
import GridElement from '../../components/Grid/GridElement';
import InfiniteScroll from '../../components/InfiniteScroll/InfiniteScroll';
import BooksPagination from '../BooksPagination/BooksPagination';

const getCoverPath = book => {
    const images = get(book, 'covers', []);
    return get(images[0], 'thumbnail', '');
};

export class Books extends Component {
    renderSelectedShelf() {
        const { selectedShelf, removeShelfSelection } = this.props;

        if (!selectedShelf) {
            return null;
        }

        return (
            <div className={styles.selectedShelf}>
                <Chip
                    value={selectedShelf}
                    deletable
                    onDeleteClick={removeShelfSelection}
                />
            </div>
        );
    }

    renderBooks() {
        const { previouslySelectedBookId } = this.props;

        return this.props.books.map(book => (
            <Link key={book._id} to={bookDetailsPage(book._id)}>
                <GridElement
                    isSelected={previouslySelectedBookId === book._id}
                    imagePath={getCoverPath(book)}
                    title={book.title}
                    rating={book.rating}
                    subtitle={book.author && book.author[0]}
                />
            </Link>
        ));
    }

    render() {
        return (
            <Fragment>
                {this.renderSelectedShelf()}
                <InfiniteScroll
                    hasMore={this.props.hasMore}
                    loadMore={this.props.loadMoreBooks}
                >
                    <Grid>{this.renderBooks()}</Grid>
                </InfiniteScroll>
                <BooksPagination className={styles.booksPagination} />
            </Fragment>
        );
    }
}

Books.propTypes = {
    books: PropTypes.arrayOf(bookType),
    hasMore: PropTypes.bool,
    selectedShelf: PropTypes.string,
    previouslySelectedBookId: PropTypes.string,
    loadMoreBooks: PropTypes.func,
    removeShelfSelection: PropTypes.func
};

Books.defaultProps = {
    books: [],
    hasMore: false,
    selectedShelf: '',
    previouslySelectedBookId: '',
    loadMoreBooks: noop,
    removeShelfSelection: noop
};

const mapStateToProps = state => ({
    books: getBooks(state),
    hasMore: getHasMoreForPage(state),
    selectedShelf: getSelectedShelf(state),
    previouslySelectedBookId: getPreviouslySelectedBookId(state)
});

const mapDispatchToProps = {
    loadMoreBooks,
    removeShelfSelection
};

export default connect(mapStateToProps, mapDispatchToProps)(Books);
