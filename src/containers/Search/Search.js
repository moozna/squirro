import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    changeSearchValue,
    fetchSuggestions,
    clearSuggestions,
    clearSearch,
    searchBooks
} from '../../data/actions/searchActions';
import {
    getSearchFieldValue,
    getSearchSuggestions
} from '../../data/reducers/searchReducer';
import { suggestionsType } from '../../types/types';
import noop from '../../utils/noop';
import AutoComplete from '../../components/AutoComplete/AutoComplete';
import SearchInput from '../../components/SearchInput/SearchInput';

export class Search extends Component {
    lastSubmittedValue = '';

    handleChange = (name, value) => {
        const { changeSearchValue } = this.props;
        changeSearchValue(value);
    };

    handleEscape = () => {
        const { value, changeSearchValue } = this.props;

        if (value !== this.lastSubmittedValue) {
            changeSearchValue(this.lastSubmittedValue);
        }
    };

    handleSubmit = value => {
        const { searchBooks } = this.props;
        this.lastSubmittedValue = value;
        searchBooks(value);
    };

    handleClearSearch = () => {
        const { clearSearch } = this.props;
        this.lastSubmittedValue = '';
        clearSearch();
    };

    render() {
        const {
            value,
            suggestions,
            fetchSuggestions,
            clearSuggestions
        } = this.props;

        return (
            <AutoComplete
                value={value}
                name="search"
                suggestions={suggestions}
                onInputChange={this.handleChange}
                fetchSuggestions={fetchSuggestions}
                clearSuggestions={clearSuggestions}
                onSubmit={this.handleSubmit}
                onEscape={this.handleEscape}
            >
                <SearchInput
                    value={value}
                    clearSearch={this.handleClearSearch}
                />
            </AutoComplete>
        );
    }
}

Search.propTypes = {
    value: PropTypes.string,
    suggestions: suggestionsType,
    changeSearchValue: PropTypes.func,
    fetchSuggestions: PropTypes.func,
    clearSuggestions: PropTypes.func,
    clearSearch: PropTypes.func,
    searchBooks: PropTypes.func
};

Search.defaultProps = {
    value: '',
    suggestions: [],
    changeSearchValue: noop,
    fetchSuggestions: noop,
    clearSuggestions: noop,
    clearSearch: noop,
    searchBooks: noop
};

const mapStateToProps = state => ({
    value: getSearchFieldValue(state),
    suggestions: getSearchSuggestions(state)
});

const mapDispatchToProps = {
    changeSearchValue,
    fetchSuggestions,
    clearSuggestions,
    clearSearch,
    searchBooks
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Search);
