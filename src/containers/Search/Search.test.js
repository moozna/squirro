import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Search } from './Search';

describe('<Search />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Search />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleChange()', () => {
        test('fires the changeSearchValue callback', () => {
            const handleChangeSearchValue = jest.fn();
            const mockName = 'search';
            const mockValue = 'king';

            const wrapper = shallow(
                <Search changeSearchValue={handleChangeSearchValue} />
            );
            wrapper.instance().handleChange(mockName, mockValue);

            expect(handleChangeSearchValue.mock.calls.length).toBe(1);
            expect(handleChangeSearchValue.mock.calls[0][0]).toBe(mockValue);
        });
    });

    describe('handleEscape()', () => {
        test('fires the changeSearchValue callback with the last submitted value', () => {
            const handleChangeSearchValue = jest.fn();
            const mockLastValue = 'king';

            const wrapper = shallow(
                <Search
                    value="sword"
                    changeSearchValue={handleChangeSearchValue}
                />
            );
            wrapper.instance().lastSubmittedValue = mockLastValue;
            wrapper.instance().handleEscape();

            expect(handleChangeSearchValue.mock.calls.length).toBe(1);
            expect(handleChangeSearchValue.mock.calls[0][0]).toBe(
                mockLastValue
            );
        });

        test('does not fire the changeSearchValue callback when value is the same as the last submitted value', () => {
            const handleChangeSearchValue = jest.fn();
            const mockLastValue = 'king';

            const wrapper = shallow(
                <Search
                    value={mockLastValue}
                    changeSearchValue={handleChangeSearchValue}
                />
            );
            wrapper.instance().lastSubmittedValue = mockLastValue;
            wrapper.instance().handleEscape();

            expect(handleChangeSearchValue.mock.calls.length).toBe(0);
        });
    });

    describe('handleSubmit()', () => {
        test('fires the searchBooks callback', () => {
            const handleSearchBooks = jest.fn();
            const mockValue = 'king';

            const wrapper = shallow(<Search searchBooks={handleSearchBooks} />);
            wrapper.instance().handleSubmit(mockValue);

            expect(handleSearchBooks.mock.calls.length).toBe(1);
            expect(handleSearchBooks.mock.calls[0][0]).toBe(mockValue);
        });

        test('saves the last submitted value', () => {
            const mockValue = 'king';

            const wrapper = shallow(<Search />);
            wrapper.instance().handleSubmit(mockValue);

            expect(wrapper.instance().lastSubmittedValue).toBe(mockValue);
        });
    });

    describe('handleClearSearch()', () => {
        test('fires the clearSearch callback', () => {
            const handleClearSearch = jest.fn();

            const wrapper = shallow(<Search clearSearch={handleClearSearch} />);
            wrapper.instance().handleClearSearch();

            expect(handleClearSearch.mock.calls.length).toBe(1);
        });

        test('resets the last submitted value to default', () => {
            const wrapper = shallow(<Search />);
            wrapper.instance().handleClearSearch();

            expect(wrapper.instance().lastSubmittedValue).toBe('');
        });
    });
});
