import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BooksPagination } from './BooksPagination';

describe('<BooksPagination />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<BooksPagination />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are provided', () => {
        const wrapper = shallow(
            <BooksPagination
                books={new Array(25)}
                numberOfDisplayedPages={3}
                numberOfEndPages={2}
                booksCount={500}
                startIndex={50}
                pageSize={25}
                pageNumber={2}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render pagination when pageCount is one', () => {
        const wrapper = shallow(
            <BooksPagination
                books={new Array(100)}
                booksCount={100}
                pageSize={100}
            />
        );

        expect(wrapper.isEmptyRender()).toBe(true);
    });

    test('does not render pagination when not all books are displayed for that page', () => {
        const wrapper = shallow(
            <BooksPagination
                books={new Array(15)}
                booksCount={500}
                pageSize={25}
            />
        );

        expect(wrapper.isEmptyRender()).toBe(true);
    });

    test('renders pagination when all the books are displayed for the last page', () => {
        const wrapper = shallow(
            <BooksPagination
                books={new Array(15)}
                booksCount={515}
                pageSize={25}
                pageNumber={21}
            />
        );

        expect(wrapper.isEmptyRender()).toBe(false);
    });

    test('renders a custom class', () => {
        const wrapper = shallow(
            <BooksPagination
                books={new Array(25)}
                className="myClass"
                booksCount={500}
                pageSize={25}
            />
        );

        expect(wrapper.hasClass('myClass')).toBe(true);
    });

    test('createPath should return the correct path', async () => {
        const mockPageNumber = 3;

        const wrapper = shallow(
            <BooksPagination
                books={new Array(25)}
                booksCount={500}
                pageSize={25}
            />
        );
        const path = wrapper.instance().createPath(mockPageNumber);

        expect(path).toEqual({
            type: 'BOOKS',
            query: { page: mockPageNumber }
        });
    });

    describe('handlePageSelect()', () => {
        test('calls loadPage', () => {
            const mockPageNumber = 3;
            const mockLoadPage = jest.fn();

            const wrapper = shallow(
                <BooksPagination
                    books={new Array(25)}
                    booksCount={500}
                    pageSize={25}
                    loadPage={mockLoadPage}
                />
            );
            wrapper.instance().handlePageSelect(mockPageNumber);

            expect(mockLoadPage.mock.calls.length).toBe(1);
            expect(mockLoadPage.mock.calls[0][0]).toBe(mockPageNumber);
        });
    });
});
