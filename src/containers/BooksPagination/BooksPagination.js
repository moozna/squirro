import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadPage } from '../../data/actions/paginationActions';
import { booksPage } from '../../data/actions/pageActions';
import { getBooks, getBooksCount } from '../../data/reducers/booksReducer';
import {
    getPageSize,
    getCurrentPageNumber
} from '../../data/reducers/paginationReducer';
import { bookType } from '../../types/types';
import noop from '../../utils/noop';
import Pagination from '../../components/Pagination/Pagination';

export class BooksPagination extends Component {
    handlePageSelect = pageNumber => {
        const { loadPage } = this.props;
        loadPage(pageNumber);
    };

    createPath = pageNumber => booksPage({ page: pageNumber });

    isLastBooks() {
        const { books, booksCount, pageSize } = this.props;
        return books.length === booksCount % pageSize;
    }

    isLastPage(pageCount) {
        const { pageNumber } = this.props;
        return pageNumber === pageCount && this.isLastBooks();
    }

    isNextPageNeeded(pageCount) {
        const { books, pageSize } = this.props;
        return books.length >= pageSize || this.isLastPage(pageCount);
    }

    renderBooksPagination(pageCount) {
        const {
            className,
            numberOfDisplayedPages,
            numberOfEndPages,
            pageNumber
        } = this.props;

        return (
            <div className={className}>
                <Pagination
                    numberOfDisplayedPages={numberOfDisplayedPages}
                    numberOfEndPages={numberOfEndPages}
                    pageCount={pageCount}
                    activePage={pageNumber}
                    createHref={this.createPath}
                    onPageSelect={this.handlePageSelect}
                />
            </div>
        );
    }

    render() {
        const { booksCount, pageSize } = this.props;
        const pageCount = Math.ceil(booksCount / pageSize);
        const isVisible = pageCount > 1 && this.isNextPageNeeded(pageCount);

        return isVisible ? this.renderBooksPagination(pageCount) : null;
    }
}

BooksPagination.propTypes = {
    books: PropTypes.arrayOf(bookType),
    className: PropTypes.string,
    numberOfDisplayedPages: PropTypes.number,
    numberOfEndPages: PropTypes.number,
    booksCount: PropTypes.number,
    pageSize: PropTypes.number,
    pageNumber: PropTypes.number,
    loadPage: PropTypes.func
};

BooksPagination.defaultProps = {
    books: [],
    className: '',
    numberOfDisplayedPages: 5,
    numberOfEndPages: 1,
    booksCount: 0,
    pageSize: 0,
    pageNumber: 0,
    loadPage: noop
};

const mapStateToProps = state => ({
    books: getBooks(state),
    booksCount: getBooksCount(state),
    pageSize: getPageSize(state),
    pageNumber: getCurrentPageNumber(state)
});

const mapDispatchToProps = {
    loadPage
};

export default connect(mapStateToProps, mapDispatchToProps)(BooksPagination);
