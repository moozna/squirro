import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    importAmazonBook,
    importGoodreadsBook,
    clearImportedData
} from '../../data/actions/importActions';
import { booksPage, editImportPage } from '../../data/actions/pageActions';
import {
    getImportedAmazonData,
    getImportedGoodreadsData,
    getDuplicateData,
    getIsFetching
} from '../../data/reducers/importReducer';
import styles from './ImportBook.css';
import { goodreadsDataType, amazonDataType } from '../../types/types';
import noop from '../../utils/noop';
import {
    asinNumberValidation,
    goodreadsIdValidation
} from '../../components/ImportForm/importValidation';
import ImportForm from '../../components/ImportForm/ImportForm';
import GoodreadsLogo from '../../components/svg/GoodreadsLogo';
import AmazonLogo from '../../components/svg/AmazonLogo';
import ImportedData from '../../components/ImportedData/ImportedData';
import ImportNavigation from '../../components/ImportForm/ImportNavigation';

const AMAZON_URL = 'https://www.amazon.com/s/browse?node=283155';
const GOODREADS_URL = 'https://www.goodreads.com';

export class ImportBook extends Component {
    handleGoodreadsImport = values => {
        const { importGoodreadsBook } = this.props;
        importGoodreadsBook(values.goodreadsId);
    };

    handleAmazonImport = values => {
        const { importAmazonBook } = this.props;
        importAmazonBook(values.asin);
    };

    handleNextPage = () => {
        const { editImportPage } = this.props;
        editImportPage();
    };

    handleCancel = () => {
        const { booksPage, clearImportedData } = this.props;
        booksPage();
        clearImportedData();
    };

    render() {
        const { amazonData, goodreadsData, duplicates, isLoading } = this.props;

        return (
            <div className={styles.importBook}>
                <div className={styles.importFields}>
                    <ImportForm
                        type="number"
                        name="goodreadsId"
                        label="Goodreads ID"
                        icon={<GoodreadsLogo />}
                        url={GOODREADS_URL}
                        validate={goodreadsIdValidation}
                        onSubmit={this.handleGoodreadsImport}
                    />
                    <ImportForm
                        name="asin"
                        label="ASIN"
                        icon={<AmazonLogo />}
                        url={AMAZON_URL}
                        validate={asinNumberValidation}
                        onSubmit={this.handleAmazonImport}
                    />
                </div>
                <ImportedData
                    amazonData={amazonData}
                    goodreadsData={goodreadsData}
                    duplicates={duplicates}
                    isLoading={isLoading}
                />
                <ImportNavigation
                    goToNext={this.handleNextPage}
                    onCancel={this.handleCancel}
                />
            </div>
        );
    }
}

ImportBook.propTypes = {
    goodreadsData: goodreadsDataType,
    amazonData: amazonDataType,
    duplicates: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    }),
    isLoading: PropTypes.shape({
        amazon: PropTypes.bool,
        goodreads: PropTypes.bool
    }),
    importAmazonBook: PropTypes.func,
    importGoodreadsBook: PropTypes.func,
    clearImportedData: PropTypes.func,
    booksPage: PropTypes.func,
    editImportPage: PropTypes.func
};

ImportBook.defaultProps = {
    amazonData: {},
    goodreadsData: {},
    duplicates: {},
    isLoading: {},
    importAmazonBook: noop,
    importGoodreadsBook: noop,
    clearImportedData: noop,
    booksPage: noop,
    editImportPage: noop
};

const mapStateToProps = state => ({
    amazonData: getImportedAmazonData(state),
    goodreadsData: getImportedGoodreadsData(state),
    duplicates: getDuplicateData(state),
    isLoading: getIsFetching(state)
});

const mapDispatchToProps = {
    importAmazonBook,
    importGoodreadsBook,
    clearImportedData,
    booksPage,
    editImportPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImportBook);
