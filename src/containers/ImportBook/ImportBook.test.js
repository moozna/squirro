import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ImportBook } from './ImportBook';

describe('<ImportBook />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<ImportBook />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('fires the importGoodreadsBook callback', () => {
        const mockValues = {
            goodreadsId: 1
        };
        const mockImportGoodreadsBook = jest.fn();

        const wrapper = shallow(
            <ImportBook importGoodreadsBook={mockImportGoodreadsBook} />
        );
        wrapper.instance().handleGoodreadsImport(mockValues);

        expect(mockImportGoodreadsBook.mock.calls.length).toBe(1);
        expect(mockImportGoodreadsBook.mock.calls[0][0]).toBe(
            mockValues.goodreadsId
        );
    });

    test('fires the importAmazonBook callback', () => {
        const mockValues = {
            asin: 'AZ'
        };
        const mockImportAmazonBook = jest.fn();

        const wrapper = shallow(
            <ImportBook importAmazonBook={mockImportAmazonBook} />
        );
        wrapper.instance().handleAmazonImport(mockValues);

        expect(mockImportAmazonBook.mock.calls.length).toBe(1);
        expect(mockImportAmazonBook.mock.calls[0][0]).toBe(mockValues.asin);
    });

    describe('handleNextPage()', () => {
        test('navigates to editImportPage page', () => {
            const mockEditImportPage = jest.fn();

            const wrapper = shallow(
                <ImportBook editImportPage={mockEditImportPage} />
            );
            wrapper.instance().handleNextPage();

            expect(mockEditImportPage.mock.calls.length).toBe(1);
        });
    });

    describe('handleCancel()', () => {
        test('navigates to books page', () => {
            const mockBooksPage = jest.fn();

            const wrapper = shallow(<ImportBook booksPage={mockBooksPage} />);
            wrapper.instance().handleCancel();

            expect(mockBooksPage.mock.calls.length).toBe(1);
        });

        test('fires the clearImportedData callback', () => {
            const mockClearImportedData = jest.fn();

            const wrapper = shallow(
                <ImportBook clearImportedData={mockClearImportedData} />
            );
            wrapper.instance().handleCancel();

            expect(mockClearImportedData.mock.calls.length).toBe(1);
        });
    });
});
