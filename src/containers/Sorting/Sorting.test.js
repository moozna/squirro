import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Sorting } from './Sorting';

describe('<Sorting />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<Sorting />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('sets "rating" as selected rating by default', () => {
        const wrapper = shallow(<Sorting />);

        expect(wrapper.state('selectedRating')).toBe('rating');
    });

    test('sets the provided value as selected rating', () => {
        const wrapper = shallow(<Sorting />);

        wrapper.instance().changeDropdownSelection('selectedRating', 'amazonRating');
        expect(wrapper.state('selectedRating')).toBe('amazonRating');
    });

    test('sets the provided value as selected reviews', () => {
        const wrapper = shallow(<Sorting />);

        wrapper.instance().changeDropdownSelection('selectedReviews', 'goodreadsReviews');
        expect(wrapper.state('selectedReviews')).toBe('goodreadsReviews');
    });

    describe('updateSortField()', () => {
        test('fires the updateSortField callback when different sort field is selected', () => {
            const handleUpdateSortField = jest.fn();
            const wrapper = shallow(
                <Sorting
                    sortField="title"
                    updateSortField={handleUpdateSortField}
                />
            );

            wrapper.instance().changeSorting('author');
            expect(handleUpdateSortField.mock.calls.length).toBe(1);
            expect(handleUpdateSortField.mock.calls[0][0]).toBe('author');
        });

        test('fires the updateSortField callback when different dropdown value is provided', () => {
            const handleUpdateSortField = jest.fn();
            const wrapper = shallow(
                <Sorting
                    sortField="rating"
                    updateSortField={handleUpdateSortField}
                />
            );

            wrapper.instance().changeDropdownSelection('selectedRating', 'amazonRating');
            expect(handleUpdateSortField.mock.calls.length).toBe(1);
            expect(handleUpdateSortField.mock.calls[0][0]).toBe('amazonRating');
        });

        test('does not fire the updateSortField callback when the same dropdown value is provided', () => {
            const handleUpdateSortField = jest.fn();
            const wrapper = shallow(
                <Sorting
                    sortField="rating"
                    updateSortField={handleUpdateSortField}
                />
            );

            wrapper.instance().changeDropdownSelection('selectedRating', 'rating');
            expect(handleUpdateSortField.mock.calls.length).toBe(0);
        });
    });

    describe('updateSortOrder()', () => {
        test('fires the updateSortOrder callback when the same sort field is selected', () => {
            const handleUpdateSortOrder = jest.fn();
            const wrapper = shallow(
                <Sorting
                    sortField="title"
                    updateSortOrder={handleUpdateSortOrder}
                />
            );

            wrapper.instance().changeSorting('title');
            expect(handleUpdateSortOrder.mock.calls.length).toBe(1);
        });
    });
});
