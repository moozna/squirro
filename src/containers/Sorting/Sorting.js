import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    updateSortField,
    updateSortOrder
} from '../../data/actions/sortingActions';
import { getSortField, getSortOrder } from '../../data/reducers/sortingReducer';
import noop from '../../utils/noop';
import ButtonGroup from '../../components/ButtonGroup/ButtonGroup';
import RadioButton from '../../components/ButtonGroup/RadioButton';
import Dropdown from '../../components/Dropdown/Dropdown';
import DropdownItem from '../../components/Dropdown/DropdownItem';
import AmazonLogo from '../../components/svg/AmazonLogo';
import GoodreadsLogo from '../../components/svg/GoodreadsLogo';
import User from '../../components/svg/User';

export class Sorting extends Component {
    state = {
        selectedRating: 'rating',
        selectedReviews: 'amazonReviews'
    };

    changeSorting = value => {
        const { sortField, updateSortField, updateSortOrder } = this.props;

        if (value === sortField) {
            updateSortOrder();
        } else {
            updateSortField(value);
        }
    };

    changeDropdownSelection = (name, value) => {
        const { updateSortField } = this.props;
        const previousValue = this.state[name];

        if (value !== previousValue) {
            this.setState({ [name]: value });
            updateSortField(value);
        }
    };

    render() {
        const { sortField, sortOrder } = this.props;
        const { selectedRating, selectedReviews } = this.state;

        return (
            <ButtonGroup
                name="sorting"
                value={sortField}
                sortOrder={sortOrder}
                onSelectionChange={this.changeSorting}
            >
                <RadioButton label="Interest" value="interestLevel" />
                <RadioButton
                    label="Reviews"
                    value={selectedReviews}
                    withDropdown
                >
                    <Dropdown
                        name="selectedReviews"
                        onSelectItem={this.changeDropdownSelection}
                        selected={selectedReviews}
                    >
                        <DropdownItem value="amazonReviews">
                            <AmazonLogo />
                        </DropdownItem>
                        <DropdownItem value="goodreadsReviews">
                            <GoodreadsLogo />
                        </DropdownItem>
                    </Dropdown>
                </RadioButton>
                <RadioButton label="Rating" value={selectedRating} withDropdown>
                    <Dropdown
                        name="selectedRating"
                        onSelectItem={this.changeDropdownSelection}
                        selected={selectedRating}
                    >
                        <DropdownItem value="rating">
                            <User />
                        </DropdownItem>
                        <DropdownItem value="amazonRating">
                            <AmazonLogo />
                        </DropdownItem>
                        <DropdownItem value="goodreadsRating">
                            <GoodreadsLogo />
                        </DropdownItem>
                    </Dropdown>
                </RadioButton>
                <RadioButton label="Pages" value="pages" />
                <RadioButton label="Added" value="createdDate" />
            </ButtonGroup>
        );
    }
}

Sorting.propTypes = {
    sortField: PropTypes.string,
    sortOrder: PropTypes.number,
    updateSortField: PropTypes.func,
    updateSortOrder: PropTypes.func
};

Sorting.defaultProps = {
    sortField: '',
    sortOrder: 1,
    updateSortField: noop,
    updateSortOrder: noop
};

const mapStateToProps = state => ({
    sortField: getSortField(state),
    sortOrder: getSortOrder(state)
});

const mapDispatchToProps = {
    updateSortField,
    updateSortOrder
};

export default connect(mapStateToProps, mapDispatchToProps)(Sorting);
