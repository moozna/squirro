import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { OwnRating } from './OwnRating';

describe('<OwnRating />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<OwnRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
