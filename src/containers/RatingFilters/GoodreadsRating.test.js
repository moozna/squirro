import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { GoodreadsRating } from './GoodreadsRating';

describe('<GoodreadsRating />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<GoodreadsRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
