import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateGoodreadsRatingRange } from '../../data/actions/rangeActions';
import { getGoodreadsRating } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import RatingSlider from '../../components/Filters/RatingSlider';

export const GoodreadsRating = ({
    goodreadsRating,
    updateGoodreadsRatingRange
}) => (
    <Fragment>
        <SubHeader caption="Goodreads Rating" />
        <RatingSlider
            values={goodreadsRating}
            onValueChange={updateGoodreadsRatingRange}
        />
    </Fragment>
);

GoodreadsRating.propTypes = {
    goodreadsRating: PropTypes.arrayOf(PropTypes.number),
    updateGoodreadsRatingRange: PropTypes.func
};

GoodreadsRating.defaultProps = {
    goodreadsRating: [],
    updateGoodreadsRatingRange: noop
};

const mapStateToProps = state => ({
    goodreadsRating: getGoodreadsRating(state)
});

const mapDispatchToProps = {
    updateGoodreadsRatingRange
};

export default connect(mapStateToProps, mapDispatchToProps)(GoodreadsRating);
