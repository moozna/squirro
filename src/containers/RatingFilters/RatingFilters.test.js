import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import RatingFilters from './RatingFilters';

describe('<RatingFilters />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<RatingFilters />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders an opened dropdown panel when toggle button is clicked', () => {
        const wrapper = shallow(<RatingFilters />);

        wrapper.find('.toggleButton').simulate('click');
        const dropdownPanel = wrapper.find('CSSTransition');

        expect(dropdownPanel.props().in).toBe(true);
    });
});
