import React, { Component } from 'react';
import { CSSTransition } from 'react-transition-group';
import styles from './RatingFilters.css';
import OwnRating from './OwnRating';
import AmazonRating from './AmazonRating';
import GoodreadsRating from './GoodreadsRating';

const ANIMATION_CLASSES = {
    enter: styles.dropdownPanelEnter,
    enterActive: styles.dropdownPanelEnterActive,
    exit: styles.dropdownPanelLeave,
    exitActive: styles.dropdownPanelLeaveActive
};

export class RatingFilters extends Component {
    state = {
        isOpen: false
    };

    toggleDropdown = () => {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }));
    };

    render() {
        const { isOpen } = this.state;

        return (
            <div>
                <div className={styles.ratingFilters}>
                    <OwnRating />
                    <CSSTransition
                        in={isOpen}
                        classNames={ANIMATION_CLASSES}
                        timeout={350}
                        unmountOnExit
                    >
                        <div className={styles.dropdownPanel}>
                            <AmazonRating />
                            <GoodreadsRating />
                        </div>
                    </CSSTransition>
                </div>
                <div className={styles.toggle}>
                    <button
                        type="button"
                        className={styles.toggleButton}
                        onClick={this.toggleDropdown}
                    />
                </div>
            </div>
        );
    }
}

export default RatingFilters;
