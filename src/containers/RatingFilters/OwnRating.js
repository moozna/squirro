import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateOwnRatingRange } from '../../data/actions/rangeActions';
import { getOwnRating } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import RatingSlider from '../../components/Filters/RatingSlider';

export const OwnRating = ({ ownRating, updateOwnRatingRange }) => (
    <Fragment>
        <SubHeader caption="Rating" />
        <RatingSlider values={ownRating} onValueChange={updateOwnRatingRange} />
    </Fragment>
);

OwnRating.propTypes = {
    ownRating: PropTypes.arrayOf(PropTypes.number),
    updateOwnRatingRange: PropTypes.func
};

OwnRating.defaultProps = {
    ownRating: [],
    updateOwnRatingRange: noop
};

const mapStateToProps = state => ({
    ownRating: getOwnRating(state)
});

const mapDispatchToProps = {
    updateOwnRatingRange
};

export default connect(mapStateToProps, mapDispatchToProps)(OwnRating);
