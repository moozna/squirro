import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateAmazonRatingRange } from '../../data/actions/rangeActions';
import { getAmazonRating } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import RatingSlider from '../../components/Filters/RatingSlider';

export const AmazonRating = ({ amazonRating, updateAmazonRatingRange }) => (
    <Fragment>
        <SubHeader caption="Amazon Rating" />
        <RatingSlider
            values={amazonRating}
            onValueChange={updateAmazonRatingRange}
        />
    </Fragment>
);

AmazonRating.propTypes = {
    amazonRating: PropTypes.arrayOf(PropTypes.number),
    updateAmazonRatingRange: PropTypes.func
};

AmazonRating.defaultProps = {
    amazonRating: [],
    updateAmazonRatingRange: noop
};

const mapStateToProps = state => ({
    amazonRating: getAmazonRating(state)
});

const mapDispatchToProps = {
    updateAmazonRatingRange
};

export default connect(mapStateToProps, mapDispatchToProps)(AmazonRating);
