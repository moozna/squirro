import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { AmazonRating } from './AmazonRating';

describe('<AmazonRating />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<AmazonRating />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
