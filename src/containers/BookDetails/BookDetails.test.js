import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BookDetails } from './BookDetails';

describe('<BookDetails />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<BookDetails />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when book is specified', () => {
        const book = {
            _id: '1',
            asin: 'B003BKZW4U',
            goodreadsId: 20,
            title: 'title1',
            author: ['Stephen King'],
            seriesTitle: 'seriesTitle',
            seriesIndex: '2',
            rating: 3.5,
            amazonRating: {
                rating: 2,
                numberOfRatings: 100
            },
            goodreadsRating: {
                rating: 4,
                numberOfRatings: 500
            },
            genres: ['action', 'adventure'],
            tags: ['read', 'wait for beautify'],
            shelves: ['Best Historical Fiction', 'Best Book Cover Art'],
            publicationYear: 2001,
            pages: 400,
            numberOfTimesRead: 3,
            description: 'description',
            covers: [
                {
                    url: '/images/covers/image1.jpg',
                    thumbnail: '/images/smallcovers/image1.jpg'
                }
            ]
        };

        const wrapper = shallow(<BookDetails book={book} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('createClosePath should return the correct path', async () => {
        const mockPageNumber = 3;

        const wrapper = shallow(<BookDetails pageNumber={mockPageNumber} />);
        const path = wrapper.instance().createClosePath();

        expect(path).toEqual({
            type: 'BOOKS',
            query: { page: mockPageNumber }
        });
    });

    describe('handleDeleteBook()', () => {
        test('fires deleteBook with the book id', async () => {
            const mockBookId = '1';
            const book = { _id: mockBookId };
            const mockDeleteBook = jest.fn();

            const wrapper = shallow(
                <BookDetails book={book} deleteBook={mockDeleteBook} />
            );
            wrapper.instance().handleDeleteBook();

            expect(mockDeleteBook.mock.calls.length).toBe(1);
            expect(mockDeleteBook.mock.calls[0][0]).toBe(mockBookId);
        });
    });
});
