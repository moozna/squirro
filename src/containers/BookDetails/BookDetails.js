import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteBook } from '../../data/actions/selectedBookActions';
import { searchBooks } from '../../data/actions/searchActions';
import { updateGenres, updateTags } from '../../data/actions/filtersActions';
import { selectShelfByName } from '../../data/actions/shelvesActions';
import { booksPage } from '../../data/actions/pageActions';
import { getSelectedBook } from '../../data/reducers/selectedBookReducer';
import { bookType } from '../../types/types';
import DetailsCard from '../../components/DetailsCard/DetailsCard';
import { getCurrentPageNumber } from '../../data/reducers/paginationReducer';
import noop from '../../utils/noop';

export class BookDetails extends Component {
    createClosePath = () => {
        const { pageNumber } = this.props;
        return booksPage({ page: pageNumber });
    };

    handleDeleteBook = () => {
        const { book, deleteBook } = this.props;
        deleteBook(book._id);
    };

    render() {
        const {
            book,
            searchBooks,
            updateGenres,
            updateTags,
            selectShelfByName
        } = this.props;
        return (
            <DetailsCard
                book={book}
                searchBooks={searchBooks}
                updateGenres={updateGenres}
                updateTags={updateTags}
                updateSelectedShelf={selectShelfByName}
                createClosePath={this.createClosePath}
                deleteBook={this.handleDeleteBook}
            />
        );
    }
}

BookDetails.propTypes = {
    book: bookType,
    pageNumber: PropTypes.number,
    searchBooks: PropTypes.func,
    updateGenres: PropTypes.func,
    updateTags: PropTypes.func,
    selectShelfByName: PropTypes.func,
    deleteBook: PropTypes.func
};

BookDetails.defaultProps = {
    book: {},
    pageNumber: 1,
    searchBooks: noop,
    updateGenres: noop,
    updateTags: noop,
    selectShelfByName: noop,
    deleteBook: noop
};

const mapStateToProps = state => ({
    book: getSelectedBook(state),
    pageNumber: getCurrentPageNumber(state)
});

const mapDispatchToProps = {
    searchBooks,
    updateGenres,
    updateTags,
    selectShelfByName,
    deleteBook
};

export default connect(mapStateToProps, mapDispatchToProps)(BookDetails);
