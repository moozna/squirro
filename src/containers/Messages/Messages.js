import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { clearErrorMessage } from '../../data/actions/errorsActions';
import { getErrors } from '../../data/reducers/errorsReducer';
import noop from '../../utils/noop';
import Toaster from '../../components/Toaster/Toaster';

export const Messages = ({ errors, clearErrorMessage }) => (
    <Toaster messages={errors} clearMessage={clearErrorMessage} />
);

Messages.propTypes = {
    errors: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            message: PropTypes.string,
            title: PropTypes.string
        })
    ),
    clearErrorMessage: PropTypes.func
};

Messages.defaultProps = {
    errors: [],
    clearErrorMessage: noop
};

const mapStateToProps = state => ({
    errors: getErrors(state)
});

const mapDispatchToProps = {
    clearErrorMessage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages);
