import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Messages } from './Messages';

describe('<Messages />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Messages />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when errors are provided', () => {
        const mockErrors = [
            {
                id: '1',
                error: 'Books error message',
                title: 'Error title'
            },
            {
                id: '2',
                error: 'Import error message'
            },
            {
                id: '3',
                error: 'Filters error message',
                title: 'Error title'
            }
        ];
        const wrapper = shallow(<Messages errors={mockErrors} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
