import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BookList } from './BookList';

describe('<BookList />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<BookList />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are specified', () => {
        const books = [
            {
                _id: '1',
                title: 'title1',
                author: ['Stephen King'],
                asin: 'B003BKZW4U'
            },
            {
                _id: '2',
                title: 'title2',
                author: ['Mark Lukens', 'Stephen King'],
                asin: 'B00FD4SP8M'
            }
        ];
        const wrapper = shallow(<BookList books={books} selectedBookId="2" />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('calls fetchBooksIfNeeded after mounting', () => {
        const mockFetchBooksIfNeeded = jest.fn();
        mount(<BookList fetchBooksIfNeeded={mockFetchBooksIfNeeded} />);

        expect(mockFetchBooksIfNeeded.mock.calls.length).toBe(1);
    });
});
