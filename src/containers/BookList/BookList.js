import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Link from 'redux-first-router-link';
import { fetchBooksIfNeeded } from '../../data/actions/booksActions';
import { booksPage, bookDetailsPage } from '../../data/actions/pageActions';
import { loadMoreBooks } from '../../data/actions/paginationActions';
import { getBooks } from '../../data/reducers/booksReducer';
import { getHasMoreForPage } from '../../data/reducers/paginationReducer';
import { getSelectedBookId } from '../../data/reducers/selectedBookReducer';
import { bookType } from '../../types/types';
import noop from '../../utils/noop';
import List from '../../components/List/List';
import ListItem from '../../components/List/ListItem';
import InfiniteScroll from '../../components/InfiniteScroll/InfiniteScroll';

export class BookList extends Component {
    componentDidMount() {
        this.props.fetchBooksIfNeeded();
    }

    createPath(id) {
        if (id === this.props.selectedBookId) {
            return booksPage();
        }

        return bookDetailsPage(id);
    }

    renderListItems() {
        return this.props.books.map(book => (
            <ListItem
                key={book._id}
                itemId={book._id}
                title={book.title}
                subtitle={book.author.join(', ')}
                isSelected={this.props.selectedBookId === book._id}
                linkComponent={Link}
                to={this.createPath(book._id)}
            />
        ));
    }

    render() {
        return (
            <InfiniteScroll
                hasMore={this.props.hasMore}
                loadMore={this.props.loadMoreBooks}
            >
                <List>{this.renderListItems()}</List>
            </InfiniteScroll>
        );
    }
}

BookList.propTypes = {
    books: PropTypes.arrayOf(bookType),
    hasMore: PropTypes.bool,
    selectedBookId: PropTypes.string,
    fetchBooksIfNeeded: PropTypes.func,
    loadMoreBooks: PropTypes.func
};

BookList.defaultProps = {
    books: [],
    hasMore: false,
    selectedBookId: '',
    fetchBooksIfNeeded: noop,
    loadMoreBooks: noop
};

const mapStateToProps = state => ({
    books: getBooks(state),
    hasMore: getHasMoreForPage(state),
    selectedBookId: getSelectedBookId(state)
});

const mapDispatchToProps = {
    fetchBooksIfNeeded,
    loadMoreBooks
};

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
