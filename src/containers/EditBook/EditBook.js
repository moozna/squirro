import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    uploadCoverImage,
    deleteCoverImage
} from '../../data/actions/imageActions';
import { saveEditedBook } from '../../data/actions/selectedBookActions';
import { bookDetailsPage } from '../../data/actions/pageActions';
import {
    getSelectedBookId,
    getSelectedBook
} from '../../data/reducers/selectedBookReducer';
import { getSuggestions } from '../../data/reducers/suggestionsReducer';
import { bookType, suggestionGroupType } from '../../types/types';
import noop from '../../utils/noop';
import DetailsForm from '../../components/DetailsForm/DetailsForm';
import bookValidation from '../../components/DetailsForm/bookValidation';

export class EditBook extends Component {
    handleSubmit = bookData => {
        const { bookId, saveEditedBook } = this.props;
        saveEditedBook(bookId, bookData);
    };

    handleCancel = () => {
        const { bookId, bookDetailsPage } = this.props;
        bookDetailsPage(bookId);
    };

    render() {
        const {
            book,
            suggestions,
            uploadCoverImage,
            deleteCoverImage
        } = this.props;

        return (
            <DetailsForm
                key={book._id}
                initialValues={book}
                suggestions={suggestions}
                validate={bookValidation}
                uploadCoverImage={uploadCoverImage}
                deleteImages={deleteCoverImage}
                onSubmit={this.handleSubmit}
                onCancel={this.handleCancel}
            />
        );
    }
}

EditBook.propTypes = {
    bookId: PropTypes.string,
    book: bookType,
    suggestions: suggestionGroupType,
    uploadCoverImage: PropTypes.func,
    deleteCoverImage: PropTypes.func,
    bookDetailsPage: PropTypes.func,
    saveEditedBook: PropTypes.func
};

EditBook.defaultProps = {
    bookId: null,
    book: {},
    suggestions: {},
    uploadCoverImage: noop,
    deleteCoverImage: noop,
    bookDetailsPage: noop,
    saveEditedBook: noop
};

const mapStateToProps = state => ({
    bookId: getSelectedBookId(state),
    book: getSelectedBook(state),
    suggestions: getSuggestions(state)
});

const mapDispatchToProps = {
    uploadCoverImage,
    deleteCoverImage,
    saveEditedBook,
    bookDetailsPage
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditBook);
