import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EditBook } from './EditBook';

describe('<EditBook />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<EditBook />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when book is specified', () => {
        const book = {
            _id: '1',
            asin: 'B003BKZW4U',
            goodreadsId: 20,
            title: 'title1',
            author: ['Stephen King'],
            seriesTitle: 'seriesTitle',
            seriesIndex: '2',
            rating: 3.5,
            amazonRating: {
                rating: 2,
                numberOfRatings: 100
            },
            goodreadsRating: {
                rating: 4,
                numberOfRatings: 500
            },
            genres: ['action', 'adventure'],
            tags: ['read', 'wait for beautify'],
            publicationYear: 2001,
            pages: 400,
            numberOfTimesRead: 3,
            description: 'description',
            covers: [
                {
                    url: '/images/covers/image1.jpg',
                    thumbnail: '/images/smallcovers/image1.jpg'
                }
            ]
        };

        const wrapper = shallow(<EditBook book={book} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleSubmit()', () => {
        test('calls saveEditBook', () => {
            const mockBookId = '1';
            const mockBookData = {
                _id: '1',
                asin: 'asin',
                title: 'title'
            };
            const mockSaveEditedBook = jest.fn();

            const wrapper = shallow(
                <EditBook
                    bookId={mockBookId}
                    saveEditedBook={mockSaveEditedBook}
                />
            );
            wrapper.instance().handleSubmit(mockBookData);

            expect(mockSaveEditedBook.mock.calls.length).toBe(1);
            expect(mockSaveEditedBook.mock.calls[0][0]).toBe(mockBookId);
            expect(mockSaveEditedBook.mock.calls[0][1]).toEqual(mockBookData);
        });
    });

    describe('handleCancel()', () => {
        test('navigates to bookDetails page', () => {
            const mockBookId = '1';
            const mockBookDetailsPage = jest.fn();

            const wrapper = shallow(
                <EditBook
                    bookId={mockBookId}
                    bookDetailsPage={mockBookDetailsPage}
                />
            );
            wrapper.instance().handleCancel();

            expect(mockBookDetailsPage.mock.calls.length).toBe(1);
            expect(mockBookDetailsPage.mock.calls[0][0]).toBe(mockBookId);
        });
    });
});
