import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { saveAddedBook } from '../../data/actions/newBookActions';
import { booksPage } from '../../data/actions/pageActions';
import { clearImportedData } from '../../data/actions/importActions';
import {
    getImportedAmazonData,
    getImportedGoodreadsData,
    getDuplicateData,
    getInitialBook
} from '../../data/reducers/importReducer';
import { getSuggestions } from '../../data/reducers/suggestionsReducer';
import {
    uploadCoverImage,
    deleteCoverImage
} from '../../data/actions/imageActions';
import styles from './EditImport.css';
import {
    goodreadsDataType,
    amazonDataType,
    suggestionGroupType,
    initialBookType
} from '../../types/types';
import noop from '../../utils/noop';
import bookValidation from '../../components/DetailsForm/bookValidation';
import DataTabs from '../../components/ImportEditForm/DataTabs';
import ImportEditForm from '../../components/ImportEditForm/ImportEditForm';

export class EditImport extends Component {
    handleSubmit = bookData => {
        const { saveAddedBook } = this.props;
        saveAddedBook(bookData);
    };

    handleCancel = () => {
        const { booksPage, clearImportedData } = this.props;
        booksPage();
        clearImportedData();
    };

    render() {
        const {
            amazonData,
            goodreadsData,
            duplicates,
            initialBook,
            suggestions,
            uploadCoverImage,
            deleteCoverImage
        } = this.props;

        return (
            <div className={styles.editImport}>
                <DataTabs
                    amazonData={amazonData}
                    goodreadsData={goodreadsData}
                    duplicates={duplicates}
                />
                <ImportEditForm
                    initialValues={initialBook}
                    suggestions={suggestions}
                    validate={bookValidation}
                    uploadCoverImage={uploadCoverImage}
                    deleteImages={deleteCoverImage}
                    onSubmit={this.handleSubmit}
                    onCancel={this.handleCancel}
                />
            </div>
        );
    }
}

EditImport.propTypes = {
    goodreadsData: goodreadsDataType,
    amazonData: amazonDataType,
    duplicates: PropTypes.shape({
        [PropTypes.string]: PropTypes.bool
    }),
    initialBook: initialBookType,
    suggestions: suggestionGroupType,
    uploadCoverImage: PropTypes.func,
    deleteCoverImage: PropTypes.func,
    saveAddedBook: PropTypes.func,
    booksPage: PropTypes.func,
    clearImportedData: PropTypes.func
};

EditImport.defaultProps = {
    amazonData: {},
    goodreadsData: {},
    duplicates: {},
    initialBook: {},
    suggestions: {},
    uploadCoverImage: noop,
    deleteCoverImage: noop,
    saveAddedBook: noop,
    booksPage: noop,
    clearImportedData: noop
};

const mapStateToProps = state => ({
    amazonData: getImportedAmazonData(state),
    goodreadsData: getImportedGoodreadsData(state),
    duplicates: getDuplicateData(state),
    initialBook: getInitialBook(state),
    suggestions: getSuggestions(state)
});

const mapDispatchToProps = {
    uploadCoverImage,
    deleteCoverImage,
    saveAddedBook,
    booksPage,
    clearImportedData
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditImport);
