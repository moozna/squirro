import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { EditImport } from './EditImport';

describe('<EditImport />', () => {
    test('has the expected html structure with default props', () => {
        const wrapper = shallow(<EditImport />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('handleSubmit()', () => {
        test('calls saveAddedBook', () => {
            const mockBookData = {
                _id: '1',
                asin: 'asin',
                title: 'title'
            };
            const mockSaveAddedBook = jest.fn();

            const wrapper = shallow(
                <EditImport saveAddedBook={mockSaveAddedBook} />
            );
            wrapper.instance().handleSubmit(mockBookData);

            expect(mockSaveAddedBook.mock.calls.length).toBe(1);
            expect(mockSaveAddedBook.mock.calls[0][0]).toEqual(mockBookData);
        });
    });

    describe('handleCancel()', () => {
        test('navigates to books page', () => {
            const mockBooksPage = jest.fn();

            const wrapper = shallow(<EditImport booksPage={mockBooksPage} />);
            wrapper.instance().handleCancel();

            expect(mockBooksPage.mock.calls.length).toBe(1);
        });

        test('fires the clearImportedData callback', () => {
            const mockClearImportedData = jest.fn();

            const wrapper = shallow(
                <EditImport clearImportedData={mockClearImportedData} />
            );
            wrapper.instance().handleCancel();

            expect(mockClearImportedData.mock.calls.length).toBe(1);
        });
    });
});
