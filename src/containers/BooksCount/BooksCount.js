import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentPage } from '../../data/reducers/pageReducer';
import { getBooksCount } from '../../data/reducers/booksReducer';
import {
    getStartIndex,
    getPageLimit
} from '../../data/reducers/paginationReducer';
import Counter from '../../components/Counter/Counter';

export const BooksCount = ({
    currentPage,
    startIndex,
    pageLimit,
    booksCount
}) => {
    if (currentPage !== 'Books') {
        return null;
    }

    return (
        <Counter
            startIndex={startIndex}
            pageLimit={pageLimit}
            count={booksCount}
        />
    );
};

BooksCount.propTypes = {
    currentPage: PropTypes.string,
    startIndex: PropTypes.number,
    pageLimit: PropTypes.number,
    booksCount: PropTypes.number
};

BooksCount.defaultProps = {
    currentPage: 'Books',
    startIndex: 0,
    pageLimit: 50,
    booksCount: 0
};

const mapStateToProps = state => ({
    currentPage: getCurrentPage(state),
    startIndex: getStartIndex(state),
    pageLimit: getPageLimit(state),
    booksCount: getBooksCount(state)
});

export default connect(mapStateToProps)(BooksCount);
