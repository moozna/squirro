import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { BooksCount } from './BooksCount';

describe('<BooksCount />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<BooksCount />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when props are provided', () => {
        const wrapper = shallow(
            <BooksCount
                currentPage="Books"
                startIndex={50}
                pageLimit={25}
                booksCount={500}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('does not render Counter when currentPage is not Books page', () => {
        const wrapper = shallow(
            <BooksCount
                currentPage="BookDetails"
                startIndex={50}
                pageLimit={25}
                booksCount={500}
            />
        );

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
