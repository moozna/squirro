import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updatePageRange } from '../../data/actions/rangeActions';
import { getPageRanges } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import FilterSlider from '../../components/Filters/FilterSlider';

export const Pages = ({ pages, updatePageRange }) => (
    <Fragment>
        <SubHeader caption="Pages" />
        <FilterSlider data={pages} onValueChange={updatePageRange} />
    </Fragment>
);

Pages.propTypes = {
    pages: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number,
        values: PropTypes.arrayOf(PropTypes.number)
    }),
    updatePageRange: PropTypes.func
};

Pages.defaultProps = {
    pages: {},
    updatePageRange: noop
};

const mapStateToProps = state => ({
    pages: getPageRanges(state)
});

const mapDispatchToProps = {
    updatePageRange
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Pages);
