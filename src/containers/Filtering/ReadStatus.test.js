import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { ReadStatus } from './ReadStatus';

describe('<ReadStatus />', () => {
    test('has the expected html structure', () => {
        const wrapper = shallow(<ReadStatus />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    describe('changeReadStatus()', () => {
        test('fires the updateReadStatus callback when different value is selected', () => {
            const handleUpdateReadStatus = jest.fn();
            const wrapper = shallow(
                <ReadStatus
                    readStatus=""
                    updateReadStatus={handleUpdateReadStatus}
                />
            );

            wrapper.instance().changeReadStatus('read');
            expect(handleUpdateReadStatus.mock.calls.length).toBe(1);
            expect(handleUpdateReadStatus.mock.calls[0][0]).toBe('read');
        });

        test('fires the updateReadStatus callback with empty string when the same value is selected', () => {
            const handleUpdateReadStatus = jest.fn();
            const wrapper = shallow(
                <ReadStatus
                    readStatus="read"
                    updateReadStatus={handleUpdateReadStatus}
                />
            );

            wrapper.instance().changeReadStatus('read');
            expect(handleUpdateReadStatus.mock.calls.length).toBe(1);
            expect(handleUpdateReadStatus.mock.calls[0][0]).toBe('');
        });
    });
});
