import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateGenres } from '../../data/actions/filtersActions';
import { getGenres } from '../../data/reducers/filtersReducer';
import noop from '../../utils/noop';
import Checkbox from '../../components/Checkbox/Checkbox';
import SubHeader from '../../components/SubHeader/SubHeader';

export const GenreList = ({ genres, updateGenres }) => (
    <Fragment>
        <SubHeader caption="Genres" />
        <div>
            {genres.map(filter => (
                <Checkbox
                    key={filter.name}
                    fieldId={filter.name}
                    checked={filter.checked}
                    label={filter.name}
                    onChange={updateGenres}
                />
            ))}
        </div>
    </Fragment>
);

GenreList.propTypes = {
    genres: PropTypes.arrayOf(PropTypes.object),
    updateGenres: PropTypes.func
};

GenreList.defaultProps = {
    genres: [],
    updateGenres: noop
};

const mapStateToProps = state => ({
    genres: getGenres(state)
});

const mapDispatchToProps = {
    updateGenres
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GenreList);
