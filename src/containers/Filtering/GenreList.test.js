import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { GenreList } from './GenreList';

describe('<GenreList />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<GenreList />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when genres are provided', () => {
        const testData = [
            {
                name: 'genre1',
                checked: false
            },
            {
                name: 'genre2',
                checked: true
            },
            {
                name: 'genre3',
                checked: false
            }
        ];
        const wrapper = shallow(<GenreList genres={testData} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
