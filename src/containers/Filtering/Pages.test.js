import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Pages } from './Pages';

describe('<Pages />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Pages />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
