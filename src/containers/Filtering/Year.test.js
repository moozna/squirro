import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { Year } from './Year';

describe('<Year />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<Year />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
