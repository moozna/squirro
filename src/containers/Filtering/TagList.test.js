import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { TagList } from './TagList';

describe('<TagList />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<TagList />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('has the expected html structure when tags are provided', () => {
        const testData = [
            {
                name: 'tag1',
                checked: false,
                excluded: true
            },
            {
                name: 'tag2',
                checked: true,
                excluded: false
            },
            {
                name: 'tag3',
                checked: false,
                excluded: true
            }
        ];
        const wrapper = shallow(<TagList tags={testData} />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
