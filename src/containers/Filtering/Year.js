import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateYearRange } from '../../data/actions/rangeActions';
import { getYearRanges } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import FilterSlider from '../../components/Filters/FilterSlider';

export const Year = ({ year, updateYearRange }) => (
    <Fragment>
        <SubHeader caption="Year" />
        <FilterSlider data={year} onValueChange={updateYearRange} />
    </Fragment>
);

Year.propTypes = {
    year: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number,
        values: PropTypes.arrayOf(PropTypes.number)
    }),
    updateYearRange: PropTypes.func
};

Year.defaultProps = {
    year: {},
    updateYearRange: noop
};

const mapStateToProps = state => ({
    year: getYearRanges(state)
});

const mapDispatchToProps = {
    updateYearRange
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Year);
