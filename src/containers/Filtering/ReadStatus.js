import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateReadStatus } from '../../data/actions/filtersActions';
import { getReadStatus } from '../../data/reducers/filtersReducer';
import styles from './Filtering.css';
import noop from '../../utils/noop';
import ButtonGroup from '../../components/ButtonGroup/ButtonGroup';
import RadioButton from '../../components/ButtonGroup/RadioButton';

export class ReadStatus extends Component {
    changeReadStatus = value => {
        const { readStatus, updateReadStatus } = this.props;

        if (value !== readStatus) {
            updateReadStatus(value);
        } else {
            updateReadStatus('');
        }
    };

    render() {
        const { readStatus } = this.props;

        return (
            <div className={styles.readStatus}>
                <ButtonGroup
                    name="ReadStatus"
                    value={readStatus}
                    onSelectionChange={this.changeReadStatus}
                >
                    <RadioButton label="Read" value="read" />
                    <RadioButton label="Not read" value="notRead" />
                </ButtonGroup>
            </div>
        );
    }
}

ReadStatus.propTypes = {
    readStatus: PropTypes.string,
    updateReadStatus: PropTypes.func
};

ReadStatus.defaultProps = {
    readStatus: '',
    updateReadStatus: noop
};

const mapStateToProps = state => ({
    readStatus: getReadStatus(state)
});

const mapDispatchToProps = {
    updateReadStatus
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReadStatus);
