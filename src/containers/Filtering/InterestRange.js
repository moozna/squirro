import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { updateInterestRange } from '../../data/actions/rangeActions';
import { getInterestRange } from '../../data/reducers/rangeReducer';
import noop from '../../utils/noop';
import SubHeader from '../../components/SubHeader/SubHeader';
import InterestSlider from '../../components/Filters/InterestSlider';

export const InterestRange = ({ interestRange, updateInterestRange }) => (
    <Fragment>
        <SubHeader caption="Interest" />
        <InterestSlider
            values={interestRange}
            onValueChange={updateInterestRange}
        />
    </Fragment>
);

InterestRange.propTypes = {
    interestRange: PropTypes.arrayOf(PropTypes.number),
    updateInterestRange: PropTypes.func
};

InterestRange.defaultProps = {
    interestRange: [],
    updateInterestRange: noop
};

const mapStateToProps = state => ({
    interestRange: getInterestRange(state)
});

const mapDispatchToProps = {
    updateInterestRange
};

export default connect(mapStateToProps, mapDispatchToProps)(InterestRange);
