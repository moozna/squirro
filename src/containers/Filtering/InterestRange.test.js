import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { InterestRange } from './InterestRange';

describe('<InterestRange />', () => {
    test('has the expected html structure by default', () => {
        const wrapper = shallow(<InterestRange />);

        expect(toJson(wrapper)).toMatchSnapshot();
    });
});
