import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    updateTags,
    updateExcludedTags
} from '../../data/actions/filtersActions';
import { getTags } from '../../data/reducers/filtersReducer';
import styles from './Filtering.css';
import noop from '../../utils/noop';
import Chip from '../../components/Chip/Chip';
import SubHeader from '../../components/SubHeader/SubHeader';
import Minus from '../../components/svg/Minus';

export const TagList = ({ tags, updateTags, updateExcludedTags }) => (
    <Fragment>
        <SubHeader caption="Tags" />
        <div className={styles.tagFilters}>
            {tags.map(tag => (
                <Chip
                    key={tag.name}
                    fieldId={tag.name}
                    value={tag.name}
                    checked={tag.checked}
                    disabled={tag.excluded}
                    onToggle={updateTags}
                    onDeleteClick={updateExcludedTags}
                    icon={Minus}
                    checkable
                    deletable
                />
            ))}
        </div>
    </Fragment>
);

TagList.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.object),
    updateTags: PropTypes.func,
    updateExcludedTags: PropTypes.func
};

TagList.defaultProps = {
    tags: [],
    updateTags: noop,
    updateExcludedTags: noop
};

const mapStateToProps = state => ({
    tags: getTags(state)
});

const mapDispatchToProps = {
    updateTags,
    updateExcludedTags
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TagList);
