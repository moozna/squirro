import unflatten from './unflatten';

describe('unflatten()', () => {
    test('is a function', () => {
        expect(typeof unflatten).toBe('function');
    });

    describe('does not throw', () => {
        test('when object is undefined', () => {
            const testObj = undefined;
            expect(unflatten(testObj)).toEqual({});
        });

        test('when object is null', () => {
            const testObj = null;
            expect(unflatten(testObj)).toEqual({});
        });
    });

    describe('returns a multiple level deep object', () => {
        test('when object is empty object', () => {
            const testObj = {};
            expect(unflatten(testObj)).toEqual({});
        });

        test('when object should be one level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King'
            };
            const expected = {
                title: 'A Conspiracy Of King'
            };

            expect(unflatten(testObj)).toEqual(expected);
        });

        test('when object should be two level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King',
                'rating.value': 3,
                'rating.numberOfRatings': 239
            };
            const expected = {
                title: 'A Conspiracy Of King',
                rating: {
                    value: 3,
                    numberOfRatings: 239
                }
            };

            expect(unflatten(testObj)).toEqual(expected);
        });

        test('when object should be three level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King',
                'rating.value.number': 3,
                'rating.value.string': '3',
                'rating.numberOfRatings': 239
            };
            const expected = {
                title: 'A Conspiracy Of King',
                rating: {
                    value: {
                        number: 3,
                        string: '3'
                    },
                    numberOfRatings: 239
                }
            };

            expect(unflatten(testObj)).toEqual(expected);
        });

        test('when object contains array', () => {
            const testObj = {
                authors: ['Stephen King', 'Megan Whalen Turner']
            };
            const expected = {
                authors: ['Stephen King', 'Megan Whalen Turner']
            };

            expect(unflatten(testObj)).toEqual(expected);
        });
    });
});
