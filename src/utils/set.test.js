import set from './set';

describe('set()', () => {
    test('is a function', () => {
        expect(typeof set).toBe('function');
    });

    describe('does not throw', () => {
        test('when object is undefined', () => {
            const testObj = undefined;
            expect(set(testObj, 'something')).toBe(undefined);
        });

        test('when object is null', () => {
            const testObj = null;
            expect(set(testObj, 'something')).toBe(null);
        });

        test('when path is not provided', () => {
            const testObj = {};
            expect(set(testObj)).toEqual({});
        });

        test('when incorrect path is provided', () => {
            const testObj = {};
            expect(set(testObj, 'something.other.', 42)).toEqual({});
        });
    });

    describe('assigns the value', () => {
        test('when the property exists', () => {
            const testObj = {
                title: 'title'
            };
            const expected = {
                title: 'Some title'
            };

            expect(set(testObj, 'title', 'Some title')).toEqual(expected);
        });

        test('when the property does not exist', () => {
            const testObj = {};
            const expected = {
                title: 'Some title'
            };

            expect(set(testObj, 'title', 'Some title')).toEqual(expected);
        });

        test('when nested property provided', () => {
            const testObj = {
                rating: {
                    amazonRating: 5
                }
            };
            const expected = {
                rating: {
                    amazonRating: 3.1
                }
            };

            expect(set(testObj, 'rating.amazonRating', 3.1)).toEqual(expected);
        });

        test('when nested property does not exist', () => {
            const testObj = {};
            const expected = {
                rating: {
                    amazonRating: 3.1
                }
            };

            expect(set(testObj, 'rating.amazonRating', 3.1)).toEqual(expected);
        });
    });
});
