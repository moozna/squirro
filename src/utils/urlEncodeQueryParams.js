function urlEncodeQueryParams(data) {
    const result = Object.keys(data)
        .map(key => {
            const val = data[key];

            if (!val) {
                return '';
            }

            let strVal;
            if (Array.isArray(val)) {
                strVal = val.map(item => encodeURIComponent(item)).join(',');
            } else {
                strVal = encodeURIComponent(val);
            }

            return strVal ? `${encodeURIComponent(key)}=${strVal}` : '';
        })
        .filter(item => item !== '')
        .join('&');

    return result ? `?${result}` : '';
}

export default urlEncodeQueryParams;
