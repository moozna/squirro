import isObject from './isObject';

const isEqual = (value, other) => {
    if (value === other) {
        return true;
    }

    if (Array.isArray(value) && Array.isArray(other)) {
        if (value.length !== other.length) {
            return false;
        }

        for (let i = 0; i < value.length; i++) {
            if (!isEqual(value[i], other[i])) {
                return false;
            }
        }
        return true;
    }

    if (isObject(value) && isObject(other)) {
        return Object.keys(value).every(key => isEqual(value[key], other[key]));
    }

    return false;
};

export default isEqual;
