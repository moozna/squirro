import isObject from './isObject';

const isEmptyObject = obj => isObject(obj) && Object.keys(obj).length === 0;

export default isEmptyObject;
