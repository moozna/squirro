import isObject from './isObject';

describe('isObject()', () => {
    test('is a function', () => {
        expect(typeof isObject).toBe('function');
    });

    describe('returns true', () => {
        test('when value is an object', () => {
            const testValue = {};
            expect(isObject(testValue)).toBeTruthy();
        });
    });

    describe('returns false', () => {
        test('when value is undefined', () => {
            const testValue = undefined;
            expect(isObject(testValue)).toBeFalsy();
        });

        test('when value is null', () => {
            const testValue = null;
            expect(isObject(testValue)).toBeFalsy();
        });

        test('when value is array', () => {
            const testValue = [];
            expect(isObject(testValue)).toBeFalsy();
        });
    });
});
