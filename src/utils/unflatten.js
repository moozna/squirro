import set from './set';
import isObject from './isObject';

const unflatten = obj => {
    const result = {};

    if (!isObject(obj)) {
        return result;
    }

    Object.entries(obj).forEach(([key, value]) => {
        set(result, key, value);
    });

    return result;
};

export default unflatten;
