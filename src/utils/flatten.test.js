import flatten from './flatten';

describe('flatten()', () => {
    test('is a function', () => {
        expect(typeof flatten).toBe('function');
    });

    describe('does not throw', () => {
        test('when object is undefined', () => {
            const testObj = undefined;
            expect(flatten(testObj)).toEqual({});
        });

        test('when object is null', () => {
            const testObj = null;
            expect(flatten(testObj)).toEqual({});
        });
    });

    describe('returns a maximum one level deep object', () => {
        test('when object is empty object', () => {
            const testObj = {};
            expect(flatten(testObj)).toEqual({});
        });

        test('when object is one level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King'
            };
            const expected = {
                title: 'A Conspiracy Of King'
            };

            expect(flatten(testObj)).toEqual(expected);
        });

        test('when object is two level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King',
                rating: {
                    value: 3,
                    numberOfRatings: 239
                }
            };
            const expected = {
                title: 'A Conspiracy Of King',
                'rating.value': 3,
                'rating.numberOfRatings': 239
            };

            expect(flatten(testObj)).toEqual(expected);
        });

        test('when object is three level deep', () => {
            const testObj = {
                title: 'A Conspiracy Of King',
                rating: {
                    value: {
                        number: 3,
                        string: '3'
                    },
                    numberOfRatings: 239
                }
            };
            const expected = {
                title: 'A Conspiracy Of King',
                'rating.value.number': 3,
                'rating.value.string': '3',
                'rating.numberOfRatings': 239
            };

            expect(flatten(testObj)).toEqual(expected);
        });

        test('when object contains array', () => {
            const testObj = {
                authors: ['Stephen King', 'Megan Whalen Turner']
            };
            const expected = {
                authors: ['Stephen King', 'Megan Whalen Turner']
            };

            expect(flatten(testObj)).toEqual(expected);
        });
    });
});
