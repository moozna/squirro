const isObject = value =>
    value && typeof value === 'object' && !Array.isArray(value);

export default isObject;
