import compareAlphabetically from './compareAlphabetically';

describe('compareAlphabetically()', () => {
    const testData = [
        { name: 'Kate' },
        { name: 'williams' },
        { name: 'David' },
        { name: 'bob' }
    ];

    const expectedData = [
        { name: 'bob' },
        { name: 'David' },
        { name: 'Kate' },
        { name: 'williams' }
    ];

    const sortTestData = compareFn => testData.slice().sort(compareFn);

    test('is a function', () => {
        expect(typeof compareAlphabetically).toBe('function');
    });

    describe('does not throw', () => {
        test('when it is an empty array', () => {
            const sortedData = [].sort(compareAlphabetically('name'));
            expect(sortedData).toEqual([]);
        });

        test('when propName is undefined', () => {
            const sortedData = sortTestData(compareAlphabetically());
            expect(sortedData).toEqual(testData);
        });

        test('when propName does not exist', () => {
            const sortedData = sortTestData(compareAlphabetically('firstName'));
            expect(sortedData).toEqual(testData);
        });
    });

    describe('returns correctly sorted array', () => {
        test('when prop values contain different letter casing', () => {
            const sortedData = sortTestData(compareAlphabetically('name'));
            expect(sortedData).toEqual(expectedData);
        });
    });
});
