import get from './get';

const compareAlphabetically = propName => (value1, value2) => {
    const name1 = get(value1, propName, '').toLowerCase();
    const name2 = get(value2, propName, '').toLowerCase();

    if (name1 > name2) {
        return 1;
    }
    if (name1 < name2) {
        return -1;
    }
    return 0;
};

export default compareAlphabetically;
