import urlEncodeQueryParams from './urlEncodeQueryParams';

describe('urlEncodeQueryParams()', () => {
    test('is a function', () => {
        expect(typeof urlEncodeQueryParams).toBe('function');
    });

    test('returns an empty string when query object does not contain any values', () => {
        const testQuery = {};
        expect(urlEncodeQueryParams(testQuery)).toBe('');
    });

    test('includes every property with truthy values', () => {
        const testQuery = {
            page: 2,
            limit: 50
        };
        expect(urlEncodeQueryParams(testQuery)).toBe('?page=2&limit=50');
    });

    test('does not include properties with falsy values', () => {
        const testQuery = {
            page: 2,
            limit: 0,
            title: ''
        };
        expect(urlEncodeQueryParams(testQuery)).toBe('?page=2');
    });

    test('does not include properties with empty array', () => {
        const testQuery = {
            page: 2,
            limit: 0,
            tags: []
        };
        expect(urlEncodeQueryParams(testQuery)).toBe('?page=2');
    });

    test('includes array values separated with comma', () => {
        const testQuery = {
            tags: ['Horror', 'Romance']
        };
        expect(urlEncodeQueryParams(testQuery)).toBe('?tags=Horror,Romance');
    });

    test('encodes special characters as a URI component', () => {
        const testQuery = {
            tags: ['Historical Fiction']
        };
        expect(urlEncodeQueryParams(testQuery)).toBe(
            '?tags=Historical%20Fiction'
        );
    });
});
