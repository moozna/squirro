import isEqual from './isEqual';

describe('isEqual()', () => {
    test('is a function', () => {
        expect(typeof isEqual).toBe('function');
    });

    describe('does not throw', () => {
        test('when value is undefined', () => {
            const testValue1 = undefined;
            const testValue2 = 'value';
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });

        test('when value is null', () => {
            const testValue1 = null;
            const testValue2 = 'value';
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });
    });

    describe('primitive values', () => {
        test('returns true when values are the same strings', () => {
            const testValue1 = 'value';
            const testValue2 = 'value';
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns false when values are different strings', () => {
            const testValue1 = 'value1';
            const testValue2 = 'value2';
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });

        test('returns true when values are the same number', () => {
            const testValue1 = 0;
            const testValue2 = 0;
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns false when values are different number', () => {
            const testValue1 = 1;
            const testValue2 = 3;
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });
    });

    describe('arrays', () => {
        test('returns true when both value is an empty array', () => {
            const testValue1 = [];
            const testValue2 = [];
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns true when every value is equal in both arrays', () => {
            const testValue1 = ['value1', 'value2'];
            const testValue2 = ['value1', 'value2'];
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns false when there are different number of values in the arrays', () => {
            const testValue1 = ['value1', 'value2'];
            const testValue2 = ['value1', 'value3', 'value4'];
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });

        test('returns false when there are different values in the arrays', () => {
            const testValue1 = ['value1', 'value2'];
            const testValue2 = ['value1', 'value3'];
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });
    });

    describe('objects', () => {
        test('returns true when both value is an empty object', () => {
            const testValue1 = {};
            const testValue2 = {};
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns true when every key-value pair is equal in both object', () => {
            const testValue1 = { title: 'title' };
            const testValue2 = { title: 'title' };
            expect(isEqual(testValue1, testValue2)).toBeTruthy();
        });

        test('returns false when there are different key-value pairs in the objects', () => {
            const testValue1 = { title: 'title', seriesIndex: '2' };
            const testValue2 = { title: 'title', seriesIndex: '3' };
            expect(isEqual(testValue1, testValue2)).toBeFalsy();
        });
    });

    describe('recursive', () => {
        describe('nested arrays', () => {
            test('returns true when every value is equal in both array', () => {
                const testValue1 = [[2, 3], [2, 3]];
                const testValue2 = [[2, 3], [2, 3]];
                expect(isEqual(testValue1, testValue2)).toBeTruthy();
            });

            test('returns false when there are different values in the arrays', () => {
                const testValue1 = [[2, 3], [2, 3]];
                const testValue2 = [[2, 3], [2, 4]];
                expect(isEqual(testValue1, testValue2)).toBeFalsy();
            });
        });

        describe('nested objects', () => {
            test('returns true when every key-value pair is equal in both object', () => {
                const testValue1 = {
                    title: 'title',
                    rating: { value: 2, numberOfRatings: 4 }
                };
                const testValue2 = {
                    title: 'title',
                    rating: { value: 2, numberOfRatings: 4 }
                };
                expect(isEqual(testValue1, testValue2)).toBeTruthy();
            });

            test('returns false when there are different key-value pairs in the objects', () => {
                const testValue1 = {
                    title: 'title',
                    rating: { value: 2, numberOfRatings: 4 }
                };
                const testValue2 = {
                    title: 'title',
                    rating: { value: 2, numberOfRatings: 5 }
                };
                expect(isEqual(testValue1, testValue2)).toBeFalsy();
            });
        });

        describe('array of objects', () => {
            test('returns true when every value is equal in both arrays', () => {
                const testValue1 = [{ title: 'title1' }, { title: 'title2' }];
                const testValue2 = [{ title: 'title1' }, { title: 'title2' }];
                expect(isEqual(testValue1, testValue2)).toBeTruthy();
            });

            test('returns false when there are different values in the arrays', () => {
                const testValue1 = [{ title: 'title1' }, { title: 'title2' }];
                const testValue2 = [{ title: 'title1' }, { title: 'title3' }];
                expect(isEqual(testValue1, testValue2)).toBeFalsy();
            });
        });
    });
});
