import isEmptyObject from './isEmptyObject';

describe('isEmptyObject()', () => {
    test('is a function', () => {
        expect(typeof isEmptyObject).toBe('function');
    });

    describe('does not throw', () => {
        test('when object is undefined', () => {
            const testValue = undefined;
            expect(isEmptyObject(testValue)).toBeFalsy();
        });

        test('when object is null', () => {
            const testValue = null;
            expect(isEmptyObject(testValue)).toBeFalsy();
        });
    });

    describe('returns true', () => {
        test('when value is an empty object', () => {
            const testValue = {};
            expect(isEmptyObject(testValue)).toBeTruthy();
        });
    });

    describe('returns false', () => {
        test('when value is not an empty object', () => {
            const testValue = { title: 'title' };
            expect(isEmptyObject(testValue)).toBeFalsy();
        });

        test('when value is an empty array', () => {
            const testValue = [];
            expect(isEmptyObject(testValue)).toBeFalsy();
        });
    });
});
