const get = (object, path = '', defaultValue = undefined) =>
    path.split('.').reduce((partialObj, pathElement) => {
        const result = partialObj ? partialObj[pathElement] : undefined;
        return result === undefined ? defaultValue : result;
    }, object);

export default get;
