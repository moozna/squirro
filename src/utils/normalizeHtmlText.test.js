import normalizeHtmlText from './normalizeHtmlText';

describe('normalizeHtmlText()', () => {
    test('is a function', () => {
        expect(typeof normalizeHtmlText).toBe('function');
    });

    test('returns the text without modification when text contains paragraphs', () => {
        const testHtml =
            '<p>Example text</p><p>First paragraph</p><p>Another paragraph</p>';
        expect(normalizeHtmlText(testHtml)).toBe(testHtml);
    });

    test('wraps the text into paragraphs', () => {
        const testHtml = 'Example text';
        const expectedHtml = '<p>Example text</p>';
        expect(normalizeHtmlText(testHtml)).toBe(expectedHtml);
    });

    test('creates paragraphs based on double <br> tags', () => {
        const testHtml =
            'Example text <br> <br> First paragraph  <br>  <br> Another paragraph';
        const expectedHtml =
            '<p>Example text</p>\n<p>First paragraph</p>\n<p>Another paragraph</p>';
        expect(normalizeHtmlText(testHtml)).toBe(expectedHtml);
    });

    test('removes unnecessary <br> tags before paragraph ending', () => {
        const testHtml = '<p>Example text <br> </p>';
        const expectedHtml = '<p>Example text</p>';
        expect(normalizeHtmlText(testHtml)).toBe(expectedHtml);
    });
});
