const removeUnnecessaryCharacters = text =>
    text.replace(/\s*(<br>)?\s*<\/p>/gi, '</p>');

const wrapIntoParagraph = text => {
    const paragraphPattern = /^\s*<p>.+<\/p>\s*$/gi;
    const isInParagraph = paragraphPattern.test(text);
    return isInParagraph ? text : `<p>${text}</p>`;
};

const replaceLineBreaksWithParagraph = text =>
    text.replace(/\s*<br>\s*<br>\s*/gi, '</p>\n<p>');

const normalizeHtmlText = text => {
    let resultText = text;
    resultText = wrapIntoParagraph(resultText);
    resultText = replaceLineBreaksWithParagraph(resultText);
    resultText = removeUnnecessaryCharacters(resultText);
    return resultText;
};

export default normalizeHtmlText;
