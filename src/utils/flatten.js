import isObject from './isObject';

const flatten = (obj, path = '') => {
    if (!isObject(obj)) {
        return {};
    }

    function step(partialObj, nextPath) {
        return [].concat(
            ...Object.entries(partialObj).map(([key, value]) => {
                const currentPath = nextPath ? `${nextPath}.${key}` : key;
                return isObject(value)
                    ? step(value, currentPath)
                    : { [currentPath]: value };
            })
        );
    }

    return Object.assign({}, ...step(obj, path));
};

export default flatten;
