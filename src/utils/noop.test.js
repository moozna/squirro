import noop from './noop';

describe('noop()', () => {
    test('is a function', () => {
        expect(typeof noop).toBe('function');
    });

    test('does not return anything', () => {
        expect(noop()).toBeUndefined();
        expect(noop('some args', 1, 2, [3])).toBeUndefined();
    });
});
