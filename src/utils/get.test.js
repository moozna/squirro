import get from './get';

describe('get()', () => {
    test('is a function', () => {
        expect(typeof get).toBe('function');
    });

    describe('does not throw', () => {
        test('when object is undefined', () => {
            const testObj = undefined;
            expect(get(testObj, 'something')).toBe(undefined);
        });

        test('when object is null', () => {
            const testObj = null;
            expect(get(testObj, 'something')).toBe(undefined);
        });

        test('when path is not provided', () => {
            const testObj = {};
            expect(get(testObj)).toBe(undefined);
        });

        test('when incorrect path is provided', () => {
            const testObj = {
                something: {
                    other: {
                        title: 'title'
                    }
                }
            };
            expect(get(testObj, 'something.other.')).toBe(undefined);
        });

        test('when the property does not exist', () => {
            const testObj = {};
            expect(get(testObj, 'something')).toBe(undefined);
        });

        test('when nested property does not exist', () => {
            const testObj = {
                something: null
            };
            expect(get(testObj, 'something.other')).toBe(undefined);
        });
    });

    describe('returns value', () => {
        test('when property exists', () => {
            const testObj = {
                something: 'title'
            };
            expect(get(testObj, 'something')).toBe('title');
        });

        test('when nested property exists', () => {
            const testObj = {
                something: {
                    book: 'title'
                }
            };
            expect(get(testObj, 'something.book')).toBe('title');
        });

        test('when property has a null value', () => {
            const testObj = {
                something: null
            };
            expect(get(testObj, 'something')).toBe(null);
        });

        test('when nested property has a null value', () => {
            const testObj = {
                something: {
                    book: null
                }
            };
            expect(get(testObj, 'something.book')).toBe(null);
        });

        test('when property has undefined value', () => {
            const testObj = {
                something: undefined
            };
            expect(get(testObj, 'something')).toBe(undefined);
        });

        test('when nested property has undefined value', () => {
            const testObj = {
                something: {
                    book: undefined
                }
            };
            expect(get(testObj, 'something.book')).toBe(undefined);
        });
    });

    describe('returns default value', () => {
        test('returns undefined when default value is not provided', () => {
            const testObj = {
                something: {}
            };
            expect(get(testObj, 'something.other')).toBe(undefined);
        });

        test('returns the provided default value', () => {
            const testObj = {
                something: {}
            };
            expect(get(testObj, 'something.other', '')).toBe('');
        });
    });
});
