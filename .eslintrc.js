module.exports = {
    env: {
        browser: true,
        jest: true
    },
    extends: 'airbnb',
    parser: 'babel-eslint',
    root: true,
    settings: {
        react: {
            pragma: 'React',
            version: '16.5.2'
        }
    },
    rules: {
        'arrow-parens': [
            'error',
            'as-needed',
            {
                requireForBlockBody: false
            }
        ],
        'comma-dangle': ['error', 'never'],
        'function-paren-newline': 'off',
        'import/no-named-as-default': 'off',
        'import/prefer-default-export': 'off',
        indent: [
            'error',
            4,
            {
                SwitchCase: 1,
                VariableDeclarator: 1,
                outerIIFEBody: 1,
                FunctionDeclaration: {
                    parameters: 1,
                    body: 1
                },
                FunctionExpression: {
                    parameters: 1,
                    body: 1
                },
                CallExpression: {
                    arguments: 1
                },
                ArrayExpression: 1,
                ObjectExpression: 1,
                ImportDeclaration: 1,
                flatTernaryExpressions: false,
                ignoredNodes: ['JSXElement', 'JSXElement *']
            }
        ],
        'implicit-arrow-linebreak': 'off',
        'linebreak-style': ['error', 'windows'],
        'no-confusing-arrow': 'off',
        'no-mixed-operators': 'off',
        'no-plusplus': 'off',
        'no-shadow': 'off',
        'no-underscore-dangle': [
            'error',
            {
                allow: ['_id', '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'],
                allowAfterThis: false,
                allowAfterSuper: false,
                enforceInMethodNames: false
            }
        ],
        'no-use-before-define': [
            'error',
            {
                functions: false,
                classes: true,
                variables: true
            }
        ],
        'object-curly-newline': 'off',
        'operator-linebreak': 'off',
        'react/destructuring-assignment': 'off',
        'react/forbid-prop-types': [
            'error',
            {
                forbid: ['any'],
                checkContextTypes: true,
                checkChildContextTypes: true
            }
        ],
        'react/jsx-filename-extension': [
            'error',
            { extensions: ['.js', '.jsx'] }
        ],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        'react/no-array-index-key': 'off',
        'react/no-danger': 'off',
        'react/prop-types': [
            'warn',
            {
                ignore: [],
                customValidators: [],
                skipUndeclared: false
            }
        ],
        'react/require-default-props': 'warn',
        'jsx-a11y/anchor-has-content': 'off',
        'jsx-a11y/aria-role': 'off',
        'jsx-a11y/aria-props': 'off',
        'jsx-a11y/aria-proptypes': 'off',
        'jsx-a11y/aria-unsupported-elements': 'off',
        'jsx-a11y/alt-text': 'off',
        'jsx-a11y/img-redundant-alt': 'off',
        'jsx-a11y/label-has-for': 'off',
        'jsx-a11y/label-has-associated-control': 'off',
        'jsx-a11y/mouse-events-have-key-events': 'off',
        'jsx-a11y/no-access-key': 'off',
        'jsx-a11y/no-onchange': 'off',
        'jsx-a11y/interactive-supports-focus': 'off',
        'jsx-a11y/role-has-required-aria-props': 'off',
        'jsx-a11y/role-supports-aria-props': 'off',
        'jsx-a11y/tabindex-no-positive': 'off',
        'jsx-a11y/heading-has-content': 'off',
        'jsx-a11y/html-has-lang': 'off',
        'jsx-a11y/lang': 'off',
        'jsx-a11y/no-distracting-elements': 'off',
        'jsx-a11y/scope': 'off',
        'jsx-a11y/click-events-have-key-events': 'off',
        'jsx-a11y/no-static-element-interactions': 'off',
        'jsx-a11y/no-noninteractive-element-interactions': 'off',
        'jsx-a11y/accessible-emoji': 'off',
        'jsx-a11y/aria-activedescendant-has-tabindex': 'off',
        'jsx-a11y/iframe-has-title': 'off',
        'jsx-a11y/no-autofocus': 'off',
        'jsx-a11y/no-redundant-roles': 'off',
        'jsx-a11y/media-has-caption': 'off',
        'jsx-a11y/no-interactive-element-to-noninteractive-role': 'off',
        'jsx-a11y/no-noninteractive-element-to-interactive-role': 'off',
        'jsx-a11y/no-noninteractive-tabindex': 'off',
        'jsx-a11y/anchor-is-valid': 'off'
    }
};
