module.exports = {
    extends: 'stylelint-config-standard',
    rules: {
        indentation: 4,
        'custom-property-empty-line-before': null
    }
};
